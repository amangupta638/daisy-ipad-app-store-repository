//
//  AppHelper.m
//  IntroduceThem
//
//  Created by Jitendra Singh on 13/06/11.
//  Copyright 2011 Tata Consultant Services. All rights reserved.
//

#import "AppHelper.h"


@implementation AppHelper

+ (UIImage*)appLogoImage
{
    return [UIImage imageNamed:@"appLogo.png"];
}


+(void)saveToUserDefaults:(id)value withKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
	if (standardUserDefaults) {
		[standardUserDefaults setObject:value forKey:key];
		[standardUserDefaults synchronize];
	}
}

+(NSString*)userDefaultsForKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSString *val = nil;
	
	if (standardUserDefaults) 
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}
+(void)removeFromUserDefaultsWithKey:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults removeObjectForKey:key];
    [standardUserDefaults synchronize];
}
+ (NSString *)getCurrentLanguage {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    
    
	return [languages objectAtIndex:0];
}

/************** Code Added For Push Notification  By Aman **********************/

+(void)saveToUserDefaultforPushNotification:(id)obje withkey:(NSString*)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([key isEqualToString:@"PayLoad"]) // Replace UserDefault with new Payload as it is //
    {
        [defaults setObject:obje forKey:key];
        [defaults synchronize];
        return ;
        
    }
    else // Check Uvisted dictionary is available or not
    {
        NSMutableDictionary *dicttemp = [defaults objectForKey:@"UnVisited"];

        if(dicttemp) // if available check unvisited count//
        {
            NSInteger unvistedcount = [[dicttemp objectForKey:@"UnVisitedCount"] integerValue];
            if(unvistedcount >0) // if it is greater than zero it means user has not visted all the notifications so we need to get previous unvisited notification count and add new notification count which is unvisted by default to it. Will get total unvisted count
            {
                NSInteger t = [[dicttemp objectForKey:@"T"] integerValue] +[[obje objectForKey:@"T"]integerValue];
                NSInteger o = [[dicttemp objectForKey:@"O"] integerValue] +[[obje objectForKey:@"O"]integerValue];
                NSInteger i = [[dicttemp objectForKey:@"I"] integerValue] +[[obje objectForKey:@"I"]integerValue];
                NSInteger uvc = [[dicttemp objectForKey:@"UnVisitedCount"] integerValue] +[[obje objectForKey:@"UnVisitedCount"]integerValue];
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:t],@"T",[NSNumber numberWithInteger:o],@"O",[NSNumber numberWithInteger:i],@"I",[NSNumber numberWithInteger:uvc],@"UnVisitedCount", nil];
                [defaults setObject:dict forKey:key];
                [defaults synchronize];
                return ;
                ////
            }
            else// if its is zero it means set it comes first time or user has visted all notifications
            {
                [defaults setObject:obje forKey:key];
                [defaults synchronize];
                return ;
            }
        }
        else
        {
            [defaults setObject:obje forKey:key];
            [defaults synchronize];
            return ;
        }
        
    }
 }



//+ (NSString*)displayStringForKey:(NSString*)key
//{
//    NSString *displayString = nil;
//    if ([key compare:kAboutMe] == NSOrderedSame) {
//        displayString = @"About Me";
//    } else if ([key compare:kSex] == NSOrderedSame) {
//        displayString = @"Sex";
//    } else if ([key compare:kInterestedIn] == NSOrderedSame) {
//        displayString = @"Interested In";
//    } else if ([key compare:kDob] == NSOrderedSame) {
//        displayString = @"Birthday";
//    } else if ([key compare:kLanguage] == NSOrderedSame) {
//        displayString = @"Language";
//    } else if ([key compare:kPhoneNumber] == NSOrderedSame) {
//        displayString = @"Phone No";
//    } else if ([key compare:kCity] == NSOrderedSame) {
//        displayString = @"City";
//    } else if ([key compare:kHomeTown] == NSOrderedSame) {
//        displayString = @"Home Town";
//    } else if ([key compare:kFirstNameKey] == NSOrderedSame) {
//        displayString = @"First Name";
//    }  else if ([key compare:kLastNameKey] == NSOrderedSame) {
//        displayString = @"Last Name";
//    }  else if ([key compare:kAvatarMedium] == NSOrderedSame) {
//        displayString = @"Avatar";
//    }  
//    return displayString;
//}

//+ (BOOL)imageExist:(NSString*)imageName
//{
//    BOOL exist = NO;
//    NSString *path = [DOC_DIR stringByAppendingPathComponent:imageName];
//    exist = [[NSFileManager defaultManager]fileExistsAtPath:path];
//    return exist;
//}

////----- show a alert massage
+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate 
            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate 
										  cancelButtonTitle:CbtnTitle otherButtonTitles:otherBtnTitles, nil];
    alert.tag = tag;
	[alert show];
	[alert release];
}
+(UIColor *)colorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    if([hexString length]>0)
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
@end
