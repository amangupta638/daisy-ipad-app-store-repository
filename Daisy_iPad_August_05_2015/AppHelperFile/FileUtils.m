//
//  FileUtils.m
//


#import "FileUtils.h"


@implementation FileUtils


// Returns document directory path

+(NSString *) documentsDirectoryPath {
	NSArray *arrPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *strDocumentsDirectory = [arrPaths objectAtIndex:0];
	return strDocumentsDirectory;
}

// Returns document directory path for file name

+(NSString *) documentsDirectoryPathForResource:(NSString *) strFileName {
	NSString *strDocumentsDirectoryPath = [FileUtils documentsDirectoryPath];
	NSString *strFinalPath = [strDocumentsDirectoryPath stringByAppendingPathComponent:strFileName];
	return strFinalPath;
}

// Check file is exist or not

+(BOOL) fileExistsAtPath:(NSString *) strPath fileName: (NSString *) strFileName {
	NSString *strCombinedPath = [strPath stringByAppendingPathComponent:strFileName];
	
	NSFileManager * manager = [NSFileManager defaultManager];
	BOOL isExists = [manager fileExistsAtPath:strCombinedPath];
	return isExists;
}

// Get next sequential file name for path

+(NSString *) nextSequentialFileName:(NSString *) strFileName fileExtenstion:(NSString *) strExtenstion  directoryPath: (NSString *) strPath {
	NSFileManager * manager = [NSFileManager defaultManager];
	NSArray *arrContents = [manager contentsOfDirectoryAtPath:strPath error:nil];
	
	if(arrContents) {
		NSString *strFormattedFileName = [[NSString alloc] initWithFormat:@"%@.%@", strFileName, strExtenstion];
		int counter = 1;
		while([arrContents indexOfObject:strFormattedFileName] != NSNotFound) {
			NSString *strNewFileName = [[NSString alloc] initWithFormat:@"%@%d.%@", strFileName, counter, strExtenstion];
			
			strFormattedFileName = strNewFileName ;
				
			counter++;
		}
		return strFormattedFileName ;
		
	} else {
		return nil;
	}		
}

// Write data to document directory 

+(BOOL) writeFileToDocuemntsDirectory:(NSData *)data withFileName:(NSString *)strFileName withExtension:(NSString *)strExtenstion {
	NSString *strDocumentsDirectoryPath = [FileUtils documentsDirectoryPath];
	NSString *strNextSequentialFileName = [FileUtils nextSequentialFileName:strFileName fileExtenstion:strExtenstion directoryPath:strDocumentsDirectoryPath];
	NSString *strFileNameWithExtension = [[NSString alloc] initWithString: strNextSequentialFileName];
	NSString *strFinalFilePath = [strDocumentsDirectoryPath stringByAppendingPathComponent:strFileNameWithExtension];
	BOOL isSuccess = [data writeToFile:strFinalFilePath atomically:YES];
	return isSuccess;
}

// Copy resources from bundle to document directory

+(void) copyResourceFileToDocumentsDirectory: (NSString *) strFileName {
    
    NSString *strDocumentsDirectory = [FileUtils documentsDirectoryPath];
    NSString *strWritablePath = [strDocumentsDirectory stringByAppendingPathComponent:strFileName];
	
	NSFileManager * manager = [NSFileManager defaultManager];
	BOOL isSucceeded = [manager fileExistsAtPath:strWritablePath]; 
	NSError *error;
	
	if(!isSucceeded) { //If file is not in the documents directory then only write
		NSString *newPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:strFileName];
	
		isSucceeded = [manager copyItemAtPath:newPath toPath:strWritablePath error:&error];
		
		if(isSucceeded == FALSE)
		{
			
		}
        else
        {
		}
	}
    else
    {
	}
}

// Copy array of resources to document directory

+(void) copyResourcesToDocumentsDirectory: (NSArray *) strResources
{
	NSInteger fileCount = [strResources count];
	for(int i = 0; i < fileCount; i++) {
		NSString *strFilename = [strResources objectAtIndex:i];
		[self copyResourceFileToDocumentsDirectory:strFilename];
	}
}

// Delete file from document directory

+(BOOL) deleteFileFromDocumentsDirectory: (NSString *) strFileName{
	NSString *arrPath = [self documentsDirectoryPath];
	NSString *strFinalpath = [self documentsDirectoryPathForResource:strFileName];
	BOOL isFileExits = [self fileExistsAtPath:arrPath fileName:strFileName];
	if(isFileExits){
		NSFileManager *fileManager = [NSFileManager defaultManager];
		[fileManager removeItemAtPath:strFinalpath error:NULL];
		return YES;
	} else{
		return NO;
	}
	
}

// Get temp directory path
+(NSString *) temporaryDirectoryPath {
	//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *strTemporaryDirectory = NSTemporaryDirectory();
	return strTemporaryDirectory;
}

// Write file tp temp directory

+(BOOL) writeFileToTemporaryDirectory:(NSData *)data withFileName:(NSString *)strFileName withExtension:(NSString *)strExtenstion {
    
	NSString *strTemporaryDirectoryPath = [FileUtils temporaryDirectoryPath];
	NSString *strNextSequentialFileName = [FileUtils nextSequentialFileName:strFileName fileExtenstion:strExtenstion directoryPath:strTemporaryDirectoryPath];
	NSString *strFileNameWithExtension = [[NSString alloc] initWithString: strNextSequentialFileName];
	NSString *strFinalFilePath = [strTemporaryDirectoryPath stringByAppendingPathComponent:strFileNameWithExtension];
	BOOL isSuccess = [data writeToFile:strFinalFilePath atomically:YES];
	return isSuccess;
}

// Get temp directory path for file name
+(NSString *) temporaryDirectoryPathForResource:(NSString *) strFileName {
	NSString *strTemporaryDirectoryPath = [FileUtils temporaryDirectoryPath];
	NSString *strFinalPath = [strTemporaryDirectoryPath stringByAppendingPathComponent:strFileName];
	return strFinalPath;
}

// Create new folder in document directory
+(NSString *) createDocumentsDirectoryPathWithNewFolder:(NSString *)strFolderName {
    
    NSString *filePath = nil;
	NSString *str=[FileUtils documentsDirectoryPath];	
	BOOL isTempImageDirectoryExists = [FileUtils fileExistsAtPath:str fileName:strFolderName];
	if(isTempImageDirectoryExists == NO)
	{				
		BOOL isTempImageDirectoryExists =[FileUtils createNewDirectoryAtPath:str andName:strFolderName];
		if(isTempImageDirectoryExists == YES)			
		{
			NSString *str1=[FileUtils documentsDirectoryPath];			
			BOOL isDirectoryExist = [FileUtils fileExistsAtPath:str1 fileName:strFolderName];
			if(isDirectoryExist == YES)
			{                
				// for Temp Image gallery path
				NSString *temp2=[[NSString alloc]initWithFormat:@"%@/%@",str1,strFolderName];
				filePath =[[NSString alloc]initWithFormat:@"%@",temp2];				
			}
		}
	}
	else
	{
		NSString *str1=[FileUtils documentsDirectoryPath];
		BOOL isDirectoryExist = [FileUtils fileExistsAtPath:str1 fileName:strFolderName];
		if(isDirectoryExist == YES)
		{            
			NSString *temp2=[[NSString alloc]initWithFormat:@"%@/%@",str1,strFolderName];
			filePath=[[NSString alloc]initWithFormat:@"%@",temp2] ;			
		}
	}
	
	return filePath;
}

// -----------------------------------------------------------------------------
// Added by mitul to add Application name Directory in Document Directory File
// Create new folder in document directory

+ (NSString *)createNewDirectoryAtGivenDocumentDirectoryPath:(NSString *)strFolderName {
    
    NSString *filePath = nil;
	NSString *str=[FileUtils documentsDirectoryPath];
    str = [str stringByAppendingPathComponent:kApplicationDocumentDirectoryName];
    
	BOOL isTempImageDirectoryExists = [FileUtils fileExistsAtPath:str fileName:strFolderName];
	if(isTempImageDirectoryExists == NO)
	{
		BOOL isTempImageDirectoryExists =[FileUtils createNewDirectoryAtPath:str andName:strFolderName];
		if(isTempImageDirectoryExists == YES)
		{
			NSString *str1=[FileUtils documentsDirectoryPath];
            str1 = [str1 stringByAppendingPathComponent:kApplicationDocumentDirectoryName];
			BOOL isDirectoryExist = [FileUtils fileExistsAtPath:str1 fileName:strFolderName];
			if(isDirectoryExist == YES)
			{
				// for Temp Image gallery path
				NSString *temp2=[[NSString alloc]initWithFormat:@"%@/%@",str1,strFolderName];
				filePath =[[NSString alloc]initWithFormat:@"%@",temp2];
			}
		}
	}
	else
	{
		NSString *str1=[FileUtils documentsDirectoryPath];
        str1 = [str1 stringByAppendingPathComponent:kApplicationDocumentDirectoryName];
		BOOL isDirectoryExist = [FileUtils fileExistsAtPath:str1 fileName:strFolderName];
		if(isDirectoryExist == YES)
		{
			NSString *temp2=[[NSString alloc]initWithFormat:@"%@/%@",str1,strFolderName];
			filePath=[[NSString alloc]initWithFormat:@"%@",temp2] ;
		}
	}
	
	return filePath;
}

// Save image to document directory
+(NSString *) saveImageToDocumentsDirectory : (NSString *) filepath UsingImagename : (NSString *) imagename andImage: (UIImage *) postimage {		
    
	NSString *filePath = nil;
  	NSData *imageData2=UIImagePNGRepresentation(postimage);

    BOOL isFileAddedToTempGallery = [FileUtils writeFileToAnyDirectory:imageData2 withFileName:imagename withExtension:@"png" andPath:filepath];
	
	if(isFileAddedToTempGallery == YES)
	{		
		NSString *strDocPath=[FileUtils documentsDirectoryPath];
		NSString *tempImageDocPath=[[NSString alloc]initWithFormat:@"%@/%@",strDocPath,@"ImageGallery"];
		NSString* temp= [[NSString alloc]initWithFormat:@"%@",tempImageDocPath];		
		filePath=[[NSString alloc]initWithFormat:@"%@/%@.png",temp,imagename];
	}
	return filePath;
}

// Create new directory 
+(BOOL) createNewDirectoryAtPath:(NSString *)strPath andName:(NSString *)strName
{
	NSFileManager * manager = [NSFileManager defaultManager];
	NSString *strNewDirectoryWithPath=[[NSString alloc]initWithFormat:@"%@/%@",strPath,strName];

    BOOL isCreated = [manager	createDirectoryAtPath:strNewDirectoryWithPath withIntermediateDirectories:YES attributes:nil error:nil];

	
	return isCreated;
}

// Write file to path specified by user with data,file name and extension
+(BOOL) writeFileToAnyDirectory:(NSData *)data withFileName:(NSString *)strFileName withExtension:(NSString *)strExtenstion andPath:(NSString *)strPath
{

	NSString *strNextSequentialFileName = [FileUtils nextSequentialFileName:strFileName fileExtenstion:strExtenstion directoryPath:strPath];
	NSString *strFileNameWithExtension = [[NSString alloc] initWithString: strNextSequentialFileName];
	NSString *strFinalFilePath = [strPath stringByAppendingPathComponent:strFileNameWithExtension];
	BOOL isSuccess = [data writeToFile:strFinalFilePath atomically:YES];
	return isSuccess;
}

// Write file to path specified by user with data,file name and extension and return path

+(NSString *) writeFileToAnyDirectoryandReturnPath:(NSData *)data withFileName:(NSString *)strFileName withExtension:(NSString *)strExtenstion andPath:(NSString *)strPath
{
	NSString *strNextSequentialFileName = [FileUtils nextSequentialFileName:strFileName fileExtenstion:strExtenstion directoryPath:strPath];
	NSString *strFileNameWithExtension = [[NSString alloc] initWithString: strNextSequentialFileName];
	NSString *strFinalFilePath = [strPath stringByAppendingPathComponent:strFileNameWithExtension];
	[data writeToFile:strFinalFilePath atomically:YES];
	return strFinalFilePath;
}

// Returns data from path

+(NSData *) getDataFromPath:(NSString *) strPath
{
	NSFileManager *manager = [NSFileManager defaultManager];
	NSData *data = nil;
	NSString *strFinalPath=[[NSString alloc]initWithFormat:@"%@",strPath];

    data=[manager contentsAtPath:strFinalPath];

    return data;

}

+(UIImage *) getImageFromPath:(NSString *) strPath
{
    UIImage *image = [UIImage imageWithContentsOfFile:strPath];
    if (image)
    {
        return image;
    }
    else
    {
        return [UIImage imageNamed:@""];
    }
}

// Get contents of path
+(NSArray*)getContentsofDirectory:(NSString *)strPath
{
	NSFileManager *manager = [NSFileManager defaultManager];
	NSArray *arrResult=nil;
	arrResult =[manager contentsOfDirectoryAtPath:strPath error:nil];

    return arrResult;
	
}

// Remove file from path
+(BOOL) removeFileFromPath:(NSString *)strPath
{
	NSFileManager *manager = [NSFileManager defaultManager];
	BOOL isRemoved=[manager removeItemAtPath:strPath error:NULL];
	return isRemoved;
}

@end
