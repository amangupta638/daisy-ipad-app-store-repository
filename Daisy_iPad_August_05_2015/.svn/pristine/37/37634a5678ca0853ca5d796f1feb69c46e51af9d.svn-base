﻿<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Content Panel.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Revising for Exams</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../../../Support/content-style.css" type="text/css"/>
</head>

<body class="panel">
	<!-- InstanceBeginEditable name="content" -->
	<div style="width:845px; clear:both;color:#575757;font-family: Arial;">
	<h1 style="padding-bottom:5px;font-size:16pt;"><strong>Revising for Exams</strong></h1>
    <img src="Images/Revising-for-exam.jpg" alt="" width="188" style="float:left;padding-right:20px;padding-bottom:10px;">
    <p>Don’t believe the student who says s/he only studies just before exams and still gets through.</p>
    <p>Revision doesn’t begin a few weeks before exams. It is very difficult to read and learn an entire semester’s (or year’s) course in a few weeks. Regular revision is essential. The earlier you start, the better prepared you will be.</p>
    <p><strong>Develop a study timetable</strong> you can stick to, with a daily outline that includes times for breaks and meals. Start early – “plan” don’t “cram”.</p>
    <p><strong>Revise after each lesson</strong>. If you have not understood a particular topic/area, you will need to do more reading, ask your teacher more questions or do more independent research.</p>
    <p><strong>Make active notes</strong> in your own words highlighting points by using headings, sub-headings, underlining and diagrams. Draw maps, flow charts, pictures etc to see and create associations. Look back over your notes for common themes/ideas. Link these together to develop your understanding of the topic.</p>
    <p><strong>Re-read your notes</strong> a day later and then a week after that. Make a summary. A summary must be brief and must act as a key to recalling other material.</p>
    <p><strong>Make a list of all your subjects’ exams</strong> and the type of exam for each: oral, multiple choice, short answer, essay, problem solving.<br>
Different types of exams require different forms of preparation.</p>
	<p><strong>Review your subjects’ course outlines</strong> to gain a sense of the main issues covered and the types of knowledge you will be expected to demonstrate. Identify what must be studied, what should be studied and what (if anything) can be ignored.</p>
    <p><strong>Use past exam papers</strong> to direct your study and take notes – this will give you an idea of the format of the exam, how questions are worded and how the material has been examined in the past. When you finish a topic, check to see if there is a related past exam question. It will be easier to prepare a response while the topic is still fresh in your mind. You can use these notes later to revise for the exam.</p>
    <p><strong>Practise verbalising answers</strong> to a friend to help clarify or reveal any gaps in your knowledge.</p>
    <p><strong>In the last few months leading up to the exams</strong>, set up a special pre-exam study timetable.</p>
    <p><strong>Practise past exam papers</strong> under exam conditions.</p>
    <ul style="margin-left:-20px; margin-top:0;">
        <li style="list-style-position:outside;">Familiarise yourself with the language used</li>
        <li style="list-style-position:outside">Prepare a glossary of important terms and explain them in your own words</li>
        <li style="list-style-position:outside">Identify areas you’ve missed or realise you don’t fully understand. Fill in the gaps.</li>
        <li style="list-style-position:outside">Do you need to work faster in order to complete the paper or slow down and include more detail?</li>
    </ul>
    <p><strong>Practise writing and/or planning essays.</strong></p>
    <p><strong>Check examiners’ reports</strong> to identify common mistakes.</p>
    <p>Keep physically fit. Eat healthy food and get plenty of sleep. Find time to exercise daily – a 15 minute jog or a brisk walk will do the job nicely. Drink plenty of water to keep you hydrated and your brain functioning at its best.</p>
    <p><strong>Learn how to relax.</strong></p>
    <p><strong>Ask for help</strong>. Ask a teacher if you don’t understand a particular topic. School counsellors and even good old mum and dad are great for getting worries “off your chest”. You’re not alone, so don’t feel that you have to be.</p>
    <p><strong>Attend any revision classes</strong> on offer even if you think you know it already.</p>
    <p><strong>Reward yourself</strong>. This will keep you motivated. Set yourself a goal and reward yourself when you achieve it. A reward can be as simple as a cup of tea or your favourite TV show.</p>
</div>
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>
