//
//  AppDelegate.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 11/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
// asdasdaaaaa

#import <UIKit/UIKit.h>
#import "NetManager.h"
#import "MyClass.h"
#if RUN_KIF_TESTS
#import "SDKTestController.h"
#endif
//Comment Added.
//----- Added by Mitul on 11-12-13 -----//
#import <DropboxSDK/DropboxSDK.h>


// --- New Key For E-Planner Production --- //
#define AppKey @"r5m3iv7g2qfmwkt"//@"n48njzalxdbzo04"
#define AppSecretKey @"cf6ynlgc65hmgdo"//@"ijwnvj2g59lys7m"
// --- End --- //

// --- New Key For Honeycomb E-Planner Production --- //
//#define AppKey @"gi5mas1iim8l9vn"
//#define AppSecretKey @"z1ehffgbedyifn5"
// --- End --- //

//TODO: Uncomment the code when creating the build for Prime
// --- New Key For Prime Planner Development--- //
//#define AppKey @"xuhchi5xer35jgr"
//#define AppSecretKey @"0nvji57ge5klm3b"
// --- End --- //



@interface AppDelegate : UIResponder <UIApplicationDelegate, DBSessionDelegate, DBNetworkRequestDelegate > // Modified by Mitul
{
    NetManager *netManager;
    IBOutlet UIView *_errorView;
    IBOutlet UIView *_errorSubView;
    
}
@property (nonatomic, retain)MyClass *_curentClass;
@property (nonatomic, assign) BOOL  _isUniqueId;
@property (nonatomic, retain) NSOperationQueue *_operationQueue;
@property (nonatomic,retain)    UINavigationController *_navi;
@property(nonatomic,retain)UIViewController *_currentViewController;
@property (nonatomic, retain) NetManager *netManager;
@property (strong, nonatomic)  IBOutlet UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,retain) NSMutableArray *dueArray;
@property(nonatomic,retain) NSMutableArray *assignedArray;
@property(nonatomic,retain) NSMutableArray *noticationArray;
@property(nonatomic,retain) NSMutableArray *glb_mArrClassCycleDays;
/*******This Array hold the Folder respective Files Name and Thier respective Path for Attachement*********/
@property (strong, nonatomic) NSMutableDictionary *glb_mdict_for_Attachement;
@property (strong, nonatomic) UIButton *glb_Button_Attachment ;

@property (strong, nonatomic) NSArray *selectedRecipents ;
@property (strong, nonatomic) NSMutableArray *marrListedAttachments;
@property (strong, nonatomic) NSMutableArray *marrDeletedAttachments;
@property (nonatomic , assign) BOOL glb_isDiaryItemEditForAttachment ;
// Aman Added -- slider updated
@property (nonatomic, assign) BOOL  glbIsComingFromGlanceSlider;
@property (nonatomic, retain) NSMutableArray  *glbMarrGlanceSliderValues;
@property (nonatomic, assign) BOOL  glbDueSectionWasSelected;

@property (nonatomic, assign) BOOL glbIsSyncCalled;
@property (nonatomic, assign) BOOL glbIsStudentSelected;

// Campus Based Start
/***********DueDate calculation Color***************/
@property (nonatomic, retain) NSMutableArray  *glbarrForCalenderCycleDay;
/***********DueDate calculation Color***************/
// Campus Based End

@property (nonatomic, assign) BOOL isClassuserDataRecevivedFromServer;

@property (nonatomic, assign) BOOL  _isClassBasedOrTaskBasedMeritDelete;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
+ (AppDelegate*) getAppdelegate;
-(void)enterIntoApp;
-(void)showIndicator:(UIView*)_view;
-(void)hideIndicator;

/// Aman Added Code For Drop Box//
-(void)calldropBox:(UIButton *)sender;

// Check Date Between
- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate;

- (BOOL)isDateInBetweenGivenDate:(NSDate *)date firstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate;
- (void)createNewUUID;
@end
