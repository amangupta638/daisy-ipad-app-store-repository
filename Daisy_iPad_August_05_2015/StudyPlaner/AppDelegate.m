//
//  AppDelegate.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 11/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "AppDelegate.h"
#import "NetManager.h"
#import "SplashViewController.h"
#import "LoginViewController.h"
#import "MainViewController.h"
#if RUN_KIF_TESTS
#import "SDKTestController.h"
#endif
#import "AppHelper.h"
#import "SSKeychain.h"
//#import "TimeTable.h"
//----- Added by Mitul on 17-Dec-13 -----//
#import "FileUtils.h"
//----- Mitul -----//

#import "Flurry.h"
#import "Service.h"
#import "MyProfile.h"
#import "CalenderViewController.h"

@interface AppDelegate()
{
    UIPopoverController *popOver;
    
}

@end

@implementation AppDelegate
@synthesize glb_mdict_for_Attachement;
@synthesize selectedRecipents ;
// 1679
@synthesize glbIsSyncCalled;
@synthesize glbIsStudentSelected;

- (void)dealloc
{
    [_curentClass release];
    [noticationArray release];
    [assignedArray release];
    [dueArray release];
    [_operationQueue release];
    [netManager release];
    [_currentViewController release];
    [_navi release];
    [_window release];
    [_managedObjectContext release];
    [_managedObjectModel release];
    [_persistentStoreCoordinator release];
    [_errorSubView release];
    [glb_Button_Attachment release];
    [super dealloc];
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator,_navi,_currentViewController;
@synthesize netManager;
@synthesize _operationQueue;
@synthesize dueArray,assignedArray,noticationArray,_isUniqueId,_curentClass;
@synthesize glb_mArrClassCycleDays;
@synthesize glb_Button_Attachment;
@synthesize marrListedAttachments ;
@synthesize marrDeletedAttachments ;
@synthesize glb_isDiaryItemEditForAttachment ;

// Aman Added -- slider updated
@synthesize glbIsComingFromGlanceSlider;
@synthesize glbMarrGlanceSliderValues;
@synthesize glbDueSectionWasSelected;
// Campus Based Start
/***********DueDate calculation Color***************/
@synthesize glbarrForCalenderCycleDay;
/***********DueDate calculation Color***************/
// Campus Based End
@synthesize isClassuserDataRecevivedFromServer;
@synthesize _isClassBasedOrTaskBasedMeritDelete;


#pragma mark
#pragma mark iCloud Dont Sync Files Method

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success)
    {
        #if DEBUG
                NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        #endif
    }
    return success;
}

+ (AppDelegate*) getAppdelegate
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSString* deviceTkn = [[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceTkn = [deviceTkn stringByReplacingOccurrencesOfString:@">" withString:@""];
    deviceTkn = [deviceTkn stringByReplacingOccurrencesOfString:@" " withString:@""];
    [AppHelper saveToUserDefaults:deviceTkn withKey:@"Token"] ;
}
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Push Notification" message:[NSString stringWithFormat:@"Error in registration. Error: %@", err] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
    [myAlertView show];
    [myAlertView release];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    /************** Code Added For Push Notification  By Aman **********************/
    
    //  application.applicationIconBadgeNumber = 0;
    NSString *msg = [NSString stringWithFormat:@"%@", userInfo];
    #if DEBUG
        NSLog(@"%@",msg);
    #endif
    
    
    application.applicationIconBadgeNumber = 0;
    
    
    for (id key in userInfo)
    {
        #if DEBUG
                NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
        #endif
    }
    
    [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue]];
    if([[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue] >0)
    {
        [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue]-1];
    }
    
    [AppHelper saveToUserDefaultforPushNotification:[userInfo objectForKey:@"aps"] withkey:@"PayLoad"];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:[[[userInfo objectForKey:@"aps"] objectForKey:@"T"] integerValue]],@"T",[NSNumber numberWithInteger:[[[userInfo objectForKey:@"aps"] objectForKey:@"O"] integerValue]],@"O",[NSNumber numberWithInteger:[[[userInfo objectForKey:@"aps"] objectForKey:@"I"] integerValue]],@"I",[NSNumber numberWithInteger:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] integerValue]],@"UnVisitedCount", nil];
    [AppHelper saveToUserDefaultforPushNotification:dict withkey:@"UnVisited"];
    
    /************** Code Added For Push Notification  By Aman **********************/
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    // Aman Added -- slider updated
    glbIsComingFromGlanceSlider=NO;
    glbDueSectionWasSelected = YES;
    
    // JIRA 1679
    self.glbIsStudentSelected = NO;
    self.glbIsSyncCalled = NO;
    
    // Aman Added for Holding files Path in Local Directory
    self.glb_mdict_for_Attachement = nil;
    self.glb_mdict_for_Attachement = [[NSMutableDictionary alloc] init];
    
    self.marrListedAttachments = nil ;
    self.marrListedAttachments = [[NSMutableArray alloc]init];
    
#if DEBUG
    NSLog(@" Delegate Class self.marrListedAttachments =%p",self.marrListedAttachments);
#endif

    
    self.marrDeletedAttachments = nil ;
    self.marrDeletedAttachments = [[NSMutableArray alloc]init];
    
    self.glb_isDiaryItemEditForAttachment = NO ;
    
    self.glb_mArrClassCycleDays = nil;
    self.glb_mArrClassCycleDays = [[NSMutableArray alloc] init];
    /************** Code Added For Push Notification  By Aman ******************/
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:0],@"T",[NSNumber numberWithInteger:0],@"O",[NSNumber numberWithInteger:0],@"I",[NSNumber numberWithInteger:0],@"UnVisitedCount", nil];
    [AppHelper saveToUserDefaultforPushNotification:dict withkey:@"UnVisited"];
    
    /************** Code Added For Push Notification  By Aman **********************/
    
    // Campus Based Start
    /***********DueDate calculation Color***************/
    
    self.glbarrForCalenderCycleDay = nil ;
    self.glbarrForCalenderCycleDay =[[NSMutableArray alloc]init];
    /***********DueDate calculation Color***************/
    // Campus Based End
    
    
    
    //------ Added by Mitul -----//
    NSString *root = kDBRootDropbox;
    DBSession* session =
    [[DBSession alloc] initWithAppKey:AppKey appSecret:AppSecretKey root:root];
    session.delegate = self; // DBSessionDelegate methods allow you to handle re-authenticating
    [DBSession setSharedSession:session];
    [DBRequest setNetworkRequestDelegate:self];
    
    NSURL *launchURL = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    NSInteger majorVersion =
    [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] integerValue];
    if (launchURL && majorVersion < 4) {
        // Pre-iOS 4.0 won't call application:handleOpenURL; this code is only needed if you support
        // iOS versions 3.2 or below
        [self application:application handleOpenURL:launchURL];
        return NO;
    }
    
    // Create NewFolder in Document Directory to store downloaded documents
    NSString *strFolderPath = [FileUtils createDocumentsDirectoryPathWithNewFolder:kApplicationDocumentDirectoryName];
    
    //----- by Mitul -----//
    
    if(![AppHelper userDefaultsForKey:@"Token"]){
        //        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert |UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound)];
        
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            UIUserNotificationSettings *notificationSettings=[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication]registerUserNotificationSettings:notificationSettings];
            [[UIApplication sharedApplication]registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication]registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert)];
        }
        
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
    
    self.dueArray=(NSMutableArray*)[NSMutableArray array];
    self.assignedArray=(NSMutableArray*)[NSMutableArray array];
    self.noticationArray=(NSMutableArray*)[NSMutableArray array];
    netManager=[[NetManager alloc]init];
    // self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    //self.window.backgroundColor = [UIColor redColor];
    
    if([AppHelper userDefaultsForKey:@"local"]){
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        [Flurry setCrashReportingEnabled:YES];
        [Flurry startSession:FLURRY_API_KEY];
        [Flurry setUserID:profobj.email];
        
        
        SplashViewController  *login=[[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
        //login.wantsFullScreenLayout=YES;
        UINavigationController *navi=[[UINavigationController alloc] initWithRootViewController:login];
        
        self._navi=navi;
        [navi release];
        [self._navi setNavigationBarHidden:YES animated:NO];
        self.window.rootViewController=self._navi;
        [login release];
        
        self.isClassuserDataRecevivedFromServer = YES;
        
        
    }
    else{
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:@"1" forKey:@"Id"];
        [dict setObject:@"0" forKey:@"diary"];
        [dict setObject:@"0" forKey:@"note"];
        [dict setObject:@"0" forKey:@"inbox"];
        [dict setObject:@"0" forKey:@"class"];
        
        [[Service sharedInstance]parseLastUpdateTableDictionary:dict];
        [dict release];
        LoginViewController  *login=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        //login.wantsFullScreenLayout=YES;
        UINavigationController *navi=[[UINavigationController alloc] initWithRootViewController:login];
        
        self._navi=navi;
        [navi release];
        [self._navi setNavigationBarHidden:YES animated:NO];
        self.window.rootViewController=self._navi;
        
        self.isClassuserDataRecevivedFromServer = NO;
        
        //[self.window addSubview:self._navi.view];
        
        [login release];
    }
    [self.window makeKeyAndVisible];
    
    
    //for drop box....
    // Aman Commented
    // NSString* appKey = @"e86n35poc20472o";
    //NSString* appSecret = @"juaue7kl2g84g1d";
    
    // NSString* appKey = @"t3ow4tvu36zlh5s";
    //NSString* appSecret = @"w4nmqlk5ul1uiw8";
    
    //    sleep(4); //Temp
    
#if RUN_KIF_TESTS
    [[SDKTestController sharedInstance] startTestingWithCompletionBlock:^{
        [[[[UIAlertView alloc] initWithTitle:@"Testing finished." message:[NSString stringWithFormat:@"%d failures.", [[SDKTestController sharedInstance] failureCount]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease] show];
    }];
#endif
    
    
    
    
    self._operationQueue = [[[NSOperationQueue alloc] init] autorelease];
    [self._operationQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
    [self._operationQueue waitUntilAllOperationsAreFinished];
    
    
    
    if(![AppHelper userDefaultsForKey:@"Token"]){
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            UIUserNotificationSettings *notificationSettings=[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication]registerUserNotificationSettings:notificationSettings];
            [[UIApplication sharedApplication]registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication]registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert)];
        }
    }
    
    
    //TODO: set iCloud flag to mange sync
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSURL *url = [NSURL fileURLWithPath:documentsDirectory];
    BOOL success = [self addSkipBackupAttributeToItemAtURL:url];
    
    /***********1878************/
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *currentTime = [dateFormatter stringFromDate:today];

    [AppHelper saveToUserDefaults:[dateFormatter dateFormat] withKey:@"DeviceTimeFormat"];
    [dateFormatter release];
    /***********1878************/
    
    
    
    return YES;
}

//----- Added By Mitul on 11-12-13 -----//

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked]) {
        }
        return YES;
    }
    
    return NO;
}

//----- By Mitul -----//

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //----- Added By Mitul on -----//
    if ([[DBSession sharedSession] handleOpenURL:url]) {
        if ([[DBSession sharedSession] isLinked])
        {
            // Post a notification to loginComplete
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil];
        }
        return YES;
    }
    
    //----- By Mitul -----//
    
    
    
    return NO;
}

-(void)enterIntoApp{
    [self._navi.view removeFromSuperview];
    LoginViewController  *login=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    //login.wantsFullScreenLayout=YES;
    UINavigationController *navi=[[UINavigationController alloc] initWithRootViewController:login];
    self._navi=navi;
    [navi release];
    [self._navi setNavigationBarHidden:YES animated:NO];
    self.window.rootViewController=self._navi;
    //[self.window addSubview:self._navi.view];
    
    [login release];
    [self.window makeKeyAndVisible];
}




- (void)applicationWillResignActive:(UIApplication *)application
{
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    
    if(![AppHelper userDefaultsForKey:@"local"]){
        [AppHelper saveToUserDefaults:NULL withKey:@"bg"];
        if([[Service sharedInstance] netManager]!=nil){ //Stop Previous Calls
            [[[Service sharedInstance] netManager] stopRequest];
        }
    }
    
    
    /************** Code Added For Push Notification  By Aman **********************/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [defaults objectForKey:@"UnVisited"];
    [application setApplicationIconBadgeNumber:[[dict objectForKey:@"UnVisitedCount"] integerValue]];
    
    /************** Code Added For Push Notification  By Aman **********************/
    //[AppHelper saveToUserDefaults:nil withKey:@"user_id"];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    [AppHelper saveToUserDefaults:[dateFormatter dateFormat] withKey:@"DeviceTimeFormat"];
    [dateFormatter release];
    
    
    
    
    MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
    dispatch_async(dispatch_get_main_queue(), ^{
        if([[Service sharedInstance] netManager]!=nil){ //Stop Previous Calls
            [[[Service sharedInstance] netManager] stopRequest];
        }
        [[NSNotificationCenter defaultCenter] removeObserver:mainVC];
        [mainVC BackGroundMethoForSync];
        
        
    });
    
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    if(![AppHelper userDefaultsForKey:@"local"]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bg" object:nil userInfo:nil];
    }
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSError *error;
    
    NSString *udid = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
    if (!udid)
    {
        [self createNewUUID];
        
        
    }
    NSString *udid1 = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
    
    [AppHelper saveToUserDefaults:udid1 withKey:@"ipadd"];
    
    
    [self performSelector:@selector(applicationSettingsDateFormatChanged) withObject:nil afterDelay:.2];
    
    
    
}


- (void)createNewUUID
{
    NSError *error;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMddYYYY-HHmmss"];
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = ( NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuidObject) ;
    CFRelease(uuidObject);
    NSString *generatedId = [NSString stringWithFormat:@"%@-%@", [formatter stringFromDate:[NSDate date]], uuidStr];
    [SSKeychain setPassword:generatedId forService:@"com.app.daisyApp" account:@"AppUser"];
    
    [formatter release];
    NSString *udid1 = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
    
    [AppHelper saveToUserDefaults:udid1 withKey:@"ipadd"];
    
}



-(void)applicationSettingsDateFormatChanged
{
    /***********1878************/
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    if([AppHelper userDefaultsForKey:@"DeviceTimeFormat"]){
        
        if(![[dateFormatter dateFormat]isEqualToString:[AppHelper userDefaultsForKey:@"DeviceTimeFormat"]])
        {
            
            self.window.rootViewController = nil;
            self._navi = nil;
            
            
            SplashViewController  *login=[[SplashViewController alloc] initWithNibName:@"SplashViewController" bundle:nil];
            UINavigationController *navi=[[UINavigationController alloc] initWithRootViewController:login];
            
            self._navi=navi;
            [self._navi setNavigationBarHidden:YES animated:NO];
            
            self.window.rootViewController=self._navi;
            
            [login release];
            [navi release];
            
        }
        
    }
    
    [dateFormatter release];
    dateFormatter = nil;
    
    
    /***********1878************/
    
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            //abort();
            
            [self showAlert];
        }
    }
}

- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Could Not Save Data"
                                                        message:@"There was a problem saving your data. If you restart the app, you can try again. Please contact support (support@domain.com) to notify us of this issue."
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    alertView.tag =11;
    [alertView show];
    [alertView release];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag ==11 || alertView.tag ==12 )
    {
        abort();
    }
    
    
}



//----- Added By Mitul on 11-12-13 -----//

#pragma mark -
#pragma mark DBSessionDelegate methods

- (void)sessionDidReceiveAuthorizationFailure:(DBSession*)session userId:(NSString *)userId {
    [[[UIAlertView alloc]
      initWithTitle:@"Dropbox Session Ended" message:@"Do you want to relink?" delegate:self
      cancelButtonTitle:@"Cancel" otherButtonTitles:@"Relink", nil]
     show];
}

#pragma mark -
#pragma mark DBNetworkRequestDelegate methods

static int outstandingRequests;

- (void)networkRequestStarted {
    outstandingRequests++;
    if (outstandingRequests == 1) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
}

- (void)networkRequestStopped {
    outstandingRequests--;
    if (outstandingRequests == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

//----- By Mitul -----//
#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"StudyPlaner" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"StudyPlaner.sqlite"];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be
         useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         Typical reasons for an error here include:
         * The persistent store is not accessible
         * The schema for the persistent store is incompatible with current managed object model
         Check the error message to determine what the actual problem was.
         */
        
        [self showAlert];
        //abort();
    }
    return _persistentStoreCoordinator;
}
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark autorotation delegate
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight ;
}

-(BOOL)shouldAutorotate{
    return NO;
}
-(void)showIndicator:(UIView*)_view{
    //    if(_view.tag==10){
    //        _errorSubView.hidden=NO;
    //    }
    //    else{
    //        _errorSubView.hidden=YES;
    //    }
    _errorView.frame=CGRectMake(0, 0, 1024, 968);
    [_view addSubview:_errorView];
}
-(void)hideIndicator{
    [_errorView removeFromSuperview];
}


#pragma mark - Drop Box

-(void)calldropBox:(UIButton*)sender
{
    /**** Aman Added code For Drop Box ***/
    /*    popOver=nil;
     popOver = [[UIPopoverController alloc] initWithContentViewController:self.rootController];
     [popOver setPopoverContentSize:CGSizeMake(320, 500)];
     [popOver presentPopoverFromRect:CGRectMake(sender.frame.size.width / 2, sender.frame.origin.y / 1, 1, 1) inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
     */
    
}

#pragma mark - Date Between
- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate
{
//    return [date compare:firstDate] == NSOrderedDescending &&
//    ([date compare:lastDate]  == NSOrderedAscending || [date compare:lastDate]  == NSOrderedSame);
    return ([date compare:firstDate] == NSOrderedDescending &&
            [date compare:lastDate]  == NSOrderedAscending) ||([date compare:firstDate]  == NSOrderedSame ||
                                                               [date compare:lastDate]  == NSOrderedSame);
}


- (BOOL)isDateInBetweenGivenDate:(NSDate *)date firstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate {
    
    return ([date compare:firstDate] == NSOrderedDescending || [date compare:firstDate]  == NSOrderedSame) &&
    ([date compare:lastDate]  == NSOrderedAscending || [date compare:lastDate]  == NSOrderedSame);
}


@end
