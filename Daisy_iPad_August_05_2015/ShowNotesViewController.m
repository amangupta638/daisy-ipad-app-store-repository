//
//  ShowNotesViewController.m
//  StudyPlaner
//
//  Created by Dhirendra on 21/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "ShowNotesViewController.h"
#import "Item.h"
#import "Service.h"
#import "AppHelper.h"
#import "NoteEntry.h"
#import "MyProfile.h"
#import "DairyItemUsers.h"
#import "MainViewController.h"
#import "Attachment.h"

@interface ShowNotesViewController ()

@end

@implementation ShowNotesViewController
//Aman added
@synthesize btnNotesPopoverDelete;
@synthesize _curentItem,_arrForDairyItems;

@synthesize NoteItemId;
@synthesize _arrForStudentID,_arrForStudentNames;
@synthesize strTeacherName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)retriveStudentData
{
    
    for (DairyItemUsers *userObj in [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:NoteItemId]) {
        if (userObj)
        {
            Student *stuObj= [[Service sharedInstance]getStudentDataInfoStoredOnlyForNote:userObj.student_id postNotification:NO];
            
            if ([stuObj.status isEqualToString:@"1"])
            {
#if DEBUG
                NSLog(@"Studentd Name %@",stuObj.firstname);
#endif
                
                [_arrForStudentNames addObject:stuObj.firstname];
            }

#if DEBUG
            NSLog(@"Studentd ID %@",userObj.student_id);
#endif
            
            strTeacherName = [NSString stringWithFormat:@"%@",userObj.approverName];
            [strTeacherName retain];
        }
    }
    
}

- (void) viewWillAppear:(BOOL)animated
{
    
    [self retriveStudentData];
    
#if DEBUG
    NSLog(@"**** %@",NoteItemId);
#endif
    
    //MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    //Aman added
    // Student
    /*if([profobj.type isEqualToString:@"Teacher"])
     {
     
     if ([self._curentItem.assignedtoMe integerValue]== 0)
     {
     // Assigned To Student
     [self lockNote];
     }else
     {
     // Assigned to Self/Teacher
     [self unlockNote];
     }
     
     }
     // Teacher
     else
     {
     if ([self._curentItem.assignedtoMe integerValue]== 0)
     {
     //
     [self lockNote];
     }else{
     [self unlockNote];
     }
     
     }*/
    
    if ([self._curentItem.createdBy isEqualToString:[AppHelper userDefaultsForKey:@"user_id"]]){
        [self unlockNote];
        lblHeadingTitle.text = @"Assigned to Students";
        [self.btn_NoteEditDoAttachment setHidden:NO];
    }else{
        [self lockNote];
        lblHeadingTitle.text = @"Assigned By";
        [self.btn_NoteEditDoAttachment setHidden:YES];
        
    }
    
    
    //            if([profobj.type isEqualToString:@"Student"]&&([self._curentItem.assignedtoMe integerValue]==0)){
    //                [self lockNote];
    //            }else{
    //                [self unlockNote];
    //            }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.edgesForExtendedLayout = UIRectEdgeNone;
    _globlFormeter=[[NSDateFormatter alloc]init];
    //_tableView.tableFooterView=_viewForFoter;
    //noteItem_Id=-1;
    // Do any additional setup after loading the view from its nib.
    
    _viewForStudentLists.hidden = YES;
    btnRecepientsView=[[UIButton alloc] init];
    
#if DEBUG
    NSLog(@"**** %@",NoteItemId);
#endif
    
    _arrForStudentID = [[NSMutableArray alloc]init];
    _arrForStudentNames = [[NSMutableArray alloc]init];
    
}


-(void)getAllNotesDairyItemMethod:(NSNumber*)item_id
{
    Item *diarItem=nil;
    if([AppDelegate getAppdelegate]._isUniqueId ){
        diarItem =[[Service sharedInstance]getItemDataInfoStored:item_id postNotification:NO];
        self._arrForDairyItems=[[Service sharedInstance]getAllNoteEntryDataIwithUniqueId:diarItem.itemId];
    }
    
    else{
        diarItem =[[Service sharedInstance]getItemDataInfoStoredForParseDate:item_id appid:nil];
        self._arrForDairyItems=[[Service sharedInstance]getAllNoteEntryDataI:diarItem.universal_Id];
        
    }
    self._curentItem=diarItem;
    noteItem_Id=[diarItem.universal_Id integerValue];
    
    

    
    
    [_tableView reloadData];
    
}

-(Item *)getItemObjectForNoteAttachment
{
    Item *diarItem=nil;

    diarItem = self._curentItem;
    return diarItem;
}



- (IBAction)btnViewAttachmentClicked:(id)sender
{
    MainViewController *mVC = (MainViewController*)[AppDelegate getAppdelegate]._currentViewController ;
    [mVC btnViewAttachmentClicked:sender];

}

- (IBAction)addAttachment:(id)sender {
    
    MainViewController *mVC = (MainViewController*)[AppDelegate getAppdelegate]._currentViewController ;
    [mVC addAttachment:sender];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
    
}

//20AugChange
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView == _tableViewForStudenList) {
        
        return 25;
    }
    else
    {
        // Return height for rows.
        
        NoteEntry *note=[self._arrForDairyItems objectAtIndex:indexPath.row];
        
        float rowHeight=0;
        rowHeight=[self dynamicCellHeights:note.text];
        //    if (rowHeight>250) {
        //        return 250;
        //    }
        //    else{
        return MAX(rowHeight+20, 54);
        
    }
    // }
    
    //return 54;
}

-(float)dynamicCellHeights:(NSString*)commentText
{
    
    float rowHeight=0;
    CGSize constraint = CGSizeMake(155.0f, 1000.0f);
    CGSize size = [commentText sizeWithFont:[UIFont fontWithName:@"Arial" size:13.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    rowHeight =  size.height;
    return rowHeight;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tableViewForStudenList) {
        
        if (_arrForStudentNames.count > 0)
            return _arrForStudentNames.count;
        else
            return 1;
    }
    else
        return self._arrForDairyItems.count;
    //return 5;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (tableView == _tableViewForStudenList)
        return 0;
    else
        return 31;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (tableView == _tableView) {
        UIView *headerView=[[[UIView alloc] init] autorelease];
        headerView.frame=CGRectMake(0, 0,230, 30);
        headerView.backgroundColor=[UIColor colorWithRed:35/255.0f green:87/255.0f blue:156/255.0f alpha:1];
        headerView.tag = 10101;

        UILabel* lblForItemType=[[UILabel alloc] init];
        lblForItemType.frame=CGRectMake(10, 2,200, 25);
        lblForItemType.font=[UIFont fontWithName:@"Arial-BoldMT" size:15];
        lblForItemType.backgroundColor=[UIColor clearColor];
        lblForItemType.textColor=[UIColor whiteColor];
        [headerView addSubview:lblForItemType];
        [lblForItemType release];
        
        
        
        if (_arrForStudentNames.count > 0) {
            
            lblForItemType.frame=CGRectMake(30, 2,200, 25);
            
            [btnRecepientsView setImage:[UIImage imageNamed:@"Students_expand.png"] forState:UIControlStateNormal];
            
            btnRecepientsView.frame=CGRectMake(5, 5,20, 20);
            btnRecepientsView.backgroundColor=[UIColor clearColor];
            [btnRecepientsView addTarget:self action:@selector(openRecepientsView:) forControlEvents:UIControlEventTouchUpInside];
            [headerView addSubview:btnRecepientsView];
            
        }
        else
        {
            
            if ([self._curentItem.createdBy isEqualToString:[AppHelper userDefaultsForKey:@"user_id"]])
            {
            }
            else{
                lblForItemType.frame=CGRectMake(30, 2,200, 25);
                
                [btnRecepientsView setImage:[UIImage imageNamed:@"Students_expand.png"] forState:UIControlStateNormal];
                
                btnRecepientsView.frame=CGRectMake(5, 5,20, 20);
                btnRecepientsView.backgroundColor=[UIColor clearColor];
                [btnRecepientsView addTarget:self action:@selector(openRecepientsView:) forControlEvents:UIControlEventTouchUpInside];
                
                
                [headerView addSubview:btnRecepientsView];
                
            }
            
        }
        
        lblForItemType.text=self._curentItem.title;
        
        return headerView;
    }
    
    return nil;
    
}


- (void) forcePopoverSize {
    
}
-(void)openRecepientsView:(UIButton *)sender
{
    @try {
        
        if (sender.selected == NO) {
            
            sender.selected = YES;
            [sender setImage:[UIImage imageNamed:@"Students_collapse.png"] forState:UIControlStateNormal];
            
            CGSize currentSetSizeForPopover = self.contentSizeForViewInPopover;
            currentSetSizeForPopover.width = 460;
            currentSetSizeForPopover.height = 324;
            self.contentSizeForViewInPopover = currentSetSizeForPopover;
            _tableView.frame = CGRectMake(230, 0, 230, 211);
            _viewForFoter.frame = CGRectMake(230, 247, 230, 77);
            [_viewForDisplayStudentLists addSubview:sender];

            [self.view addSubview:_viewForDisplayStudentLists];
            _viewForDisplayStudentLists.frame = CGRectMake(0, 0, 230, 324);
            
            
        }
        else
        {
            for (UIView *subview in _tableView.subviews){
                
                if(subview.tag == 10101)
                {
                    [subview addSubview:sender];
                }
            }
            sender.selected  = NO;
            [sender setImage:[UIImage imageNamed:@"Students_expand.png"] forState:UIControlStateNormal];
            
            CGSize currentSetSizeForPopover = self.contentSizeForViewInPopover;
            currentSetSizeForPopover.width = 230;
            currentSetSizeForPopover.height = 324;
            self.contentSizeForViewInPopover = currentSetSizeForPopover;
            _tableView.frame = CGRectMake(0, 0, 230, 211);
            _viewForFoter.frame = CGRectMake(0, 247, 230, 77);
            
            [_viewForDisplayStudentLists removeFromSuperview];
            //   _viewForDisplayStudentLists.frame = CGRectMake(-400, 0, 230, 247);
        }
        
        
    }
    @catch (NSException *exception)
    {
#if DEBUG
        NSLog(@"Error at Note student list view");
#endif
      
    }
    // _viewForStudentLists.hidden = NO;
    
    
    
    /*
     DisplayReceipantsListViewController *viewpopup=[[DisplayReceipantsListViewController alloc]initWithNibName:@"DisplayReceipantsListViewController" bundle:nil];
     
     UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:viewpopup];
     
     [pop setPopoverContentSize:CGSizeMake(viewpopup.view.frame.size.width, viewpopup.view.frame.size.height) animated:YES];
     pop.delegate = self;
     
     [pop  presentPopoverFromRect:CGRectMake(-120 ,163,10,10 )inView:sender.superview
     permittedArrowDirections:nil animated:YES];*/
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //20AugChange
    
    
    
    if (tableView == _tableViewForStudenList) {
        
        NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
        UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (myCell == nil){
            myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
            
            if (_arrForStudentNames.count > 0)
                myCell.textLabel.text = [NSString stringWithFormat:@"%@",[_arrForStudentNames objectAtIndex:indexPath.row]];
            else
            {
                MyTeacher *teacherData=[[Service sharedInstance]getTeacherDataInfoStored:self._curentItem.createdBy postNotification:NO];
                
                
                if ([teacherData.name isEqualToString:@"(null)"] || teacherData.name == NULL || teacherData.name == nil) {
                    myCell.textLabel.text = [NSString stringWithFormat:@"%@",strTeacherName];
                }
                else
                {
                    myCell.textLabel.text = [NSString stringWithFormat:@"%@ %@",teacherData.salutation,teacherData.name];
                }
            }
            myCell.textLabel.font = [UIFont systemFontOfSize:14.0f];
            
        }
        return myCell;
    }
    else
    {
        static NSString *CellIdentifier = @"myIdenfier";
        UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSArray *nib;
        
        if (myCell == nil)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"ShowNotesCustomCell" owner:self options:nil];
            myCell = [nib objectAtIndex:0];
            myCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            myCell.backgroundColor=[UIColor clearColor ];
            myCell.selectionStyle = UITableViewCellSelectionStyleNone;
            myCell.accessoryType=UITableViewCellAccessoryNone;
            //
            //        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(75, 7, 160, 40)];
            //        name.font=[UIFont fontWithName:@"Arial" size:13.0];
            //        name.backgroundColor=[UIColor clearColor];
            //        name.numberOfLines=0;
            //        name.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
            //        name.tag=88;
            //        UILabel *date=[[UILabel alloc]initWithFrame:CGRectMake(5, 7, 70, 40)];
            //        date.font=[UIFont fontWithName:@"Arial" size:12];
            //        date.backgroundColor=[UIColor clearColor];
            //        date.numberOfLines=0;
            //        date.textColor=[UIColor lightGrayColor];
            //        date.tag=90;
            //
            //        UIImageView * cellBgImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 230, 54)];
            //        cellBgImg.backgroundColor=[UIColor clearColor];
            //        cellBgImg.tag=89;
            //
            //        [myCell.contentView addSubview:cellBgImg];
            //        [myCell.contentView addSubview:name];
            //         [myCell.contentView addSubview:date];
            //        [cellBgImg release];
            //        [name release];
            //        [date release];
            //
        }
        
        UIImageView *cellBgImg=(UIImageView*)[myCell.contentView viewWithTag:88];
        UILabel *lblForItemTitle=(UILabel*)[myCell.contentView viewWithTag:89];
        UILabel *lableDate=(UILabel*)[myCell.contentView viewWithTag:90];
        
        
        cellBgImg.image=[UIImage imageNamed:@"noteEnterBgCell.png"];
        
        NoteEntry *note=[self._arrForDairyItems objectAtIndex:indexPath.row];
        [_globlFormeter setDateFormat:kDATEFORMAT];
        NSString *str=[_globlFormeter stringFromDate:note.created];
        // 24Hrs
//        [_globlFormeter setTimeStyle:NSDateFormatterShortStyle];
//        [_globlFormeter setDateFormat:[_globlFormeter dateFormat]];
        [_globlFormeter setDateStyle:NSDateFormatterNoStyle];
        [_globlFormeter setTimeStyle:NSDateFormatterShortStyle];
        //[_globlFormeter setDateFormat:kDateFormatToShowOnUI_hhmma];
        // 24Hrs
        
        NSString *str1=[_globlFormeter stringFromDate:note.created];
        
        lblForItemTitle.text=note.text;
        lableDate.text=[NSString stringWithFormat:@"%@\n%@",str,str1];
        return myCell;
        
    }
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark Button life
- (IBAction)saveButtonAction:(id)sender {
    
    MainViewController *mVC = (MainViewController*)[AppDelegate getAppdelegate]._currentViewController ;

    //service for get Diary Items

    if(!NSSTRING_HAS_DATA (textViewForComment.text))
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Note entry can not be empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    else{
        NSMutableDictionary *dictForNote=[[NSMutableDictionary alloc]init];
        NSMutableDictionary *dictForNote1=[[NSMutableDictionary alloc]init];
        NSMutableArray *arryForNote=[[NSMutableArray alloc]init];
        // NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
        
        NSNumber *appId=[[Service sharedInstance] nextAvailble:@"unique_id" forEntityName:@"NoteEntry"];
        [dictForNote setObject:appId forKey:@"AppId"];
        [dictForNote setObject:appId forKey:@"appId"];
        [dictForNote setObject:[[Service sharedInstance] miliSecondFromDateMethod:[NSDate date]] forKey:@"createddate"];
        [dictForNote setObject:[[Service sharedInstance] miliSecondFromDateMethod:[NSDate date]] forKey:@"lastupdate"];
        
        [dictForNote setObject:textViewForComment.text forKey:@"text"];
        
        if([self._curentItem.itemId integerValue]>0){
            [dictForNote setObject:[NSString stringWithFormat:@"%d",[self._curentItem.itemId integerValue]] forKey:@"noteId"];
            [dictForNote setObject:[NSString stringWithFormat:@"%d",[self._curentItem.universal_Id integerValue]] forKey:@"localnoteid"];
            [dictForNote setObject:@"1" forKey: @"isSync"];

        }
        else{
            [dictForNote setObject:[NSString stringWithFormat:@"%d",[self._curentItem.universal_Id integerValue]] forKey:@"localnoteid"];
            [dictForNote setObject:@"0" forKey: @"isSync"];

            
        }
        [AppHelper saveToUserDefaults:[NSString stringWithFormat:@"%d",[self._curentItem.universal_Id integerValue]] withKey:@"noteid"];
        [dictForNote setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"userid"];
        
        // [dictForNote setObject:[NSNumber numberWithBool:NO] forKey:@"isSync"];
        [dictForNote setObject:APP_Key forKey:@"AppType"];
        [dictForNote setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];
        [dictForNote setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"IpAddress"];

        
        // this is for delete individual exisiting attachemnts
        
        
        
        [arryForNote addObject:dictForNote];
        [dictForNote1 setObject:arryForNote forKey:@"det"];
        
        if ([[DELEGATE marrDeletedAttachments] count] != 0) {
            NSMutableArray *arrForDeleteAttachment=[[NSMutableArray alloc]init];
            for (Attachment *atc in [DELEGATE marrDeletedAttachments]) {
                
                [arrForDeleteAttachment addObject:atc.aID];
            }
            
            NSString *strDeletedIds=[arrForDeleteAttachment componentsJoinedByString:@","];
            [dictForNote1 setObject:strDeletedIds forKey:@"deletedAttachmentId"];
            
            [arrForDeleteAttachment release];
        }

        

        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_insertNoteEntry object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insdertUpdateNoteEntryDidGot:) name:NOTIFICATION_insertNoteEntry object:nil];
        BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
        if (networkReachable)
        {
            
            if([[[DELEGATE glb_mdict_for_Attachement]allKeys] count]>0)
            {
            
            
                if([mVC isAttachmentSizeAcceptable])
                {
                    [[Service sharedInstance]insertUpadteNoteEntry:dictForNote1];
                }
                else
                {
                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[NSString stringWithFormat:@"Only 15 MB Data is acceptable along with only %d or less attachments.",kAttachmentCount] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
                    [myAlertView show];
                    [myAlertView release];
                    return;
                }
            }
            else
            {
                [[Service sharedInstance]insertUpadteNoteEntry:dictForNote1];

            }
            
            
        }
        else{
            //  [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
            if([[[DELEGATE glb_mdict_for_Attachement]allKeys] count]>0)
            {

                if([mVC isAttachmentSizeAcceptable])
                {
                    [dictForNote setObject:@"1" forKey:@"ErrorCode"];

                    [[Service sharedInstance]parseNoteEntryDataWithDictionary:dictForNote];
                }
                else
                {
                    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:[NSString stringWithFormat:@"Only 15 MB Data is acceptable along with only %d or less attachments.",kAttachmentCount] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
                    [myAlertView show];
                    [myAlertView release];
                    return;
                }
            }
            else
            {
                [dictForNote setObject:@"1" forKey:@"ErrorCode"];
                
                [[Service sharedInstance]parseNoteEntryDataWithDictionary:dictForNote];

            }
            
            
        }
        [dictForNote release];
        [arryForNote release];
        [dictForNote1 release];
        
        [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
    }
    
    
    
}



-(void)insdertUpdateNoteEntryDidGot:(NSNotification*)note
{
    NSInteger dID = 0;
    MainViewController *mVC = (MainViewController*)[AppDelegate getAppdelegate]._currentViewController ;

    [[AppDelegate getAppdelegate] hideIndicator];
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_insertNoteEntry object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            for(NSDictionary *dict in [[note userInfo] valueForKey:@"details"]){
                [[Service sharedInstance]parseNoteEntryDataWithDictionary:dict];
            }
            
            // Attachment offline

            for(NSDictionary *dict in [[note userInfo] valueForKey:@"details"]){
                if (![[dict objectForKey:@"deletedAttachmentId"]isKindOfClass:[NSNull class]])
                {
                    NSArray *arrDeletedAttachments = [[dict objectForKey:@"deletedAttachmentId"] componentsSeparatedByString:@","];
                    for (NSString *str in arrDeletedAttachments)
                    {
                        [[Service sharedInstance] deleteAttachmentForId:str];
                    }
                    [[DELEGATE marrDeletedAttachments] removeAllObjects];
                }
            
            }
            
            
            for(NSDictionary *dict in [[note userInfo] valueForKey:@"details"]){
             
                if ([dict objectForKey:@"noteId"])
                {
                    dID = [[dict valueForKey:@"noteId"] integerValue];

                }
                else
                {
                    dID = [[dict valueForKey:@"localnoteid"] integerValue];

                }
                if(dID != 0)
                {
                    
                    Item *objItem = [[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:dID] postNotification:NO];
                    [mVC saveAttachmentLocallyForDiaryItem:dID itemtypeID:[objItem.itemType_id integerValue]];
                }
                else
                {
                    Item *objItem = [[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:dID] postNotification:NO];

                    [mVC saveAttachmentLocallyForDiaryItem:[NSNumber numberWithInteger:dID] itemtypeID:[objItem.itemType_id integerValue]];
                }
            }
                      // Attachment offline
            
           
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
    
    // Remove all objects from global array
    [[DELEGATE glb_mdict_for_Attachement] removeAllObjects];
    [[DELEGATE marrListedAttachments] removeAllObjects];
 //   [DELEGATE setMarrListedAttachments:nil];

    [[DELEGATE marrDeletedAttachments] removeAllObjects];

}


- (IBAction)deleteButtonAction:(id)sender {
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:self._curentItem.type forKey:@"type"];
    int itemId=0;
    if([AppDelegate getAppdelegate]._isUniqueId ){
        itemId=[self._curentItem.itemId integerValue];
    }
    else{
        itemId=[self._curentItem.universal_Id integerValue];
    }
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if( itemId>0){
        [dict setObject:[NSNumber numberWithInteger:itemId] forKey:@"id"];
        
        if (networkReachable){
            [[AppDelegate getAppdelegate] showIndicator:self.view];
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(diaryItemDidDelet:) name:NOTIFICATION_DeleteDairyItem object:nil];
            
            
            [[Service sharedInstance]deleteDairyItems:dict];
            
            
        }
        else{
            //  [dict setObject:diarItem.universal_Id forKey:@"AppId"];
            self._curentItem.isDelete=[NSNumber numberWithBool:YES];
            self._curentItem.isSync=[NSNumber numberWithBool:NO];
            NSError *error=nil;
            [self deleteNoteEntry];
            
            if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
            [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
            
        }
    }
    else{
        NSManagedObject *eventToDelete = self._curentItem;
        if (eventToDelete)
        {
            [[AppDelegate getAppdelegate].managedObjectContext deleteObject:eventToDelete];
        }
        
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
        
        [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
        
    }
    
    
    
    [dict release];
    
    
}
-(void)diaryItemDidDelet:(NSNotification*)note
{
    [[AppDelegate getAppdelegate] hideIndicator];
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_DeleteDairyItem object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            [[Service sharedInstance]deleteObjectOfDairyItem:self._curentItem.itemId];
            [self deleteNoteEntry];
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}


-(void)deleteNoteEntry{
    NSError *error;
    for(NoteEntry *event in self._arrForDairyItems){
        NSManagedObject *eventToDelete = event;
        [[AppDelegate getAppdelegate].managedObjectContext deleteObject:eventToDelete];
        
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Error deleting  - error:%@",error);
#endif
      
        }
    }
}

- (void)dealloc {
    [_globlFormeter release];
    [_arrForDairyItems release];
    [_tableView release];
    [textViewForComment release];
    [_viewForFoter release];
    [self.btnNotesPopoverDelete release];
    [_imgLockIcon release];
    [_btnNotesPopoverSave release];
    [_btnNoteEditViewAttachment release];
    [_btn_NoteEditDoAttachment release];
    [super dealloc];
}
- (void)viewDidUnload {
    [_tableView release];
    _tableView = nil;
    [textViewForComment release];
    textViewForComment = nil;
    [_viewForFoter release];
    _viewForFoter = nil;
    [super viewDidUnload];
}

- (void) lockNote
{
    [textViewForComment setEditable:NO];
    [self.btnNotesPopoverSave setHidden:YES];
    [self.btnNotesPopoverDelete setHidden:YES];
    [self.imgLockIcon setHidden:NO];
}

- (void) unlockNote
{
    [textViewForComment setEditable:YES];
    [self.btnNotesPopoverSave setHidden:NO];
    [self.btnNotesPopoverDelete setHidden:NO];
    [self.imgLockIcon setHidden:YES];
    
}

@end
