//
//  CalendarMonth.m
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright 2010 Savage Media Pty Ltd. All rights reserved.
//

#import "CalendarMonth.h"

#import "CalendarLogic.h"
#import "Service.h"
#import "AppDelegate.h"
#define kCalendarDayWidth	46.0f
#define kCalendarDayHeight	44.0f


@implementation CalendarMonth


#pragma mark -
#pragma mark Getters / setters

@synthesize calendarLogic;
@synthesize datesIndex;
@synthesize buttonsIndex;

@synthesize numberOfDaysInWeek;
@synthesize selectedButton;
@synthesize selectedDate;



#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	self.calendarLogic = nil;
	self.datesIndex = nil;
	self.buttonsIndex = nil;
	self.selectedDate = nil;
	
    [super dealloc];
}



#pragma mark -
#pragma mark Initialization

// Calendar object init
- (id)initWithFrame:(CGRect)frame logic:(CalendarLogic *)aLogic
{
    
    
	// Size is static
	NSInteger numberOfWeeks = 5;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        frame.origin.y = 45;

    }
	frame.size.width = 320;
	frame.size.height = ((numberOfWeeks + 1) * kCalendarDayHeight) + 60;
	selectedButton = -1;
	
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
	NSDate *todayDate = [calendar dateFromComponents:components];
	
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		self.backgroundColor = [UIColor whiteColor]; // Red should show up fails.
		self.opaque = YES;
		self.clipsToBounds = NO;
		self.clearsContextBeforeDrawing = NO;
		
        //		UIImageView *headerBackground = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CalendarBackground.png"]] autorelease];
        
        //Aman added -- iOS7 optimisation --------------------------------------
        UIImageView *headerBackground = [[UIImageView alloc]init];
        [headerBackground setBackgroundColor:[UIColor whiteColor]];
        //Aman added -- iOS7 optimisation --------------------------------------
        
		[headerBackground setFrame:CGRectMake(0, 0, 320, 60)];
		[self addSubview:headerBackground];
		
		UIImageView *calendarBackground = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"CalendarBackground.png"]] autorelease];
		[calendarBackground setFrame:CGRectMake(0, 60, 320, (numberOfWeeks + 1) * kCalendarDayHeight)];
		[self addSubview:calendarBackground];
		
		
		
		NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
		NSArray *daySymbols = [formatter shortWeekdaySymbols];
		self.numberOfDaysInWeek = [daySymbols count];
		
		
		UILabel *aLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 40)] autorelease];
		aLabel.backgroundColor = [UIColor clearColor];
		aLabel.textAlignment = NSTextAlignmentCenter;
		aLabel.font = [UIFont boldSystemFontOfSize:20];
		aLabel.textColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CalendarTitleColor.png"]];
		aLabel.shadowColor = [UIColor whiteColor];
		aLabel.shadowOffset = CGSizeMake(0, 1);
		
		[formatter setDateFormat:@"MMMM yyyy"];
		aLabel.text = [formatter stringFromDate:aLogic.referenceDate];
		[self addSubview:aLabel];
		
		
		UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, 59, 320, 1)] autorelease];
		lineView.backgroundColor = [UIColor lightGrayColor];
		[self addSubview:lineView];
		
		
		// Setup weekday names
		NSInteger firstWeekday = [calendar firstWeekday] - 1;
		for (NSInteger aWeekday = 0; aWeekday < numberOfDaysInWeek; aWeekday ++) {
 			NSInteger symbolIndex = aWeekday + firstWeekday;
			if (symbolIndex >= numberOfDaysInWeek) {
				symbolIndex -= numberOfDaysInWeek;
			}
			
			NSString *symbol = [daySymbols objectAtIndex:symbolIndex];
			CGFloat positionX = (aWeekday * kCalendarDayWidth) - 1;
			CGRect aFrame = CGRectMake(positionX, 40, kCalendarDayWidth, 20);
			
			aLabel = [[[UILabel alloc] initWithFrame:aFrame] autorelease];
			aLabel.backgroundColor = [UIColor clearColor];
			aLabel.textAlignment = NSTextAlignmentCenter;
			aLabel.text = symbol;
			aLabel.textColor = [UIColor darkGrayColor];
			aLabel.font = [UIFont systemFontOfSize:12];
			aLabel.shadowColor = [UIColor whiteColor];
			aLabel.shadowOffset = CGSizeMake(0, 1);
			[self addSubview:aLabel];
		}
		
		// Build calendar buttons (6 weeks of 7 days)
		NSMutableArray *aDatesIndex = [[[NSMutableArray alloc] init] autorelease];
		NSMutableArray *aButtonsIndex = [[[NSMutableArray alloc] init] autorelease];
		
		for (NSInteger aWeek = 0; aWeek <= numberOfWeeks; aWeek ++) {
			CGFloat positionY = (aWeek * kCalendarDayHeight) + 60;
			
			for (NSInteger aWeekday = 1; aWeekday <= numberOfDaysInWeek; aWeekday ++) {
				CGFloat positionX = ((aWeekday - 1) * kCalendarDayWidth) - 1;
				CGRect dayFrame = CGRectMake(positionX, positionY, kCalendarDayWidth, kCalendarDayHeight);
				NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday
														 onWeek:aWeek
												  referenceDate:[aLogic referenceDate]];
                
				NSDateComponents *dayComponents = [calendar
												   components:NSDayCalendarUnit|NSMonthCalendarUnit fromDate:dayDate];
                
                //////
                
                //
                
                ////
				
				UIColor *titleColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CalendarTitleColor.png"]];
				if ([aLogic distanceOfDateFromCurrentMonth:dayDate] != 0) {
					titleColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CalendarTitleDimColor.png"]];
				}
				
				UIButton *dayButton = [UIButton buttonWithType:UIButtonTypeCustom];
				dayButton.opaque = YES;
				dayButton.clipsToBounds = NO;
				dayButton.clearsContextBeforeDrawing = NO;
				dayButton.frame = dayFrame;
				dayButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
				dayButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
				dayButton.tag = [aDatesIndex count];
				dayButton.adjustsImageWhenHighlighted = NO;
				dayButton.adjustsImageWhenDisabled = NO;
				dayButton.showsTouchWhenHighlighted = YES;
				
				
				// Normal
				[dayButton setTitle:[NSString stringWithFormat:@"%ld", (long)[dayComponents day]]
						   forState:UIControlStateNormal];
				
				// Selected
				[dayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
				[dayButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateSelected];
				
				if ([dayDate compare:todayDate] != NSOrderedSame) {
					// Normal
					[dayButton setTitleColor:titleColor forState:UIControlStateNormal];
					[dayButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    //					[dayButton setBackgroundImage:[UIImage imageNamed:@"CalendarDayTile.png"] forState:UIControlStateNormal];
                    
					//Aman added -- iOS7 optimisation --------------------------------------
                    
                    [dayButton setBackgroundColor:[UIColor whiteColor]];
                    [[dayButton layer] setBorderColor:[UIColor lightGrayColor].CGColor];
                    [[dayButton layer] setBorderWidth:0.5f];
					
                    //Aman added -- iOS7 optimisation --------------------------------------
                    
                    
                    
                    
					// Selected
					[dayButton setBackgroundImage:[UIImage imageNamed:@"CalendarDaySelected.png"] forState:UIControlStateSelected];
                    
                    
					
				} else {
					// Normal
					[dayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
					[dayButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
					[dayButton setBackgroundImage:[UIImage imageNamed:@"CalendarDayToday.png"] forState:UIControlStateNormal];
					
					// Selected
					[dayButton setBackgroundImage:[UIImage imageNamed:@"CalendarDayTodaySelected.png"] forState:UIControlStateSelected];
				}
                
                [dayButton addTarget:self action:@selector(dayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
				[self addSubview:dayButton];
				
				// Save
				[aDatesIndex addObject:dayDate];
				[aButtonsIndex addObject:dayButton];
                
                //Aman added -- Semester changed
               
                // Campus Based Start
                NSArray *aarrrr = [DELEGATE glbarrForCalenderCycleDay];
#if DEBUG
                NSLog(@"%@",[DELEGATE glbarrForCalenderCycleDay]);
#endif
                
                for(int tt =0 ;tt<[aarrrr count];tt++)
                {
                    UIColor *bgColorForClass = nil;
                    CalendarCycleDay *ccc = [[aarrrr objectAtIndex:tt]valueForKey:@"CycleDates"];
                    MyClass *casss = [[aarrrr objectAtIndex:tt]valueForKey:@"ClassSelected"];
                    
                    if ([dayDate compare:ccc.dateRef] == NSOrderedSame) {
                        
                        [dayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [dayButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
                        if ([casss.code isEqualToString:@"#FFFFFF"])
                        {
                            [dayButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                            [dayButton setBackgroundColor:[UIColor whiteColor]];
                            
                        }
                        else
                        {
                            bgColorForClass = [self customizeColorButton:casss.code];
                            
                            if ([casss.code isEqualToString:@""])
                            {
                                bgColorForClass = [self customizeColorButton:@"#FFFFFF"];
                                
                            }
                            dayButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
                            [dayButton setBackgroundColor:bgColorForClass];
                        }
                        
                    }
                    
                }
                // Campus Based End
            }
		}
        
		// save
		self.calendarLogic = aLogic;
		self.datesIndex = [[aDatesIndex copy] autorelease];
		self.buttonsIndex = [[aButtonsIndex copy] autorelease];
        
        
    }
    return self;
}



-(NSString*)methodForGetiingShortlable:(int)value :(int) timetableID
{
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableCycleData];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"timeTableCycle_id" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    if(value>0)
    {
        for (TimeTableCycle *tabCycle in arr)
        {
            if (value == [tabCycle.cycle_day integerValue])  //Devashree changed
            {
                return tabCycle.short_lable;

                break;
            }
        }
    }
    return @" ";
}

// Campus Based Start
-(NSString*)methodForGettingCalendarCycleLable:(NSDate *)date
{
    
    NSArray *arr=[[Service sharedInstance]getStoredCalendarCycleDayForGivenDate:date];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dateRef" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    NSString *strCycleDayLable = @" ";
    
    for(CalendarCycleDay *cycle in arr)
    {
        strCycleDayLable = cycle.cycleDayLabel;
#if DEBUG
        NSLog(@"cycleDayLabel %@",strCycleDayLable);
#endif
        
    }
    
    return strCycleDayLable;
    
}
// Campus Based End


-(BOOL)getCurentDayIsWeekend:(NSDate*)fromdate{
    
    NSDateFormatter *_dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [_dateFormat setDateFormat:@"d,E"];
    
    
    NSDateFormatter *_globlDateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [_globlDateFormat setDateFormat:@"E"];
    
    if([[_globlDateFormat stringFromDate:fromdate] isEqualToString:@"Sun"] || [[_globlDateFormat stringFromDate:fromdate] isEqualToString:@"Sat"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


#pragma mark -
#pragma mark UI Controls

- (void)dayButtonPressed:(id)sender {
	[calendarLogic setReferenceDate:[datesIndex objectAtIndex:[sender tag]]];
}
- (void)selectButtonForDate:(NSDate *)aDate {
	if (selectedButton >= 0) {
		NSDate *todayDate = [CalendarLogic dateForToday];
		UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
		
		CGRect selectedFrame = button.frame;
		if ([selectedDate compare:todayDate] != NSOrderedSame) {
			selectedFrame.origin.y = selectedFrame.origin.y + 1;
			selectedFrame.size.width = kCalendarDayWidth;
			selectedFrame.size.height = kCalendarDayHeight;
		}
		
		button.selected = NO;
		button.frame = selectedFrame;
		
		self.selectedButton = -1;
		self.selectedDate = nil;
	}
	
	if (aDate != nil) {
		// Save
		self.selectedButton = [calendarLogic indexOfCalendarDate:aDate];
		self.selectedDate = aDate;
		
		NSDate *todayDate = [CalendarLogic dateForToday];
		UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
		
		CGRect selectedFrame = button.frame;
		if ([aDate compare:todayDate] != NSOrderedSame) {
			selectedFrame.origin.y = selectedFrame.origin.y - 1;
			selectedFrame.size.width = kCalendarDayWidth + 1;
			selectedFrame.size.height = kCalendarDayHeight + 1;
		}
		
		button.selected = YES;
		button.frame = selectedFrame;
		[self bringSubviewToFront:button];
	}
}

-(UIColor *)customizeColorButton: (NSString*)hexColor
{
    UIColor *rgbColor = [AppHelper colorFromHexString:hexColor];
    return rgbColor;
}


-(TimeTable *)getSemester :(NSDate *)dayDate
{
    
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    TimeTable *_currentTimeTable=nil;
    
    int index =1;
    NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
    dayComponent.day = 1;

    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dayComponent1 = [[[NSDateComponents alloc] init] autorelease];
    dayComponent1.day = -1;
    
    NSCalendar *theCalendar1 = [NSCalendar currentCalendar];
    
    

    for(TimeTable *time in arr){
       
        NSDate *startdate = [theCalendar1 dateByAddingComponents:dayComponent1 toDate:time.startDate options:0];
        NSDate *enddate = [theCalendar dateByAddingComponents:dayComponent toDate:time.end_date options:0];

        if([DELEGATE isDate:dayDate inRangeFirstDate:startdate  lastDate:enddate])
        {
            _currentTimeTable=time;
        }
        
        index++;
    }
    if(_currentTimeTable == nil)
    {
        _currentTimeTable=[arr firstObject];
    
    }
    return _currentTimeTable;
}





-(void)getCurrentTimeTable:(NSDate*)dayDate
{
    _curentTimeTable = nil;
    BOOL _isFirstSem=NO;
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    TimeTable *_currentTimeTable=nil;
    
    int index =1;
    
    for(TimeTable *time in arr){
        
        if([DELEGATE isDate:dayDate inRangeFirstDate:time.startDate  lastDate:time.end_date])
            
        {
            _isFirstSem=YES;
            _currentTimeTable=time;
            
            _curentTimeTable=time; //Devashree added
        }
        
        index++;
    }
    if(_currentTimeTable == nil)
    {
        _currentTimeTable=[arr firstObject];
        
        _curentTimeTable=[arr firstObject]; //Devashree added
    }
}


#pragma mark CalendarLogic delegatet
-(BOOL)getNewDateOfamonth:(NSDate *)dayDate
{
    
    NSCalendar* calendar = [NSCalendar currentCalendar] ;

    NSDateComponents *components1 = [calendar components:0 fromDate:dayDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSArray *daySymbols = [formatter weekdaySymbols];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    [components1 setYear:[components1 year]];
    [components1 setMonth:[components1 month]];
    [components1 setDay:[components1 day]];
    
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:dayDate];
    
    NSString *str=nil;

    [self getCurrentTimeTable:dayDate];
    
    if(_curentTimeTable){
        
        TableForCycle *dayCycle=[[Service sharedInstance]getlastcycleandDayTableDataInfoStored:[calendar dateFromComponents:components]];
        NSInteger cycleday=[dayCycle.cycleDay integerValue];
        NSInteger cycleLenght=[dayCycle.cycle integerValue];
        
        NSString *dayStr=[NSString stringWithFormat:@"%@ %d,%@",_curentTimeTable.cycleLable,cycleLenght,[self getShortLabel:cycleday]];
        
        if([self getCurentDayIsWeekend:[calendar dateFromComponents:components]]){
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
        }
        else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[calendar dateFromComponents:components]]){
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
        }
        else{
            str=[NSString stringWithFormat:@"%@ \n %@ \n %@",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]],dayStr];
        }
        
    }
    else{
        str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
    }
    
    NSArray *arrItem=[str componentsSeparatedByString:@"\n"];
    
    NSString *str3=[arrItem objectAtIndex:2];
    
    calendar = nil;
    
    if ([str3 length]>0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
    
}


-(NSString*)getShortLabel:(int)value{
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableCycleData];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"timeTableCycle_id" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];

    if(value>0){
        TimeTableCycle *tabCycle=[arr objectAtIndex:value-1];
        return tabCycle.short_lable;
    }
    else{
        return @" ";
    }
    
}


-(BOOL)isPeriodThereOnTimeTable :(NSNumber*) timeTableId :(NSString*) cycleDay :(NSString*)periodName
{
    NSArray *arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:timeTableId cycle:cycleDay];
        if(!arrOfp.count>0){
        arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:timeTableId cycle:@"0"];
    }

    for (int i=0; i<[arrOfp count]; i++)
    {
        Period *perObj = [arrOfp objectAtIndex:i];

        if ([perObj.title isEqualToString:periodName])
        {
             MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:cycleDay timeTable:perObj.timeTable_id] ;
            if (class1)
            {
                return YES;
            }
        }
    }
    return NO;
}


//-------------------   Class is on cycleDay 0   -------------------//

-(BOOL)isPeriodThereOnTimeTableForCycleDay0 :(NSNumber*) timeTableId :(NSString*) cycleDay :(NSString*)periodName
{
    NSArray *arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:timeTableId cycle:cycleDay];
    if(!arrOfp.count>0){
        arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:timeTableId cycle:@"0"];
    }
    
    for (int i=0; i<[arrOfp count]; i++)
    {
        Period *perObj = [arrOfp objectAtIndex:i];

        if ([perObj.title isEqualToString:periodName])
        {
            MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:@"0" timeTable:perObj.timeTable_id] ;
            if (class1)
            {
                return YES;
            }
        }
    }
    return NO;
}

//-------------------   Class is on cycleDay 0   -------------------//

@end
