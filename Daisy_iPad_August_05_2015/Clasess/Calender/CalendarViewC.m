//
//  CalendarViewController.m
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright Savage Media Pty Ltd 2010. All rights reserved.
//

#import "CalendarViewC.h"

#import "CalendarLogic.h"
#import "CalendarMonth.h"
#import "MainViewController.h"
#import "School_detail.h"
@implementation CalendarViewC


#pragma mark -
#pragma mark Getters / setters

@synthesize calendarViewControllerDelegate;

@synthesize calendarLogic;
@synthesize calendarView;
@synthesize calendarViewNew;
@synthesize selectedDate;

////added for new calendar--aman
@synthesize _curentTimeTable;

- (void)setSelectedDate:(NSDate *)aDate {
	[selectedDate autorelease];
	selectedDate = [aDate retain];
	
	[calendarLogic setReferenceDate:aDate];
	[calendarView selectButtonForDate:aDate];
}

@synthesize leftButton;
@synthesize rightButton;



#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	self.calendarViewControllerDelegate = nil;
	
	self.calendarLogic.calendarLogicDelegate = nil;
	self.calendarLogic = nil;
	
	self.calendarView = nil;
	self.calendarViewNew = nil;
	
	self.selectedDate = nil;
	
	self.leftButton = nil;
	self.rightButton = nil;
	self._curentTimeTable = nil;
    
    [super dealloc];
}



#pragma mark -
#pragma mark Controller initialisation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		// Init
    }
    return self;
}



#pragma mark -
#pragma mark View delegate

- (void)viewDidLoad {
    [super viewDidLoad];	
	
	self.title = NSLocalizedString(@"Calendar", @"");
	self.view.bounds = CGRectMake(0, 0, 320, 324);
	self.view.clearsContextBeforeDrawing = NO;
	self.view.opaque = YES;
	self.view.clipsToBounds = NO;
	
	NSDate *aDate = selectedDate;
	if (aDate == nil) {
		aDate = [CalendarLogic dateForToday];
	}
	
	CalendarLogic *aCalendarLogic = [[CalendarLogic alloc] initWithDelegate:self referenceDate:aDate];
	self.calendarLogic = aCalendarLogic;
	[aCalendarLogic release];
	// Aman Commented//
	/*UIBarButtonItem *aClearButton = [[UIBarButtonItem alloc]
									 initWithTitle:NSLocalizedString(@"Clear", @"") style:UIBarButtonItemStylePlain
									 target:self action:@selector(actionClearDate:)];
	self.navigationItem.rightBarButtonItem = aClearButton;
	[aClearButton release];*/
    // Aman Commented//
    
    int yPosition = 0;
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        yPosition = 45;
    }
    
	
	CalendarMonth *aCalendarView = [[CalendarMonth alloc] initWithFrame:CGRectMake(0, yPosition, 320, 324) logic:calendarLogic];
	[aCalendarView selectButtonForDate:selectedDate];
	[self.view addSubview:aCalendarView];
	
	self.calendarView = aCalendarView;
	[aCalendarView release];
	
	
	
	UIButton *aLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
	aLeftButton.frame = CGRectMake(0, yPosition, 60, 60);
	aLeftButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 20);
	[aLeftButton setImage:[UIImage imageNamed:@"CalendarArrowLeft.png"] forState:UIControlStateNormal];
	[aLeftButton addTarget:calendarLogic 
					action:@selector(selectPreviousMonth) 
		  forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:aLeftButton];
	self.leftButton = aLeftButton;
	
	UIButton *aRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
	aRightButton.frame = CGRectMake(260, yPosition, 60, 60);
	aRightButton.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 20, 0);
	[aRightButton setImage:[UIImage imageNamed:@"CalendarArrowRight.png"] forState:UIControlStateNormal];
	[aRightButton addTarget:calendarLogic 
					 action:@selector(selectNextMonth) 
		   forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:aRightButton];
	self.rightButton = aRightButton;
}
- (void)viewDidUnload {
	self.calendarLogic.calendarLogicDelegate = nil;
	self.calendarLogic = nil;
	
	self.calendarView = nil;
	self.calendarViewNew = nil;
	
	self.selectedDate = nil;
	
	self.leftButton = nil;
	self.rightButton = nil;
}

- (CGSize)contentSizeForViewInPopoverView {
	return CGSizeMake(320, 324);
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad || interfaceOrientation == UIInterfaceOrientationPortrait;
}



#pragma mark -
#pragma mark UI events
/* Aman Commented
- (void)actionClearDate:(id)sender {
    
	self.selectedDate = nil;
	[calendarView selectButtonForDate:nil];
	// Delegate called later.
	//[calendarViewControllerDelegate calendarViewController:self dateDidChange:nil];
}
 */



#pragma mark -
#pragma mark CalendarLogic delegate

-(NSString*)getCurrentYearForMonth:(int)month
{
    NSString *str = nil;
    return str;
}

- (void)calendarLogic:(CalendarLogic *)aLogic dateSelected:(NSDate *)aDate {
	[selectedDate autorelease];
	selectedDate = [aDate retain];
    
    NSDateFormatter *_globlDateFormat = [[NSDateFormatter alloc] init];
    [_globlDateFormat setDateFormat:@"dd/MM/yyyy"];
    
    
    
//    self.strForDayDate=[_globlDateFormat stringFromDate:[NSDate date ]];
    self.strForDayDate=[_globlDateFormat stringFromDate:aDate];
    MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
    mainVC._lblForDueDateAddVw.text = [_globlDateFormat stringFromDate:aDate];
    // For Prime Only
    if (![[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
    {
        NSDateFormatter *DF = [[NSDateFormatter alloc] init];
        [DF setDateFormat:kDATEFORMAT_Slash];
        mainVC._lblForDueDateAddVw.text = [DF stringFromDate:aDate];
        [DF release];
        DF = nil;

    }
    // Added by Mitul = Start 12/24 issue - 2322
    //[_globlDateFormat setDateFormat:kDateFormatToShowOnUI_hhmma];
    [_globlDateFormat setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    [_globlDateFormat setDateStyle:NSDateFormatterNoStyle];
    [_globlDateFormat setTimeStyle:NSDateFormatterShortStyle];
    // Added by Mitul = End 12/24 issue - 2322
    
    mainVC._lablForDueTimeAddVw.text = [_globlDateFormat stringFromDate:[NSDate date]];

    [self getCurrentTimeTable: selectedDate]; //Devashree changed --  replaced 'getCureentTimeTable' method
    [self createDataForDayView:aDate];
    

	
	if ([calendarLogic distanceOfDateFromCurrentMonth:selectedDate] == 0) {
		[calendarView selectButtonForDate:selectedDate];
	}
	
	[calendarViewControllerDelegate calendarViewController:self dateDidChange:aDate];
}
- (void)calendarLogic:(CalendarLogic *)aLogic monthChangeDirection:(NSInteger)aDirection {
	BOOL animate = self.isViewLoaded;
	
	CGFloat distance = 320;
	if (aDirection < 0) {
		distance = -distance;
	}
	
	leftButton.userInteractionEnabled = NO;
	rightButton.userInteractionEnabled = NO;
	
	CalendarMonth *aCalendarView = [[CalendarMonth alloc] initWithFrame:CGRectMake(distance, 0, 320, 308) logic:aLogic];
	aCalendarView.userInteractionEnabled = NO;
	if ([calendarLogic distanceOfDateFromCurrentMonth:selectedDate] == 0) {
		[aCalendarView selectButtonForDate:selectedDate];
	}
	[self.view insertSubview:aCalendarView belowSubview:calendarView];
	
	self.calendarViewNew = aCalendarView;
	[aCalendarView release];
	
	if (animate) {
		[UIView beginAnimations:NULL context:nil];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(animationMonthSlideComplete)];
		[UIView setAnimationDuration:0.3];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	}
	
	calendarView.frame = CGRectOffset(calendarView.frame, -distance, 0);
	aCalendarView.frame = CGRectOffset(aCalendarView.frame, -distance, 0);
	
	if (animate) {
		[UIView commitAnimations];
		
	} else {
		[self animationMonthSlideComplete];
	}
}

- (void)animationMonthSlideComplete {
	// Get rid of the old one.
	[calendarView removeFromSuperview];
	
	// replace
	self.calendarView = calendarViewNew;
	self.calendarViewNew = nil;

	leftButton.userInteractionEnabled = YES;
	rightButton.userInteractionEnabled = YES;
	calendarView.userInteractionEnabled = YES;
}

#pragma mark - 
#pragma mark - New methods added for selected date

-(void)createDataForDayView: (NSDate*)parameterDate
{
    
    BOOL _isCycleday ;
    NSInteger cycleday ;
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
#if DEBUG
    NSLog(@"createDataForDayView  %@",parameterDate);
    NSLog(@"createDataForDayView  %@",[formatter stringFromDate:parameterDate]);
#endif

    
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    // Campus Based End

    NSMutableArray *_arr = nil;
    _arr = [[NSMutableArray alloc]init];
    
    NSDateFormatter *dtFormatForPeriods = [[NSDateFormatter alloc] init];
    [dtFormatForPeriods setTimeStyle:NSDateFormatterShortStyle];
    
#if DEBUG
    NSLog(@"dtFormatForPeriods format%@",[dtFormatForPeriods dateFormat]);
#endif
    
    [dtFormatForPeriods setDateFormat:[dtFormatForPeriods dateFormat]];

#if DEBUG
    NSLog(@"[dtFormatForPeriods dateFormat] : %@",[dtFormatForPeriods dateFormat]);
#endif
    
    if ([[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS8] || [[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS7])
    {
        
        for(int i=0;i<9;i++){
            NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
            if(i==0){
                [dict setObject:@"12:00 AM" forKey:@"start"];
                [dict setObject:@"08:00 AM" forKey:@"end"];
                
                [dict setObject:@"Before School" forKey:@"Period"];
            }
            else  if(i==1){
                [dict setObject:@"08:00 AM" forKey:@"start"];
                [dict setObject:@"10:00 AM" forKey:@"end"];
                
                [dict setObject:@" Home Group" forKey:@"Period"];
                
            }
            else  if(i==2){
                [dict setObject:@"10:00 AM" forKey:@"start"];
                [dict setObject:@"12:00 PM" forKey:@"end"];
                [dict setObject:@"Period 1" forKey:@"Period"];
                
            }
            else  if(i==3){
                [dict setObject:@"12:00 PM" forKey:@"start"];
                [dict setObject:@"02:00 PM" forKey:@"end"];
                
                [dict setObject:@"Period 2" forKey:@"Period"];
                
            }
            else  if(i==4){
                [dict setObject:@"02:00 PM" forKey:@"start"];
                [dict setObject:@"04:00 PM" forKey:@"end"];
                
                [dict setObject:@"Recess" forKey:@"Period"];
                
            }
            else  if(i==5){
                [dict setObject:@"04:00 PM" forKey:@"start"];
                [dict setObject:@"06:00 PM" forKey:@"end"];
                
                [dict setObject:@"Period 3" forKey:@"Period"];
                
            }
            else  if(i==6){
                [dict setObject:@"06:00 PM" forKey:@"start"];
                [dict setObject:@"08:00 PM" forKey:@"end"];
                
                [dict setObject:@"Period 4" forKey:@"Period"];
                
            }
            else  if(i==7){
                [dict setObject:@"08:00 PM" forKey:@"start"];
                [dict setObject:@"10:00 PM" forKey:@"end"];
                [dict setObject:@"Lunch" forKey:@"Period"];
                
            }
            else  if(i==8){
                [dict setObject:@"10:00 PM" forKey:@"start"];
                [dict setObject:@"11:59 PM" forKey:@"end"];
                [dict setObject:@"Period 5" forKey:@"Period"];
                
            }
            [_arr addObject:dict];
            [dict release];
        }
    }
    else
    {
        for(int i=0;i<9;i++)
        {
            NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
            if(i==0){
                
                [dict setObject:@"00:00" forKey:@"start"];
                [dict setObject:@"08:00" forKey:@"end"];
                
                [dict setObject:@"Before School" forKey:@"Period"];
            }
            else  if(i==1){
                
                [dict setObject:@"08:00" forKey:@"start"];
                [dict setObject:@"10:00" forKey:@"end"];
                [dict setObject:@" Home Group" forKey:@"Period"];
                
            }
            else  if(i==2){
                [dict setObject:@"10:00" forKey:@"start"];
                [dict setObject:@"12:00" forKey:@"end"];
                [dict setObject:@"Period 1" forKey:@"Period"];
                
            }
            else  if(i==3){
                [dict setObject:@"12:00" forKey:@"start"];
                [dict setObject:@"14:00" forKey:@"end"];
                
                [dict setObject:@"Period 2" forKey:@"Period"];
                
            }
            else  if(i==4){
                [dict setObject:@"14:00" forKey:@"start"];
                [dict setObject:@"16:00" forKey:@"end"];
                
                [dict setObject:@"Recess" forKey:@"Period"];
                
            }
            else  if(i==5){
                [dict setObject:@"16:00" forKey:@"start"];
                [dict setObject:@"18:00" forKey:@"end"];
                
                [dict setObject:@"Period 3" forKey:@"Period"];
                
            }
            else  if(i==6){
                [dict setObject:@"18:00" forKey:@"start"];
                [dict setObject:@"20:00" forKey:@"end"];
                
                [dict setObject:@"Period 4" forKey:@"Period"];
                
            }
            else  if(i==7){
                [dict setObject:@"20:00" forKey:@"start"];
                [dict setObject:@"22:00" forKey:@"end"];
                [dict setObject:@"Lunch" forKey:@"Period"];
                
            }
            else  if(i==8){
                [dict setObject:@"22:00" forKey:@"start"];
                [dict setObject:@"23:59" forKey:@"end"];
                [dict setObject:@"Period 5" forKey:@"Period"];
                
            }
            [_arr addObject:dict];
            
            [dict release];
        }
    }
    
    // Campus Based Start
    NSMutableArray *m_Arr = [[NSMutableArray alloc] init];

    if([schoolDetailObj.noOfCampus integerValue]>1)
    {
        for(NSMutableDictionary *dictNr in _arr)
        {
            [dictNr setObject:@"" forKey:@"Period"];
            [m_Arr addObject:dictNr];
        }
        [_arr removeAllObjects];
        [_arr setArray:(NSMutableArray*)m_Arr];
    }
    [m_Arr release];
    m_Arr = nil;
    // Campus Based End
    
     TimeTable *timetableObj=nil ;
    NSArray *arrOfp=nil;
    // Aman Commented
    if(self._curentTimeTable)
    {
        if([self getCurentDayIsWeekend:[formatter dateFromString:self.strForDayDate]])
        {
            _isCycleday=NO;
        }
        else{
            
            // Campus Based Start

            _isCycleday=YES;
            
            CalendarCycleDay *dayCycle2 = nil;
             cycleday=100000;
            NSArray *arrcalenderCycleDay=[[Service sharedInstance]getStoredCalendarCycleDayForGivenDate:[formatter dateFromString:self.strForDayDate]];
            for(CalendarCycleDay *dayCycle in arrcalenderCycleDay)
            {
                dayCycle2 = dayCycle;
                if([dayCycle.cycleDayLabel isEqualToString:@""])
                {
                    _isCycleday=NO;
                }
            }
#if DEBUG
            NSLog(@"dayCycle2 %@",dayCycle2);
#endif
            
            if(dayCycle2!=nil)
            {
                cycleday=[dayCycle2.cycleDay integerValue];
            }
            
            
            NSArray *arr1=[[Service sharedInstance]getStoredAllTimeTableData];
            NSMutableArray *arrofTimeTabelID = [[[NSMutableArray alloc] init] autorelease];
            for(TimeTable *timetable in arr1)
            {
                if([DELEGATE isDate:[formatter dateFromString:self.strForDayDate] inRangeFirstDate:timetable.startDate  lastDate:timetable. end_date])
                    
                {
                    [arrofTimeTabelID addObject:timetable];
                    
                }
                
            }
            NSArray *arrOfAllClasses = [[Service sharedInstance]getStoredAllClassData];
           
            for(TimeTable *timetable in arrofTimeTabelID)
            {
                for(MyClass *classes in arrOfAllClasses)
                {
                    if(([timetable.timeTable_id integerValue]== [classes.timeTable_id integerValue]) && [classes.cycle_day integerValue] == cycleday)
                    {
                        timetableObj = timetable;
                    }
                }
            }
            
            if(timetableObj!=nil)
            {
                arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:timetableObj.timeTable_id cycle:[NSString stringWithFormat:@"%d",cycleday]];
            }
            
            if(!arrOfp.count>0){
                arrOfp =[[Service sharedInstance]getStoredAllPeriodTableData:timetableObj.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];
            }
            
            // Campus Based End

            
            
        }
        
    }
    else{
        _isCycleday=NO;
        
    }
    
    
    if(arrOfp.count>0){
        NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
        [_arr removeAllObjects];
        // 24Hrs
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateFormat:[formatter dateFormat]];
        
        //done  //23/09/2013
        for(Period *perObj in arrOfp)
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            [dict setObject:[[formatter stringFromDate:perObj.start_time] uppercaseString] forKey:@"start"];
            [dict setObject:[[formatter stringFromDate:perObj.end_time] uppercaseString] forKey:@"end"];
            [dict setObject:perObj.title forKey:@"Period"];
            [dict setObject:[NSString stringWithFormat:@"%d",[perObj.period_id integerValue]] forKey:@"Period_id"];
            [dict setObject:[NSString stringWithFormat:@"%d",[perObj.timeTable_id integerValue]] forKey:@"timeTable"];
            
            
            [_arr addObject:dict];
            [dict release];
            
        }
        Period *firstPeriod=[arrOfp objectAtIndex:0];
        Period *lastObj=[arrOfp objectAtIndex:arrOfp.count-1];
        
        NSArray *arr=[[NSArray alloc]initWithObjects:firstPeriod, nil];
        
        NSString *str_12Or24Before = nil;
        NSString *str_12Or24After = nil;
        if ([[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS8] || [[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS7] )
        {
            str_12Or24Before = @"12:00 AM";
            str_12Or24After = @"11:59 PM" ;
        }
        
        else
        {
            str_12Or24Before = @"00:00";
            str_12Or24After = @"23:59" ;
            
        }
        
        NSPredicate *predicate=  [NSPredicate predicateWithFormat:@"start_time > %@ ",[formatter dateFromString:str_12Or24Before]];
        NSArray *tempArray = [arr  filteredArrayUsingPredicate:predicate];
        if(tempArray.count>0){
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setObject:str_12Or24Before forKey:@"start"];
            [dict1 setObject:[[formatter stringFromDate:firstPeriod.start_time] uppercaseString] forKey:@"end"];
            [dict1 setObject:@"Before School" forKey:@"Period"];
            [_arr insertObject:dict1 atIndex:0];
            [dict1 release];
            NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
            [dict2 setObject:str_12Or24After forKey:@"end"];
            [dict2 setObject:[[formatter stringFromDate:lastObj.end_time] uppercaseString] forKey:@"start"];
            [dict2 setObject:@"After School" forKey:@"Period"];
            [_arr addObject:dict2];
            [dict2 release];
            
        }
        
    }
#if DEBUG
    NSLog(@"_arr: %@",_arr);
    NSLog(@"globalArr: %@",[DELEGATE glb_mArrClassCycleDays]);
#endif

    int kBreakLoop = 0;
    
    for (int i=0; i<[[DELEGATE glb_mArrClassCycleDays] count]; i++)
    {
        //-------------------   Class is on cycleDay 0   -------------------//
        
        if ([[[[DELEGATE glb_mArrClassCycleDays] objectAtIndex:i] valueForKey:@"CycleDayForPeriod"] isEqualToString:@"0"])
        {
            for (NSMutableDictionary *dictTemp in _arr)
            {
                
                if ([[dictTemp valueForKey:@"Period"] isEqualToString:[[[DELEGATE glb_mArrClassCycleDays] objectAtIndex:i]valueForKey:@"forCalendarVCClass"]] )
                {
                    
                    BOOL isHoliday = [self getCurentDayIsWeekend:parameterDate];
                    if (!isHoliday)
                    {
                        if(timetableObj!=nil)
                        {
                            if ([timetableObj.timeTable_id integerValue] == [[[[DELEGATE glb_mArrClassCycleDays] objectAtIndex:i] valueForKey:@"timeTableId"]integerValue])
                            {
                                MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
                                
#if DEBUG
                                NSLog(@"dictTemp valueForKey:start : %@", [dictTemp valueForKey:@"start"]);
#endif
                                
                                mainVC._lablForDueTimeAddVw.text = [dictTemp valueForKey:@"start"];
#if DEBUG
                                NSLog(@"calendarLogic : mainVC._lablForDueTimeAddVw.text : %@",mainVC._lablForDueTimeAddVw.text);
#endif
                                kBreakLoop=1;
                            }
                        }
                    }
                }
            }
        }
        
        //-------------------   Class is on cycleDay 0   -------------------//
        
        
        if ([[NSString stringWithFormat:@"%@",[[[DELEGATE glb_mArrClassCycleDays] objectAtIndex:i] valueForKey:@"CycleDayForPeriod"] ] isEqualToString:[NSString stringWithFormat:@"%d",cycleday]])
        {
            for (NSMutableDictionary *dictTemp in _arr)
            {
                if ([[dictTemp valueForKey:@"Period"] isEqualToString:[[[DELEGATE glb_mArrClassCycleDays] objectAtIndex:i]valueForKey:@"forCalendarVCClass"]] )
                {
                    BOOL isHoliday = [self getCurentDayIsWeekend:parameterDate];

                    if (!isHoliday)
                    {
                        if(timetableObj!=nil)
                        {
                            if ([timetableObj.timeTable_id integerValue] == [[[[DELEGATE glb_mArrClassCycleDays] objectAtIndex:i] valueForKey:@"timeTableId"]integerValue])
                            {
                                MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
                            
#if DEBUG
                                NSLog(@"dictTemp valueForKey:start : %@", [dictTemp valueForKey:@"start"]);
#endif
                            
                            
                                mainVC._lablForDueTimeAddVw.text = [dictTemp valueForKey:@"start"];
#if DEBUG
                                NSLog(@"calendarLogic : mainVC._lablForDueTimeAddVw.text : %@",mainVC._lablForDueTimeAddVw.text);
#endif
                                kBreakLoop=1;
                            }
                        }
                    }
                }
            }
        }
        
        if(kBreakLoop ==1)
        {
            break;
        }
    }
    [formatter release];
}


-(BOOL)getCurentDayIsWeekend:(NSDate*)fromdate{
    
    NSDateFormatter *_dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [_dateFormat setDateFormat:@"d,E"];
    
    
    NSDateFormatter *_globlDateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [_globlDateFormat setDateFormat:@"E"];
    
    if([[_globlDateFormat stringFromDate:fromdate] isEqualToString:@"Sun"] || [[_globlDateFormat stringFromDate:fromdate] isEqualToString:@"Sat"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void)getCureentTimeTable:(NSDate*)fromDate
{
    
    NSDateFormatter *_globlDateFormat = [[[NSDateFormatter alloc]init] autorelease];
    [_globlDateFormat setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    NSMutableArray *arrFortime=[[NSMutableArray alloc]init];
    for(TimeTable *time in arr){
        int timeDiff=(int)[fromDate timeIntervalSinceDate:time.startDate];
        
        if(timeDiff>=0)
        {
            [arrFortime addObject:time];
        }
    }
    if(arrFortime.count>0)
    {
        if(arrFortime.count>1)
        {
            _isFirstSem=NO;
            self._curentTimeTable=[arr objectAtIndex:1];
        }
        else
        {
            self._curentTimeTable=[arr firstObject];
            _isFirstSem=YES;
        }
    }
    else
    {
        self._curentTimeTable=nil;
    }
        [arrFortime release];
}



-(BOOL)getNewDateOfamonth:(NSDate *)dayDate
{
    NSCalendar* calendar = [NSCalendar currentCalendar] ;
    
    NSDateComponents *components1 = [calendar components:0 fromDate:dayDate];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSArray *daySymbols = [formatter weekdaySymbols];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    [components1 setYear:[components1 year]];
    [components1 setMonth:[components1 month]];
    [components1 setDay:[components1 day]];
    
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:dayDate];
    
    NSString *str=nil;

    [self getCurrentTimeTable:dayDate];
    
    if(_curentTimeTable)
    {
        TableForCycle *dayCycle=[[Service sharedInstance]getlastcycleandDayTableDataInfoStored:[calendar dateFromComponents:components]];
        NSInteger cycleday=[dayCycle.cycleDay integerValue];
        NSInteger cycleLenght=[dayCycle.cycle integerValue];
        
        NSString *dayStr=[NSString stringWithFormat:@"%@ %d,%@",_curentTimeTable.cycleLable,cycleLenght,[self getShortLabel:cycleday]];
        
        if([self getCurentDayIsWeekend:[calendar dateFromComponents:components]])
        {
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
        }
        else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[calendar dateFromComponents:components]])
        {
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
        }
        else
        {
            str=[NSString stringWithFormat:@"%@ \n %@ \n %@",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]],dayStr];
        }
    }
    else{
        str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
    }
    
    NSArray *arrItem=[str componentsSeparatedByString:@"\n"];
    
    NSString *str3=[arrItem objectAtIndex:2];
    
    calendar = nil;
    
    if ([str3 length]>0)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

//Devashree added
-(void)getCurrentTimeTable:(NSDate*)dayDate
{
    _curentTimeTable = nil;
    BOOL _isFirstSem=NO;
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    TimeTable *_currentTimeTable=nil;
    
    int index =1;
    
    for(TimeTable *time in arr){
        
        NSDateComponents *dayComponent = [[[NSDateComponents alloc] init] autorelease];
        dayComponent.day = 1;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        
        NSDateComponents *dayComponent1 = [[[NSDateComponents alloc] init] autorelease];
        dayComponent1.day = -1;
        
        NSCalendar *theCalendar1 = [NSCalendar currentCalendar];

        NSDate *startdate = [theCalendar1 dateByAddingComponents:dayComponent1 toDate:time.startDate options:0];
        NSDate *enddate = [theCalendar dateByAddingComponents:dayComponent toDate:time.end_date options:0];
        
        if([DELEGATE isDate:dayDate inRangeFirstDate:startdate  lastDate:enddate])
            
        {
            _isFirstSem=YES;
            _currentTimeTable=time;
            
            _curentTimeTable=time; //Devashree added
            
        }
        
        index++;
    }
    if(_currentTimeTable == nil)
    {
        _currentTimeTable=[arr firstObject];
        
        _curentTimeTable=[arr firstObject]; //Devashree added
        
    }
}

-(NSString*)getShortLabel:(int)value
{
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableCycleData];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"timeTableCycle_id" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    if(value>0)
    {
        TimeTableCycle *tabCycle=[arr objectAtIndex:value-1];
        return tabCycle.short_lable;
    }
    else
    {
        return @" ";
    }
}


@end
