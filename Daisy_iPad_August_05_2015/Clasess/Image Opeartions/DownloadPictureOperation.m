//
//  DownloadPictureOperation.m
//

#import "DownloadPictureOperation.h"
#import "NetManager.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "School_Images.h"
#import "MyProfile.h"
#import "FileUtils.h"

@implementation UIImage (imageWithShadow)

- (UIImage*)imageWithShadow 
{
    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef shadowContext = CGBitmapContextCreate(NULL, self.size.width + 10, self.size.height + 10, CGImageGetBitsPerComponent(self.CGImage), 0, 
													   colourSpace, kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colourSpace);
	
    CGContextSetShadowWithColor(shadowContext, CGSizeMake(5, -5), 5, [UIColor grayColor].CGColor);
    CGContextDrawImage(shadowContext, CGRectMake(0, 10, self.size.width, self.size.height), self.CGImage);
	
    CGImageRef shadowedCGImage = CGBitmapContextCreateImage(shadowContext);
    CGContextRelease(shadowContext);
	
    UIImage * shadowedImage = [UIImage imageWithCGImage:shadowedCGImage];
    CGImageRelease(shadowedCGImage);
	
    return shadowedImage;
}

- (UIImage *)imageWithRoundedRect:(double)radius
{
	return nil;
}

@end


@implementation DownloadPictureOperation

@synthesize thumbURL;
@synthesize storedObject;
@synthesize mode;

- (id) initWithMode:(UIViewContentMode)contentMode 
{
	self.mode = contentMode;
	num=0;
	return [super init];
}
- (id) initWithMode:(UIViewContentMode)contentMode whichImage:(int)code
{
	self.mode = contentMode;
	num =code;
	return [super init];
}
- (void)createTask 
{
	if (![self.ownerQueue didFailInQueueOperation]) 
	{
		globalNetManager = [[NetManager alloc] initWithURL:thumbURL 
									   target:self
						finishLoadingSelector:@"netManagerDidFinishLoading:response:" 
								   postParameters:nil
							 showErrorMessage:NO 
								 storedObject:storedObject];
		[globalNetManager startRequest];
	} else 
	{
		[self finish];
	}
	
}

#pragma mark net manager
- (void)netManagerDidFinishLoading:(NetManager *)netManager response:(NSMutableData *)responseData {
	
    UIImageView *arrayView;
    int shouldISetImg=0;
    int another=0;
    if([netManager.storedObject objectForKey:@"cellImageView"])
    {
        arrayView = (UIImageView *)[netManager.storedObject objectForKey:@"cellImageView"];
    }
    if([netManager.storedObject objectForKey:@"tableView"])
    { 
        shouldISetImg=1;
        
        UITableView *tempTabView=[netManager.storedObject objectForKey:@"tableView"];
        if([tempTabView isKindOfClass:[UITableView class]])
        {
            NSArray *newArr=[tempTabView indexPathsForVisibleRows];
            
            for(NSIndexPath *newInPath in newArr)
            {
                if([netManager.storedObject objectForKey:@"currentRow"])
                {
                    NSIndexPath *tempInPath=[netManager.storedObject objectForKey:@"currentRow"];
                    if([tempInPath isKindOfClass:[NSIndexPath class]])
                    {
                        if(tempInPath.row==newInPath.row && tempInPath.section==newInPath.section)
                        {
                            another=1;
                            break;
                        }
                    }
                }
            }
        }
    }
    else
    {
        shouldISetImg=0;
    }
	
	id model = [netManager.storedObject objectForKey:@"model"];
    if([model isKindOfClass:[School_Images class]]){
        // UIImage *image = nil;
        School_Images*imagModle=(School_Images*)model;
       NSString  *str = [netManager.storedObject objectForKey:@"str"];

        NSError *error;
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:str]){
            [[NSFileManager defaultManager] createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        NSString * imageName =imagModle.image_name;
        
        str  = [str stringByAppendingPathComponent:imageName];
        
        [responseData writeToFile:str atomically:NO];
        imagModle.isDownload=[NSNumber numberWithBool:YES];
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            
        }
    }
    else if([model isKindOfClass:[ListofValues class]])
    {
        ListofValues *imagModle=(ListofValues*)model;
        NSString  *str = [netManager.storedObject objectForKey:@"str"];
        NSError *error;
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:str]){
            [[NSFileManager defaultManager] createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        NSString * imageName =[netManager.storedObject objectForKey:@"imageName"];
        
        str  = [str stringByAppendingPathComponent:imageName];
        
        // Get list of files in Directory
        NSArray *arrListOfFiles = [FileUtils getContentsofDirectory:[netManager.storedObject objectForKey:@"str"]];
        
        for (NSString *strFileName in arrListOfFiles)
        {
#if DEBUG
            NSLog(@"strFileName %@",strFileName);
#endif

            NSArray *arrOfDirectoryImageName = [strFileName componentsSeparatedByString:@"."];
            NSArray *arrOfGivenModelImageName = [imageName componentsSeparatedByString:@"."];
            
            if ([[arrOfDirectoryImageName objectAtIndex:0] isEqualToString:[arrOfGivenModelImageName objectAtIndex:0]])
            {
                NSString *filepath = [NSString stringWithFormat:@"%@/%@",[netManager.storedObject objectForKey:@"str"],strFileName];
#if DEBUG
                NSLog(@"filepath... %@",filepath);
#endif
                if ([[NSFileManager defaultManager] fileExistsAtPath:filepath])
                {

                    if ([FileUtils removeFileFromPath:filepath])
                    {
#if DEBUG
                        NSLog(@"successfully Removed File %@",strFileName);
#endif
                    }
                }
            }
        }
        
        // if LOV sub type is active then only write this file on Directory
        if ([imagModle.active integerValue] == 1)
        {
            [responseData writeToFile:str atomically:NO];
        }
        
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
    }
    else{
        
        NSString *imgKey = (NSString *)[netManager.storedObject objectForKey:@"imageKey"];
        if (netManager.error)
        {
#if DEBUG
            NSLog(@"error");
#endif
        }
        else
        {
            
            UIImage *image = nil;
            image = [UIImage imageWithData:responseData];
            if(shouldISetImg==0)
            {
                arrayView.image = image;
                arrayView.contentMode = self.mode;
            }
            
            if (model && imgKey)
            {
                if([model isKindOfClass:[School_detail class]])
                {
                    [model setValue:responseData forKey:imgKey];
                    
                }
                if([model isKindOfClass:[MyProfile class]])
                {
                    [model setValue:responseData forKey:imgKey];
                }
                
                if([model isKindOfClass:[MyTeacher class]])
                {
                    [model setValue:responseData forKey:imgKey];
                }
                if([model isKindOfClass:[Student class]])
                {
                    [model setValue:responseData forKey:imgKey];
                }
                // Code commented App Crashes on 31 March 2015//
                
                //[model setValue:responseData forKey:imgKey];
            }
            
            NSError *error;
            if (![ [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext] save:&error])
            {
#if DEBUG
                NSLog(@"netManagerDidFinishLoading  my profile Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
            if(another==1)
            {
                if([netManager.storedObject objectForKey:@"tableView"])
                {
                    if([netManager.storedObject objectForKey:@"currentRow"])
                    {
                        UITableView *tempTabView=[netManager.storedObject objectForKey:@"tableView"];
                        if([tempTabView isKindOfClass:[UITableView class]])
                        {
                            NSIndexPath *tempInPath=[netManager.storedObject objectForKey:@"currentRow"];
                            NSArray *newArr=[[NSArray alloc] initWithObjects:tempInPath, nil];
                            if([newArr count]>0)
                            {
                                if(tempTabView.superview)
                                {
                                    [tempTabView reloadRowsAtIndexPaths:newArr withRowAnimation:UITableViewRowAnimationNone];
                                }
                            }
                            
                        }
                        
                    }
                }
            }
            
        }
    }
    
	[netManager release];
	[self finish];

}

- (void)dealloc 
{
	[thumbURL release];
	[storedObject release];
    [super dealloc];
}

@end
