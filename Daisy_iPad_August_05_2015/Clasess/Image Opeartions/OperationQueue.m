//
//  OperationQueue.m
//  Pepsi
//
//  Created by Admin on 09.04.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "OperationQueue.h"


@implementation OperationQueue
@synthesize storedObject;
@synthesize didFailInQueueOperation;
@synthesize failOperationMessage;
@synthesize numberExpectedOperations;

- (void)clearError {
	didFailInQueueOperation = NO;
	self.failOperationMessage = @"";
}

- (void)dealloc {
	[failOperationMessage release];
	[super dealloc];
}

@end
