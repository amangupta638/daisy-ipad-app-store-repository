
//
//  HomeViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "Service.h"
#import "Item.h"
#import "MyProfile.h"
#import <QuartzCore/QuartzCore.h>
#import "Annoncements.h"
//#import "ShowNotesViewController.h"
#import "AppHelper.h"
#import "MainViewController.h"
#import "InboxViewController.h"
#import "NSString+HTML.h"
#import "FileUtils.h"

@interface HomeViewController ()
{
    //Aman added -- Glance Three Sections
    //-----------------------------------------------------------------------------------------------//
    
    NSMutableArray *marrForMainTableOverDue;
    NSMutableArray *marrForMainTableToday;
    NSMutableArray *marrForMainTableUpcoming;
    
    BOOL isInTodaySection;
    BOOL isComingFromDueToggle;
    
    //-----------------------------------------------------------------------------------------------//
    //Aman added -- Glance Three Sections
    
    // Pass For Timer Logic
    NSTimer *countDownTimer;
    NSCalendar *gregorianCalendar;

}

@end

@implementation HomeViewController
@synthesize _tableForMainView,_tableForProfilView;
@synthesize _popover,_curentDateFormeter,_popoverControler;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        queueOperation = [[OperationQueue alloc] init];
        [queueOperation setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
        // Custom initialization
        
        // Custom initialization
    }
    return self;
}
- (void)dealloc {
    [_arrForMainTable release];
    [_curentDateFormeter release];
    [_arrForDue release];
    [_arr release];
    [_popover release];
    [_arrForPrfileTable release];
    [_tableForMainView release];
    [_tableForProfilView release];
    [_userImgView release];
    [_nameLbl release];
    [_glanceBtn release];
    [_newsAndAnnouncementBtn release];
    [_newsAndAnnoCountLbl release];
    [_lblForOverdueAssign release];
    [_lblForOverdueDesc release];
    [_viewForDueItem release];
    [_imgVForIconBg release];
    [_ImgVForLeft release];
    [_btnForViewAll release];
    [_lblForViewAll release];
    [_imgViewForBanner release];
    [_lblSortBy release];
    [lblApprovedPassTimer release];
    
    // Timer
    if (countDownTimer)
    {
        [countDownTimer release];
        countDownTimer= nil;
    }
    
    if (gregorianCalendar)
    {
        [gregorianCalendar release];
    }

    if ( selectedDiaryItem )
    {
        selectedDiaryItem = nil;
    }
    [lblPassApprovedByTitle release];
    [lblElapsedPassTimer release];
    [lblPassDate release];
    [btn_DeletePass release];
    [super dealloc];
}


- (void)setApprovedPassDetails
{
    NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
    [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
    
    NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
    [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    
    /*NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
    [formaterToDisplay setDateFormat:kDateFormatToShowOnUI_hhmma];
    
    NSString *fromTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:selectedDiaryItem.assignedTime]];
    
    NSString *toTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:selectedDiaryItem.dueTime]];
    */
    
    NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
    [formaterToDisplay setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
    [formaterToDisplay setDateStyle:NSDateFormatterNoStyle];
    
    NSString *fromTime = [formaterToDisplay stringFromDate:selectedDiaryItem.assignedDate];
    
    NSString *toTime = [formaterToDisplay stringFromDate:selectedDiaryItem.dueDate];
    
    lblFromTime.text = fromTime;
    lblToTime.text = toTime;
    lblPassDate.text = [formterDate stringFromDate:selectedDiaryItem.assignedDate];
    
    [formterDate release];
    [formaterFromDB release];
    [formaterToDisplay release];
    
    lblPassType.text = selectedDiaryItem.title;
    
    
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if([profobj.type isEqualToString:@"Teacher"])
    {
        [btn_DeletePass setHidden:NO];
        lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:selectedDiaryItem];

    }
    else
    {
        [btn_DeletePass setHidden:YES];
        lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[selectedDiaryItem.itemId integerValue]]];


    }
    
    [viewForApprovedPassInner setBackgroundColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:16/255.0f alpha:1]];

}

#pragma mark
#pragma mark ViewLife

- (void)viewDidLoad
{
    //Aman added -- Glance Three Sections
    //-----------------------------------------------------------------------------------------------//
    marrForMainTableOverDue = [[NSMutableArray alloc]init];
    marrForMainTableToday = [[NSMutableArray alloc]init];
    marrForMainTableUpcoming = [[NSMutableArray alloc]init];
    
    selectedDiaryItem = nil;
    
    if ([DELEGATE glbDueSectionWasSelected])
    {
        isComingFromDueToggle = TRUE;
        [self.lblDue setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:14.0]];
        self.switctDueAssign.on = NO;
    }
    else
    {
        isComingFromDueToggle = FALSE;
        [self.lblAssigned setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:14.0]];
        self.switctDueAssign.on = YES;
    }
    
    self.segmentedControl.selectedSegmentIndex = 0;
    
    //Aman added -- iOS7 optimisation --------------------------------------
    if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.switctDueAssign.transform = CGAffineTransformMakeScale(0.6, 0.6);
    }
    else
    {
        self.switctDueAssign.transform = CGAffineTransformMakeScale(0.9, 0.9);
        self.switctDueAssign.frame = CGRectMake(self.switctDueAssign.frame.origin.x+13, self.switctDueAssign.frame.origin.y, self.switctDueAssign.frame.size.width, self.switctDueAssign.frame.size.height);
    }
    //Aman added -- iOS7 optimisation --------------------------------------
    
    
    
    //Aman added -- Glance Three Sections
    //-----------------------------------------------------------------------------------------------//
    
    
    
    [super viewDidLoad];
    _curentDateFormeter = [[NSDateFormatter alloc] init];
    [_curentDateFormeter setDateFormat:@"dd/MM/yyyy"];
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    //23AugChanged done
    NSString *lastnamestr=@"";
    
    if (profobj.last_name!=nil){
        lastnamestr=profobj.last_name;
    }
    _nameLbl.text=[NSString stringWithFormat:@"%@ %@",profobj.name,lastnamestr];
    
    // Flurry : Crash Log Fix for setObjectForKey: object cannot be nil (key: model)
    if (profobj)
    {
        if(profobj.image==nil){
            DownloadPictureOperation *pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFill];
            
            NSString *strng =[NSString stringWithFormat:@"%@%@",pic_BaseUrl,profobj.image_url];

            // NSString *strng=@"http://www.productdynamics.expensetrackingapplication.com/UserProfileImages/pic.jpg";
            [pictOp setThumbURL:[NSURL URLWithString:strng]];
            
            NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
            [storedObject setObject:_userImgView forKey:@"cellImageView"];
            [storedObject setObject:profobj forKey:@"model"];
            [storedObject setObject:@"image" forKey:@"imageKey"];
            
            
            [pictOp setStoredObject:storedObject];
            [storedObject release];
            [queueOperation addOperation:pictOp];
            [pictOp release];
        }
        else{
            _userImgView.image=[UIImage imageWithData:profobj.image];
        }
    }
    
    _glanceBtn.selected=YES;
    _newsAndAnnouncementBtn.selected=NO;
    _arrForPrfileTable=[[NSMutableArray alloc]init];
    
    //for week day
    _arr=[[NSMutableArray alloc]init];
    _arrForDue=[[NSMutableArray alloc]init];
    _arrForMainTable=[[NSMutableArray alloc]init];
    _btnForViewAll.selected=YES;
    _lblForViewAll.text=[NSString stringWithFormat:@"%@ \n %@",@"HIDE",@"ALL"];
    _tableForProfilView.hidden=YES;//_tableForProfilView.hidden=NO;
    
    [self addItemButtonAction];
    [_tableForMainView reloadData];
    // Do any additional setup after loading the view from its nib.
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Flurry : Crash Log Fix for setObjectForKey: object cannot be nil (key: model)
    if (schoolDetailObj)
    {
        if(schoolDetailObj.school_Image==nil){
            _imgViewForBanner.backgroundColor=[AppHelper colorFromHexString:schoolDetailObj.theme_color];
            
            DownloadPictureOperation *pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleToFill];
            
            NSString *strng =[NSString stringWithFormat:@"%@%@",pic_BaseUrlForSchoolLogo,schoolDetailObj.school_Image_Url];
            
            //NSString *strng=@"http://www.productdynamics.expensetrackingapplication.com/UserProfileImages/pic.jpg";
            [pictOp setThumbURL:[NSURL URLWithString:strng]];
            
            NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
            [storedObject setObject:_imgViewForBanner forKey:@"cellImageView"];
            [storedObject setObject:schoolDetailObj forKey:@"model"];
            [storedObject setObject:@"school_Image" forKey:@"imageKey"];
            
            
            [pictOp setStoredObject:storedObject];
            [storedObject release];
            [queueOperation addOperation:pictOp];
            [pictOp release];
        }
        else
        {
            _imgViewForBanner.image=[UIImage imageWithData:schoolDetailObj.school_Image];
            _imgViewForBanner.backgroundColor=[UIColor clearColor];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    
    _btnForViewAll.selected=YES;
    _lblForViewAll.text=[NSString stringWithFormat:@"%@ \n %@",@"HIDE",@"ALL"];
    _tableForProfilView.hidden=YES;//_tableForProfilView.hidden=NO;
    
    [self addItemButtonAction];
    
    [_tableForMainView reloadData];
    
}

- (void)viewDidUnload {
    [_userImgView release];
    _userImgView = nil;
    [_nameLbl release];
    _nameLbl = nil;
    [_tableForProfilView release];
    _tableForProfilView = nil;
    [_glanceBtn release];
    _glanceBtn = nil;
    [_newsAndAnnouncementBtn release];
    _newsAndAnnouncementBtn = nil;
    [_newsAndAnnoCountLbl release];
    _newsAndAnnoCountLbl = nil;
    [_tableForMainView release];
    _tableForMainView = nil;
    [_lblForOverdueAssign release];
    _lblForOverdueAssign = nil;
    [_lblForOverdueDesc release];
    _lblForOverdueDesc = nil;
    [_viewForDueItem release];
    _viewForDueItem = nil;
    [_imgVForIconBg release];
    _imgVForIconBg = nil;
    [_ImgVForLeft release];
    _ImgVForLeft = nil;
    [_btnForViewAll release];
    _btnForViewAll = nil;
    [_lblForViewAll release];
    _lblForViewAll = nil;
    [_imgViewForBanner release];
    _imgViewForBanner = nil;
    [super viewDidUnload];
}

#pragma mark -
#pragma mark Generic Pass Methods  -

// -----------------------------------------------------------------------------
// Pass Status

- (NSInteger)getPassStatus:(Item *)selectedDI
{
    NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
    [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
    
    NSDateFormatter *formterToCompareTime=[[NSDateFormatter alloc]init];
    [formterToCompareTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    [formterToCompareTime setDateStyle:NSDateFormatterNoStyle];
    [formterToCompareTime setTimeStyle:NSDateFormatterShortStyle];
    
    NSDateFormatter *dbFormaterTime=[[NSDateFormatter alloc]init];
    [dbFormaterTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    [dbFormaterTime setDateStyle:NSDateFormatterNoStyle];
    [dbFormaterTime setTimeStyle:NSDateFormatterShortStyle];
    
    NSDate *now = [NSDate date];
    
    NSString *strNowTime = [dbFormaterTime stringFromDate:now];
    NSString *strFromTime = [dbFormaterTime stringFromDate:selectedDI.assignedDate];
    NSString *strToTime = [dbFormaterTime stringFromDate:selectedDI.dueDate];
    
    NSString *strFromDate = [formterDate stringFromDate:now];
    NSString *strToDate = [formterDate stringFromDate:selectedDI.dueDate];
    
    
    // if Now/From date is Same as To Date
    if([[formterDate dateFromString:strFromDate] compare:[formterDate dateFromString:strToDate]] == NSOrderedSame)
    {
        if([[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strFromTime]] == NSOrderedSame )
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Pass Startedwith Same time...");
#endif
            return passStarted;
        }
        // if Now time inside From and To
        if ( [DELEGATE isDate:[formterToCompareTime dateFromString:strNowTime] inRangeFirstDate:[formterToCompareTime dateFromString:strFromTime] lastDate:[formterToCompareTime dateFromString:strToTime]] )
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Pass Started...");
#endif
            return passStarted;
        }
        // if Now Time is Later in time than TO Time
        else if([[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strToTime]] == NSOrderedDescending ||
                [[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strToTime]] == NSOrderedSame
                )
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Status: Pass Elapsed...");
#endif
            return passElapsed;
        }
        // if Now Time is early in time than From Time
        else if ([[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strFromTime]] == NSOrderedAscending)
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Status: Pass Not Started...");
#endif
            return passNotStarted;
        }
        else
        {
#if DEBUG
            NSLog(@"Status: else nothing...");
            NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
#endif
        }
    }
    // if Now Date is early than From Date
    else if ([[formterDate dateFromString:strFromDate] compare:[formterDate dateFromString:strToDate]] == NSOrderedAscending)
    {
#if DEBUG
        NSLog(@"Status: Notstarted.. FromDate %@ === ToDate %@",strFromDate,strToDate);
#endif
        
        return passNotStarted;
    }
    else
    {
#if DEBUG
        NSLog(@"/*/*/*/*//*/*/*/*/*/*/*/*/*");
        NSLog(@"NowDate %@ ===  ToTime %@",strFromDate,strToDate);
#endif
        return passElapsed;
    }
    [formterDate release];
    [formterToCompareTime release];
    
    return passUnExpected;
}

 // -----------------------------------------------------------------------------
 // Countdown Timer
 
 -(void)updateClock:(NSTimer *)timer
{
    NSDateFormatter *countDownDateFormatter = [[NSDateFormatter alloc] init];
    [countDownDateFormatter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    
    NSDate *now = [NSDate date];
    NSString *strFromTime = [countDownDateFormatter stringFromDate:now];
    //NSString *strToTime = selectedDiaryItem.dueTime;//[countDownDateFormatter stringFromDate:objItemSliderSelected.dueTime];
    NSString *strToTime = [countDownDateFormatter stringFromDate:selectedDiaryItem.dueDate];
    
    NSDateComponents *comp = [gregorianCalendar components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
                                                  fromDate:[countDownDateFormatter dateFromString:strFromTime]
                                                    toDate:[countDownDateFormatter dateFromString:strToTime]
                                                   options:0];
    
    NSString *strTimeRemaining = nil;
    
    // if date have not expired
    if([[countDownDateFormatter dateFromString:strFromTime] compare:[countDownDateFormatter dateFromString:strToTime]] == NSOrderedAscending)
    {
        strTimeRemaining = [[NSString alloc] initWithFormat:@"%02ld:%02ld:%02ld", (long)[comp hour],  (long)[comp minute], (long)[comp second]];
    }
    else
    {
        // time has expired, set time to 00:00 and set boolean flag to no
        strTimeRemaining = [[NSString alloc] initWithString:@"00:00:00"];
        
        if (countDownTimer)
        {
            [countDownTimer invalidate];
            countDownTimer =nil;
        }

    }
    
    lblApprovedPassTimer.text = strTimeRemaining;
    lblApprovedPassTimer.hidden = NO;
    lblElapsedPassTimer.text=@"";
    lblElapsedPassTimer.hidden = YES;
    
    [countDownDateFormatter release];
    [strTimeRemaining release];
}
 
// -----------------------------------------------------------------------------
// Show ApprovedPass View

 - (void)showApprovedViewDetails
{
    NSInteger approvedPassStatus = [self getPassStatus:selectedDiaryItem];
    
    switch (approvedPassStatus)
    {
        case passStarted:
        {
            [self setApprovedPassDetails];
            
            gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateClock:) userInfo:nil repeats:YES];
            [countDownTimer fire];
            
            break;
        }
        case passNotStarted:
        {
            if (countDownTimer)
            {
                [countDownTimer invalidate];
                countDownTimer =nil;
            }
            
            [self setApprovedPassDetails];
            lblApprovedPassTimer.text = @"NOT STARTED";
            lblElapsedPassTimer.text = @"";
            lblElapsedPassTimer.hidden = YES;
            lblApprovedPassTimer.hidden = NO;
            
            break;
        }
        case passElapsed:
        {
            [self showElapsedViewDetails];
            break;
        }
        case passUnExpected:
        {
#if DEBUG
            NSLog(@"getApprovedPassStatus == 9... unexpected case");
#endif
        }
    }
}

- (void)showElapsedViewDetails
{
    if (countDownTimer)
    {
        [countDownTimer invalidate];
        countDownTimer =nil;
    }
    
    NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
    [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
    
    NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
    [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    
   /* NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
    [formaterToDisplay setDateFormat:kDateFormatToShowOnUI_hhmma];
    
    NSString *fromTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:selectedDiaryItem.assignedTime]];
    
    NSString *toTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:selectedDiaryItem.dueTime]];*/
    
    NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
    [formaterToDisplay setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    [formaterToDisplay setDateStyle:NSDateFormatterNoStyle];
    [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
    
    NSString *fromTime = [formaterToDisplay stringFromDate:selectedDiaryItem.assignedDate];
    
    NSString *toTime = [formaterToDisplay stringFromDate:selectedDiaryItem.dueDate];
    
    
    lblPassDate.text = [formterDate stringFromDate:selectedDiaryItem.assignedDate];
    lblFromTime.text = fromTime;
    lblToTime.text = toTime;
    
    [formterDate release];
    [formaterFromDB release];
    [formaterToDisplay release];
    
    lblPassType.text = selectedDiaryItem.title;
    lblApprovedPassTimer.hidden = YES;
    lblApprovedPassTimer.text = @"";
    lblElapsedPassTimer.text = @"ELAPSED";
    lblElapsedPassTimer.hidden = NO;
    
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if([profobj.type isEqualToString:@"Teacher"])
    {
         lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:selectedDiaryItem];
        [btn_DeletePass setHidden:NO];
    }
    else
    {
        [btn_DeletePass setHidden:YES];
        lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[selectedDiaryItem.itemId integerValue]]];

        
    }
    
    [viewForApprovedPassInner setBackgroundColor:[UIColor grayColor]];
    

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(tableView==self._tableForProfilView)
    {
        return 1;
    }
    else
    {
        if (_glanceBtn.selected)
        {
            if(isComingFromDueToggle)
                return 3;
            else
                return 2;
        }
        else
        {
            // flurry Crash : added array count condition
            if ( [_arr count] > 0 )
            {
                return [_arr count];
            }
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return the hieight of rows.
    if(tableView==self._tableForProfilView)
    {
        return 45;
    }
    else
    {
        if (_glanceBtn.selected)
        {
            //Aman added -- iOS7 optimisation --------------------------------------
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                return  53;
            }
            else
            {
                return 45;
            }
            //Aman added -- iOS7 optimisation --------------------------------------

        }
        else{
            return 61;
        }
        
    }
    return 0;
}

//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(tableView==self._tableForProfilView)
    {
        // flurry Crash : added array count condition
        if ([_arrForDue count]>0)
        {
            return _arrForDue.count;
        }
    }
    else
    {
        if (_glanceBtn.selected)
        {
            // flurry Crash : added array count condition
            NSInteger rowCount=0;
            
            if (isComingFromDueToggle)
            {
                if (section == 0)
                {
                    // flurry Crash : added array count condition
                    if ([marrForMainTableOverDue count]>0)
                    {
                        rowCount = [marrForMainTableOverDue count];
                    }
                }
                else if(section == 1)
                {
                    // flurry Crash : added array count condition
                    if ([marrForMainTableToday count]>0)
                    {
                        rowCount = [marrForMainTableToday count];
                    }
                }
                else
                {
                    // flurry Crash : added array count condition
                    if ([marrForMainTableUpcoming count]>0)
                    {
                        rowCount = [marrForMainTableUpcoming count];
                    }
                }
            }
            else
            {
                if (section == 0)
                {
                    // flurry Crash : added array count condition
                    if ([marrForMainTableToday count]>0)
                    {
                        rowCount = [marrForMainTableToday count];
                    }
                }
                else
                {
                    // flurry Crash : added array count condition
                    if ([marrForMainTableOverDue count]>0)
                    {
                        rowCount = [marrForMainTableOverDue count];
                    }
                }
            }
            
            return rowCount;
        }
        else{
            // flurry Crash : added array count condition
            NSInteger rowCount= 0;
            
            if ([_arr count]>section)
            {
                if ([[[_arr objectAtIndex:section]objectForKey:@"result"] count]>0)
                {
                    rowCount = [[[_arr objectAtIndex:section]objectForKey:@"result"] count];
                }
            }
            
            return rowCount;
        }
    }
    return 0;
    
}
//-----------------------------------------------------------------------------------------------//
//Aman added -- Glance Three Sections


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView==self._tableForProfilView)
    {
        return 0;
    }
    else
    {
        if (_glanceBtn.selected)
        {
            return 30;
        }
        else{
            return 30;
        }
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView==self._tableForProfilView)
    {
        return nil;
    }
    else{
        UIView *headerView=[[[UIView alloc] init] autorelease];
        headerView.backgroundColor=[UIColor clearColor];
        UIImageView *sectionImage=[[UIImageView alloc] init];
        
        sectionImage.backgroundColor=[UIColor clearColor];
        [headerView addSubview:sectionImage];
        
        headerView.frame=CGRectMake(0, 0,835, 30);
        sectionImage.frame=CGRectMake(0, 0,836, 30);
        
        
        
        [sectionImage release];
        
        UILabel* sectionDayLabel=[[UILabel alloc] init];
        sectionDayLabel.frame=CGRectMake(10, 0,240, 30);
        sectionDayLabel.font=[UIFont boldSystemFontOfSize:14.0f];
        sectionDayLabel.backgroundColor=[UIColor clearColor];
        sectionDayLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
        [headerView addSubview:sectionDayLabel];
        [sectionDayLabel release];
        
        UILabel* sectionDateLabel=[[UILabel alloc] init];
        sectionDateLabel.frame=CGRectMake(750, 0,240, 30);
        sectionDateLabel.backgroundColor=[UIColor clearColor];
        sectionDateLabel.font=[UIFont boldSystemFontOfSize:14.0f];
        sectionDateLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
        [headerView addSubview:sectionDateLabel];
        [sectionDateLabel release];
        
        //Aman added -- Glance Three Sections
        //-----------------------------------------------------------------------------------------------//
        if(_glanceBtn.selected)
        {
            if (isComingFromDueToggle)
            {
                if(section==0)
                {
                    sectionImage.image=[UIImage imageNamed:@"tabletocell.png"];
                    sectionDayLabel.text=@"Overdue";
                }
                else if(section == 1)
                {
                    sectionImage.image=[UIImage imageNamed:@"tablemidcelltop.png"];
                    sectionDayLabel.text=@"Today";
                }
                else
                {
                    sectionImage.image=[UIImage imageNamed:@"tablemidcelltop.png"];
                    sectionDayLabel.text=@"Upcoming";
                }
            }
            else
            {
                if(section==0)
                {
                    sectionImage.image=[UIImage imageNamed:@"tabletocell.png"];
                    sectionDayLabel.text=@"Today";
                }
                else
                {
                    sectionImage.image=[UIImage imageNamed:@"tablemidcelltop.png"];
                    sectionDayLabel.text=@"Previous";
                    
                }
                
            }
            
            
        }
        //-----------------------------------------------------------------------------------------------//
        //Aman added -- Glance Three Sections
        
        else{
            
            if(section==0)
            {
                sectionImage.image=[UIImage imageNamed:@"tabletocell.png"];
                
            }
            else
            {
                sectionImage.image=[UIImage imageNamed:@"tablemidcelltop.png"];
                
            }
            
            
            [self._curentDateFormeter setDateFormat:@"dd MMM yy"];
            
            NSDateFormatter *DF = [[NSDateFormatter alloc] init];
            [DF setDateFormat:kDATEFORMAT_ddMMMyy];

            //if([[self._curentDateFormeter stringFromDate:[[NSDate date] dateByAddingTimeInterval:24*60*60]] isEqualToString:[[_arr objectAtIndex:section] objectForKey:@"date"]]){
            if([[self._curentDateFormeter stringFromDate:[NSDate date] ] isEqualToString:[[_arr objectAtIndex:section] objectForKey:@"date"]]){
                sectionDayLabel.text=@"Today";
                sectionDateLabel.text=[[_arr objectAtIndex:section]objectForKey:@"date"];

                // For Prime Only
                if (![[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    
                    sectionDateLabel.text= [DF stringFromDate:[self._curentDateFormeter dateFromString:[[_arr objectAtIndex:section]objectForKey:@"date"]]];

                    
                }

            }
            else if([[self._curentDateFormeter stringFromDate:[[NSDate date] dateByAddingTimeInterval:24*60*60]] isEqualToString:[[_arr objectAtIndex:section] objectForKey:@"date"]]){
                sectionDayLabel.text=@"Tomorrow";
                sectionDateLabel.text=[[_arr objectAtIndex:section]objectForKey:@"date"];
                // For Prime Only
                if (![[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    
                    sectionDateLabel.text= [DF stringFromDate:[self._curentDateFormeter dateFromString:[[_arr objectAtIndex:section]objectForKey:@"date"]]];
                    
                    
                }
            }
            else if([[self._curentDateFormeter stringFromDate:[[NSDate date] dateByAddingTimeInterval:-24*60*60]] isEqualToString:[[_arr objectAtIndex:section] objectForKey:@"date"]]){
                sectionDayLabel.text=@"Yesterday";
                sectionDateLabel.text=[[_arr objectAtIndex:section]objectForKey:@"date"];
                // For Prime Only
                if (![[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    
                    sectionDateLabel.text= [DF stringFromDate:[self._curentDateFormeter dateFromString:[[_arr objectAtIndex:section]objectForKey:@"date"]]];
                    
                    
                }
            }
            else{
                sectionDayLabel.text=[[_arr objectAtIndex:section]objectForKey:@"day"];
                sectionDateLabel.text=[[_arr objectAtIndex:section]objectForKey:@"date"];

                // For Prime Only
                if (![[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    
                    sectionDateLabel.text= [DF stringFromDate:[self._curentDateFormeter dateFromString:[[_arr objectAtIndex:section]objectForKey:@"date"]]];
                    
                    
                }
            }
            [DF release];
            DF = nil;

        }
        
        return headerView;
        
    }
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==self._tableForProfilView)
    {
        static NSString *CellIdentifier = @"myIdenfier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSArray *nib;
        if (cell == nil)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"HomeVCProfileCustomCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView * imgViewForHeaderBg=[[UIImageView alloc]initWithFrame:CGRectMake(118, 7, 30, 30)];
            imgViewForHeaderBg.backgroundColor=[UIColor clearColor];
            imgViewForHeaderBg.tag=11;
            
            UIImageView * imgViewForItemTye=[[UIImageView alloc]initWithFrame:CGRectMake(123, 12, 20, 20)];
            imgViewForItemTye.backgroundColor=[UIColor clearColor];
            imgViewForItemTye.tag=12;
            
            UIImageView * imgViewForLeftSide=[[UIImageView alloc]initWithFrame:CGRectMake(110, 1, 5, 42)];
            imgViewForLeftSide.backgroundColor=[UIColor clearColor];
            imgViewForLeftSide.tag=13;
            
            [cell.contentView addSubview:imgViewForHeaderBg];
            [imgViewForHeaderBg release];
            [cell.contentView addSubview:imgViewForItemTye];
            [imgViewForItemTye release];
            [cell.contentView addSubview:imgViewForLeftSide];
            [imgViewForLeftSide release];
        }
        
        UIImageView *bgImgView=(UIImageView*)[cell.contentView viewWithTag:1];
        UILabel *lblForAssignment = (UILabel*)[cell.contentView viewWithTag:2];
        UILabel *lblForTestAndPract=(UILabel*)[cell.contentView viewWithTag:3];
        UIButton *crossBtn=(UIButton*)[cell.contentView viewWithTag:4];
        UILabel *lblForDate=(UILabel*)[cell.contentView viewWithTag:5];
        UIImageView *cellBgImgView=(UIImageView*)[cell.contentView viewWithTag:6];
        
        UIImageView *imgViewForHeaderBg=(UIImageView*)[cell.contentView viewWithTag:11];
        UIImageView *imgViewForItemTye=(UIImageView*)[cell.contentView viewWithTag:12];
        UIImageView *imgViewForLeftSide=(UIImageView*)[cell.contentView viewWithTag:13];
        
        Item *item=[_arrForDue objectAtIndex:indexPath.row];
        [crossBtn addTarget:self action:@selector(crossBtuttonAction:) forControlEvents:UIControlEventTouchUpInside];
        NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
        [formeter setDateFormat:@"dd MMM"];
        lblForAssignment.text=item.title;
        lblForDate.text=[formeter stringFromDate:item.dueDate];
        
        /////////
        NSCalendar *calender=[NSCalendar currentCalendar];
        [formeter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        int noOfDays = 0;
        if([item.onTheDay integerValue]==0){
            NSDateComponents *dayComponentsIsl = [calender
                                                  components:(NSMinuteCalendarUnit |NSHourCalendarUnit |NSSecondCalendarUnit) fromDate:item.dueDate];
            
            NSArray *arr=[item.dueTime componentsSeparatedByString:@":"];
            
            [dayComponentsIsl setHour:[[arr objectAtIndex:0] integerValue]];
            [dayComponentsIsl setMinute:[[arr objectAtIndex:1] integerValue]];
            NSDate *d2=[calender dateByAddingComponents:dayComponentsIsl toDate:item.dueDate options:0];
            //int timeDiff=(int)[d2 timeIntervalSinceDate:[NSDate date]];
            int timeDiff1=(int)[[NSDate date] timeIntervalSinceDate:d2];
            
            if(timeDiff1>0){
                noOfDays=timeDiff1/(24*60*60);
                
                if (noOfDays<2)
                    lblForTestAndPract.text=@"Today";
                else
                    lblForTestAndPract.text=[NSString stringWithFormat:@"%d days ago",noOfDays];
                
            }
        }
        else{
            NSDateComponents *dayComponentsIsl = [calender
                                                  components:(NSMinuteCalendarUnit |NSHourCalendarUnit |NSSecondCalendarUnit) fromDate:item.dueDate];
            
            NSDate *d2=[calender dateByAddingComponents:dayComponentsIsl toDate:item.dueDate options:0];
            //int timeDiff=(int)[d2 timeIntervalSinceDate:[formeter dateFromString:[formeter stringFromDate:[NSDate date]]]];
            int timeDiff=(int)[[formeter dateFromString:[formeter stringFromDate:[NSDate date]]] timeIntervalSinceDate:d2];
            if(timeDiff>0){
                noOfDays=timeDiff/(24*60*60);
                if (noOfDays<2)
                    lblForTestAndPract.text=@"Today";
                else
                    lblForTestAndPract.text=[NSString stringWithFormat:@"%d days ago",noOfDays];
                
            }
            
        }
        if (indexPath.row==0) {
            _lblForOverdueAssign.text=item.title;
            _lblForOverdueDesc.text=lblForTestAndPract.text;
        }
        
        /////////
        
        [formeter release];
        
        
        //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
        MyClass *class1=nil;
        
        class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",item.class_id]];
        
        if(class1.code!=nil)
        {
            imgViewForHeaderBg.backgroundColor=[AppHelper colorFromHexString:class1.code];
            imgViewForLeftSide.backgroundColor=[AppHelper colorFromHexString:class1.code];
        }
        else{
            imgViewForHeaderBg.backgroundColor=[UIColor grayColor];
            imgViewForLeftSide.backgroundColor=[UIColor grayColor];
        }
        
       
        
        //--------- By Mitul ---------//

        
        
        imgViewForHeaderBg.contentMode=UIViewContentModeScaleAspectFit;
        [imgViewForHeaderBg.layer setMasksToBounds:YES];
        [imgViewForHeaderBg.layer setCornerRadius:2.0f];
        [imgViewForHeaderBg.layer setBorderWidth:1.0];
        [imgViewForHeaderBg.layer setBorderColor:[[UIColor clearColor] CGColor]];
        
        
        imgViewForLeftSide.contentMode=UIViewContentModeScaleAspectFit;
        [imgViewForLeftSide.layer setMasksToBounds:YES];
        [imgViewForLeftSide.layer setCornerRadius:2.0f];
        [imgViewForLeftSide.layer setBorderWidth:1.0];
        [imgViewForLeftSide.layer setBorderColor:[[UIColor clearColor] CGColor]];
        
        
        
       
        imgViewForItemTye.image=[UIImage imageNamed:@"overdueicon.png"];
        if([item.type isEqualToString:@"OutOfClass"] || [item.type isEqualToString:@"Out Of Class"])
        {
            if([item.isApproved integerValue] == 0)
            {
                imgViewForItemTye.image=[UIImage imageNamed:@"passPendingicon.png"];
            }
            else  if([item.isApproved integerValue] == 1)
            {
                imgViewForItemTye.image=[UIImage imageNamed:@"passApprovedicon.png"];
            }
            else  if([item.isApproved integerValue] == 2)
            {
                imgViewForItemTye.image=[UIImage imageNamed:@"passElapsedicon.png"];
            }
        }
        else if([item.type isEqualToString:k_MERIT_CHECK] )
        {
            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:item.passTypeId];
            if (lov)
            {
                
                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                {
                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                    imgViewForItemTye.image=[FileUtils getImageFromPath:strPath];
                }
            }
        }
        else
        {
                bgImgView.image=[UIImage imageNamed:@"dueBgcell@2x.png"];
        }
        

        
        if (indexPath.row==0) {
            cellBgImgView.image=[UIImage imageNamed:@"overduetopcell.png"];
        }
        else if (indexPath.row==_arrForDue.count-1)
        {
            cellBgImgView.image=[UIImage imageNamed:@"overduebasecell.png"];
        }
        else{
            cellBgImgView.image=[UIImage imageNamed:@"overduetopcell.png"];
        }
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"myIdenfier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSArray *nib;
        if (_glanceBtn.selected)
        {
            if (cell == nil)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"HomeVCMainCustomCellForGlance" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            //Aman added -- EZMOBILE-25
            //-----------------------------------------------------------------------------------------------------//
            
            id typeOfDiaryItem;
            
            if (isComingFromDueToggle)
            {
                if (indexPath.section == 0)
                {
                    typeOfDiaryItem = [[marrForMainTableOverDue objectAtIndex:indexPath.row] valueForKey:@"result"];
                    isInTodaySection = FALSE;
                }
                else if (indexPath.section == 1)
                {
                    typeOfDiaryItem = [[marrForMainTableToday objectAtIndex:indexPath.row] valueForKey:@"result"];
                    isInTodaySection = TRUE;
                }
                else
                {
                    typeOfDiaryItem = [[marrForMainTableUpcoming objectAtIndex:indexPath.row] valueForKey:@"result"];
                    isInTodaySection = FALSE;
                }
            }
            else
            {
                if (indexPath.section == 0)
                {
                    typeOfDiaryItem = [[marrForMainTableToday objectAtIndex:indexPath.row] valueForKey:@"result"];
                    isInTodaySection = TRUE;
                }
                else
                {
                    typeOfDiaryItem = [[marrForMainTableOverDue objectAtIndex:indexPath.row] valueForKey:@"result"];
                    isInTodaySection = FALSE;
                }
            }
            
            
            if ([typeOfDiaryItem isKindOfClass:[Item class]])  //Aman added -- EZMOBILE-25
            {
                
                Item *item = typeOfDiaryItem;
#if DEBUG
                NSLog(@"item: %@",item);
#endif
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                int x=20;
                int y=7;
                UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, y, 180, 28)];
                cellDescView.backgroundColor=[UIColor clearColor];
                cellDescView.clipsToBounds=YES;
                
                cellDescView.tag = 200; //Aman added --slider functionality
                
                
                UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 28)];
                cellDescBgImgView.backgroundColor=[UIColor clearColor];
                
                
                UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 28)];
                UIImageView * iconImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                
                
                UIImageView * imgViewForLine=[[UIImageView alloc]initWithFrame:CGRectMake(42, 13, 120, 2)];
                imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                imgViewForLine.backgroundColor=[UIColor grayColor];
                imgViewForLine.tag = 201; //Aman added --slider functionality
                
                //Aman added -- iOS7 optimisation --------------------------------------
                UIImageView *imgViewItemColor = [[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    imgViewItemColor.frame = CGRectMake(2, 1, 10, 50);
                }
                else
                {
                    imgViewItemColor.frame = CGRectMake(2, 1, 10, 42);
                }
                [cell.contentView addSubview:imgViewItemColor];
                
                
                
                
                NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc]init];
                [shortDateFormatter setDateFormat:kDATEFORMAT_dd_MMM];
                NSString *strShortDate = [shortDateFormatter stringFromDate:item.assignedDate];
                
                UILabel * lblForShortDate = [[UILabel alloc]initWithFrame:CGRectMake(355, 15 ,110, 15)];
                lblForShortDate.font=[UIFont fontWithName:@"Arial" size:12];
                lblForShortDate.backgroundColor=[UIColor clearColor];
                if (isComingFromDueToggle)
                {
                    
                    lblForShortDate.text = strShortDate;
                }
                else
                {
                    if (isInTodaySection)
                    {
                       // After 12/24hr Format Support
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateStyle:NSDateFormatterNoStyle];
                        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                        NSString *TimeString = [dateFormatter stringFromDate:item.assignedDate];
                        lblForShortDate.text = TimeString;
                        // After 12/24hr Format Support

                    }
                    else
                        lblForShortDate.text = strShortDate;
                    
                }
                
                [cell.contentView addSubview:lblForShortDate];
                [lblForShortDate release];
                [shortDateFormatter release];
                
                
                
                UILabel *lblForTime = (UILabel*)[cell.contentView viewWithTag:2];
                
                
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",item.class_id]];
                
                if(class1.code!=nil)
                {
                    iconImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
                    imgViewItemColor.backgroundColor=[AppHelper colorFromHexString:class1.code];
                }
                else{
                    iconImgView.backgroundColor=[UIColor grayColor];
                    imgViewItemColor.backgroundColor = [UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                iconImg.center=iconImgView.center;
                iconImgView.contentMode=UIViewContentModeScaleAspectFit;
                [iconImgView.layer setMasksToBounds:YES];
                [iconImgView.layer setCornerRadius:3.0f];
                [iconImgView.layer setBorderWidth:1.0];
                [iconImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 180, 28);
                popUpButton.backgroundColor=[UIColor clearColor];
                popUpButton.userInteractionEnabled = NO; //Aman added -- popover blocked
                
                if ([item.type isEqualToString:@"Note"])
                {
                    [popUpButton addTarget:self action:@selector(showAllNotesItemButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                }
                else{
                    [popUpButton addTarget:self action:@selector(showPopupButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                }
                
                if ([item.progress isEqualToString:@"100"])
                {
                    imgViewForLine.hidden=NO;
                }
                else
                {
                    imgViewForLine.hidden=YES;
                }
                
                
                [cellDescView addSubview:popUpButton];
                [cellDescView addSubview:cellDescBgImgView];
                [cellDescView addSubview:iconImgView];
                [cellDescView addSubview:iconImg];
                [cellDescView addSubview:imgViewForLine];
                NSString *isTypeStr= item.type;
                
                
                // check for due
                if(![item.type isEqualToString:@"Note"]){
                    int timeDiff;
                    [formeter setDateFormat:@"dd MMM yy"];
                    
                    
                    
                    //Aman added -- Glance Three Sections
                    //-----------------------------------------------------------------------------------------------//
                    NSString *date;
                    if (isComingFromDueToggle)
                    {
                        if (indexPath.section == 0)
                        {
                            date = [[marrForMainTableOverDue objectAtIndex:indexPath.row] valueForKey:@"date"];
                        }
                        else if (indexPath.section == 1)
                        {
                            date = [[marrForMainTableToday objectAtIndex:indexPath.row] valueForKey:@"date"];
                        }
                        else
                        {
                            date = [[marrForMainTableUpcoming objectAtIndex:indexPath.row] valueForKey:@"date"];
                        }

                    }
                    else
                    {
                        if (indexPath.section == 0)
                        {
                            date = [[marrForMainTableToday objectAtIndex:indexPath.row] valueForKey:@"date"];
                        }
                        else if (indexPath.section == 1)
                        {
                            date = [[marrForMainTableOverDue objectAtIndex:indexPath.row] valueForKey:@"date"];
                        }
                    }
                    
                    //-----------------------------------------------------------------------------------------------//
                    //Aman added -- Glance Three Sections
                    
                    
                    if([item.onTheDay integerValue]==0)
                    {
                        timeDiff = (int)[item.dueDate timeIntervalSinceDate:[formeter dateFromString:date]]; //Aman added -- Glance Three Sections
                        
                        
                        if(timeDiff<=0.0){
                          
                            if([item.type isEqualToString:@"OutOfClass"] || [item.type isEqualToString:@"Out Of Class"]) {
                                if([item.isApproved integerValue] == 0)
                                {
                                    iconImg.image=[UIImage imageNamed:@"passPendingicon.png"];
                                }
                                else  if([item.isApproved integerValue] == 1)
                                {
                                    iconImg.image=[UIImage imageNamed:@"passApprovedicon.png"];
                                }
                                else  if([item.isApproved integerValue] == 2)
                                {
                                    iconImg.image=[UIImage imageNamed:@"passElapsedicon.png"];
                                }
                            }
                            else if([item.type isEqualToString:k_MERIT_CHECK] )
                            {
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:item.passTypeId];
                                if (lov)
                                {
                                    
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                        iconImg.image=[FileUtils getImageFromPath:strPath];
                                    }
                                }
                            }
                            else
                            {
                             iconImg.image=[UIImage imageNamed:@"overdueicon.png"];
                            }
                            /////////
                            if (isComingFromDueToggle)
                            {
                                UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                                dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                                dulLbl.numberOfLines=1;
                                dulLbl.backgroundColor=[UIColor clearColor];
                                dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                                dulLbl.textColor=[UIColor grayColor];
                                dulLbl.text=@"Due:" ;
                                
                                [cellDescView addSubview:dulLbl];
                                if([item.type isEqualToString:k_MERIT_CHECK] ){
                                    dulLbl.text=@"" ;
                                }

                                [dulLbl release];

                            }
                            
                            [formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
                            
                        }
                        else{
                            if ([isTypeStr isEqualToString:@"Assignment"]) {
                                
                                iconImg.image=[UIImage imageNamed:@"dairygreenicon.png"];
                            }
                            else if([isTypeStr isEqualToString:@"Homework"]) {
                                iconImg.image=[UIImage imageNamed:@"homeredicon.png"];
                            }
                            else{
                                iconImg.image=[UIImage imageNamed:@"noteicon"];
                            }
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,135, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.lineBreakMode=NSLineBreakByCharWrapping;
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            lblForTitle.text=item.title ;
                            [lblForTitle release];
                            [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                            
                            [formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
                        }
                    }
                    else{
                        [formeter setDateFormat:@"dd-mm-yyyy"];
                        [self._curentDateFormeter setDateFormat:kDATEFORMAT_ddMMMyy];
                        
                        
                        //if([[formeter stringFromDate:[self._curentDateFormeter dateFromString:[[_arrForMainTable objectAtIndex:indexPath.section]objectForKey:@"date"]]] isEqualToString:[formeter stringFromDate:item.dueDate]])
                        if([[formeter stringFromDate:[self._curentDateFormeter dateFromString:date]] isEqualToString:[formeter stringFromDate:item.dueDate]])  //Aman added -- Glance Three Sections
                        {
                            if([item.type isEqualToString:@"OutOfClass"] || [item.type isEqualToString:@"Out Of Class"]) {
                                if([item.isApproved integerValue] == 0)
                                {
                                    iconImg.image=[UIImage imageNamed:@"passPendingicon.png"];
                                }
                                else  if([item.isApproved integerValue] == 1)
                                {
                                    iconImg.image=[UIImage imageNamed:@"passApprovedicon.png"];
                                }
                                else  if([item.isApproved integerValue] == 2)
                                {
                                    iconImg.image=[UIImage imageNamed:@"passElapsedicon.png"];
                                }
                            }
                            else if([item.type isEqualToString:k_MERIT_CHECK] )
                            {
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:item.passTypeId];
                                if (lov)
                                {
                                    
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                        iconImg.image=[FileUtils getImageFromPath:strPath];
                                    }
                                }
                            }
                            else
                            {
                                iconImg.image=[UIImage imageNamed:@"overdueicon.png"];
                            }
                            if (isComingFromDueToggle)
                            {
                                UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                                dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                                dulLbl.numberOfLines=1;
                                dulLbl.backgroundColor=[UIColor clearColor];
                                dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                                dulLbl.textColor=[UIColor grayColor];
                                dulLbl.text=@"Due:" ;
                                if([item.type isEqualToString:k_MERIT_CHECK] ){
                                    dulLbl.text=@"" ;
                                }
                                [cellDescView addSubview:dulLbl];
                                [dulLbl release];

                            }
                        }
                        else{
                            if ([isTypeStr isEqualToString:@"Assignment"]) {
                                
                                iconImg.image=[UIImage imageNamed:@"dairygreenicon.png"];
                            }
                            else if([isTypeStr isEqualToString:@"Homework"]) {
                                iconImg.image=[UIImage imageNamed:@"homeredicon.png"];
                            }
                            else{
                                iconImg.image=[UIImage imageNamed:@"noteicon"];
                            }
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,135, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.lineBreakMode=NSLineBreakByCharWrapping;
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            lblForTitle.text=item.title ;
                            [lblForTitle release];
                            [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                            
                            [formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
                        }
                        
                    }
                }
                else{
                    
                    
                    
                    iconImg.image=[UIImage imageNamed:@"noteicon"];
                    
                    
                    UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(70, 7,110, 15)];
                    lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                    lblForTitle.backgroundColor=[UIColor clearColor];

                    lblForTitle.text=item.title ;
                    [lblForTitle release];
                    [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];

                    [formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
                    
                }
                
                [cell.contentView addSubview:cellDescView];
                
                
                UIImageView *cellBgImgView=(UIImageView*)[cell.contentView viewWithTag:1];
                
                cellBgImgView.image=[UIImage imageNamed:@"tablecell.png"];
//                cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                [cell.contentView bringSubviewToFront:lblForTime];
                
                
                
                if ([item.type isEqualToString:@"Assignment"] || [item.type isEqualToString:@"Homework"])
                {
                    //[cell.contentView setFrame:CGRectMake(0, 0, 831, 200)];
                    
                    //for slider
                    ///------------------------------------------------------------------------------------------------------//
                    
                    UISlider *mSlider = [[UISlider alloc]initWithFrame:CGRectMake(520, 22, 250, 5)];
                    mSlider.minimumValue = 0;
                    mSlider.maximumValue = 100;
                    mSlider.value = [item.progress floatValue];
                    mSlider.exclusiveTouch = YES;
                    mSlider.tag=indexPath.row;
                    mSlider.continuous = NO;
                    
                    
                    //Aman added -- iOS7 optimisation --------------------------------------
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        mSlider.transform = CGAffineTransformMakeScale(0.8, 0.8);
                        mSlider.frame = CGRectMake(mSlider.frame.origin.x, mSlider.frame.origin.y-5, mSlider.frame.size.width, mSlider.frame.size.height);
                    }
                    //Aman added -- iOS7 optimisation --------------------------------------
                    
                    
                    if([item.itemId integerValue]>0) //slider tag is corresponding Item-Id
                    {
                        mSlider.tag=[item.itemId integerValue];
                        [AppDelegate getAppdelegate]._isUniqueId=YES;
                    }
                    else
                    {
                        mSlider.tag=[item.universal_Id integerValue];
                        [AppDelegate getAppdelegate]._isUniqueId=NO;
                    }
                    
                    [mSlider addTarget:self action:@selector(updateValue:) forControlEvents:UIControlEventValueChanged];
                    [mSlider addTarget:self action:@selector(saveSliderValueInDatabase:) forControlEvents:UIControlEventValueChanged];
                    
                    //Find co-ordinates of slider thumb image
                    float sliderRange = mSlider.frame.size.width - mSlider.currentThumbImage.size.width;
                    float sliderOrigin = mSlider.frame.origin.x + (mSlider.currentThumbImage.size.width / 2.0);
                    
                    float sliderValueToPixels = (((mSlider.value - mSlider.minimumValue) / (mSlider.maximumValue - mSlider.minimumValue)) * sliderRange) + sliderOrigin;
                    
                    UIImageView *imgV=[[UIImageView alloc]initWithFrame:CGRectMake(mSlider.frame.origin.x-4, mSlider.frame.origin.y-18, 34, 18)];
                    imgV.image=[UIImage imageNamed:@"pogressbarpopup.png"];
                    [imgV setFrame:CGRectMake(sliderValueToPixels-15, imgV.frame.origin.y, imgV.frame.size.width, imgV.frame.size.height)];
                    imgV.tag=100;
                    
                    
                    
                    UILabel *popView = [[UILabel alloc]initWithFrame:CGRectMake(sliderValueToPixels-32, mSlider.frame.origin.y-18, 70, 15)];
                    [popView setTextAlignment:NSTextAlignmentCenter];
                    [popView setBackgroundColor:[UIColor clearColor]];
                    [popView setAlpha:1.f];
                    popView.font=[UIFont fontWithName:@"Arial" size:10];
                    popView.textColor=[UIColor whiteColor];
                    popView.tag=100;
                    NSInteger v = mSlider.value+0.5;
                    [popView setText:[NSString stringWithFormat:@"%ld%@",(long)v,@"%"]];
                    
                    UILabel *lblItemClassId = [[UILabel alloc]init];
                    lblItemClassId.text = [item.itemId stringValue];

                    lblItemClassId.hidden = YES;
                    
                    [cell.contentView addSubview:mSlider];
                    [cell.contentView addSubview:imgV];
                    [cell.contentView addSubview:popView];
                    [cell.contentView addSubview:lblItemClassId];
                    
                    [imgV release];
                    [popView release];
                    [mSlider release];
                    [lblItemClassId release];
                    
                    ///------------------------------------------------------------------------------------------------------//
                    //for slider
                    
                    
                    //for complete functionality
                    ///------------------------------------------------------------------------------------------------------//
                    
                    UILabel *lblCompleted = [[UILabel alloc]initWithFrame:CGRectMake(440,15,70,15)];
                    [lblCompleted setTextAlignment:NSTextAlignmentCenter];
                    [lblCompleted setBackgroundColor:[UIColor clearColor]];
                    lblCompleted.font=[UIFont fontWithName:@"Arial" size:12];
                    lblCompleted.textColor=[UIColor darkGrayColor];
                    [lblCompleted setText:@"Complete"];
                    
                    UIButton *btnCheckBoxComplete = [[UIButton alloc]initWithFrame:CGRectMake(400, 2, 60, 40)];
                    if([item.itemId integerValue]>0)  //checkbox button tag is corresponding Item-Id
                    {
                        btnCheckBoxComplete.tag=[item.itemId integerValue];
                        [AppDelegate getAppdelegate]._isUniqueId=YES;
                    }
                    else
                    {
                        btnCheckBoxComplete.tag=[item.universal_Id integerValue];
                        [AppDelegate getAppdelegate]._isUniqueId=NO;
                    }
                    
                    if ([item.isCompelete integerValue] == 0)  //Task is incomplete
                    {
                        [btnCheckBoxComplete setImage:[UIImage imageNamed:@"img_uncheck.png"] forState:UIControlStateNormal];
                        btnCheckBoxComplete.titleLabel.text = @"uncheck";
                        
                    }
                    else
                    {
                        [btnCheckBoxComplete setImage:[UIImage imageNamed:@"img_check.png"] forState:UIControlStateNormal];
                        btnCheckBoxComplete.titleLabel.text = @"check";
                        
                    }
                    
                    [btnCheckBoxComplete addTarget:self action:@selector(actionCheckBoxComplete:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    [cell.contentView addSubview:lblCompleted];
                    [cell.contentView addSubview:btnCheckBoxComplete];
                    
                    
                    [lblCompleted release];
                    [btnCheckBoxComplete release];
                    
                    ///------------------------------------------------------------------------------------------------------//
                    //for complete functionality
                    
                    // Merits icon added on cell for Task Based Merits
                    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                    
                    // Only FOr Student
                    if ([profobj.type isEqualToString:@"Student"])
                    {
                        // Fetching Diary Item Users on the bases of DI id
                        NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:item.itemType_id];
                        if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                        {
                        NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:item.itemId];
#if DEBUG
                            NSLog(@"Before arrDIU %@",arrDIU);
#endif
                        arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                            NSLog(@"After arrDIU %@",arrDIU);
#endif
                        
                        for (DairyItemUsers *assinUserobj in arrDIU)
                        {
                            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                            {
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                if (lov)
                                {
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                        
                                        UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(760, 12, 25, 25)];
                                        [btnTaskBasedMerits setImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                                        [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                                        btnTaskBasedMerits.tag=[item.itemId integerValue];
                                        
                                        [cell.contentView addSubview:btnTaskBasedMerits];
                                        [btnTaskBasedMerits release];
                                    }
                                }
                            }
                        }
                    }
                    }
                }
                
                //for edit functionality
                ///------------------------------------------------------------------------------------------------------//
                
                UIButton *btnEdit = [[UIButton alloc]initWithFrame:CGRectMake(800, 12, 25, 25)];
                [btnEdit setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
                
                if([item.itemId integerValue]>0)
                {
                    btnEdit.tag=[item.itemId integerValue];
                    [AppDelegate getAppdelegate]._isUniqueId=YES;
                }
                else
                {
                    btnEdit.tag=[item.universal_Id integerValue];
                    [AppDelegate getAppdelegate]._isUniqueId=NO;
                }
                
                if ([item.type isEqualToString:@"Note"])
                    [btnEdit addTarget:self action:@selector(showAllNotesItemButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                else
                    [btnEdit addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.contentView addSubview:btnEdit];
                
                [btnEdit release];
                
                ///------------------------------------------------------------------------------------------------------//
                //for edit functionality
                
                if (!isComingFromDueToggle)
                {
                    if ([item.type isEqualToString:@"Assignment"])
                    {
                        iconImg.image = [UIImage imageNamed:@"dairygreenicon.png"];
                    }
                    else if ([item.type isEqualToString:@"Homework"])
                    {
                        iconImg.image = [UIImage imageNamed:@"homeredicon.png"];
                    }
                    else if([item.type isEqualToString:@"OutOfClass"] || [item.type isEqualToString:@"Out Of Class"]) {
                        if([item.isApproved integerValue] == 0)
                        {
                            iconImg.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([item.isApproved integerValue] == 1)
                        {
                            iconImg.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([item.isApproved integerValue] == 2)
                        {
                            iconImg.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([item.type isEqualToString:k_MERIT_CHECK] )
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:item.passTypeId];
                        if (lov)
                        {
                            
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImg.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }

                }
                
                iconImgView.backgroundColor = [UIColor clearColor];
                [iconImg release];
                [cellDescBgImgView release];
                [iconImgView release];
                [cellDescView release];
                [imgViewForLine release];
                
                
                [formeter release];
                
                
                if (isComingFromDueToggle)
                {
                    if (isInTodaySection)
                    {
                        if ([item.onTheDay integerValue] == 1)
                            lblForTime.text = @"On the Day";
                        else
                        {
                            
                            // After 12/24hr Format Support
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
                            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                            NSString *TimeString = [dateFormatter stringFromDate:item.dueDate];
                             lblForTime.text = TimeString;
                            // After 12/24hr Format Support
                            
                            //Before 12/24hr Format Support

                            
                            /*NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            dateFormatter.dateFormat = kDateFormatOnlyTimeToSaveFetchInFromDB;
                            NSDate *date = [dateFormatter dateFromString:item.dueTime];
                            
                            dateFormatter.dateFormat = kDateFormatToShowOnUI_hhmma;
                            NSString *TimeString = [dateFormatter stringFromDate:date];
                            
                            lblForTime.text = TimeString;*/
                            //Before 12/24hr Format Support

                        }
                    }
                    else
                    {
                        if (indexPath.section == 2)
                        {
                            NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc]init];
                            [shortDateFormatter setDateFormat:kDATEFORMAT_dd_MMM];
                            NSString *strShortDate = [shortDateFormatter stringFromDate:item.dueDate];
                            lblForTime.text = strShortDate;
                        }
                        else
                        {
                            NSString *strDifference = [self getDaysOfOverdueUpcoming:item.dueDate];
                            lblForTime.text = strDifference;
                        }
                    }
                    
                }
                else
                {
                    NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc]init];
                    [shortDateFormatter setDateFormat:kDATEFORMAT_dd_MMM];
                    NSString *strShortDate = [shortDateFormatter stringFromDate:item.dueDate];
                    lblForTime.text = strShortDate;
                }
                
                if (isComingFromDueToggle)
                {
                if([item.type isEqualToString:@"OutOfClass"] || [item.type isEqualToString:@"Out Of Class"]) {
                    if([item.isApproved integerValue] == 0)
                    {
                        iconImg.image=[UIImage imageNamed:@"passPendingicon.png"];
                    }
                    else  if([item.isApproved integerValue] == 1)
                    {
                        iconImg.image=[UIImage imageNamed:@"passApprovedicon.png"];
                    }
                    else  if([item.isApproved integerValue] == 2)
                    {
                        iconImg.image=[UIImage imageNamed:@"passElapsedicon.png"];
                    }
                }
                else if([item.type isEqualToString:k_MERIT_CHECK] )
                {
                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:item.passTypeId];
                    if (lov)
                    {
                        
                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                        {
                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                            iconImg.image=[FileUtils getImageFromPath:strPath];
                        }
                    }
                }
                else
                {
                    iconImg.image=[UIImage imageNamed:@"overdueicon.png"]; //clock icon in Due section or all Diaryitems
                }
                }
                
                
                UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(68, 7,110, 15)];
                lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                lblForTitle.backgroundColor=[UIColor clearColor];
                lblForTitle.lineBreakMode=NSLineBreakByCharWrapping;
                if (isComingFromDueToggle)
                    [lblForTitle setFrame:CGRectMake(68, 7, 110, 15)];
                else
                    [lblForTitle setFrame:CGRectMake(40, 7, 110, 15)];
                lblForTitle.text=item.title ;
                [cellDescView addSubview:lblForTitle];
                [lblForTitle release];
                
                

                
            }
            
            //Aman added -- EZMOBILE-25
            ///------------------------------------------------------------------------------------------------------//
            
            else
            {
                EventItem *objEvent = typeOfDiaryItem;
#if DEBUG
                NSLog(@"objEvent: %@",objEvent);
#endif
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                
                [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                
                [formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
                
                int x=20;
                int y=7;
                UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, y, 180, 28)];
                cellDescView.backgroundColor=[UIColor clearColor];
                cellDescView.clipsToBounds=YES;
                UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 28)];
                cellDescBgImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView *cellBgImgView=(UIImageView*)[cell.contentView viewWithTag:1];
                cellBgImgView.image=[UIImage imageNamed:@"tablecell.png"];
                
                UILabel * lblEventTitle=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,110, 15)];
                lblEventTitle.font=[UIFont fontWithName:@"Arial" size:12];
                lblEventTitle.backgroundColor=[UIColor clearColor];
                lblEventTitle.lineBreakMode=NSLineBreakByCharWrapping;
                lblEventTitle.text = objEvent.eventName;
                lblEventTitle.textColor = [UIColor colorWithRed:19/255.0f green:19/255.0f blue:252/255.0f alpha:1];
                
                NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc]init];
                [shortDateFormatter setDateFormat:kDATEFORMAT_dd_MMM];
                NSString *strShortDate = [shortDateFormatter stringFromDate:objEvent.startDate];
                
                UILabel * lblForShortDate = [[UILabel alloc]initWithFrame:CGRectMake(355, 15 ,115, 15)];
                lblForShortDate.font=[UIFont fontWithName:@"Arial" size:12];
                lblForShortDate.backgroundColor=[UIColor clearColor];
                
                if (isComingFromDueToggle) //Due Section Events
                {
                    lblForShortDate.text = [shortDateFormatter stringFromDate:objEvent.startDate];
                }
                else
                {
                    if (indexPath.section == 1)
                    {
                        lblForShortDate.text = strShortDate;
                    }
                    else
                    {
                        if ([objEvent.onTheDay integerValue]==1)
                        {
                            lblForShortDate.text = @"On the Day";
                        }
                        else
                        {
                            // After 12/24hr Format Support
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
                            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                            NSString *TimeString = [dateFormatter stringFromDate:objEvent.startDate];
                            lblForShortDate.text = TimeString;
                            // After 12/24hr Format Support
                            
                            //Before 12/24hr Format Support
                        }
                    }
                }
                
                [cell.contentView addSubview:lblForShortDate];
                [lblForShortDate release];
                [shortDateFormatter release];
                
                UILabel *lblForTime = (UILabel*)[cell.contentView viewWithTag:2];
                
                if (isComingFromDueToggle)
                {
                    if (isInTodaySection)
                    {
                        if ([objEvent.onTheDay integerValue] == 1)
                            lblForTime.text = @"On the Day";
                        else
                        {
                            // After 12/24hr Format Support
                            
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
                            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
                            NSString *TimeString = [dateFormatter stringFromDate:objEvent.endDate];
                            lblForTime.text = TimeString;
                            // After 12/24hr Format Support
                            
                            //Before 12/24hr Format Support

                         }
                    }
                    else
                    {
                        if (indexPath.section == 2)
                        {
                            NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc]init];
                            [shortDateFormatter setDateFormat:kDATEFORMAT_dd_MMM];
                            NSString *strShortDate = [shortDateFormatter stringFromDate:objEvent.startDate];
                            lblForTime.text = strShortDate;
                        }
                        else
                        {
                            NSString *strDifference = [self getDaysOfOverdueUpcoming:objEvent.startDate];
                            lblForTime.text = strDifference;
                        }
                    }
                }
                else
                {
                    if (indexPath.section == 0)
                    {
                        lblForTime.text = strShortDate;
                    }
                    else
                    {
                        lblForTime.text = strShortDate;
                    }
                    
                }
                
                
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 180, 28);
                popUpButton.backgroundColor=[UIColor clearColor];
                [popUpButton addTarget:self action:@selector(showAllEventsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                popUpButton.userInteractionEnabled = NO;
                
                
                //Aman added -- EZMOBILE-25
                if([objEvent.item_Id integerValue]>0)
                {
                    popUpButton.tag=[objEvent.item_Id integerValue];
                    popUpButton.titleLabel.text=@"1";
                    // popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"1",assinegdDate];
                    [AppDelegate getAppdelegate]._isUniqueId=YES;
                }
                else{
                    popUpButton.tag=[objEvent.universal_id integerValue];
                    popUpButton.titleLabel.text=@"0";
                    popUpButton.userInteractionEnabled = NO;
                    //popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"0",assinegdDate];
                    [AppDelegate getAppdelegate]._isUniqueId=NO;
                    
                }
                
                //for edit functionality
                ///------------------------------------------------------------------------------------------------------//
                
                UIButton *btnEdit = [[UIButton alloc]initWithFrame:CGRectMake(800, 12, 25, 25)];
                [btnEdit setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
                
                if([objEvent.item_Id integerValue]>0)
                {
                    btnEdit.tag=[objEvent.item_Id integerValue];
                    btnEdit.titleLabel.text=@"1";
                    [AppDelegate getAppdelegate]._isUniqueId=YES;
                }
                else{
                    btnEdit.tag=[objEvent.universal_id integerValue];
                    btnEdit.titleLabel.text=@"0";
                    [AppDelegate getAppdelegate]._isUniqueId=NO;
                    
                }
                
                [btnEdit addTarget:self action:@selector(showAllEventsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btnEdit];
                
                [btnEdit release];
                
                
                ///------------------------------------------------------------------------------------------------------//
                //for edit functionality
                
                [cellDescView addSubview:cellDescBgImgView];
                [cellDescView addSubview:lblEventTitle];
                [cellDescView addSubview:popUpButton];
                [cell.contentView addSubview:cellDescView];
                [cell.contentView bringSubviewToFront:lblForTime];
                
                [cellDescView release];
                [cellDescBgImgView release];
                [lblEventTitle release];
                
            }
            
            ///--------------------------------------------------------------------------------------------//
            
            return cell;
        }
        else
        {
            if (cell == nil)
            {
                nib = [[NSBundle mainBundle] loadNibNamed:@"HomeVCMainCustomCellForNews" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
            
            UIImageView *cellBgImgVForNews=(UIImageView*)[cell.contentView viewWithTag:1];
            UILabel *lblForUpText = (UILabel*)[cell.contentView viewWithTag:2];
            UILabel *lblForDownText = (UILabel*)[cell.contentView viewWithTag:3];
            UILabel *lblForTime = (UILabel*)[cell.contentView viewWithTag:5];
            UILabel *lblForName = (UILabel*)[cell.contentView viewWithTag:6];
            NSArray *resultaArr= [[_arr objectAtIndex:indexPath.section]objectForKey:@"result"];
            
            id model = [resultaArr objectAtIndex:indexPath.row];
            
            if([model isKindOfClass:[Annoncements class]])
            {
                Annoncements *item=(Annoncements*)model;
                NSString *plaintext = [item.details stringByConvertingHTMLToPlainText];
                lblForDownText.text = plaintext ;
                lblForName.text=item.created_by;
                lblForUpText.text=@"Announcement";
                
                
                [formatter setDateFormat:@"dd-MM-YYYY"];
#if DEBUG
                NSLog(@"[formatter stringFromDate:item.created_time] %@",[formatter stringFromDate:item.created_time]);
#endif
                
                if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:item.created_time]]){
                    [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                    lblForTime.text=[formatter stringFromDate:item.created_time];
                }
                else{
                    [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                    lblForTime.text=[formatter stringFromDate:item.created_time];
                }
                
                lblForName.hidden=NO;
            }
            else  if([model isKindOfClass:[News class]])
            {
                News *item=(News*)model;
                NSString *plaintext = [item.title stringByConvertingHTMLToPlainText];
                lblForDownText.text = plaintext ;
                
                lblForUpText.text=@"News";
                
                [formatter setDateFormat:@"dd-MM-YYYY"];
                
                if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:item.created_time]]){
                    [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                    lblForTime.text=[formatter stringFromDate:item.created_time];
                }
                else{
                    [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                    lblForTime.text=[formatter stringFromDate:item.created_time];
                }
                
                lblForName.hidden=YES;
            }
            
            
            cellBgImgVForNews.image=[UIImage imageNamed:@"news&announcementcelldhi.png"];
            //lblForUpText.text=@"Math Test at 3 pm";
            //lblForTime.text=@"2 sec ago";
            if (formatter) {
                [formatter release];
            }
            
            
            return cell;
        }
        
    }
    
    return nil;
    
    //    return cell;
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_newsAndAnnouncementBtn.selected){
        if (tableView==_tableForMainView) {
            
            NSArray *resultaArr= [[_arr objectAtIndex:indexPath.section]objectForKey:@"result"];
            // Flurry Crash :
            if([resultaArr count]>=indexPath.row)
            {
                id model = [resultaArr objectAtIndex:indexPath.row];
                
                MainViewController *mainVC= (MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
                [mainVC selectInbox:model];

            }
            
        }
        
    }
}

#pragma mark
#pragma mark ButtonAction

- (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}


//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//

- (void)addItemButtonAction
{
    [self dayOfWeekFromDate:0];
    
    if (isComingFromDueToggle)
        [self refillMainTableArraysForDueToggle];
    else
        [self refillMainTableArraysForAssignedToggle];
}
//-----------------------------------------------------------------------------------------------//
//Aman added -- Glance Three Sections



//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//
-(void) refillMainTableArraysForDueToggle
{
    [marrForMainTableOverDue removeAllObjects];
    [marrForMainTableToday removeAllObjects];
    [marrForMainTableUpcoming removeAllObjects];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSMutableArray  *arrForDateDescending=[[NSMutableArray alloc]init];
    
    [self._curentDateFormeter setDateFormat:@"yyyy-dd-MM"];
    NSDate *toDay=[self._curentDateFormeter dateFromString:[self._curentDateFormeter stringFromDate:[NSDate date]]];
    for(int i=14;i>=-15;i--)
    {
        [arrForDateDescending addObject:[toDay dateByAddingTimeInterval:24*60*60*i]];
    }
    
    NSArray* arrForDate = [[arrForDateDescending reverseObjectEnumerator] allObjects];
    //For Due DiaryItems
    for (int  i=0; i<arrForDate.count; i++)
    {
        NSMutableArray  *arrForItems = [[NSMutableArray alloc]init];
        NSMutableArray  *arrForEvents = [[NSMutableArray alloc]init];
        
        NSDate *dayDate = [arrForDate objectAtIndex:i];
        
        NSDateFormatter *formatterDDMMYYYY = [[[NSDateFormatter alloc] init] autorelease];
        [formatterDDMMYYYY setDateFormat:@"MM/dd/yyyy"];
        
        NSString *dateStringDayDate = [formatterDDMMYYYY stringFromDate:dayDate] ;
        NSDate *formattedDayDate = [formatterDDMMYYYY dateFromString:dateStringDayDate] ;
        
        NSString *dateStringTodayDate = [formatterDDMMYYYY stringFromDate:[NSDate date]] ;
        NSDate *formattedTodayDate = [formatterDDMMYYYY dateFromString:dateStringTodayDate] ;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSArray *daySymbols  = [formatter weekdaySymbols];
        
        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:dayDate];
        NSString *weekday=[NSString stringWithFormat:@"%@",[daySymbols objectAtIndex:[components weekday]-1]];
        [formatter release];
        
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
        df1.dateStyle = NSDateFormatterMediumStyle;
        [df1 setDateFormat:@"dd MMM yy"];
        NSString * assinegdDate1=[df1 stringFromDate:[arrForDate objectAtIndex:i]];
        [df1 release];

        
        //------ASSIGNMENTS AND HOMEWORK FOR THE DATE------//
        NSArray  *DiaryItemArr=[[Service sharedInstance]getStoredAllDueItemData:[arrForDate objectAtIndex:i ]];
        
        for(Item *diary in DiaryItemArr)
        {
            if(![arrForItems containsObject:diary])
            {
                if (!([diary.type isEqualToString:@"Note"]|| [[diary.type lowercaseString] isEqualToString:@"merits"] || [[diary.type lowercaseString] isEqualToString:@"merit"])) //Notes not included in Due Section
                {
                    //Aman added -- EZTEST 863
                    
                    if ([formattedDayDate compare: formattedTodayDate] == NSOrderedAscending) //items in overdue section
                    {
                        if (![diary.progress isEqualToString:@"100"]) //item should be seen in OVERDUE if it is 100% complete
                        {
                            [arrForItems addObject:diary];
                        }
                    }
                    else
                    {
                        [arrForItems addObject:diary];
                    }
                }
            }
        }
        
        if ([DiaryItemArr count]>0) //Sort Diary Items
        {
            NSSortDescriptor *sortBasedOnDueTime;
            sortBasedOnDueTime = [NSSortDescriptor sortDescriptorWithKey:@"dueTime"ascending:YES];
            NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortBasedOnDueTime];
            DiaryItemArr =[NSArray arrayWithArray:[arrForItems  sortedArrayUsingDescriptors:sortDescriptorArr]];
        }
        
        //------------ Out Of Class Pass Logic Starts -----------------
        //correction
        //
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        //NSMutableArray *marrElapsedPass = [[NSMutableArray alloc] init];
        
        for (Item *itemObj in DiaryItemArr)
        {
            [dict setObject:itemObj.itemId forKey:@"AppId"];
            [dict setObject:itemObj.ipAdd forKey:@"IpAddress"];
            [dict setObject:itemObj.itemId forKey:@"Id"];
            if ([itemObj.type isEqualToString:@"OutOfClass"] || [itemObj.type isEqualToString:@"Out Of Class"] )
            {
                NSInteger approvedPassStatus = [self getPassStatus:itemObj];
                BOOL isElapsed = NO;
                
                if ( [itemObj.isApproved integerValue] == 0 )
                {
                    switch (approvedPassStatus)
                    {
                        case passElapsed:
                        {
                            isElapsed = YES;
                            break;
                        }
                    }
                }
                else if ( [itemObj.isApproved integerValue] == 1)
                {
                    switch (approvedPassStatus)
                    {
                        case passElapsed:
                        {
                            isElapsed = YES;
                            break;
                        }
                    }
                }
                
                if ( isElapsed )
                {
                    // set isApproved to 2 as it is elapsed...
                    [dict setObject:@"2" forKey:@"pass_status"];
                    
                    [[Service sharedInstance]updateLocalPass:dict];
                    
                    itemObj.isApproved = [NSNumber numberWithInteger:2];
                    
                }
            }
        }
        
        // ----------- Out Of Class Pass Logic Ends -------------


        NSArray *eventItemArr = [[NSArray alloc]init];
        //--------------EVENTS FOR THE DATE--------------//
        if (([formattedDayDate compare: formattedTodayDate] == NSOrderedDescending) || ([formattedDayDate compare: formattedTodayDate] == NSOrderedSame)) //include events only due today and later
        {
           eventItemArr = [[Service sharedInstance] getStoredAllEventItemData:[arrForDate objectAtIndex:i]];
            if ([eventItemArr count]>0)
            {
                for (EventItem *objEvent in eventItemArr)
                {
                    [arrForEvents addObject:objEvent];
                }
            }
            
            if ([eventItemArr count]>0)  //Sort Events --On the Day events on the top
            {
                NSMutableArray *marrOntheDay = [[NSMutableArray alloc]init];
                NSMutableArray *marrTobeSorted = [[NSMutableArray alloc]init];
                NSArray *eventItemArr1 = [[NSArray alloc]init];
                
                for (int k=0; k<[eventItemArr count]; k++)
                {
                    EventItem *objE = [eventItemArr objectAtIndex:k];
                    
                    if ([objE.onTheDay integerValue] == 1)
                        [marrOntheDay addObject:objE];
                    else
                        [marrTobeSorted addObject:objE];
                }
                NSSortDescriptor *sortBasedOnDueTime;
                sortBasedOnDueTime = [NSSortDescriptor sortDescriptorWithKey:@"endTime"ascending:YES];
                NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortBasedOnDueTime];
                eventItemArr1 =[NSArray arrayWithArray:[marrTobeSorted  sortedArrayUsingDescriptors:sortDescriptorArr]];
                eventItemArr = [marrOntheDay arrayByAddingObjectsFromArray:eventItemArr1];
            }

        }
        
        //--------------MERGE ITEMS AND EVENTS--------------//
        NSMutableArray *allItemsArr = [[NSMutableArray alloc]init];
        allItemsArr = [[DiaryItemArr arrayByAddingObjectsFromArray:eventItemArr] mutableCopy];
        
        //--------------CREATE A DICT FOR EACH DATE --------------//
        
        if ([allItemsArr count]>0)
        {
            
            for (int j=0; j<[allItemsArr count]; j++)
            {
                NSMutableDictionary *secDict = nil;
                secDict = [[NSMutableDictionary alloc]init];
                [secDict setObject:weekday forKey:@"day"];
                [secDict setObject:assinegdDate1 forKey:@"date"];
                [secDict setObject:[allItemsArr objectAtIndex:j] forKey:@"result"];
                
                
                //--------------ADD THE DICT TO DUE/ UPCOMING/ TODAY ARRAY--------------//
                if ([formattedDayDate compare: formattedTodayDate] == NSOrderedSame)
                {
                    [marrForMainTableToday addObject:secDict];
                }
                else if ([formattedDayDate compare: formattedTodayDate] == NSOrderedAscending)
                {
                    [marrForMainTableOverDue addObject:secDict];
                }
                else if ([formattedDayDate compare: formattedTodayDate] == NSOrderedDescending)
                {
                    [marrForMainTableUpcoming addObject:secDict];
                }
            }
        }
    }
    
    //--------------Print the three arrays--------------//
    
    for (int i=0; i<[marrForMainTableToday count]; i++)
    {
        NSMutableDictionary *mDict = nil;
        mDict = [marrForMainTableToday objectAtIndex:i];
    }
    for (int i=0; i<[marrForMainTableOverDue count]; i++)
    {
        NSMutableDictionary *mDict = nil;
        mDict = [marrForMainTableOverDue objectAtIndex:i];
    }
    for (int i=0; i<[marrForMainTableUpcoming count]; i++)
    {
        NSMutableDictionary *mDict = nil;
        mDict = [marrForMainTableUpcoming objectAtIndex:i];
    }
    
    
    [_tableForMainView reloadData];
    [self updateDueitemHome];
}


-(void) refillMainTableArraysForAssignedToggle
{
    [marrForMainTableOverDue removeAllObjects];
    [marrForMainTableToday removeAllObjects];
    [marrForMainTableUpcoming removeAllObjects];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSMutableArray  *arrForDateDescending=[[NSMutableArray alloc]init];
    
    [self._curentDateFormeter setDateFormat:@"yyyy-dd-MM"];
    NSDate *toDay=[self._curentDateFormeter dateFromString:[self._curentDateFormeter stringFromDate:[NSDate date]]];
    for(int i=1;i>=-15;i--)
    {
        [arrForDateDescending addObject:[toDay dateByAddingTimeInterval:24*60*60*i]];
    }
    
    NSArray* arrForDate = [[arrForDateDescending reverseObjectEnumerator] allObjects];
    
    //For Due DiaryItems
    for (int  i=0; i<arrForDate.count; i++)
    {
        NSMutableArray  *arrForItems = [[NSMutableArray alloc]init];
        NSMutableArray  *arrForEvents = [[NSMutableArray alloc]init];
        
        NSDate *dayDate = [arrForDate objectAtIndex:i];
        
        NSDateFormatter *formatterDDMMYYYY = [[[NSDateFormatter alloc] init] autorelease];
        [formatterDDMMYYYY setDateFormat:@"MM/dd/yyyy"];
        
        NSString *dateStringDayDate = [formatterDDMMYYYY stringFromDate:dayDate] ;
        NSDate *formattedDayDate = [formatterDDMMYYYY dateFromString:dateStringDayDate] ;
        
        NSString *dateStringTodayDate = [formatterDDMMYYYY stringFromDate:[NSDate date]] ;
        NSDate *formattedTodayDate = [formatterDDMMYYYY dateFromString:dateStringTodayDate] ;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSArray *daySymbols  = [formatter weekdaySymbols];
        
        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:dayDate];
        NSString *weekday=[NSString stringWithFormat:@"%@",[daySymbols objectAtIndex:[components weekday]-1]];
        [formatter release];
        
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
        df1.dateStyle = NSDateFormatterMediumStyle;
        [df1 setDateFormat:@"dd MMM yy"];
        NSString * assinegdDate1=[df1 stringFromDate:[arrForDate objectAtIndex:i]];
        [df1 release];

        
        //------ASSIGNMENTS AND HOMEWORK FOR THE DATE------//
        //        NSArray  *DiaryItemArr=[[Service sharedInstance]getStoredAllDueItemData:[arrForDate objectAtIndex:i ]];
        NSArray  *DiaryItemArr= [[Service sharedInstance] getStoredAllItemData:[arrForDate objectAtIndex:i ]]; //Assigned change
        
        for(Item *diary in DiaryItemArr)
        {
            if(![arrForItems containsObject:diary])
            {
                
            //Aman added -- EZTEST 863 ---------------------------------------------------------------------------
                
                //Assigned change
                if ([diary.type isEqualToString:@"Note"]) //Notes does not have progress value for 100% completion
                {
                    [arrForItems addObject:diary];
                }
                else
                {
                    if ([formattedDayDate compare: formattedTodayDate] == NSOrderedAscending)
                    {
                        if (![diary.progress isEqualToString:@"100"]) //item should be seen in previous if it is not 100% complete
                        {
                            [arrForItems addObject:diary];
                        }
                    }
                    else
                    {
                        [arrForItems addObject:diary];
                    }
                }
                
            //Aman added -- EZTEST 863 ---------------------------------------------------------------------------
                
            }
        }
        
        if ([DiaryItemArr count]>0) //Sort Diary Items
        {
            NSSortDescriptor *sortBasedOnDueTime;
            sortBasedOnDueTime = [NSSortDescriptor sortDescriptorWithKey:@"assignedTime"ascending:YES]; //Assigned change
            NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortBasedOnDueTime];
            DiaryItemArr =[NSArray arrayWithArray:[arrForItems  sortedArrayUsingDescriptors:sortDescriptorArr]];
        }
        
        NSArray *eventItemArr = [[NSArray alloc]init];
        
        //--------------EVENTS FOR THE DATE--------------//
        if (([formattedDayDate compare: formattedTodayDate] == NSOrderedDescending) || ([formattedDayDate compare: formattedTodayDate] == NSOrderedSame)) //include events only due today and later
        {
            eventItemArr = [[Service sharedInstance] getStoredAllEventItemData:[arrForDate objectAtIndex:i]];
            if ([eventItemArr count]>0)
            {
                for (EventItem *objEvent in eventItemArr)
                {
                    [arrForEvents addObject:objEvent];
                }
            }
            
            if ([eventItemArr count]>0)  //Sort Events --On the Day events on the top
            {
                NSMutableArray *marrOntheDay = [[NSMutableArray alloc]init];
                NSMutableArray *marrTobeSorted = [[NSMutableArray alloc]init];
                NSArray *eventItemArr1 = [[NSArray alloc]init];
                
                for (int k=0; k<[eventItemArr count]; k++)
                {
                    EventItem *objE = [eventItemArr objectAtIndex:k];
                    
                    if ([objE.onTheDay integerValue] == 1)
                        [marrOntheDay addObject:objE];
                    else
                        [marrTobeSorted addObject:objE];
                }
                NSSortDescriptor *sortBasedOnDueTime;
                sortBasedOnDueTime = [NSSortDescriptor sortDescriptorWithKey:@"endTime"ascending:YES];
                NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortBasedOnDueTime];
                eventItemArr1 =[NSArray arrayWithArray:[marrTobeSorted  sortedArrayUsingDescriptors:sortDescriptorArr]];
                eventItemArr = [marrOntheDay arrayByAddingObjectsFromArray:eventItemArr1];
            }
        }
        //--------------MERGE ITEMS AND EVENTS--------------//
        NSMutableArray *allItemsArr = [[NSMutableArray alloc]init];
        allItemsArr = [[DiaryItemArr arrayByAddingObjectsFromArray:eventItemArr] mutableCopy];
        
        //--------------CREATE A DICT FOR EACH DATE --------------//
        
        if ([allItemsArr count]>0)
        {
            
            for (int j=0; j<[allItemsArr count]; j++)
            {
                NSMutableDictionary *secDict = nil;
                secDict = [[NSMutableDictionary alloc]init];
                [secDict setObject:weekday forKey:@"day"];
                [secDict setObject:assinegdDate1 forKey:@"date"];
                [secDict setObject:[allItemsArr objectAtIndex:j] forKey:@"result"];
                
                
                //--------------ADD THE DICT TO DUE/ UPCOMING/ TODAY ARRAY--------------//
                if ([formattedDayDate compare: formattedTodayDate] == NSOrderedSame)
                {
                    [marrForMainTableToday addObject:secDict];
                }
                else if ([formattedDayDate compare: formattedTodayDate] == NSOrderedAscending)
                {
                    [marrForMainTableOverDue addObject:secDict];
                }
            }
        }
    }
    
    //--------------Print the three arrays--------------//
    
    for (int i=0; i<[marrForMainTableToday count]; i++)
    {
        NSMutableDictionary *mDict = nil;
        mDict = [marrForMainTableToday objectAtIndex:i];
    }
    for (int i=0; i<[marrForMainTableOverDue count]; i++)
    {
        NSMutableDictionary *mDict = nil;
        mDict = [marrForMainTableOverDue objectAtIndex:i];
    }
    for (int i=0; i<[marrForMainTableUpcoming count]; i++)
    {
        NSMutableDictionary *mDict = nil;
        mDict = [marrForMainTableUpcoming objectAtIndex:i];
    }
    
    [_tableForMainView reloadData];
    [self updateDueitemHome];
}

//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//
- (NSDate *)convertToLocalDateFromGMTDate:(NSDate *)gmtDate
{
    NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    //Create the date assuming the given string is in GMT
    dtFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [dtFormatter dateFromString:[dtFormatter stringFromDate:gmtDate]];
    
    //Create a date string in the local timezone
    dtFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSString *localDateString = [dtFormatter stringFromDate:date];
    
    return [dtFormatter dateFromString:localDateString];
}

- (NSDate *)convertToGMTDateFromLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
     NSDate *gmtDate = [localDate dateByAddingTimeInterval:-timeZoneOffset]; // NOTE the "-" sign!
     
    return gmtDate;
}


- (void)dayOfWeekFromDate:(int)week {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSMutableArray  *arrForDate=[[NSMutableArray alloc]init];
    
    [self._curentDateFormeter setDateFormat:@"yyyy-dd-MM"];
    
    NSDate *toDay= [NSDate date] ;
    
    NSDateFormatter *formtr = [[NSDateFormatter alloc]init];
    [formtr setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    for(int i=14;i>=-15;i--)
    {
        [arrForDate addObject:[toDay dateByAddingTimeInterval:24*60*60*i]];

    }
    
    [_arr removeAllObjects];
    
    for (int  i=0; i<arrForDate.count; i++)
    {
        //-----------------------------------------------------------------------------------------------------------------//
        //Array for News and Announcements
        
        NSDate  *currentDate =  [self convertToGMTDateFromLocalDate:[arrForDate objectAtIndex:i]];
        
        NSString *currentDateString = [formtr stringFromDate:currentDate];
        
        NSArray * arrForAnn= [[Service sharedInstance] getStoredAllIAnnouncement:currentDateString];
        NSArray * arrForNews=[[Service sharedInstance] getStoredAllINewsForWithDate:currentDateString];
        NSArray *  itemArr =[arrForAnn arrayByAddingObjectsFromArray:arrForNews];
        //
#if DEBUG
        NSLog(@"itemArr====%lu",(unsigned long)itemArr.count);
#endif
        
        if(itemArr.count>0){
            NSSortDescriptor *sortByName;
            
            sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:NO];
            NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
            itemArr =[NSMutableArray arrayWithArray:[itemArr sortedArrayUsingDescriptors:sortDescriptorArr]];
            NSMutableDictionary *secDict=[[NSMutableDictionary alloc]init];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSArray *daySymbols = [formatter weekdaySymbols];

            [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
            
            NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:currentDate];
            
            
            NSString *weekday=[NSString stringWithFormat:@"%@",[daySymbols objectAtIndex:[components weekday]-1]];
            
            
            NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
            df1.dateStyle = NSDateFormatterMediumStyle;
            [df1 setDateFormat:@"dd MMM yy"];

            NSString * assinegdDate1=[df1 stringFromDate:[arrForDate objectAtIndex:i]];
            
            [df1 release];
            [formatter release];
            [secDict setObject:weekday forKey:@"day"];
            [secDict setObject:assinegdDate1 forKey:@"date"];
            [secDict setObject:itemArr forKey:@"result"];
            
            [_arr addObject:secDict];
            
            [secDict release];
        }
    }
    
    [arrForDate release];
    
    [_tableForMainView reloadData];
    [self updateDueitemHome];
}

//-----------------------------------------------------------------------------------------------//
//Aman added -- Glance Three Sections

-(void)updateDueitemHome
{
#if DEBUG
    NSLog(@"updateDueitemHome _arr %@",_arr);
#endif
    
    int read=0;
    for(NSDictionary *dict in _arr){
        NSArray *arr=[dict objectForKey:@"result"];
        for(id modle in arr){
            if([modle isKindOfClass:[News class]]){
                News *newNewz=(News*)modle;
                if([newNewz.isReadNews integerValue]==0){
                    read=read+1;
                }
            }
            else    if([modle isKindOfClass:[Annoncements class]]){
                Annoncements *newNewz=(Annoncements*)modle;
                if([newNewz.isReadAnn integerValue]==0){
                    read=read+1;
                }
            }
            
        }
    }
#if DEBUG
    NSLog(@" read %d",read);
#endif
    
    _newsAndAnnoCountLbl.text=[NSString stringWithFormat:@"%d",read];
    
    if(_glanceBtn.selected)
    {
        [_arrForDue removeAllObjects];
        
        NSArray  *item1=[[Service sharedInstance]getStoredAllItemDataInApp];
        NSCalendar *calender = [NSCalendar currentCalendar];
        NSDateFormatter *formeter=[[[NSDateFormatter alloc]init] autorelease];
        [formeter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        
        for(Item *diaryItem in item1){
            
            if(![diaryItem.type isEqualToString:@"Note"])
            {
                if([diaryItem.onTheDay integerValue]==0)
                {
                    NSDateComponents *dayComponentsIsl = [calender
                                                          components:(NSMinuteCalendarUnit |NSHourCalendarUnit |NSSecondCalendarUnit) fromDate:diaryItem.dueDate];
                    
                    NSArray *arr=[diaryItem.dueTime componentsSeparatedByString:@":"];
                    [dayComponentsIsl setHour:[[arr objectAtIndex:0] integerValue]];
                    [dayComponentsIsl setMinute:[[arr objectAtIndex:1] integerValue]];
                    NSDate *d2=[calender dateByAddingComponents:dayComponentsIsl toDate:diaryItem.dueDate options:0];
                    int timeDiff=(int)[d2 timeIntervalSinceDate:[NSDate date]];
                    if(timeDiff<0)
                    {
                        if( ![[AppDelegate getAppdelegate].dueArray containsObject:diaryItem])
                        {
                            if ([diaryItem.isCompelete integerValue]==0) {
                                [_arrForDue addObject:diaryItem];
                            }
                        }
                    }
                }
                else
                {
                    NSDateComponents *dayComponentsIsl = [calender
                                                          components:(NSMinuteCalendarUnit |NSHourCalendarUnit |NSSecondCalendarUnit) fromDate:diaryItem.dueDate];
                    
                    NSDate *d2=[calender dateByAddingComponents:dayComponentsIsl toDate:diaryItem.dueDate options:0];
                    int timeDiff=(int)[d2 timeIntervalSinceDate:[formeter dateFromString:[formeter stringFromDate:[NSDate date]]]];
                    if(timeDiff<0)
                    {
                        if( ![[AppDelegate getAppdelegate].dueArray containsObject:diaryItem])
                        {
                            if ([diaryItem.isCompelete integerValue]==0) {
                                [_arrForDue addObject:diaryItem];
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    if(_arrForDue.count>0){
        Item *diary=[_arrForDue objectAtIndex:0];
        
        //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
        MyClass *class1=nil;
        
        class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diary.class_id]];
        
        if(class1.code!=nil)
        {
            _imgVForIconBg.backgroundColor=[AppHelper colorFromHexString:class1.code];
            _ImgVForLeft.backgroundColor=[AppHelper colorFromHexString:class1.code];
        }
        else{
            _imgVForIconBg.backgroundColor=[UIColor grayColor];
            _ImgVForLeft.backgroundColor=[UIColor grayColor];
        }
        //--------- By Mitul ---------//

        _imgVForIconBg.contentMode=UIViewContentModeScaleAspectFit;
        [_imgVForIconBg.layer setMasksToBounds:YES];
        [_imgVForIconBg.layer setCornerRadius:2.0f];
        [_imgVForIconBg.layer setBorderWidth:1.0];
        [_imgVForIconBg.layer setBorderColor:[[UIColor clearColor] CGColor]];
        
        _ImgVForLeft.contentMode=UIViewContentModeScaleAspectFit;
        [_ImgVForLeft.layer setMasksToBounds:YES];
        [_ImgVForLeft.layer setCornerRadius:2.0f];
        [_ImgVForLeft.layer setBorderWidth:1.0];
        [_ImgVForLeft.layer setBorderColor:[[UIColor clearColor] CGColor]];
        
        [_tableForProfilView reloadData];
        _viewForDueItem.hidden=NO;
    }
    else{
        _viewForDueItem.hidden=YES;
    }
    
    if(_glanceBtn.selected){
        if(_arrForMainTable.count>0){
            [self._curentDateFormeter setDateFormat:@"dd MMM yy"];
            NSInteger sec=0;
            for(NSDictionary *dict in _arrForMainTable){
                if([[dict objectForKey:@"date"]isEqualToString:[self._curentDateFormeter stringFromDate:[NSDate date]]]){
                    sec=[_arrForMainTable indexOfObject:dict];
                }
            }
            NSIndexPath* ip = [NSIndexPath indexPathForRow:0 inSection:sec];
            [_tableForMainView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    }
    else{
        [self._curentDateFormeter setDateFormat:@"dd MMM yy"];
        if(_arr.count>0){
            NSInteger sec=0;
            for(NSDictionary *dict in _arr){
                if([[dict objectForKey:@"date"]isEqualToString:[self._curentDateFormeter stringFromDate:[NSDate date]]]){
                    sec=[_arr indexOfObject:dict];
                }
            }
            NSIndexPath* ip = [NSIndexPath indexPathForRow:0 inSection:sec];
            [_tableForMainView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
}
#pragma mark- Glance Button Action
- (IBAction)glanceButtonAction:(id)sender {
    
    [self.lblDue setHidden:NO];
    [self.lblAssigned setHidden:NO];
    [self.switctDueAssign setHidden:NO];
    [self.lblSortBy setHidden:NO];
    
    if(_glanceBtn.selected==NO){
        _glanceBtn.selected=YES;
        _newsAndAnnouncementBtn.selected=NO;
        [_arr removeAllObjects];
        [self addItemButtonAction];
        
        [self._tableForMainView reloadData];
    }
    else{
        [self._curentDateFormeter setDateFormat:@"dd MMM yy"];
        if(_arrForMainTable.count>0){
            NSInteger sec=0;
            for(NSDictionary *dict in _arrForMainTable){
                if([[dict objectForKey:@"date"]isEqualToString:[self._curentDateFormeter stringFromDate:[NSDate date]]]){
                    sec=[_arrForMainTable indexOfObject:dict];
                }
            }
            NSIndexPath* ip = [NSIndexPath indexPathForRow:0 inSection:sec];
            [_tableForMainView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
}
#pragma mark- News And Announcement Button Action
- (IBAction)newsAndAnnouncementBtnAction:(id)sender {
    
    [self.lblDue setHidden:YES];
    [self.lblAssigned setHidden:YES];
    [self.switctDueAssign setHidden:YES];
    [self.lblSortBy setHidden:YES];
    
    if(_newsAndAnnouncementBtn.selected==NO){
        _glanceBtn.selected=NO;
        _newsAndAnnouncementBtn.selected=YES;
        
        [self addItemButtonAction];
        [self._tableForMainView reloadData];
        
    }
    else{
        [self._curentDateFormeter setDateFormat:@"dd MMM yy"];
        if(_arr.count>0){
            NSInteger sec=0;
            for(NSDictionary *dict in _arr){
                if([[dict objectForKey:@"date"]isEqualToString:[self._curentDateFormeter stringFromDate:[NSDate date]]]){
                    sec=[_arr indexOfObject:dict];
                }
            }
            NSIndexPath* ip = [NSIndexPath indexPathForRow:0 inSection:sec];
            [_tableForMainView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    }
    
}

- (IBAction)crossBtuttonAction:(id)sender {
    
    UIButton* button=(UIButton*)sender;
    UITableViewCell* cell=(UITableViewCell*)[[[button superview] superview]superview];
    NSIndexPath *indexpath=[self._tableForProfilView indexPathForCell:cell];
    
    [self updateDues:indexpath.row];
    
}
-(void)updateDues:(NSInteger)index
{
    Item *item=nil;
    if (_arrForDue.count>0) {
        item=[_arrForDue objectAtIndex:index];
    }
    
    BOOL isExit=NO;
    for(Item *items in _arrForDue)
    {
        if([item isEqual:items])
        {
            if( [[AppDelegate getAppdelegate].dueArray containsObject:item])
            {
                isExit=YES;
            }
            else
            {
                isExit=NO;
            }
        }
    }
    
    if(isExit)
    {
        if (item)
        {
            [_arrForDue removeObject:item];
        }
    }
    else
    {
        if (item) {
            [[AppDelegate getAppdelegate].dueArray addObject:item];
            [_arrForDue removeObject:item];
        }
    }
    if(_arrForDue.count>0){
        
        _viewForDueItem.hidden=NO;
    }
    else{
        _viewForDueItem.hidden=YES;
    }
    [self updateDueitemHome];
}

- (IBAction)viewAllButtonAction:(id)sender
{
    if(_tableForProfilView.hidden){
        _lblForViewAll.text=[NSString stringWithFormat:@"%@ \n %@",@"HIDE",@"ALL"];
        _btnForViewAll.selected=YES;
        _tableForProfilView.hidden=NO;
        [_tableForProfilView reloadData];
        
        
        /////////////
        //for update view all items
        _glanceBtn.selected=YES;
        _newsAndAnnouncementBtn.selected=NO;
        //     [_arrForMainTable removeAllObjects];
        [self addItemButtonAction];
        [self._tableForMainView reloadData];
        [self updateDueitemHome];
        
        ////////////////
    }
    else{
        _lblForViewAll.text=[NSString stringWithFormat:@"%@ \n %@",@"VIEW",@"ALL"];
        _btnForViewAll.selected=NO;
        _tableForProfilView.hidden=YES;
        _viewForDueItem.hidden=YES;
    }
    
    
    
}

- (IBAction)myProfileButtonAction:(id)sender
{
    [[AppDelegate getAppdelegate]._currentViewController selectProfile];
}

-(void)showPopupButtonPressed:(id)sender
{
    //change
    UIButton *btn=(UIButton*)sender;
    UITableViewCell*cell=(UITableViewCell*) btn.superview.superview.superview;
    
    NSInteger row=[_tableForMainView indexPathForCell:cell].row;
    NSInteger section=[_tableForMainView indexPathForCell:cell].section;
    
    //Aman added -- Glance Three Sections
    //-----------------------------------------------------------------------------------------------//
    Item *itmobj;
    if (isComingFromDueToggle)
    {
        if (section == 0)
        {
            itmobj = [[marrForMainTableOverDue objectAtIndex:row] valueForKey:@"result"];
        }
        else if (section == 1)
        {
            itmobj = [[marrForMainTableToday objectAtIndex:row] valueForKey:@"result"];
        }
        else
        {
            itmobj = [[marrForMainTableUpcoming objectAtIndex:row] valueForKey:@"result"];
        }
    }
    else
    {
        if (section == 0)
        {
            itmobj = [[marrForMainTableToday objectAtIndex:row] valueForKey:@"result"];
        }
        else
        {
            itmobj = [[marrForMainTableOverDue objectAtIndex:row] valueForKey:@"result"];
        }
    }
    
    //-----------------------------------------------------------------------------------------------//
    //Aman added -- Glance Three Sections
    
    UIView *popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 230, 82)];
    popUpView.backgroundColor=[UIColor clearColor];
    
    UIImageView *popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 230, 82)];
    popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
    [popUpView addSubview:popUpBgImgView];
    [popUpBgImgView release];
    
    UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(2, 5, 20, 19)];
    iconImgView.backgroundColor=[UIColor clearColor];
    
    
    //20AugChange //line for complete
    UIImageView * imgViewForLine=[[UIImageView alloc]initWithFrame:CGRectMake(35, 13, 150, 2)];
    imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
    imgViewForLine.backgroundColor=[UIColor grayColor];
    
    NSString *isTypeStr= itmobj.type;
    
    NSDateFormatter *formeter=[[[NSDateFormatter alloc]init] autorelease];
    
    if(![itmobj.type isEqualToString:@"Note"])
    {
        NSCalendar *calender=[NSCalendar currentCalendar];
        
        NSDateComponents *dayComponentsIsl = [calender
                                              components:(NSMinuteCalendarUnit |NSHourCalendarUnit |NSSecondCalendarUnit) fromDate:itmobj.dueDate];
        int timeDiff;
        if([itmobj.onTheDay integerValue]==0){
            
            NSArray *arr=[itmobj.dueTime componentsSeparatedByString:@":"];

            [dayComponentsIsl setHour:[[arr objectAtIndex:0] integerValue]];
            [dayComponentsIsl setMinute:[[arr objectAtIndex:1] integerValue]];
            NSDate *d2=[calender dateByAddingComponents:dayComponentsIsl toDate:itmobj.dueDate options:0];
            timeDiff=(int)[d2 timeIntervalSinceDate:[NSDate date]];

        }
        else
        {
            [formeter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
            NSDate *d2=[calender dateByAddingComponents:dayComponentsIsl toDate:itmobj.dueDate options:0];
            timeDiff=(int)[d2 timeIntervalSinceDate:[formeter dateFromString:[formeter stringFromDate:[NSDate date]]]];
        }
        if(timeDiff<0)
        {
             if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                if([itmobj.isApproved integerValue] == 0)
                {
                    iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                }
                else  if([itmobj.isApproved integerValue] == 1)
                {
                    iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                }
                else  if([itmobj.isApproved integerValue] == 2)
                {
                    iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                }
            }
            else
            {
            iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
            }

        }
        else{
            
            if ([isTypeStr isEqualToString:@"Assignment"]) {
                
                iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
            }
            else if([isTypeStr isEqualToString:@"Homework"]) {
                iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
            }
            
            else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                if([itmobj.isApproved integerValue] == 0)
                {
                    iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                }
                else  if([itmobj.isApproved integerValue] == 1)
                {
                    iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                }
                else  if([itmobj.isApproved integerValue] == 2)
                {
                    iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                }
                
            }

            
        }
        
    }
    
    if ([itmobj.isCompelete integerValue]==1) {
        imgViewForLine.hidden=NO;
    }
    else
    {
        imgViewForLine.hidden=YES;
    }
    
    
    
    
    UIImageView *imgViewForBgHeader=[[UIImageView alloc]initWithFrame:CGRectMake(1, 0, 227, 26)];
//    Subjects *sub=[[Service sharedInstance]getSubjectsDataInfoStored:itmobj.subject_name  postNotification:NO];
    //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
    MyClass *class1=nil;
    
    class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itmobj.class_id]];
    
    if(class1.code!=nil)
    {
        imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
    }
    else{
        imgViewForBgHeader.backgroundColor=[UIColor grayColor];
    }
    
    /*
     if(sub.subject_color!=nil)
     imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:sub.subject_color ];
     else{
     imgViewForBgHeader.backgroundColor=[UIColor grayColor];
     }
     */
    
    //--------- By Mitul ---------//

    imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
    [imgViewForBgHeader.layer setMasksToBounds:YES];
    [imgViewForBgHeader.layer setCornerRadius:3.0f];
    [imgViewForBgHeader.layer setBorderWidth:1.0];
    [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
    [popUpView addSubview:imgViewForBgHeader];
    [imgViewForBgHeader release];
    
    
    
    UILabel *popUpHeaderLbl=[[UILabel alloc]initWithFrame:CGRectMake(30, 2,200, 25)];
    popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
    popUpHeaderLbl.numberOfLines=1;
    popUpHeaderLbl.backgroundColor=[UIColor clearColor];
    popUpHeaderLbl.textColor=[UIColor whiteColor];
    [popUpView addSubview:popUpHeaderLbl];
    [popUpHeaderLbl release];
    
    [popUpView addSubview:iconImgView];
    [iconImgView release];
    [popUpView addSubview:imgViewForLine];
    [imgViewForLine release];
    
    UILabel *popSubNameLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 30,200, 15)];
    popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
    popSubNameLbl.numberOfLines=1;
    popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
    popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
    popSubNameLbl.backgroundColor=[UIColor clearColor];
    [popUpView addSubview:popSubNameLbl];
    [popSubNameLbl release];
    
    UILabel *popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 48,200, 32)];
    popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
    popUDescLbl.numberOfLines=4;
    popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
    popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
    popUDescLbl.backgroundColor=[UIColor clearColor];
    [popUpView addSubview:popUDescLbl];
    [popUDescLbl release];
    
    UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
    [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
    editBtn.frame=CGRectMake(200,1,26, 26);
    [popUpView addSubview:editBtn];
    
    
       
    
    
    
    popUpHeaderLbl.text=itmobj.title ;
    
    if(itmobj.subject_name!=nil){
        popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", itmobj.subject_name];
    }
    else{
        popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
    }
    
    popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",itmobj.itemDescription] ;
    if (itmobj.itemDescription!=nil) {
        popUDescLbl.hidden=NO;
    }
    else{
        popUDescLbl.hidden=YES;
    }
    
    // editBtn.tag=[itmobj.universal_Id integerValue];
    
    if([itmobj.itemId integerValue]>0){
        editBtn.tag=[itmobj.itemId integerValue];
        [AppDelegate getAppdelegate]._isUniqueId=YES;
    }
    else{
        editBtn.tag=[itmobj.universal_Id integerValue];
        [AppDelegate getAppdelegate]._isUniqueId=NO;
        
    }
    
    
    
    UIViewController* popoverContent = [[UIViewController alloc]init];
    popoverContent.view = popUpView;
    
    
    UIPopoverController *popover=[[UIPopoverController alloc] initWithContentViewController:popoverContent];
    self._popover =popover;
    [popover release];
    
    [self._popover setPopoverContentSize:CGSizeMake(230, 82) animated:YES];// TODO: Code to Add WebLink
    self._popover.delegate = self;
    [self._popover presentPopoverFromRect:CGRectMake(40 ,15,10,10 )inView:btn.superview
                 permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    [popoverContent release];
    [popUpView release];
    //[_popover release];
    
    
}

// Merits Edit

- (void)editTaskBasedMerits:(id)sender
{
    [[AppDelegate getAppdelegate]._currentViewController editAddedTaskBasedMerit:[sender tag]];
    
    if([self._popover isPopoverVisible])
    {
        [self._popover dismissPopoverAnimated:YES];
        self._popover=nil;
        return;
    }
}

-(void)edtiButtunPessed:(id)sender
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    UIButton *btn=(UIButton*)sender;
    btn.selected=YES;
    
    Item *diarItem=nil;
  
    if([AppDelegate getAppdelegate]._isUniqueId ){
        diarItem =[[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:btn.tag] postNotification:NO];
    }
    else{
        diarItem =[[Service sharedInstance]getItemDataInfoStoredForParseDate:[NSNumber numberWithInteger:btn.tag] appid:nil];
    }
#if DEBUG
    NSLog(@"*****edtiButtunPessed %@",diarItem.type);
    NSLog(@"*****edtiButtunPessed %@",diarItem);
#endif
    NSString *isTypeTemplateStr=  diarItem.type;
    
    if([isTypeTemplateStr isEqualToString:@"OutOfClass"] || [isTypeTemplateStr isEqualToString:@"Out Of Class"])
    {
        if (countDownTimer)
        {
            [countDownTimer invalidate];
            countDownTimer =nil;
        }
        // Pending Pass
        if([diarItem.isApproved integerValue] == 0)
        {
           [[AppDelegate getAppdelegate]._currentViewController editAddedDiaryItem:btn.tag]; 
        }
        // Approved Pass
        else  if([diarItem.isApproved integerValue] == 1)
        {
            selectedDiaryItem = diarItem;
            [self showApprovedViewDetails];
            
            if([profobj.type isEqualToString:@"Teacher"])
                {
                    lblPassApprovedByTitle.text = @"Issued For";

                }else{
                    lblPassApprovedByTitle.text = @"Issued By";

                }
            self._popoverControler=nil;
            UIViewController* popoverContent = [[UIViewController alloc]init];
            popoverContent.view = viewForApprovedPass;
            
            UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler=pop;
            //self._popover = self._popoverControler;

            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler setPopoverContentSize:CGSizeMake( popoverContent.view.frame.size.width, popoverContent.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler setPopoverContentSize:CGSizeMake(popoverContent.view.frame.size.width, popoverContent.view.frame.size.height) animated:YES];
            }
            
            self._popoverControler.delegate = self;
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(795 ,10,30,30 )inView:btn.superview
                         permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
//            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn.superview
//                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [popoverContent release];
            [pop release];
            
        }
        // Elapsed Pass
        else  if([diarItem.isApproved integerValue] == 2)
        {
            if([profobj.type isEqualToString:@"Teacher"])
            {
                lblPassApprovedByTitle.text = @"Issued For";
                
            }else{
                lblPassApprovedByTitle.text = @"Issued By";
                
            }
            
            selectedDiaryItem = diarItem;
            [self showElapsedViewDetails];
            
            self._popoverControler=nil;
            UIViewController* popoverContent = [[UIViewController alloc]init];

            popoverContent.view = viewForApprovedPass;
            
            UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler=pop;
            //self._popover = self._popoverControler;
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler setPopoverContentSize:CGSizeMake( popoverContent.view.frame.size.width, popoverContent.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler setPopoverContentSize:CGSizeMake(popoverContent.view.frame.size.width, popoverContent.view.frame.size.height) animated:YES];
            }
            
            self._popoverControler.delegate = self;
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(795 ,10,30,30 )inView:btn.superview
                         permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
            
//            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn.superview
//                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [popoverContent release];
            [pop release];
            
        }
    }
    else
    {
        
#if DEBUG
        NSLog(@"btn.tag %ld",(long)btn.tag);
#endif
        
        [[AppDelegate getAppdelegate]._currentViewController editAddedDiaryItem:btn.tag];
    
        if([self._popover isPopoverVisible])
        {
            [self._popover dismissPopoverAnimated:YES];
            self._popover=nil;
            return;
        }
    }
}



- (void)dismisPopover
{
    //26AUG2013
    if([self._popover isPopoverVisible]){
        [self._popover dismissPopoverAnimated:NO];
        // self._popover=nil;
        [self addItemButtonAction];
        [_tableForMainView reloadData];
        
        return;
    }
}


//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//

-(void)showAllNotesItemButtonPressed:(id)sender
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    
    UIButton *btn=(UIButton*)sender;
    
    //Aman added -- iOS7 optimisation --------------------------------------
    UITableViewCell *cell = nil;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        cell = (UITableViewCell *)btn.superview.superview.superview;
    }
    else
    {
        cell = (UITableViewCell *)btn.superview.superview;
    }
    
    //Aman added -- iOS7 optimisation --------------------------------------

    
    NSInteger row=[_tableForMainView indexPathForCell:cell].row;
    NSInteger section=[_tableForMainView indexPathForCell:cell].section;
    
    //Aman added -- Glance Three Sections
    //-----------------------------------------------------------------------------------------------//
    Item *itmobj;
    if (section == 0)
    {
        itmobj = [[marrForMainTableToday objectAtIndex:row] valueForKey:@"result"];
    }
    else
    {
        itmobj = [[marrForMainTableOverDue objectAtIndex:row] valueForKey:@"result"];
    }
    
    //-----------------------------------------------------------------------------------------------//
    //Aman added -- Glance Three Sections

    
    // Fetch Student Id Based on this Item ID
    viewpopup=nil;
     viewpopup=[[ShowNotesViewController alloc]initWithNibName:@"ShowNotesViewController" bundle:nil];
    
       
    
    viewpopup.NoteItemId = itmobj.itemId;
    
    if([itmobj.itemId integerValue]>0){
        [AppDelegate getAppdelegate]._isUniqueId=YES;
        [viewpopup getAllNotesDairyItemMethod:itmobj.itemId ];

    }
    else{
        [AppDelegate getAppdelegate]._isUniqueId=NO;
        [viewpopup getAllNotesDairyItemMethod:itmobj.universal_Id ];
    }
    self._popover=nil;
    
   // [viewpopup.view removeFromSuperview];
    
    UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:viewpopup];
    self._popover=pop;
    [self._popover setPopoverContentSize:CGSizeMake(viewpopup.view.frame.size.width, viewpopup.view.frame.size.height) animated:YES];
    self._popover.delegate = self;

#if DEBUG
    NSLog(@"btn.superview %@",btn.superview);
    NSLog(@"self._popover %@",self._popover);
#endif
    
    [self._popover presentPopoverFromRect:CGRectMake(795 ,10,10,10 )inView:btn.superview
                 permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    
    
    

    [DELEGATE setGlb_isDiaryItemEditForAttachment:YES];
    [DELEGATE setGlb_Button_Attachment:nil];
    [DELEGATE setGlb_Button_Attachment:viewpopup.btnNoteEditViewAttachment];
    
    NSArray *tempArray = [[Service sharedInstance]getAttachmentsForItemId:itmobj.itemId withType:[[Service sharedInstance]getFormTemplateOnItemIDBasis:itmobj.itemType_id]] ;
    [DELEGATE setMarrListedAttachments:nil];
    [DELEGATE setMarrListedAttachments:[[NSMutableArray alloc] initWithArray:tempArray]] ;
    
    NSInteger count = [[DELEGATE marrListedAttachments]count];
    
    if (count>0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %ld",(long)count]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if (count > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }

    

    [pop release];
    [viewpopup release];
}

//-----------------------------------------------------------------------------------------------//
//Aman added -- Glance Three Sections


//Aman added -- EZMOBILE-25
-(void)showAllEventsButtonPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    btn.selected=YES;

    if([btn.titleLabel.text isEqualToString:@"1"])
    {
        [AppDelegate getAppdelegate]._isUniqueId =YES;
    }
    else
    {
        [AppDelegate getAppdelegate]._isUniqueId =NO;
    }
    
    [(MainViewController*)[AppDelegate getAppdelegate]._currentViewController editAddedEventDiaryItem:btn.tag];
    
    [self addItemButtonAction];
    [_tableForMainView reloadData];
}


#pragma mark SliderValue Updation

- (void)updateValue:(id)slider
{
    BOOL isComplete;
    
    UISlider *objSlider = slider;
    
    float sliderRange = objSlider.frame.size.width - objSlider.currentThumbImage.size.width;
    float sliderOrigin = objSlider.frame.origin.x + (objSlider.currentThumbImage.size.width / 2.0);
    
    float sliderValueToPixels = (((objSlider.value - objSlider.minimumValue) / (objSlider.maximumValue - objSlider.minimumValue)) * sliderRange) + sliderOrigin;
    
    NSInteger v = objSlider.value+0.5;
    
    if (v>=100)
        isComplete = YES;
    else
        isComplete = NO;
    
    
    //Aman added -- iOS7 optimisation --------------------------------------
    UITableViewCell *cell = nil;
    
    
     if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && SYSTEM_VERSION_LESS_THAN(@"8.0") )
    {
        cell = (UITableViewCell *)objSlider.superview.superview.superview;
#if DEBUG
        NSLog(@" Else Condition 7.0 cell = %@",(UITableViewCell *)objSlider.superview.superview.superview);
#endif
    }
    else
    {
        cell = (UITableViewCell *)objSlider.superview.superview;
#if DEBUG
        NSLog(@" Else Condition Below 8.0 and above 8 cell = %@",(UITableViewCell *)objSlider.superview.superview);
#endif
    }
    //Aman added -- iOS7 optimisation --------------------------------------

    for (UIView *view in cell.contentView.subviews)
    {
        if ([view isKindOfClass:[UILabel class]])
        {
            UILabel *lbl = (UILabel*)view;
            if (lbl.tag == 100)        //Label to show slider value percentage
            {
                [view setFrame:CGRectMake(sliderValueToPixels-32, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
                [lbl setText:[NSString stringWithFormat:@"%ld%@",(long)v,@"%"]];
            }
        }
        
        if ([view isKindOfClass:[UIImageView class]])
        {
            if (view.tag == 100)        //move blue image for slider value
            {
                [view setFrame:CGRectMake(sliderValueToPixels-15, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
            }
        }
        
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *correspondingViewButton = (UIButton*)view;
            
            if([correspondingViewButton.titleLabel.text isEqualToString:@"check"] || [correspondingViewButton.titleLabel.text isEqualToString:@"uncheck"])
            {
                UIButton *checkBox = (UIButton*)view;
                
                if (v>=100) //Task is complete then check the checkbox
                {
                    [checkBox setImage:[UIImage imageNamed:@"img_check.png"] forState:UIControlStateNormal];
                    checkBox.titleLabel.text = @"check";
                }
                else
                {
                    [checkBox setImage:[UIImage imageNamed:@"img_uncheck.png"] forState:UIControlStateNormal];
                    checkBox.titleLabel.text = @"uncheck";
                }
            }
        }
        
        if ([view isKindOfClass: [UIView class]])
        {
            if (view.tag == 200)
            {
                for (UIView *insideViews in view.subviews)
                {
                    if ([insideViews isKindOfClass:[UIImageView class]])  //line when task is complete
                    {
                        if (insideViews.tag == 201)
                        {
                            if (isComplete)
                                insideViews.hidden  = NO;
                            else
                                insideViews.hidden = YES;
                        }
                    }
                }
            }
        }
    }
}

-(void)saveSliderValueInDatabase:(id)slider
{
    UISlider *objSlider = slider;
    
    UIButton *btnSender = [[UIButton alloc]init];
    btnSender.tag = objSlider.tag;
    
    BOOL isComplete;
    int progressValue;
    
    progressValue = objSlider.value+0.5;
    
    if (progressValue>=100)
        isComplete = YES;
    else
        isComplete = NO;
    
    [[AppDelegate getAppdelegate] setGlbIsComingFromGlanceSlider:YES];
    [AppDelegate getAppdelegate].glbMarrGlanceSliderValues = nil;
    [AppDelegate getAppdelegate].glbMarrGlanceSliderValues = [[NSMutableArray alloc] init];
    [[AppDelegate getAppdelegate].glbMarrGlanceSliderValues addObject:[NSString stringWithFormat:@"%d", progressValue]];
    [[AppDelegate getAppdelegate].glbMarrGlanceSliderValues addObject:[NSString stringWithFormat:@"%d",isComplete]];
    
    [self edtiButtunPessed:btnSender];
    
}


//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//

-(void)actionCheckBoxComplete :(id)sender
{
    UIButton *btn = sender;
    BOOL isComplete;
    int progressValue;
    UISlider *objSlider;
    float sliderValueToPixels, sliderRange, sliderOrigin;
    
    
//    UITableViewCell *cell = (UITableViewCell *)btn.superview.superview;
    
    //Aman added -- iOS7 optimisation --------------------------------------
    UITableViewCell *cell = nil;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") && SYSTEM_VERSION_LESS_THAN(@"8.0"))
    {
        cell = (UITableViewCell *)btn.superview.superview.superview;
    }
    else
    {
        cell = (UITableViewCell *)btn.superview.superview;
    }
    
    //Aman added -- iOS7 optimisation --------------------------------------

    
    for (UIView *view in cell.contentView.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            UIButton *correspondingViewButton = (UIButton*)view;
            
            if([correspondingViewButton.titleLabel.text isEqualToString:@"check"] || [correspondingViewButton.titleLabel.text isEqualToString:@"uncheck"])   //checkbox button
            {
                UIButton *checkBox = (UIButton*)view;
                
                if([checkBox.titleLabel.text isEqualToString: @"check"])  //Task is complete
                {
                    [checkBox setImage:[UIImage imageNamed:@"img_uncheck.png"] forState:UIControlStateNormal];
                    checkBox.titleLabel.text = @"uncheck";
                    
                    isComplete=NO;
                    progressValue = 0;
                }
                else
                {
                    [checkBox setImage:[UIImage imageNamed:@"img_check.png"] forState:UIControlStateNormal];
                    checkBox.titleLabel.text = @"check";
                    
                    isComplete=YES;
                    progressValue = 100;
                }
            }
        }
    }
    
    for (UIView *view in cell.contentView.subviews)
    {
        if ([view isKindOfClass:[UISlider class]])//change slider value appropriately
        {
            objSlider = (UISlider*)view;
            
            if (isComplete)
            {
                objSlider.value = 100;
                sliderRange = objSlider.frame.size.width - objSlider.currentThumbImage.size.width;
                sliderOrigin = objSlider.frame.origin.x + (objSlider.currentThumbImage.size.width / 2.0);
                
                sliderValueToPixels = (((objSlider.value - objSlider.minimumValue) / (objSlider.maximumValue - objSlider.minimumValue)) * sliderRange) + sliderOrigin;
            }
            else
            {
                objSlider.value = 0;
                sliderRange = objSlider.frame.size.width - objSlider.currentThumbImage.size.width;
                sliderOrigin = objSlider.frame.origin.x + (objSlider.currentThumbImage.size.width / 2.0);
                
                sliderValueToPixels = (((objSlider.value - objSlider.minimumValue) / (objSlider.maximumValue - objSlider.minimumValue)) * sliderRange) + sliderOrigin;
            }
        }
        
        if ([view isKindOfClass:[UILabel class]])
        {
            if (view.tag == 100)
            {
                UILabel *lblPercentComplete = (UILabel*)view;
                [view setFrame:CGRectMake(sliderValueToPixels-32, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
                if (isComplete)
                {
                    [lblPercentComplete setText:@"100%"];
                }
                else
                {
                    [lblPercentComplete setText:@"0%"];
                }
                
            }
        }
        
        if ([view isKindOfClass:[UIImageView class]])
        {
            if (view.tag == 100)
            {
                [view setFrame:CGRectMake(sliderValueToPixels-15, view.frame.origin.y, view.frame.size.width, view.frame.size.height)];
            }
        }
        
        if ([view isKindOfClass: [UIView class]])
        {
            if (view.tag == 200)
            {
                for (UIView *insideViews in view.subviews)
                {
                    if ([insideViews isKindOfClass:[UIImageView class]])  //line when task is complete
                    {
                        if (insideViews.tag == 201)
                        {
                            if (isComplete)
                                insideViews.hidden  = NO;
                            else
                                insideViews.hidden = YES;
                        }
                    }
                }
            }
        }
    }
    
    
    [[AppDelegate getAppdelegate] setGlbIsComingFromGlanceSlider:YES];
    [AppDelegate getAppdelegate].glbMarrGlanceSliderValues = nil;
    [AppDelegate getAppdelegate].glbMarrGlanceSliderValues = [[NSMutableArray alloc] init];
    [[AppDelegate getAppdelegate].glbMarrGlanceSliderValues addObject:[NSString stringWithFormat:@"%d", progressValue]];
    [[AppDelegate getAppdelegate].glbMarrGlanceSliderValues addObject:[NSString stringWithFormat:@"%d",isComplete]];
    
    [self edtiButtunPessed:btn];
    
    if (isComplete)  //Aman added -- EZTEST 863
    {
        [_tableForMainView reloadData];
    }
    
}
//-----------------------------------------------------------------------------------------------//
//Aman added -- Glance Three Sections



//Aman added -- Glance Three Sections
//-----------------------------------------------------------------------------------------------//
-(NSString *) getDaysOfOverdueUpcoming :(NSDate *) date
{
    NSString *strDifference = [[NSString alloc]init];
    NSInteger difference;
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    NSDate *startDate = [NSDate date];
    //    NSDate *endDate = [f dateFromString:date];
    NSDate *endDate = date;
    
    
    
    NSCalendar *gregorianCalendar1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar1 components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    difference = components.day;
    
    if (difference>0) //Upcoming
    {
        if (difference == 1)
            strDifference = [NSString stringWithFormat:@"%ld day later",(long)difference];
        else
            strDifference = [NSString stringWithFormat:@"%ld days later",(long)difference];
    }
    
    else if (difference <0) //Overdue
    {
        difference = labs(difference);
        
        if (difference == 1)
            strDifference = [NSString stringWithFormat:@"%ld day ago",(long)difference];
        else
            strDifference = [NSString stringWithFormat:@"%ld days ago",(long)difference];
    }
    else if (difference == 0)
    {
        //strDifference = [NSString stringWithFormat:@"Due Today"];
        strDifference = [NSString stringWithFormat:@"1 day ago"];
    }
    
    return strDifference;
}

-(IBAction) segmentedControlIndexChanged
{
    switch (self.segmentedControl.selectedSegmentIndex)
    {
        case 0:
            isComingFromDueToggle = TRUE;
            [self addItemButtonAction];
            break;
            
        case 1:
            isComingFromDueToggle = FALSE;
            [self addItemButtonAction];
            break;
            
        default:
            break;
    }
}
//-----------------------------------------------------------------------------------------------//
//Aman added -- Glance Three Sections

-(IBAction) switchDueAssignToggled :(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
//        UIFont *font = self.lblDue.font;
        
        BOOL isOnForDue = [sender isOn];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!isOnForDue)
            {
                isComingFromDueToggle = TRUE;
                [[AppDelegate getAppdelegate] setGlbDueSectionWasSelected:YES];
                
                [self addItemButtonAction];
                [self.lblDue setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:14.0]];
                [self.lblAssigned setFont:[UIFont fontWithName:@"Arial" size:14.0]];
            }
            else
            {
                isComingFromDueToggle = FALSE;
                [[AppDelegate getAppdelegate] setGlbDueSectionWasSelected:NO];
                
                [self addItemButtonAction];
                [self.lblAssigned setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:14.0]];
                [self.lblDue setFont:[UIFont fontWithName:@"Arial" size:14.0]];    }
        });
    });
    
    
    
}

#pragma mark- deletePassAction

- (IBAction)deletePassAction:(id)sender {
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:selectedDiaryItem.type forKey:@"type"];
    int itemId=[selectedDiaryItem.itemId integerValue];
   
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if( itemId>0)
    {
        [dict setObject:[NSNumber numberWithInteger:itemId] forKey:@"id"];
        
        if (networkReachable)
        {
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(diaryItemDidDelet:) name:NOTIFICATION_DeleteDairyItem object:nil];
            
            [[Service sharedInstance]deleteDairyItems:dict];
            
        }
        else{
            //  [dict setObject:diarItem.universal_Id forKey:@"AppId"];
            selectedDiaryItem.isDelete=[NSNumber numberWithBool:YES];
            selectedDiaryItem.isSync=[NSNumber numberWithBool:NO];
            NSError *error=nil;
            
            if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
            [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
        }
    }
    else{
        NSManagedObject *eventToDelete = selectedDiaryItem;
        
        if (eventToDelete) {
            [[AppDelegate getAppdelegate].managedObjectContext deleteObject:eventToDelete];
        }
        
        
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
        [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
    }
    [dict release];
}


-(void)diaryItemDidDelet:(NSNotification*)note
{
#if DEBUG
    NSLog(@"diaryItemDidDelet %@",note.userInfo);
#endif
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_DeleteDairyItem object:nil];
        
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            [[Service sharedInstance]deleteObjectOfDairyItem:selectedDiaryItem.itemId];
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
            [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
            
            if([self._popoverControler isPopoverVisible])
            {
                [self._popoverControler dismissPopoverAnimated:NO];
                [self addItemButtonAction];
                [_tableForMainView reloadData];
            }
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
    }
}


@end
