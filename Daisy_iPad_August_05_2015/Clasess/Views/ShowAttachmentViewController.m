//
//  ShowAttachmentViewController.m
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/10/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "ShowAttachmentViewController.h"

@interface ShowAttachmentViewController ()
{
    UIActivityIndicatorView *activityIndicator;
}
@end

@implementation ShowAttachmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    activityIndicator = nil;
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
    [self.view addSubview:activityIndicator];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_vebViewShowAtachment release];
    [_btnCloseWebView release];
    [super dealloc];
}

- (void)setDocumentDetail:(Attachment *)attachment
{
    
    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement]allKeys];
    NSString *strURL = @"" ;
    NSURL *url = nil ;
    
    if (arrayOfKeys.count > 0) {
        // get file path from global dict
//        strURL = attachment.localFilePath ;
//        url = [NSURL fileURLWithPath:strURL];
        
        // - 1816
        // New Conditions added to fix crash isue on view attachment at time of
        // editing DI and after adding new attachment viewing old Attachment.
        if (attachment.localFilePath.length>0)
        {
#if DEBUG
            NSLog(@"method Name - setDocumentDetail attachment.localFilePath = %@ ",attachment.localFilePath);
#endif
            // gdoc Implementation
            strURL = attachment.localFilePath ;
            if ([attachment.fileType isEqualToString:@"gdoc"])
            {
                url = [NSURL URLWithString:strURL];
            }
            else
            {
                url = [NSURL fileURLWithPath:strURL];
                NSString *ulrmodified = [url absoluteString];
                ulrmodified = [ulrmodified stringByReplacingOccurrencesOfString:@"file:///" withString:@""];
                url = [NSURL URLWithString:ulrmodified];
#if DEBUG
                NSLog(@"method Name - setDocumentDetail ulrmodified= %@ ",url);
#endif
            }
        }
        else if (attachment.remoteFilePath.length > 0) {
            strURL = attachment.remoteFilePath;
            strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            url = [NSURL URLWithString:strURL];
        }else if (attachment.filePath.length > 0){
            strURL = attachment.filePath;
            url = [NSURL fileURLWithPath:strURL];
        }else{
            // File not found
        }

    }else{
        // get file path from database
        
        if (attachment.remoteFilePath.length > 0) {
            strURL = attachment.remoteFilePath;
            strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            url = [NSURL URLWithString:strURL];
        }else if (attachment.filePath.length > 0){
            strURL = attachment.filePath;
            // gdoc Implementation
            if ([attachment.type isEqualToString:@"gdoc"])
            {
                url = [NSURL URLWithString:strURL];
            }
            else
            {
                url = [NSURL fileURLWithPath:strURL];
            }
        }else{
            // File not found
        }
        
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.vebViewShowAtachment loadRequest:request];
}

- (void)openLinkOnWebView:(NSString*)urlwebLink
{
    NSString *strLink = @"http://";
    
    if ([self Contains:@"http://" on:urlwebLink]) {
        strLink = urlwebLink;
        
        
    } else {
        strLink = [strLink stringByAppendingFormat:@"%@",urlwebLink];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strLink]];
    [self.vebViewShowAtachment loadRequest:request];
}
-(BOOL)Contains:(NSString *)StrSearchTerm on:(NSString *)StrText
{
    return  [StrText rangeOfString:StrSearchTerm options:NSCaseInsensitiveSearch].location==NSNotFound?FALSE:TRUE;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityIndicator startAnimating];
    
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator removeFromSuperview];
}
- (void)openCalendarAccessOnWebView:(NSURL*)urlwebLink
{
    NSURLRequest *request = [NSURLRequest requestWithURL:urlwebLink];
    [self.vebViewShowAtachment loadRequest:request];
    
}

- (IBAction)btnCloseClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.vebViewShowAtachment stopLoading];
}

@end
