//
//  DropBoxFilesViewController.m
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/11/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "DropBoxFilesViewController.h"
#import "CustomCellFolderFilesList.h"
#import "Utilities.h"
#import "Attachment.h"

@interface DropBoxFilesViewController ()
{
    NSMutableArray *marr_fileNames;
    NSMutableArray *marr_folderNames;
    NSMutableString *strPathDB;
    NSMutableArray *marrOfFolderFiles;
    UIAlertView *alert;

}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *authButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshButton;

- (IBAction)authButtonClicked:(id)sender;
- (IBAction)refreshButtonClicked:(id)sender;
- (IBAction)btn_back_pressed:(id)sender;

@end

@implementation DropBoxFilesViewController

@synthesize authButton = _authButton;
@synthesize refreshButton = _refreshButton;

@synthesize restClient;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [marr_fileNames release];
    [marr_folderNames release];
    [strPathDB release];

    //----- Added By Mitul on 15-Dec-2013 -----//
    [marrOfFolderFiles release];
    //--------- By Mitul ---------//
    
    [_btnBack release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    marr_folderNames = [[NSMutableArray alloc] init];
    strPathDB = [[NSMutableString alloc] init];
    
  
    // Add an observer that will respond to loginComplete
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(singnInOfDBComplete:)
                                                 name:@"loginComplete" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void) viewWillDisappear:(BOOL)animated {
    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement]allKeys];
    NSMutableArray *marrAttachedDoc = [[NSMutableArray alloc] init];
    for(NSString *str in arrayOfKeys)
    {
        for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
        {
            [marrAttachedDoc addObject:attachment];
        }
    }
    
    for (Attachment *a in [DELEGATE marrListedAttachments]) {
        [marrAttachedDoc addObject:a ];
    }
    
    if ([marrAttachedDoc count]>0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %lu",(unsigned long)[marrAttachedDoc count]]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if ([marrAttachedDoc count] > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];

        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) %d",[marrAttachedDoc count]] forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];

        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) 0"] forState:UIControlStateNormal];
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Sort Drive Files by modified date (descending order).
    
    // Disable Back button
    [self.btnBack setEnabled:NO];

    if ([[DBSession sharedSession] isLinked])
    {
		//[[DBSession sharedSession] linkFromController:self];
        self.authButton.title = @"Sign Out";
        [strPathDB setString:@""];
        //----- Added By Mitul on 18-Dec Network Check -----//
        BOOL networkReachable = [[DELEGATE netManager]networkReachable];
        if (networkReachable)
        {
            [[self restClient] loadMetadata:@"/"];
        }
        else
        {
            [DELEGATE hideIndicator];
            
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        //--------- By Mitul ---------//
    }
    else
        self.authButton.title = @"Sign In";

}

- (void)viewDidUnload
{
    [self setAuthButton:nil];
    [self setRefreshButton:nil];
    marrOfFolderFiles = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// the function specified in the same class where we defined the addObserver
- (void)singnInOfDBComplete:(NSNotification *)note
{
    self.authButton.title = @"Sign Out";
    [self refreshButtonClicked:nil];
}

- (BOOL)isFileAttachedOrNot:(DBMetadata *)inDBFileMetadata
{

    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement] allKeys];
    NSArray *arrString  = [strPathDB componentsSeparatedByString: @"/"];
    NSString *strKeyDirName;
    if ([[arrString lastObject] isEqualToString:@""])
    {
        strKeyDirName = [NSString stringWithFormat:@"DropBox"];
    }
    else
    {
        strKeyDirName = [NSString stringWithFormat:@"%@",[arrString lastObject]];
    }

    NSString *strFilePath = [NSString stringWithFormat:@"%@/%@",strKeyDirName,inDBFileMetadata.filename];
    
    for(NSString *str in arrayOfKeys)
    {
        int i=0;
        for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
        {
            if ([attachment.localFilePath rangeOfString:strFilePath].location == NSNotFound)
            {
            }
            else
            {
                return YES;
            }
            i++;
        }
        
    }
    return NO;
}

- (IBAction)authButtonClicked:(id)sender
{
    if (![[DBSession sharedSession] isLinked])
    {
		[[DBSession sharedSession] linkFromController:self];
    }
    else
    {
        [[DBSession sharedSession] unlinkAll];
        restClient = nil;
        [[[UIAlertView alloc]
          initWithTitle:@"Account Unlinked!" message:@"Your dropbox account has been unlinked"
          delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
         show];
        self.authButton.title = @"Sign In";
        [marr_folderNames removeAllObjects];
        [self.tbl_View reloadData];
    }
}

- (IBAction)btn_back_pressed:(id)sender {
    
    //----- Added By Mitul on 18-Dec Network Check -----//
    
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:[strPathDB componentsSeparatedByString:@"/"]];
        
        [marr removeObjectAtIndex:0];
        [marr removeLastObject];
        
        [strPathDB setString:@""];
        for (NSString *str in marr)
        {
            strPathDB = [[strPathDB stringByAppendingString:@"/"] mutableCopy];
            strPathDB = [[strPathDB stringByAppendingString:str] mutableCopy];
        }
        if ([strPathDB length] < 1)
        {
            [self.btnBack setEnabled:NO];
            strPathDB = [[strPathDB stringByAppendingString:@"/"] mutableCopy];
        }
        [[self restClient] loadMetadata:strPathDB];
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}
- (IBAction)refreshButtonClicked:(id)sender
{
    //----- Added By Mitul on 18-Dec Network Check -----//
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        [strPathDB setString:@""];
        [[self restClient] loadMetadata:@"/"];
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

#pragma mark TableDelegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([marr_folderNames count]>0)
        return [marr_folderNames count];
    else
        return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCellFolderFilesList *cell = (CustomCellFolderFilesList *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCellFolderFilesList" owner:self options:nil];
        cell = [cellArray lastObject];
    }
    cell.btn_Attachment.hidden = true;
    
    if ([marr_folderNames count]>0){
        DBMetadata *file = [marr_folderNames objectAtIndex:indexPath.row];
        cell.lbl_Heading.text = file.filename;

        if (!file.isDirectory)
        {
            if([self isFileAttachedOrNot:file])
            {
                //cell.btn_Attachment.titleLabel.text = @"Detach";
                [cell.btn_Attachment setTitle:@"Attached" forState:UIControlStateNormal];
                //----- Added By Mitul on 16-Dec-2013 -----//
                [cell.btn_Attachment setEnabled:NO];
                //--------- By Mitul ---------//
            }
            
            cell.btn_Attachment.hidden = false;
            
            NSString *strFile = [FileUtils documentsDirectoryPathForResource:file.filename];
            UIImage *img = [UIImage imageWithContentsOfFile:strFile];
            cell.imageView.image = img;
            cell.userInteractionEnabled = YES ;

        }
    }
    else{
        cell.lbl_Heading.text = @"Loading Dropbox Account";
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        cell.userInteractionEnabled = NO ;
    }
    
    cell.btn_Attachment.tag = indexPath.row;
    [cell.btn_Attachment addTarget:self action:@selector(clickAttachment:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBMetadata *file = [marr_folderNames objectAtIndex:indexPath.row];
    if (file.isDirectory)
    {
        //----- Added By Mitul on 18-Dec Network Check -----//
        BOOL networkReachable = [[DELEGATE netManager]networkReachable];
        if (networkReachable)
        {
            // Back button Enabled
            [self.btnBack setEnabled:YES];

            [marrOfFolderFiles removeAllObjects];
            
            strPathDB = [[strPathDB stringByAppendingString:[NSString stringWithFormat:@"/%@",file.filename]] mutableCopy];
            [[self restClient] loadMetadata:strPathDB];
        }
        else
        {
            
            [DELEGATE hideIndicator];
            
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        //--------- By Mitul ---------//
    }
    else
    {
        //--------- By Mitul ---------//
    }
}

- (NSString *)getLocalDropBoxFilePath:(NSString *)strFileName fileIndex:(NSInteger)inFileIndex
{
    //----- Added By Mitul on 17-Dec-2013 For Application DirectoryName -----//
    NSString *strGoogleDrivePath = [FileUtils createNewDirectoryAtGivenDocumentDirectoryPath:@"DropBox"];
    
    NSString *dataPath = [strGoogleDrivePath stringByAppendingPathComponent:strPathDB];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strFileName]];
    
    
    NSArray *arrFolderName = [strPathDB componentsSeparatedByString:@"/"];
    NSString *strCurrentFolder = [arrFolderName lastObject];

    if ([strCurrentFolder isEqualToString:@""])
    {
        strCurrentFolder = @"DropBox";
    }
    arrFolderName = nil;
    arrFolderName = [strFileName componentsSeparatedByString:@"."];
    
    marrOfFolderFiles = nil;
    marrOfFolderFiles = [[NSMutableArray alloc] init];
    
    Attachment *attachment = [[Attachment alloc]init];
    attachment.driveName = @"DropBox" ;
    attachment.displayName = strFileName ;
    attachment.localFilePath = filePath ;
    attachment.indexPath = [NSString stringWithFormat:@"%ld",(long)inFileIndex];
    attachment.localFolderName = strCurrentFolder ;
    attachment.fileType = [arrFolderName lastObject] ;
    [marrOfFolderFiles addObject:attachment];
    attachment = nil ;

    //--------- By Mitul ---------//

    return filePath;
}

- (IBAction)clickAttachment:(id)sender
{
    //----- Added By Mitul on 18-Dec Network Check -----//
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        UIButton *button = (UIButton *)sender;
        DBMetadata *file = [marr_folderNames objectAtIndex:button.tag];
        
        if([button.titleLabel.text isEqualToString:@"Attach"])
        {
            NSString *dbFilePath = [NSString stringWithFormat:@"%@/%@",strPathDB,file.filename];
            
            NSString *strLocalDBPath = [self getLocalDropBoxFilePath:file.filename fileIndex:button.tag];
            
            
            [[self restClient] loadFile:dbFilePath intoPath:strLocalDBPath];
            alert = [Utilities showLoadingMessageWithTitle:@"Loading file content"
                                                  delegate:self];
            
            [sender setTitle:@"Attached" forState:UIControlStateNormal];
            [sender setEnabled:NO];
        }
        else if([button.titleLabel.text isEqualToString:@"Attached"])
        {
            [sender setTitle:@"Attach" forState:UIControlStateNormal];
        }
    }
    else
    {
        
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}


#pragma mark DBRestClientDelegate methods

- (DBRestClient*)restClient {
    if (restClient == nil)
    {
        if ( [[DBSession sharedSession].userIds count] )
        {
            restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
            restClient.delegate = self;
        }
    }
    return restClient;
}


- (void)restClient:(DBRestClient*)client loadedMetadata:(DBMetadata*)metadata {
   // This code writes all contents of drop box into documnet directory which is not needed in PD.
    
    /*NSArray* validExtensions = [NSArray arrayWithObjects:@"jpg", @"jpeg", nil];
    for (DBMetadata* child in metadata.contents) {
        NSString* extension = [[child.path pathExtension] lowercaseString];
        if (!child.isDirectory && [validExtensions indexOfObject:extension] != NSNotFound) {
            NSString *temp = [NSString stringWithFormat:@"%@/%@",strPathDB,child.filename];
            //NSLog(@"temp = %@",temp);
            //[[self restClient] loadFile:temp intoPath:[FileUtils documentsDirectoryPathForResource:child.filename]];
        }
    }*/
    
    [marr_folderNames removeAllObjects];
    if (metadata.isDirectory)
    {
        for (DBMetadata *file in metadata.contents)
        {
            [marr_folderNames addObject:file];
        }
    }
    [self.tbl_View reloadData];
}

- (void)restClient:(DBRestClient*)client metadataUnchangedAtPath:(NSString*)path {
}

- (void)restClient:(DBRestClient*)client loadMetadataFailedWithError:(NSError*)error
{
}

- (void)restClient:(DBRestClient*)client loadedThumbnail:(NSString*)destPath {
}

- (void)restClient:(DBRestClient*)client loadThumbnailFailedWithError:(NSError*)error {
}


//delegate method to download files
- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)localPath
       contentType:(NSString*)contentType metadata:(DBMetadata*)metadata
{

    //----- Added By Mitul on 15-Dec-2013 -----//
    NSArray *arrString  = [strPathDB componentsSeparatedByString: @"/"];
    
    NSString *strKeyDirName;
    
    if ([[arrString lastObject] isEqualToString:@""])
    {
        strKeyDirName = @"DropBox";
    }
    else
    {
        strKeyDirName = [arrString lastObject];
    }
    
    NSMutableArray *aar =nil;
    
    aar = [[NSMutableArray alloc] init];
    
    NSArray *arrObjKeys = [[DELEGATE glb_mdict_for_Attachement] allKeys];
    
    BOOL flag = FALSE;
    
    flag = [arrObjKeys containsObject:strKeyDirName];
    
    if (flag == TRUE)
    {
        aar =[[DELEGATE glb_mdict_for_Attachement] valueForKey:strKeyDirName];
        
        if([aar count]==0)
        {
            aar = nil;
            aar = [[NSMutableArray alloc] init];
        }
        [aar addObject:[marrOfFolderFiles objectAtIndex:0]];
        [[DELEGATE glb_mdict_for_Attachement] setObject:aar forKey:strKeyDirName];
    }
    
    if(!flag)
    {
        aar =[[DELEGATE glb_mdict_for_Attachement] valueForKey:strKeyDirName];
        
        if([aar count]==0)
        {
            aar = nil;
            aar = [[NSMutableArray alloc] init];
        }
        
        [aar addObject:[marrOfFolderFiles objectAtIndex:0]];
        
        [[DELEGATE glb_mdict_for_Attachement] setObject:aar forKey:strKeyDirName];
    }
    
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    //--------- By Mitul ---------//
}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    
#if DEBUG
    NSLog(@"There was an error loading the file - %@", error);
#endif
    
}


@end
