//
//  AddAttachmentViewController.m
//  StudyPlaner
//
//  Created by Aman Gupta on 11/26/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "FileUtils.h"

#import "AddAttachmentViewController.h"
#import "FolderListViewController.h"
#import "DropBoxFilesViewController.h"
#import "Attachment.h"

@interface AddAttachmentViewController ()
{
    UIPopoverController *popOver;
    FolderListViewController *ptr_FolderListViewController;
    //----- Added By Mitul on -----//
    DropBoxFilesViewController *ptr_DropBoxFilesViewController;
    UINavigationController *dorpBoxNav;
    //----- By Mitul -----//
    UINavigationController *tempNav;
}
@end

@implementation AddAttachmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Aman added -- iOS7 optimisation --------------------------------------
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.view.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithRed:234/255.0f green:234/255.0f blue:234/255.0f alpha:1];
    }
    
    //Aman added -- iOS7 optimisation --------------------------------------

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----- Added by Mitul on 9 Dec -----//

- (void)viewWillDisappear:(BOOL)animated {
    
    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement]allKeys];
    NSMutableArray *marrAttachedDoc = [[NSMutableArray alloc] init];
    for(NSString *str in arrayOfKeys)
    {
        #if DEBUG
                NSLog(@"arr %@",[[DELEGATE glb_mdict_for_Attachement] valueForKey:str]);
        #endif
        
        for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
        {
            
            [marrAttachedDoc addObject:attachment];
        }
    }
    
    for (Attachment *a in [DELEGATE marrListedAttachments]) {
        [marrAttachedDoc addObject:a ];
    }
    #if DEBUG
        NSLog(@"viewWillDisappear Count %d ",[marrAttachedDoc count]);
    #endif
    
    
    if ([marrAttachedDoc count]>0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %lu",(unsigned long)[marrAttachedDoc count]]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if ([marrAttachedDoc count] > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];

        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) %d",[marrAttachedDoc count]] forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];

        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) 0"] forState:UIControlStateNormal];
    }
}
//----- by Mitul on 9 Dec -----//

//----- Added By Mitul on 18-Dec-2013 For Gallery Multi Save -----//

- (void)saveFileInGlobalArray:(NSMutableArray *)inMarrOfFolderFiles
{
    //TODO: Manage - File stored in Document Directory is Diff when we Save file with same name
    NSString *strKeyDirName = @"Gallery";
    
    NSMutableArray *aar =nil;
    
    aar = [[NSMutableArray alloc] init];
    
    #if DEBUG
        //NSLog(@"inMarrOfFolderFiles %@", inMarrOfFolderFiles);
    #endif
    
    
    NSArray *arrObjKeys = [[DELEGATE glb_mdict_for_Attachement] allKeys];
    
    BOOL flag = FALSE;
    
    flag = [arrObjKeys containsObject:strKeyDirName];
    
    if (flag == TRUE)
    {
        aar =[[DELEGATE glb_mdict_for_Attachement] valueForKey:strKeyDirName];
        
        if([aar count]==0)
        {
            aar = nil;
            aar = [[NSMutableArray alloc] init];
        }
        [aar addObject:[inMarrOfFolderFiles objectAtIndex:0]];
        [[DELEGATE glb_mdict_for_Attachement] setObject:aar forKey:strKeyDirName];
    }
    
    if(!flag)
    {
        aar =[[DELEGATE glb_mdict_for_Attachement] valueForKey:strKeyDirName];
        
        if([aar count]==0)
        {
            aar = nil;
            aar = [[NSMutableArray alloc] init];
        }
        
        [aar addObject:[inMarrOfFolderFiles objectAtIndex:0]];
        
        [[DELEGATE glb_mdict_for_Attachement] setObject:aar forKey:strKeyDirName];
    }
    
#if DEBUG
    NSLog(@"[DELEGATE glb_mdict_for_Attachement] %@",[DELEGATE glb_mdict_for_Attachement]);
#endif

}

- (void)removeObjectFromGlobalArrayForPath:(NSString *)inStrFilePath
{
    NSMutableDictionary *dictLocalCopy = [DELEGATE glb_mdict_for_Attachement];
    int i=0;
    for(Attachment *att in [[DELEGATE glb_mdict_for_Attachement] valueForKey:@"Gallery"])
    {
        
        if ( [att.localFilePath isEqualToString:inStrFilePath])
        {
            [[dictLocalCopy valueForKey:@"Gallery"] removeObjectAtIndex:i];
            break;
        }
        i++;
    }

    [DELEGATE setGlb_mdict_for_Attachement:dictLocalCopy];
    
}

//--------- By Mitul ---------//

- (void)createGalleryStructureToSaveImage:(NSString *)strFileName fileData:(NSData *)inFileData
{
    //----- Added By Mitul on 17-Dec-2013 For Application DirectoryName -----//
    NSString *dataPath = [FileUtils createNewDirectoryAtGivenDocumentDirectoryPath:@"Gallery"];
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strFileName]];
    
    NSArray *arrString  = [strFileName componentsSeparatedByString: @"."];
    NSMutableArray *marrOfFolderFiles = [[NSMutableArray alloc] init];
    
    Attachment *attachment = [[Attachment alloc]init];
    attachment.driveName = @"Gallery" ;
    attachment.displayName = strFileName ;
    attachment.localFilePath = filePath ;
    attachment.indexPath = @"0";
    attachment.localFolderName = @"Gallery" ;
    attachment.fileType = [arrString lastObject] ;
    [marrOfFolderFiles addObject:attachment];
    attachment = nil ;
    
    
    
//
//    [marrOfFolderFiles addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"Gallery",@"DriveName",strFileName,@"Title",filePath,@"FilePath",0,@"IndexPath",@"Gallery",@"LocalFolderName", [arrString lastObject], @"Type", nil]];
    
    // If File Exists
    if ( ![FileUtils fileExistsAtPath:dataPath fileName:strFileName])
    {
        BOOL isFileSaved = [FileUtils writeFileToAnyDirectory:inFileData withFileName:[arrString objectAtIndex:0] withExtension:[arrString lastObject] andPath:dataPath];
        
        if (isFileSaved)
        {
            //----- Added By Mitul on 18-Dec For Gallery Multi Save -----//
            [self saveFileInGlobalArray:marrOfFolderFiles];
            //--------- By Mitul ---------//
            //[[DELEGATE glb_mdict_for_Attachement] setObject:marrOfFolderFiles forKey:@"Gallery"];
        }
        else
        {
            // Show alert that file not Saved please try after some time.
        }

    }
    else
    {
        BOOL isFileRemoved = [FileUtils removeFileFromPath:filePath];
        if (isFileRemoved)
        {
            [self removeObjectFromGlobalArrayForPath:filePath];
            
            BOOL isFileSaved = [FileUtils writeFileToAnyDirectory:inFileData withFileName:[arrString objectAtIndex:0] withExtension:[arrString lastObject] andPath:dataPath];
            if (isFileSaved)
            {
                [self saveFileInGlobalArray:marrOfFolderFiles];
            }
            else
            {
                // Show alert that file not Saved please try after some time.
            }
        }
        else
        {
            // Show alert that file not removed please try after some time.
        }
    }
    //--------- By Mitul ---------//
    
}


#pragma mark
#pragma mark Button Actions

- (IBAction)selectGoogleDrive:(id)sender {
    
    //----- Added By Mitul on 18-Dec Network Check -----//
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        UIButton *button = (UIButton*)sender;
        
        ptr_FolderListViewController = [[FolderListViewController alloc] init];
        tempNav = [[UINavigationController alloc] initWithRootViewController:ptr_FolderListViewController];
        
        popOver=nil;
        popOver = [[UIPopoverController alloc] initWithContentViewController:tempNav];
        [popOver setPopoverContentSize:CGSizeMake(320, 500)];
        [popOver presentPopoverFromRect:CGRectMake(button.frame.size.width / 2, button.frame.size.height / 2, 1, 1) inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

- (IBAction)selectDropBox:(id)sender
{
    //----- Added By Mitul on 18-Dec Network Check -----//
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        UIButton *button = (UIButton*)sender;
        
        ptr_DropBoxFilesViewController = [[DropBoxFilesViewController alloc] init];
        dorpBoxNav = [[UINavigationController alloc] initWithRootViewController:ptr_DropBoxFilesViewController];
        
        //DropBoxFilesViewController *dropBoxVC = [[DropBoxFilesViewController alloc] init];
        
        popOver=nil;
        popOver = [[UIPopoverController alloc] initWithContentViewController:dorpBoxNav];
        [popOver setPopoverContentSize:CGSizeMake(320, 500)];
        [popOver presentPopoverFromRect:CGRectMake(button.frame.size.width / 2, button.frame.size.height / 2, 1, 1) inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

- (IBAction)selectCamera:(id)sender {
    UIButton *button = (UIButton*)sender;

    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.allowsEditing = NO;
        
        //-----------
        if([[[UIDevice currentDevice] systemVersion] integerValue] >= 7)
        {
            // do something for iOS 7
            CGFloat scaleFactor=1.3f;
            //CGFloat scaleFactor=1.5f;
            
            switch ([UIApplication sharedApplication].statusBarOrientation) {
                    
                case UIInterfaceOrientationLandscapeLeft:
                {
                    if([[[UIDevice currentDevice] systemVersion] integerValue] >= 7 && [[[UIDevice currentDevice] systemVersion] integerValue] <8)
                    {
                        controller.cameraViewTransform = CGAffineTransformScale(CGAffineTransformMakeRotation(M_PI * 90 / 180.0), scaleFactor, scaleFactor);

                    }
                }
                    break;
                    
                case UIInterfaceOrientationLandscapeRight:
                {
                    if([[[UIDevice currentDevice] systemVersion] integerValue] >= 7 && [[[UIDevice currentDevice] systemVersion] integerValue] <8)
                    {
                    
                    controller.cameraViewTransform = CGAffineTransformScale(CGAffineTransformMakeRotation(M_PI * -90 / 180.0), scaleFactor, scaleFactor);
                    }
                }
                    break;
                    
                case UIInterfaceOrientationPortraitUpsideDown:
                    
                    controller.cameraViewTransform = CGAffineTransformMakeRotation(M_PI * 180 / 180.0);
                    
                    break;
                    
                default:
                    break;
            }
        }
        else
        {
            // do something for iOS 6 or less
//            CGFloat scaleFactor=1.3f;
            //CGFloat scaleFactor=1.5f;
            
            switch ([UIApplication sharedApplication].statusBarOrientation) {
                    
                /*case UIInterfaceOrientationLandscapeLeft:
                    
                    controller.cameraViewTransform = CGAffineTransformScale(CGAffineTransformMakeRotation(M_PI * 90 / 180.0), scaleFactor, scaleFactor);
                    
                    break;
                */
                /*case UIInterfaceOrientationLandscapeRight:
                    
                    controller.cameraViewTransform = CGAffineTransformScale(CGAffineTransformMakeRotation(M_PI * -90 / 180.0), scaleFactor, scaleFactor);
                    
                    break;
                */
                case UIInterfaceOrientationPortraitUpsideDown:
                    
                    controller.cameraViewTransform = CGAffineTransformMakeRotation(M_PI * 180 / 180.0);
                    
                    break;

                case UIInterfaceOrientationPortrait:
                    
                    controller.cameraViewTransform = CGAffineTransformMakeRotation(M_PI * 180 / 180.0);
                    
                    break;
                    
                default:
                    break;
            }
        }
        
        
        controller.delegate = self;
        //-----------
        //controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];

        //-------
        popOver=nil;
        popOver = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popOver setPopoverContentSize:CGSizeMake(320, 500)];
        [popOver presentPopoverFromRect:CGRectMake(button.frame.size.width / 2, button.frame.size.height / 2, 1, 1) inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

        //-------
        //popOver=nil;
        //popOver = [[UIPopoverController alloc] initWithContentViewController:controller];
        //[popOver setPopoverContentSize:CGSizeMake(320, 500)];
        //[popOver presentPopoverFromRect:CGRectMake(button.frame.size.width / 2, button.frame.size.height / 2, 1, 1) inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

    }
    
}

- (IBAction)selectPhotoLibrary:(id)sender
{
    UIButton *button = (UIButton*)sender;

    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = NO;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];
        controller.delegate = self;
        
        popOver=nil;
        popOver = [[UIPopoverController alloc] initWithContentViewController:controller];
        [popOver setPopoverContentSize:CGSizeMake(320, 500)];
        [popOver presentPopoverFromRect:CGRectMake(button.frame.size.width / 2, button.frame.size.height / 2, 1, 1) inView:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark -
#pragma mark scaleAndRotateImage Camera Image Method  -

- (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 320; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

#pragma mark -
#pragma mark ImagePicker Delegate Methods  -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //----- Added By Mitul on -----//
    //UIImage *initialImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    //NSData *data = UIImagePNGRepresentation(self.initialImage);
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    NSData *imageData = nil;
    imageData = UIImageJPEGRepresentation(image, 0.4);
    
    //----------
    //image = [self scaleAndRotateImage:image];
    
    //----------
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        if (fileName)
        {
            [self createGalleryStructureToSaveImage:fileName fileData:imageData];
        }
        else
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            
            NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
            [timeFormat setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
            
            NSDate *now = [[NSDate alloc] init];
            
            NSString *theDate = [dateFormat stringFromDate:now];
            NSString *theTime = [timeFormat stringFromDate:now];
            
            fileName = [NSString stringWithFormat:@"IMG_%@-%@.jpg", theDate, theTime];
            
            [self createGalleryStructureToSaveImage:fileName fileData:imageData];
        }
    };
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    
    if([popOver isPopoverVisible])
    {
        [popOver dismissPopoverAnimated:YES];
    }
    //----- By Mitul -----//
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}

@end
