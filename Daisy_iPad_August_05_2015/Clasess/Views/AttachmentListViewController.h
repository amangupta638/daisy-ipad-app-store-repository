//
//  AttachmentListViewController.h
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/9/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttachmentListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
}

@property (retain, nonatomic) IBOutlet UITableView *tblViewShowAttachmentList;
@property (retain, nonatomic) NSMutableDictionary *mdictSelectedAttachments;
@property (assign, nonatomic) BOOL shouldItemLock ;

- (void)setAttachmentListArray;

@end
