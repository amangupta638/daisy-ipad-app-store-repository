//
//  AboutViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "AboutViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "School_Information.h"
#import "Service.h"
#import "MyProfile.h"

@interface AboutViewController ()
{
    NSString *strAbout;
}
@end

@implementation AboutViewController

@synthesize btn_youTube;
@synthesize btn_PDF;

@synthesize _arrForAboutData;
@synthesize _testlayer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_webView.layer setMasksToBounds:YES];
    [_webView.layer setCornerRadius:5.0f];
    [_webView.layer setBorderWidth:1.0];
    [_webView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    
    count=0;
    
    _tablInfoView.frame=CGRectMake(900, _tablInfoView.frame.origin.y, _tablInfoView.frame.size.width, _tablInfoView.frame.size.height);
    
    //----- Added By Mitul on 16-Dec-2013 For About -----//
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];

    
    
    // For Daisy
    if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
    {
        //strAbout = @"about Daisy";
        strAbout = kStrAboutImage;
        
        if([profobj.type isEqualToString:@"Teacher"])
        {
            self._arrForAboutData=[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",strAbout],@"home screen",@"Day View",@"month view",@"add item",@"Timetable",@"my class", @"Inbox", @"my profile screen", nil];
        }
        else
        {
            self._arrForAboutData=[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",strAbout],@"home screen",@"Day View",@"month view",@"add item",@"Timetable",@"Inbox",@"my parents screen", @"my profile screen", nil];
        }
    }
    // For Prime
    // For Teacher And Student Same content
    else
    {
        strAbout = kStrAboutImage;
        if([profobj.type isEqualToString:@"Teacher"])
        {
            self._arrForAboutData=[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",strAbout],@"Home_Screen",@"Day_View",@"Month_View",@"Add_Item",@"Timetable",@"My_Class", @"_Inbox", nil];
        }
        else
        {
            self._arrForAboutData=[NSArray arrayWithObjects:[NSString stringWithFormat:@"%@",strAbout],@"Home_Screen",@"Day_View",@"Month_View",@"Add_Item",@"Timetable", @"_Inbox", @"My_Profile", nil];
        }
    }
    
#if DEBUG
    NSLog(@"profobj.type %@",profobj.type );
#endif
    
    //--------- By Mitul ---------//
    
    [self updateAboutDaisy];
}
-(void)updateAboutDaisy{
    
    //Important
    
    count=0;
    if(_arrForAboutData.count>0)
    {
        //_tempImg.image=[UIImage imageNamed:[self._arrForAboutData objectAtIndex:count]];
        
        [self setTempImage:count];
        
      //  School_Information *about=[self._arrForAboutData objectAtIndex:count];
    //    [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
        
        [_tableView reloadData];
    }
    
    
    
    if(_arrForAboutData.count>0){
      //  School_Information *about2=[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-1];
       // School_Information *about3=[self._arrForAboutData objectAtIndex:1];
        _labelForPrev.text=[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-1];
        
        // NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",@"Daisy Installation and System Overview"];
        NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:1]];
        
        NSArray *arrItem=[str componentsSeparatedByString:@"  "];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                             initWithString:str];
        NSString *str1=[arrItem objectAtIndex:0];
        NSString *str2=[arrItem objectAtIndex:1];
        CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
        CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold1
                       range:[str rangeOfString:str2]];
        
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor darkGrayColor].CGColor
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                       range:[str rangeOfString:str2]];
        
        if (self._testlayer!=nil){
            
            [self._testlayer removeFromSuperlayer];
        }
        CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
        normalTextLayer_.string =string;
        normalTextLayer_.wrapped = YES;
        normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
        normalTextLayer_.alignmentMode = kCAAlignmentRight;
        normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
        self._testlayer=normalTextLayer_;
        [_lableForNext.layer addSublayer: self._testlayer];
        [_lableForNext setNeedsDisplay];
        [normalTextLayer_ release];
        [string release];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    // [btn_youTube setFrame:CGRectMake(62, 466, 368, 30)];
    
    // Check if Login with Teacher or With Student
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    // For Daisy
    if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
    {
        if([profobj.type isEqualToString:@"Teacher"])
        {
            [btn_youTube setTitle:kAboutTeacherYouTubeURL forState:UIControlStateNormal];
            [self.btn_PDF setTitle:kAboutTeacherPDFURL forState:UIControlStateNormal];
        }
        else
        {
            [btn_youTube setTitle:kAboutStudentYouTubeURL forState:UIControlStateNormal];
            [self.btn_PDF setTitle:kAboutStudentPDFURL forState:UIControlStateNormal];
        }
        //self.btn_PDF.titleLabel.text = kAboutPDFURL;
    }
    // For Prime
    // For Teacher And Student Same content
    else
    {
        if([profobj.type isEqualToString:@"Teacher"])
        {
            [btn_youTube setTitle:kAboutTeacherYouTubeURL forState:UIControlStateNormal];
            [self.btn_PDF setTitle:kAboutTeacherPDFURL forState:UIControlStateNormal];
        }
        else
        {
            [btn_youTube setTitle:kAboutStudentYouTubeURL forState:UIControlStateNormal];
            [self.btn_PDF setTitle:kAboutStudentPDFURL forState:UIControlStateNormal];
        }
    }
    //[btn_PDF setFrame:CGRectMake(62, 524, 490, 30)];
}

//for search
-(void)searchAboutContent:(School_Information*)infoObj
{
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setTempImage:(NSInteger)indexCount
{
    [self.btn_PDF setHidden:YES];
    [self.btn_youTube setHidden:YES];
    if ( [[self._arrForAboutData objectAtIndex:indexCount] isEqualToString:@"my profile screen"] )
    {
        // Check if Login with Teacher or With Student
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        // Teacher Login
        if([profobj.type isEqualToString:@"Teacher"])
        {
            _tempImg.image=[UIImage imageNamed:@"my profile screen teacher"];
        }
        else // Student Login
        {
            _tempImg.image=[UIImage imageNamed:@"my profile screen student"];
        }
    }
    else if ( [[self._arrForAboutData objectAtIndex:indexCount] isEqualToString:@"home screen"] )
    {
        // Check if Login with Teacher or With Student
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        // Teacher Login
        if([profobj.type isEqualToString:@"Teacher"])
        {
            _tempImg.image=[UIImage imageNamed:@"home screen teacher"];
        }
        else // Student Login
        {
            _tempImg.image=[UIImage imageNamed:@"home screen student"];
        }
    }
    else if ( [[self._arrForAboutData objectAtIndex:indexCount] isEqualToString:@"Timetable"] )
    {
        // For Daisy
        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
        {
            _tempImg.image=[UIImage imageNamed:@"Timetable_Daisy"];
        }
        // For Prime
        // For Teacher And Student Same content
        else
        {
            _tempImg.image=[UIImage imageNamed:@"Timetable_Prime"];
        }
    }
    else
    {
       
        _tempImg.image=[UIImage imageNamed:[self._arrForAboutData objectAtIndex:indexCount]];

        if ( [[self._arrForAboutData objectAtIndex:indexCount] isEqualToString:strAbout])
       {
           [self.btn_PDF setHidden:NO];
           [self.btn_youTube setHidden:NO];
       }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return height for rows.
    return 45;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{        return self._arrForAboutData.count;
    
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0;
    
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    //    UIView *headerView=[[[UIView alloc] init] autorelease];
    //    headerView.backgroundColor=[UIColor clearColor];
    //    UIImageView *sectionImage=[[UIImageView alloc] init];
    //
    //    sectionImage.backgroundColor=[UIColor clearColor];
    //    [headerView addSubview:sectionImage];
    //
    //    headerView.frame=CGRectMake(0, 0,220, 45);
    //    sectionImage.frame=CGRectMake(0, 0,220, 45);
    //
    //    [sectionImage release];
    //
    //
    //    UILabel* sectionLabel=[[UILabel alloc] init];
    //    sectionLabel.frame=CGRectMake(10, 0,240, 30);
    //    sectionLabel.font=[UIFont boldSystemFontOfSize:14.0f];
    //    sectionLabel.backgroundColor=[UIColor clearColor];
    //    sectionLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
    //    [headerView addSubview:sectionLabel];
    //    [sectionLabel release];
    //
    //    if (section==0) {
    //        sectionImage.image=[UIImage imageNamed:@"topcell.png"];
    //        sectionLabel.text=@"Student Profile";
    //    }
    //    else{
    //        sectionImage.image=[UIImage imageNamed:@"profilemidcell.png"];
    //
    //        sectionLabel.text=@"About Empower";
    //    }
    return nil;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
    UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (myCell == nil){
        myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
        myCell.backgroundColor=[UIColor clearColor ];
        myCell.selectionStyle = UITableViewCellSelectionStyleGray;
        myCell.accessoryType=UITableViewCellAccessoryNone;
        
        
        UILabel * lblForTitle=[[UILabel alloc]init];
        lblForTitle.numberOfLines=2;
        lblForTitle.tag=1000;
        lblForTitle.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
        lblForTitle.backgroundColor=[UIColor clearColor];
        
        UIImageView * cellBgImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 220, 45)];
        cellBgImg.backgroundColor=[UIColor clearColor];
        cellBgImg.tag=89;
        [myCell.contentView addSubview:cellBgImg];
        [myCell.contentView addSubview:lblForTitle];
        [cellBgImg release];
        [lblForTitle release];
        
    }
    UILabel * lblForTitle=(UILabel*)[myCell.contentView viewWithTag:1000];
    //UIImageView *cellBgImg=(UIImageView*)[myCell.contentView viewWithTag:89];
    
    //cellBgImg.image=[UIImage imageNamed:@"noteEnterBgCell.png"];
    
    //School_Information *about=[self._arrForAboutData objectAtIndex:indexPath.row];
    
    
    
//    if ([about.parent_id intValue]==0) {
//        lblForTitle.frame=CGRectMake(10, 7,200, 30);
//        lblForTitle.font=[UIFont boldSystemFontOfSize:12.0f];
//        lblForTitle.textColor=[UIColor darkGrayColor];
//    }
//    else{
//        
//        lblForTitle.frame=CGRectMake(30, 7,190, 30);
//        lblForTitle.font=[UIFont systemFontOfSize:12.0f];
//        lblForTitle.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
//    }
    lblForTitle.frame=CGRectMake(30, 7,190, 30);
           lblForTitle.font=[UIFont systemFontOfSize:12.0f];
            lblForTitle.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
    lblForTitle.text=[self._arrForAboutData objectAtIndex:indexPath.row];
    
    
    return myCell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(count!=indexPath.row){
        count=indexPath.row;
        
        if (indexPath.row==0)
        {
//            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            //_tempImg.image=[UIImage imageNamed:[self._arrForAboutData objectAtIndex:count]];
            
//            School_Information *about=[self._arrForAboutData objectAtIndex:count];
//            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
//            School_Information *about2=[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-1];
//            School_Information *about3=[self._arrForAboutData objectAtIndex:count+1];
//            
            
            _labelForPrev.text=[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-1];
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:count+1]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
        else if(indexPath.row==self._arrForAboutData.count-1){
            
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            //_tempImg.image=[UIImage imageNamed:[self._arrForAboutData objectAtIndex:count]];

            //_labelForPrev.text=[self._arrForAboutData objectAtIndex:0];
            _labelForPrev.text = [self._arrForAboutData objectAtIndex:self._arrForAboutData.count-2];//NextPrev_Logic Changed
            
            //NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-2]];
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:0]];//NextPrev_Logic Changed
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
        else{
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            
            _labelForPrev.text=[self._arrForAboutData objectAtIndex:count-1];
            
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:count+1]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
        
        
    }
    
}

#pragma mark -
#pragma mark Check Login  -

#pragma mark
#pragma mark Button Action
- (IBAction)informationButtonAction:(id)sender {
    
    if (_informationBtn.frame.origin.x==861) {
        [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_blackImgView];
        [UIView animateWithDuration:0.3
                         animations:^{
                             _informationBtn.frame=CGRectMake(746, _informationBtn.frame.origin.y, _informationBtn.frame.size.width, _informationBtn.frame.size.height);
                             _tablInfoView.frame=CGRectMake(775, _tablInfoView.frame.origin.y, _tablInfoView.frame.size.width, _tablInfoView.frame.size.height);
                             
                         }];
        [_informationBtn setImage:[UIImage imageNamed:@"popuparrow.png"] forState:UIControlStateNormal];
    }
    else{
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             _informationBtn.frame=CGRectMake(861, _informationBtn.frame.origin.y, _informationBtn.frame.size.width, _informationBtn.frame.size.height);
                             _tablInfoView.frame=CGRectMake(900, _tablInfoView.frame.origin.y, _tablInfoView.frame.size.width, _tablInfoView.frame.size.height);
                         }];
        [_blackImgView removeFromSuperview];
        [_informationBtn setImage:[UIImage imageNamed:@"informationpopup.png"] forState:UIControlStateNormal];
        
    }
    
    
}

- (IBAction)previousButtonAction:(id)sender {
    
    if (self._arrForAboutData.count>0){
        if (count>1) {
            count=count-1;
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//

            _labelForPrev.text=[self._arrForAboutData objectAtIndex:count-1];
            
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:count+1]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
            //for text lable
        }
        else if(count==1){
            count=count-1;
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            
            _labelForPrev.text=[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-1];
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:count+1]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
            
        }
        else if(count==0){
            count=self._arrForAboutData.count-1;
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
        
            NSString *about2=nil;
            if (count-1==-1) {
                about2=[self._arrForAboutData objectAtIndex:0];
            }
            else{
                about2=[self._arrForAboutData objectAtIndex:count-1];
            }
            
            _labelForPrev.text=about2;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:0]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
        

    }
    
       
    
    
}

- (IBAction)nextButtonAction:(id)sender {
    if (self._arrForAboutData.count>0) {
        if (count==self._arrForAboutData.count-1) {
            count=0;
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            
             _labelForPrev.text=[self._arrForAboutData objectAtIndex:self._arrForAboutData.count-1];
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:count+1]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
        else if (count==self._arrForAboutData.count-2)
        {
            count=count+1;
            
            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            
            _labelForPrev.text=[self._arrForAboutData objectAtIndex:count-1];
            
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:0]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
        else
        {
            count=count+1;

            //----- Added By Mitul on 16-Dec-2013 For About -----//
            [self setTempImage:count];
            //--------- By Mitul ---------//
            
            _labelForPrev.text=[self._arrForAboutData objectAtIndex:count-1];
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",[self._arrForAboutData objectAtIndex:count+1]];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }

    }
    
    
}

- (IBAction)youtubeLinkToOpen:(id)sender
{
    // Check if Login with Teacher or With Student
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    // For Teacher And Student Different content
    // Teacher Login
    if([profobj.type isEqualToString:@"Teacher"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAboutTeacherYouTubeURL]];
    }
    // Student Login
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAboutStudentYouTubeURL]];
    }
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAboutYouTubeURL]];
}

- (IBAction)pdfLinkToOpen:(id)sender
{
    // Check if Login with Teacher or With Student
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    // For Teacher And Student Different content
    // Teacher Login
    if([profobj.type isEqualToString:@"Teacher"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAboutTeacherPDFURL]];
    }
    // Student Login
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAboutStudentPDFURL]];
    }
    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAboutPDFURL]];
}

- (void)dealloc {
    [_testlayer release];
    [_arrForAboutData release];
    [_blackImgView release];
    [_tablInfoView release];
    [_informationBtn release];
    [_tableView release];
    [_btnForPrev release];
    [_labelForPrev release];
    [_btnForNext release];
    [_lableForNext release];
    [_webView release];
    [_tempImg release];
    [self.btn_youTube release];
    [self.btn_PDF release];
    [super dealloc];
}
- (void)viewDidUnload {
    [_blackImgView release];
    _blackImgView = nil;
    [_tablInfoView release];
    _tablInfoView = nil;
    [_informationBtn release];
    _informationBtn = nil;
    [_tableView release];
    _tableView = nil;
    [_btnForPrev release];
    _btnForPrev = nil;
    
    [_labelForPrev release];
    _labelForPrev = nil;
    [_btnForNext release];
    _btnForNext = nil;
    [_lableForNext release];
    _lableForNext = nil;
    [_webView release];
    _webView = nil;
    [_tempImg release];
    _tempImg = nil;
    [super viewDidUnload];
}
@end
