//
//  Utilities.h
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject
+ (UIAlertView *)showLoadingMessageWithTitle:(NSString *)title
                                    delegate:(id)delegate;
+ (void)showErrorMessageWithTitle:(NSString *)title
                          message:(NSString *)message
                         delegate:(id)delegate;
@end
