//
//  CalenderViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarLogicDelegate.h"
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "TimeTable.h"
#import "School_Information.h"
@class CalendarLogic;
@class ShowAllDairyItemViewController;
@class ShowNotesViewController;

#import "CountDownTimerUtility.h"

@interface CalenderViewController : UIViewController<CalendarLogicDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIPopoverControllerDelegate,UIWebViewDelegate,CountDownTimerProtocol>{
    IBOutlet UIButton *_buttonForDay;
    IBOutlet UIButton *_buttonFormonth;
    IBOutlet UIButton *_buttonForWeek;
    IBOutlet UIButton *_buttonForGlance;
    IBOutlet UIButton *_buttonForFilterEvent;
    IBOutlet UIButton *_buttonFortaskFilter;
    IBOutlet UIButton *_buttonForFilterNotes;
    IBOutlet UIView *_viewForOption;
    IBOutlet UIView *_viewForFilter;
    IBOutlet UILabel *_lableForMonthName;
    IBOutlet UIImageView *_selectedMonthImg;
    IBOutlet UITableView *_table;
    IBOutlet UIView *_viewForWeek;
    NSMutableArray *_arr;
    IBOutlet UILabel *_lableForDate;
    IBOutlet UILabel *_lableForDay;
    IBOutlet UIView *_viewForDay;
    IBOutlet UILabel *_lableForDayHeading;
    IBOutlet UIView *_viewForMonthweek;
    IBOutlet UIView *_viewForPicker;
    IBOutlet UILabel *_lableForMonthHeading;
    UIPickerView *pickerViewDay;
    IBOutlet UILabel *_lableForGlancemonth2;
    IBOutlet UILabel *_lableForGlancemonth3;
    IBOutlet UILabel *_lableForGlancemonth1;
    IBOutlet UILabel *_lableForGlancemonth4;
    IBOutlet UIView *_viewForGlanceTable;
    IBOutlet UIView *_viewForItemDetails;
   
    IBOutlet UITableView *eventTableV;
    IBOutlet UIButton *_buttonForDiscrption;
    IBOutlet UIWebView *_webView;
    NSInteger itemCount;
    NSInteger count;
    NSInteger aDatesIndexForMonth;
    IBOutlet UIButton *_buttonForDateIcon;
    NSDateFormatter *_globlDateFormat;
    BOOL _isFirstSem;
    NSMutableArray *_arrForweek;
    NSMutableArray *_arrForMonth;
    NSMutableArray *_arrForEvent;
    BOOL _isCycleday;
   
    NSMutableDictionary *dictFlurryParameter;
    
    /************* Out Of Class Start *****************/
    IBOutlet UIView *viewForApprovedPass;
    IBOutlet UIView *viewForApprovedPassInner;
    IBOutlet UILabel *lblPassType;
    IBOutlet UILabel *lblFromTime;
    IBOutlet UILabel *lblToTime;
    IBOutlet UILabel *lblPassAppovedBy;
    IBOutlet UILabel *lblApprovedPassTimer;
    IBOutlet UILabel *lblElapsedPassTimer;
    IBOutlet UILabel *lblPassDate;
    
    UIButton *btn_ToHoldClickMonthDate;
    IBOutlet UILabel *lblPassApprovedByTitle;
    IBOutlet UIButton *dateIconButton;
    /************* Out Of Class End *****************/
    
    Item *selectedDiaryItem;
    
    IBOutlet UIButton *btn_DeletePass;
    
    // Campus Based Start
    /*********************************/
    NSMutableArray *marrOFPickerMonthYears;
    NSInteger selectedMonthIndex;
    /*********************************/
    // Campus Based End
}
//@property (nonatomic,retain)NSArray *_arrForShortLble;
- (IBAction)dateButtonIconAction:(id)sender;
@property(nonatomic,retain)  IBOutlet UITableView *_table;

@property (nonatomic,retain)TimeTable *_curentTimeTable;
@property (nonatomic,retain)NSArray *_arrForCalenderData;
@property(nonatomic,retain) ShowAllDairyItemViewController *_viewForShowAllDairyItem;
@property(nonatomic,retain) ShowNotesViewController *_viewForShowAllNotesItem;
@property(nonatomic,retain) NSString *strForSub;
@property(nonatomic,retain) NSString *strForDayDate;
@property(nonatomic,retain) NSString *startTimeForDayView;

@property(nonatomic,retain) UIPopoverController *_popoverControler;
//@property(nonatomic,strong) UIPopoverController *_popoverControler;

@property(retain,nonatomic) CATextLayer *_testlayer;
@property (nonatomic, retain) NSArray *datesIndex;
@property (nonatomic, retain) NSArray *buttonsIndex;
@property (nonatomic) NSInteger numberOfDaysInWeek;
@property (nonatomic) NSInteger selectedButton;
@property (nonatomic, retain) CalendarLogic *calendarLogic;
@property (nonatomic, retain) UIView *calendarView;
@property (nonatomic, retain) NSDate *selectedDate;
@property (nonatomic, retain) NSDate *_dateForPicker;
- (IBAction)detailsDescriptionButtonClick:(id)sender;
- (IBAction)dayButtonAction:(id)sender;
- (IBAction)filterButtonAction:(id)sender;
- (IBAction)weekButtonAction:(id)sender;
- (IBAction)monthButtonAction:(id)sender;
- (IBAction)glanceButtonAction:(id)sender;
- (void)animationMonthSlideComplete;
- (IBAction)viewAllButtonAction:(id)sender;
-(void)updateCalenderView;
-(void)getNewDateOfamonth:(NSInteger)month day:(NSInteger)day;
-(void)searchPanelContent:(School_Information*)infoObj;

- (IBAction)openCalendarAccessURL:(id)sender;
-(void)showPopupButtonPressedForWeekAndMonth:(id)sender;
-(void)showPopupButtonPressed:(id)sender;
-(void)showPassesInMonthviewWhenclickFromShowAllDairyItem:(id)sender;
- (IBAction)deletePassAction:(id)sender;

// Campus Based Start
-(void)updateClock:(NSTimer *)timer;

// Campus Based End

//------------------------------------------------------------------------------
// Metod added to remove warnings

- (void)dismisPopover;
-(void)updateCalenderlDaisy;
//------------------------------------------------------------------------------

@end
