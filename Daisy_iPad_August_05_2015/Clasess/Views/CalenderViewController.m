    //
//  CalenderViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//hh @"AM"

#import "CalenderViewController.h"
#import "CalendarLogic.h"
#import "Service.h"
#import "Item.h"
#import "Subjects.h"
#import "AppHelper.h"
#import "ShowAllDairyItemViewController.h"
#import "EventItem.h"

#import "TimeTableCycle.h"
#import "Period.h"
#import "MyClass.h"
#import "DayOfNotes.h"
#import "ShowNotesViewController.h"
#import "TableForCycle.h"
#import "SchoolInformation.h"
#import "ShowAttachmentViewController.h"
#import "DiaryItemTypes.h"
#import "Flurry.h"
#import "MyProfile.h"
#import "MainViewController.h"
// Campus Based Start
#import "CalendarCycleDay.h"
// Campus Based End

#import "FileUtils.h"

#define kWEEK_DAY_LABEL_HEIGHT 20
#define kGAP_BETWEEN_LABELS 5
#define kADDITIONAL_HEIGHT 15

@interface CalenderViewController ()
{
    // Pass For Timer Logic
    NSTimer *countDownTimer;
    NSCalendar *gregorianCalendar;
    
// TODO: Set Flag if Button Pressed to fix calendar picker issue 2565
    BOOL isSpecificDateSelected;
}
@end

@implementation CalenderViewController
#define kCalendarDayWidth	125.0f
//Cycle labels are not displaying in calendar Month and Glance views - start
#define kCalendarDayHeight	82.0f
//Cycle labels are not displaying in calendar Month and Glance views - end
@synthesize calendarLogic;
@synthesize calendarView,_testlayer;
@synthesize selectedDate,selectedButton,datesIndex,buttonsIndex,numberOfDaysInWeek,_dateForPicker,_popoverControler;
@synthesize strForDayDate;
@synthesize startTimeForDayView;
@synthesize _viewForShowAllDairyItem,_arrForCalenderData,_curentTimeTable;
@synthesize _viewForShowAllNotesItem,_table,strForSub;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil

{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark scrolViewLifeCycle
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if(_buttonFormonth.selected || _buttonForGlance.selected){
        
        
        UIView *viewForMonth=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
        viewForMonth.backgroundColor=[UIColor clearColor];
        CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
        rotate = CGAffineTransformScale(rotate, 0.5, 4.0);
        [viewForMonth setTransform:rotate];
        viewForMonth.tag=1000;
        UILabel *lableForMonth=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
        lableForMonth.backgroundColor=[UIColor clearColor];
        lableForMonth.tag=1;
        lableForMonth.textAlignment=NSTextAlignmentCenter;
        lableForMonth.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
        lableForMonth.text=[_arrForMonth objectAtIndex:row];
        lableForMonth.textColor=[UIColor darkGrayColor];
        [viewForMonth addSubview:lableForMonth];
        [lableForMonth release];
        
        return   [viewForMonth autorelease];
    }
    else if(_buttonForWeek.selected)
    {
        
        UIView *viewForMonth=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
        
        viewForMonth.backgroundColor=[UIColor clearColor];
        CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
        rotate = CGAffineTransformScale(rotate, 0.5, 4.0);
        [viewForMonth setTransform:rotate];
        viewForMonth.tag=1000;
        UILabel *lableForMonth=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
        lableForMonth.backgroundColor=[UIColor clearColor];
        lableForMonth.tag=1;
        lableForMonth.textAlignment=NSTextAlignmentCenter;
        lableForMonth.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
        // lableForMonth.text=[monthSymbols objectAtIndex:row];
        lableForMonth.textColor=[UIColor darkGrayColor];
        UILabel *lableForMonthWeek=[[UILabel alloc]initWithFrame:CGRectMake(0, 38, 80, 25)];
        lableForMonthWeek.backgroundColor=[UIColor clearColor];
        lableForMonthWeek.tag=2;
        lableForMonthWeek.textAlignment=NSTextAlignmentCenter;
        lableForMonthWeek.font=[UIFont fontWithName:@"Arial" size:10];
        // lableForMonth.text=[monthSymbols objectAtIndex:row];
        lableForMonthWeek.textColor=[UIColor darkGrayColor];
        
        
        [_globlDateFormat setDateFormat:@"dd"];
        lableForMonth.text=[NSString stringWithFormat:@"%@ to %@",[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"start"]],[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"end"]]];
        [_globlDateFormat setDateFormat:@"MMM"];
        NSString *firstMonth=[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"start"]];
        NSString *secondMonth=[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"end"]];
        if([secondMonth isEqualToString:firstMonth]){
            
            lableForMonthWeek.text=firstMonth;
        }
        else{
            lableForMonthWeek.text=[NSString stringWithFormat:@"%@-%@",firstMonth,secondMonth];
        }
        [viewForMonth addSubview:lableForMonth];
        [lableForMonth release];
        [viewForMonth addSubview:lableForMonthWeek];
        [lableForMonthWeek release];
        
        return   [viewForMonth autorelease];
        
    }
    else  if(_buttonForDay.selected){
        if(component==0){
            
            UIView *viewForMonth=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
            viewForMonth.backgroundColor=[UIColor clearColor];
            CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
            rotate = CGAffineTransformScale(rotate, 0.5, 4.0);
            [viewForMonth setTransform:rotate];
            viewForMonth.tag=1000;
            UILabel *lableForMonth=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
            lableForMonth.backgroundColor=[UIColor clearColor];
            lableForMonth.tag=1;
            lableForMonth.textAlignment=NSTextAlignmentCenter;
            lableForMonth.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
            lableForMonth.text=[_arrForMonth objectAtIndex:row];
            lableForMonth.textColor=[UIColor darkGrayColor];
#if DEBUG
            NSLog(@"component %ld lableForMonth %@",(long)component,lableForMonth);
#endif
            
            [viewForMonth addSubview:lableForMonth];
            [lableForMonth release];
            
            return   [viewForMonth autorelease];
        }
        else
        {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            
            UIView *viewForMonth=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 80, 55)];
            viewForMonth.backgroundColor=[UIColor clearColor];
            CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
            rotate = CGAffineTransformScale(rotate, 0.5, 4.0);
            [viewForMonth setTransform:rotate];
            viewForMonth.tag=1000;
            UILabel *lableForMonth=[[UILabel alloc]initWithFrame:CGRectMake(0,5, 80, 55)];
            lableForMonth.backgroundColor=[UIColor clearColor];
            lableForMonth.tag=1;
            lableForMonth.textAlignment=NSTextAlignmentCenter;
            lableForMonth.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
            lableForMonth.text=[NSString stringWithFormat:@"%lu",(unsigned long)row+1];
            lableForMonth.textColor=[UIColor darkGrayColor];
            
#if DEBUG
            NSLog(@"component %ld lableForMonth %@",(long)component,lableForMonth);
#endif
            [viewForMonth addSubview:lableForMonth];
            [lableForMonth release];
            [formatter release];
            return   [viewForMonth autorelease];
        }
    }
    else{
        return nil;
    }
}

- (void)updatePasses:(Item *)itemObj
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:itemObj.itemId forKey:@"AppId"];
    [dict setObject:itemObj.ipAdd forKey:@"IpAddress"];
    [dict setObject:itemObj.itemId forKey:@"Id"];

    NSInteger approvedPassStatus = [self getPassStatus:itemObj];
    BOOL isElapsed = NO;
    
    if ( [itemObj.isApproved integerValue] == 1)
    {
        switch (approvedPassStatus)
        {
            case passElapsed:
            {
                isElapsed = YES;
                break;
            }
        }
    }
    if ( isElapsed )
    {
        // set isApproved to 2 as it is elapsed...
        [dict setObject:@"2" forKey:@"pass_status"];
        
        [[Service sharedInstance]updateLocalPass:dict];
        itemObj.isApproved = [NSNumber numberWithInteger:2];
    }
}

#pragma mark - picker Action lifecycle
-(void)updateCalenderView
{
    //5julyBUG done
    if(_buttonForDay.selected){
        // TODO: [self showevent] this method calling is commented because Client needs to display only Current date events . If uncommented will display event according to date
        //[self showevent];
        [self dayButtonAction:nil];
        [_table reloadData];
        
    }
    else if(_buttonForWeek.selected){
        [self weekButtonAction:nil];
        [_table reloadData];
    }
    else if(_buttonForGlance.selected){
        [self dismisPopover];
        [self.view setUserInteractionEnabled:YES];
        [self glanceButtonAction:nil];
        [_table reloadData];
    }
    else if(_buttonFormonth.selected)
    {
        
        [self dismisPopover];
        [self.view setUserInteractionEnabled:YES];
        _buttonFormonth.selected=NO;
        [self monthButtonAction:nil];
    }
    else
    {
    }
    
    // Method call to refresh View also so that red batch get updated with latest DI count
    // JIRA issu 2095
    //[self createViewForCalender];
}


-(void)showevent
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;

    UIButton* button=(UIButton*) [_viewForDay viewWithTag:123];
    button.hidden=YES;
    [formatter setDateFormat:@"dd/MM/yyyy"];

    NSArray  *eventItemArr= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:self.strForDayDate]]  ;
    [_arrForEvent removeAllObjects];

    for(EventItem* eventObj in eventItemArr)
    {
        if (![eventObj.created_by isEqualToString:[AppHelper userDefaultsForKey:@"user_id"]])
        {
            [_arrForEvent addObject:eventObj];
        }
        else
        {
        }
    }
    
    [formatter release];
    
    [eventTableV reloadData];
    
    _lableForDayHeading.text=[NSString stringWithFormat:@"On this day(%lu)",(unsigned long)_arrForEvent.count] ;

    // Aman Added

    NSDateFormatter *dateformatter = [[[NSDateFormatter alloc] init] autorelease];
    [dateformatter setDateFormat:@"dd/MMM/yyyy"];
    NSString *currentDate = [dateformatter stringFromDate:[NSDate date]];
    NSArray *currentdateArr = [currentDate componentsSeparatedByString:@"/"];

    NSArray *temp = [self.strForDayDate componentsSeparatedByString:@"/"];
    [dateformatter setDateFormat:@"EEEE, MMM dd, yyyy"];
    NSString *todaysdate = [dateformatter stringFromDate:[NSDate date]];

    if([[currentdateArr objectAtIndex:0]isEqualToString:[temp objectAtIndex:0]])
    {
        //_lableForDayHeading.text=[NSString stringWithFormat:@"Today [%@] (%d)",todaysdate,_arrForEvent.count] ;
        _lableForDayHeading.text=[NSString stringWithFormat:@"%@",todaysdate] ;

    }
    
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if(_buttonForDay.selected)
    {
        return 2;
    }
    else{
        return 1;
    }
    
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(_buttonFormonth.selected || _buttonForGlance.selected)
    {
        if ([_arrForMonth count] > 0)
        {
            return _arrForMonth.count;
        }
        else
        {
            return 0;
        }
    }
    else if(_buttonForWeek.selected)
    {
        if ([_arrForweek count] > 0)
        {
            return _arrForweek.count;
        }
        else
        {
            return 0;
        }
    }
    else   if(_buttonForDay.selected)
    {
        if(component==0)
        {
            // Campus Based Start
            if ([_arrForMonth count] > 0)
            {
                return _arrForMonth.count;
            }
            else
            {
                return 0;
            }
            // Campus Based End
        }
        else
        {
#if DEBUG
            NSLog(@"numberOfRowsInComponent self._dateForPicker %@",self._dateForPicker);
#endif
            
            if (self._dateForPicker)
            {
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSRange range = [calendar rangeOfUnit:NSDayCalendarUnit
                                               inUnit:NSMonthCalendarUnit
                                              forDate:self._dateForPicker];
                
#if DEBUG
                NSLog(@"self._dateForPicker %@",self._dateForPicker);
                NSLog(@"range.length %lu",(unsigned long)range.length);
#endif
                return range.length;
            }
            else
            {
                return 0;
            }
        }
    }
    else
    {
        return 0;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(_buttonForGlance.selected)
    {
        // Campus Based Start
        if(component ==0)
            selectedMonthIndex = [pickerViewDay selectedRowInComponent:0];
#if DEBUG
        NSLog(@"_buttonForGlance selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        // Campus Based End

        [self setSelectedMonth:row];

        [self getdateOfamonth:row];
        UIView *view1=(UIView*)[pickerViewDay viewForRow:row forComponent:0];
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
    }
    else if(_buttonFormonth.selected){
        [_globlDateFormat setDateFormat:@"MM"];
        // Campus Based Start

        if(component ==0)
            selectedMonthIndex = [pickerViewDay selectedRowInComponent:0];
        
#if DEBUG
        NSLog(@"_buttonFormonth selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        
        // Campus Based End
        [self setSelectedMonth:row];
        
        [self.calendarLogic selectNewMonth:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:row]]] integerValue]];
        UIView *view1=(UIView*)[pickerViewDay viewForRow:row forComponent:0];
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
    }
    else if(_buttonForWeek.selected){
        
        // Campus Based Start
        if(component ==0)
            selectedMonthIndex = [pickerViewDay selectedRowInComponent:0];
        
#if DEBUG
        NSLog(@"_buttonForWeek selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        
        // Campus Based End

        [self setSelectedMonth:row];
        UIView *view1=(UIView*)[pickerViewDay viewForRow:row forComponent:0];
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        UILabel *name2=(UILabel*)[view1 viewWithTag:2];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
        name2.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
        [self dayOfWeekFromDate:row ];
        
    }
    else
    {
        [_globlDateFormat setDateFormat:@"MM"];
    
#if DEBUG
        NSLog(@"[pickerViewDay selectedRowInComponent:0] %ld",(long)[pickerViewDay selectedRowInComponent:0]);
        NSLog(@"[pickerViewDay selectedRowInComponent:1] %ld",(long)[pickerViewDay selectedRowInComponent:1]);
#endif
        UIView *view1=(UIView*)[pickerViewDay viewForRow:row forComponent:0];
        UIView *view2=(UIView*)[pickerViewDay viewForRow:row forComponent:1];
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        UILabel *name2=(UILabel*)[view2 viewWithTag:1];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
        name2.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
#if DEBUG
        NSLog(@"name.text %@",name.text);
        NSLog(@"name2.text %@",name2.text);
#endif
        // Campus Based Start
        if(component ==0)
        {
#if DEBUG
            NSLog(@"else component %ld",(long)component);
#endif
            
            selectedMonthIndex = [pickerViewDay selectedRowInComponent:0];
        }
#if DEBUG
        NSLog(@"else Day Button selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        // Campus Based End

        if ([_arrForMonth count] > 0)
        {
            [self getNewDateOfamonth:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:[pickerViewDay selectedRowInComponent:0]]]] integerValue] day:[pickerViewDay selectedRowInComponent:1]+1];
        }
    }
}


- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 30;
    
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if(_buttonForDay.selected){
        if(component==0){
            return 80;
        }
        
        else{
            return 180;
        }
    }
    else{
        return 230;
    }
    
}
#pragma mark viewLifeCycle
- (void)handleDrag:(UIPanGestureRecognizer *)gesture
{
    [self.view bringSubviewToFront:_viewForItemDetails];
    if (gesture.state == UIGestureRecognizerStateChanged ||
        gesture.state == UIGestureRecognizerStateEnded)
    {
        CGPoint translation = [gesture translationInView:self.view];
        CGRect newFrame = _viewForItemDetails.frame;

        newFrame.origin.y = newFrame.origin.y + translation.y;
        newFrame.size.height=  newFrame.size.height - translation.y;
        
        if(   newFrame.origin.y>=-15 && newFrame.origin.y<630)
        {
            _viewForItemDetails.frame = newFrame;
            [gesture setTranslation:CGPointZero inView:self.view];
        }
        // you need to reset this to zero each time
        // or the effect stacks and that's not what you want
    }
}
-(void)mangeFrameOfView
{
    if(_viewForItemDetails.frame.origin.y>600){
        _viewForItemDetails.frame=CGRectMake(_viewForItemDetails.frame.origin.x, 630, _viewForItemDetails.frame.size.width, _viewForItemDetails.frame.size.height);
    }
    
    if(_viewForItemDetails.frame.origin.y<20)
    {
        _viewForItemDetails.frame=CGRectMake(_viewForItemDetails.frame.origin.x, -15, _viewForItemDetails.frame.size.width, _viewForItemDetails.frame.size.height);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    viewForApprovedPass.hidden = YES;
    /************** Code Added For Push Notification  By Aman **********************/
    [self configureFlurryParameterDictionary];
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     NSMutableDictionary *dict = [defaults objectForKey:@"UnVisited"];
    
     NSInteger t = [[dict objectForKey:@"T"] integerValue] ;
     NSInteger o = [[dict objectForKey:@"O"] integerValue] ;
     NSInteger i = [[dict objectForKey:@"I"] integerValue] ;
     
     NSInteger uvc = [[dict objectForKey:@"UnVisitedCount"] integerValue]- t;
     
     NSMutableDictionary *dicttemp = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:0],@"T",[NSNumber numberWithInteger:o],@"O",[NSNumber numberWithInteger:i],@"I",[NSNumber numberWithInteger:uvc],@"UnVisitedCount", nil];
     
    [AppHelper saveToUserDefaults:dicttemp withKey:@"UnVisited"];
    
    /************** Code Added For Push Notification  By Aman **********************/


    UIPanGestureRecognizer *pan=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handleDrag:)];
    [_viewForItemDetails addGestureRecognizer:pan];
    [pan release];
    _arrForMonth=[[NSMutableArray alloc]init];
    _arrForweek=[[NSMutableArray alloc]init];
    _arrForEvent=[[NSMutableArray alloc]init];
    _globlDateFormat = [[NSDateFormatter alloc] init];
    [_globlDateFormat setDateFormat:@"dd/MM/yyyy"];
    
    self.strForDayDate=[_globlDateFormat stringFromDate:[NSDate date]];
    
    itemCount=1;
    [self getlistofMonth];
    
    _viewForItemDetails.hidden=YES;
    _buttonFormonth.selected=YES;
    pickerViewDay = [[UIPickerView alloc] initWithFrame:CGRectZero];
    [pickerViewDay setDelegate:self];
    [pickerViewDay setShowsSelectionIndicator:NO];
    pickerViewDay.backgroundColor=[UIColor clearColor];
    CGAffineTransform rotate = CGAffineTransformMakeRotation(-M_PI/2);
    rotate = CGAffineTransformScale(rotate, 0.25, 2.0);
    [pickerViewDay setTransform:rotate];
    //  [pickerViewDay setCenter:CGPointMake(self.view.frame.size.width/2, (pickerViewDay.frame.size.height/2)-3)];
    pickerViewDay.center=CGPointMake(_viewForPicker.frame.size.width/2, _viewForPicker.frame.size.height/2);
    [_viewForPicker addSubview:pickerViewDay];
    _arr=[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view from its nib.
    NSDate *aDate = selectedDate;
	if (aDate == nil) {
		aDate = [CalendarLogic dateForToday];
	}
    
	CalendarLogic *aCalendarLogic = [[CalendarLogic alloc] initWithDelegate:self referenceDate:aDate];
	self.calendarLogic = aCalendarLogic;
	[aCalendarLogic release];
    
    UIView *aCalendarView = [[UIView alloc] initWithFrame:CGRectMake(5, 150, 875, 500) ];
    aCalendarView.backgroundColor=[UIColor clearColor];
    self.calendarView = aCalendarView;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"MMMM"];

    if([_arrForMonth count]>0)
    {
#if DEBUG
        NSLog(@"selected Row %lu",(unsigned long)[_arrForMonth indexOfObject:[formatter stringFromDate:self.calendarLogic.referenceDate]]);
#endif
        
    [pickerViewDay selectRow:[_arrForMonth indexOfObject:[formatter stringFromDate:self.calendarLogic.referenceDate]] inComponent:0 animated:YES];
    }
        // Campus Based Start
#if DEBUG
    NSLog(@"viewDidLoad Before selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
    
    if([_arrForMonth count]>0)
    {
        selectedMonthIndex = (NSInteger)[_arrForMonth indexOfObject:[formatter stringFromDate:self.calendarLogic.referenceDate]];

    }
#if DEBUG
    NSLog(@"viewDidLoad After selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
    // Campus Based End

    /* Ashish Will test the impact after commenting below two Method call */
    //[self createViewForCalender];
    //[self selectButtonForDate:selectedDate];
    /* Ashish Will test the impact after commenting below two Method call */

    
    [self.view addSubview:aCalendarView];
    _viewForDay.frame=CGRectMake(444, -5, 434, 145);
    [self.view addSubview:_viewForDay];
    self.calendarView = aCalendarView;
    [aCalendarView release];
    
    //view For Month
    _viewForDay.hidden=YES;
    _lableForDate.hidden=YES;
    _lableForDay.hidden=YES;
    [formatter release];
    ///
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if ([profobj.type isEqualToString:@"Teacher"])
    {
        [_buttonForDateIcon setUserInteractionEnabled:TRUE];
        [dateIconButton setImage:[UIImage imageNamed:@"dateiconnormal.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_buttonForDateIcon setUserInteractionEnabled:FALSE];

        [dateIconButton setImage:[UIImage imageNamed:@"dateiconnormalStudent.png"] forState:UIControlStateNormal];
        [dateIconButton addTarget:self action:@selector(dateButtonIconAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
}

//----- Added By Mitul Content Logic -----//
- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate
{
    BOOL res = NO;
    if ([firstDate isEqualToDate:date] || [lastDate isEqualToDate:date])
    {
        res = YES;
    }
    else if ([firstDate compare:date] == NSOrderedAscending && [lastDate compare:date]  == NSOrderedDescending)
    {
        res = YES;
    }
    
    return res;
}
//--------- By Mitul ---------//


-(void)updateCalenderlDaisy
{

#if DEBUG
    NSLog(@"updateCalenderlDaisy Start...");
#endif
    
    NSArray *arrData = nil;
    NSString *contentType = [[Service sharedInstance] getContentType];
    
    // New code added to Show/Hide Content Panel
    if ([contentType isEqualToString:@"None"])
    {
        // hide Content panel
        _viewForItemDetails.hidden = YES;
    }
    else
    {
        // Show Content panel as before
        _viewForItemDetails.hidden = NO;
        
        arrData = [[Service sharedInstance] parseManifestFile:contentType];
        
        NSMutableArray *arrOfData = nil;
        arrOfData =  [[NSMutableArray alloc] init] ;
        
        if( self.strForDayDate!=nil)
        {
            for (SchoolInformation *arrSchoolInfo in arrData)
            {
                if ([self isDate:[self dateWithJSONString:self.strForDayDate] inRangeFirstDate:arrSchoolInfo.startDate lastDate:arrSchoolInfo.endDate])
                {
                    [arrOfData addObject:arrSchoolInfo];
                }
            }
            
            if (self._arrForCalenderData)
            {
                self._arrForCalenderData = nil;
                self._arrForCalenderData = [[NSArray alloc] init] ;
            }
            self._arrForCalenderData = arrOfData;
            
            NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"Content/%@",contentType]];
            
            count=0;
            if(self._arrForCalenderData.count>0)
            {
                School_Information *about=[self._arrForCalenderData objectAtIndex:count];
                [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
            }
            else
            {
                [_webView loadHTMLString:@"" baseURL:nil];
            }
        }
    }
#if DEBUG
    NSLog(@"updateCalenderlDaisy End...");
#endif
    
}

- (NSDate*)dateWithJSONString:(NSString*)dateStr
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];

    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // This is for check the output
    [dateFormat release];
    dateFormat = nil;
    
    return date;
}



//for search
-(void)searchPanelContent:(School_Information*)infoObj
{
    //----- Added By Mitul For Content Logic -----//
    NSArray *arrData = nil;
    NSString *contentType = [[Service sharedInstance] getContentType];
    arrData = [[Service sharedInstance] parseManifestFile:contentType];
    
    NSMutableArray *arrOfData = [[[NSMutableArray alloc] init] autorelease];
    
    for (SchoolInformation *arrSchoolInfo in arrData)
    {
        if ([arrSchoolInfo.title isEqualToString:infoObj.title])
        {
            [arrOfData addObject:arrSchoolInfo];
        }
    }
    
    if (self._arrForCalenderData)
    {
        self._arrForCalenderData = nil;
        self._arrForCalenderData = [[NSArray alloc] init] ;
    }
    self._arrForCalenderData = arrOfData;
    
    
    NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"Content/%@",contentType]];
    
    
    if(self._arrForCalenderData.count>0)
    {
        School_Information *about=[self._arrForCalenderData objectAtIndex:0];
        [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
    }
    //--------- By Mitul ---------//
}


-(void)viewDidAppear:(BOOL)animated
{
    [self getlistofWeek];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self customizepikerView];
    [self dayButtonAction:nil];
    
    
}
-(void)customizepikerView
{

#if DEBUG
    NSLog(@"customizepikerView Start...");
#endif

    pickerViewDay.backgroundColor=[UIColor clearColor];
    for(int i=0;i< pickerViewDay.subviews.count;i++)
    {
        //Aman added -- iOS7 optimisation --------------------------------------
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            [(UIView*)[[ pickerViewDay subviews] objectAtIndex:i] setAlpha:1.0];
            ((UIView *)[pickerViewDay.subviews objectAtIndex:1]).backgroundColor = [UIColor clearColor];
            ((UIView *)[pickerViewDay.subviews objectAtIndex:2]).backgroundColor = [UIColor clearColor];
        }
        else
        {
            [(UIView*)[[ pickerViewDay subviews] objectAtIndex:i] setAlpha:0.0];
        }
        //Aman added -- iOS7 optimisation --------------------------------------
    }

#if DEBUG
    NSLog(@"customizepikerView End...1");
#endif
    
    if ([[ pickerViewDay subviews] count] > 2 )
    {
        [(UIView*)[[ pickerViewDay subviews] objectAtIndex:2] setAlpha:1.0];
    }
    
#if DEBUG
    NSLog(@"[pickerViewDay selectedRowInComponent:0] %ld",(long)[pickerViewDay selectedRowInComponent:0]);
    NSLog(@"customizepikerView End...2");
#endif
    
    [self setSelectedMonth:[pickerViewDay selectedRowInComponent:0]];

#if DEBUG
    NSLog(@"customizepikerView End...3");
#endif
    
    UIView *view1=(UIView*)[pickerViewDay viewForRow:[pickerViewDay selectedRowInComponent:0] forComponent:0];
    UILabel *name=(UILabel*)[view1 viewWithTag:1];
    name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
    
#if DEBUG
    NSLog(@"name %@",name.text);
    NSLog(@"customizepikerView End...4");
#endif

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    [strForSub release];
    [_arrForEvent release];
    [_viewForShowAllNotesItem release];
    [_arrForMonth release];
    [_arrForweek release];
    [_globlDateFormat release];
    [_curentTimeTable release];
    [_arrForCalenderData release];
    [startTimeForDayView release];
    [_viewForShowAllDairyItem release];
    [strForDayDate release];
    [_dateForPicker release];

    [_popoverControler release];
    
    [pickerViewDay release];
    [_testlayer release];
    [_arr release];
    [_buttonForDay release];
    [_buttonForWeek release];
    [_buttonFormonth release];
    [_buttonForGlance release];
    [_buttonFortaskFilter release];
    [_buttonForFilterEvent release];
    [_buttonForFilterNotes release];
    [_viewForFilter release];
    [_viewForOption release];
    [_lableForMonthName release];
    [_selectedMonthImg release];
    [_viewForWeek release];
    [_lableForDay release];
    [_lableForDate release];
    [_viewForDay release];
    [_lableForDayHeading release];
    [_viewForMonthweek release];
    [_lableForMonthHeading release];
    [_viewForPicker release];
    [_lableForGlancemonth1 release];
    [_lableForGlancemonth2 release];
    [_lableForGlancemonth3 release];
    [_lableForGlancemonth4 release];
    [_viewForGlanceTable release];
    [_viewForItemDetails release];
    [_buttonForDiscrption release];
    [_buttonForDateIcon release];
    [_webView release];
    [eventTableV release];
    
    // Timer
    if (countDownTimer)
    {
        [countDownTimer release];
        countDownTimer = nil;
    }
    
    if (gregorianCalendar)
    {
        [gregorianCalendar release];
    }

    [lblApprovedPassTimer release];
    [lblPassApprovedByTitle release];
    [lblElapsedPassTimer release];
    [dateIconButton release];
    [lblPassDate release];
    [btn_DeletePass release];
    [super dealloc];
}
- (void)viewDidUnload {
    _arrForEvent=nil;
    [_buttonForDay release];
    _buttonForDay = nil;
    [_buttonForWeek release];
    _buttonForWeek = nil;
    [_buttonFormonth release];
    _buttonFormonth = nil;
    [_buttonForGlance release];
    _buttonForGlance = nil;
    [_buttonFortaskFilter release];
    _buttonFortaskFilter = nil;
    [_buttonForFilterEvent release];
    _buttonForFilterEvent = nil;
    [_buttonForFilterNotes release];
    _buttonForFilterNotes = nil;
    [_viewForFilter release];
    _viewForFilter = nil;
    [_viewForOption release];
    _viewForOption = nil;
    [_lableForMonthName release];
    _lableForMonthName = nil;
    [_selectedMonthImg release];
    _selectedMonthImg = nil;
    [_table release];
    _table = nil;
    [_viewForWeek release];
    _viewForWeek = nil;
    [_lableForDay release];
    _lableForDay = nil;
    [_lableForDate release];
    _lableForDate = nil;
    [_viewForDay release];
    _viewForDay = nil;
    [_lableForDayHeading release];
    _lableForDayHeading = nil;
    [_viewForMonthweek release];
    _viewForMonthweek = nil;
    [_lableForMonthHeading release];
    _lableForMonthHeading = nil;
    [_viewForPicker release];
    _viewForPicker = nil;
    [_lableForGlancemonth1 release];
    _lableForGlancemonth1 = nil;
    [_lableForGlancemonth2 release];
    _lableForGlancemonth2 = nil;
    [_lableForGlancemonth3 release];
    _lableForGlancemonth3 = nil;
    [_lableForGlancemonth4 release];
    _lableForGlancemonth4 = nil;
    [_viewForGlanceTable release];
    _viewForGlanceTable = nil;
    [_viewForItemDetails release];
    _viewForItemDetails = nil;
    [_buttonForDiscrption release];
    _buttonForDiscrption = nil;
    [_buttonForDateIcon release];
    _buttonForDateIcon = nil;
    [_webView release];
    _webView = nil;
    [eventTableV release];
    eventTableV = nil;
    [super viewDidUnload];
}

#pragma mark -
#pragma mark WebView Delegate  -

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
}

#pragma mark -
#pragma mark Generic Pass Methods  -

// -----------------------------------------------------------------------------
// Pass Status

- (NSInteger)getPassStatus:(Item *)selectedDI
{
    NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
    [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
    
    NSDateFormatter *formterToCompareTime=[[NSDateFormatter alloc]init];
    [formterToCompareTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    [formterToCompareTime setDateStyle:NSDateFormatterNoStyle];
    [formterToCompareTime setTimeStyle:NSDateFormatterShortStyle];
    
    NSDateFormatter *dbFormaterTime=[[NSDateFormatter alloc]init];
    [dbFormaterTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    [dbFormaterTime setDateStyle:NSDateFormatterNoStyle];
    [dbFormaterTime setTimeStyle:NSDateFormatterShortStyle];
    
    NSDate *now = [NSDate date];
    
    NSString *strNowTime = [dbFormaterTime stringFromDate:now];
    NSString *strFromTime = [dbFormaterTime stringFromDate:selectedDI.assignedDate];
    NSString *strToTime = [dbFormaterTime stringFromDate:selectedDI.dueDate];

    NSString *strFromDate = [formterDate stringFromDate:now];
    NSString *strToDate = [formterDate stringFromDate:selectedDI.dueDate];
    
    
    // if Now/From date is Same as To Date
    if([[formterDate dateFromString:strFromDate] compare:[formterDate dateFromString:strToDate]] == NSOrderedSame)
    {
        if([[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strFromTime]] == NSOrderedSame )
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Pass Startedwith Same time...");
#endif
            return passStarted;
        }
        // if Now time inside From and To
        if ( [DELEGATE isDate:[formterToCompareTime dateFromString:strNowTime] inRangeFirstDate:[formterToCompareTime dateFromString:strFromTime] lastDate:[formterToCompareTime dateFromString:strToTime]] )
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Pass Started...");
#endif
        
            return passStarted;
        }
        // if Now Time is Later in time than TO Time
        else if([[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strToTime]] == NSOrderedDescending ||
                [[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strToTime]] == NSOrderedSame
                )
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Status: Pass Elapsed...");
#endif
            
            return passElapsed;
        }
        // if Now Time is early in time than From Time
        else if ([[formterToCompareTime dateFromString:strNowTime] compare:[formterToCompareTime dateFromString:strFromTime]] == NSOrderedAscending)
        {
#if DEBUG
            //NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
            NSLog(@"Status: Pass Not Started...");
#endif
            
            return passNotStarted;
        }
        else
        {
#if DEBUG
            NSLog(@"Status: else nothing...");
            NSLog(@"NowTime %@ === FromTime %@ === ToTime %@",strNowTime,strFromTime,strToTime);
#endif

        }
    }
    // if Now Date is early than From Date
    else if ([[formterDate dateFromString:strFromDate] compare:[formterDate dateFromString:strToDate]] == NSOrderedAscending)
    {
        
#if DEBUG
        NSLog(@"Status: Notstarted.. FromDate %@ === ToDate %@",strFromDate,strToDate);
#endif
        return passNotStarted;
    }
    else
    {
#if DEBUG
        NSLog(@"/*/*/*/*//*/*/*/*/*/*/*/*/*");
        NSLog(@"NowDate %@ ===  ToTime %@",strFromDate,strToDate);
#endif
        
        return passElapsed;
    }
    [formterDate release];

    [formterToCompareTime release];
    
    return passUnExpected;
}

// -----------------------------------------------------------------------------
// Countdown Timer

-(void)updateClock:(NSTimer *)timer
{
    NSDateFormatter *countDownDateFormatter = [[NSDateFormatter alloc] init];
    [countDownDateFormatter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    
    
    NSDate *now = [NSDate date];
    NSString *strFromTime = [countDownDateFormatter stringFromDate:now];

    NSString *strToTime = [countDownDateFormatter stringFromDate:selectedDiaryItem.dueDate];
    
    NSDateComponents *comp = [gregorianCalendar components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
                                                  fromDate:[countDownDateFormatter dateFromString:strFromTime]
                                                    toDate:[countDownDateFormatter dateFromString:strToTime]
                                                   options:0];
    
    NSString *strTimeRemaining = nil;
    
    // if date have not expired
    if([[countDownDateFormatter dateFromString:strFromTime] compare:[countDownDateFormatter dateFromString:strToTime]] == NSOrderedAscending)
    {
        strTimeRemaining = [[NSString alloc] initWithFormat:@"%02ld:%02ld:%02ld", (long)[comp hour],  (long)[comp minute], (long)[comp second]];
        lblApprovedPassTimer.text = strTimeRemaining;
        lblApprovedPassTimer.hidden = NO;
        lblElapsedPassTimer.text=@"";
        lblElapsedPassTimer.hidden = YES;
    }
    else
    {
        // time has expired, set time to 00:00 and set boolean flag to no
        //strTimeRemaining = [[NSString alloc] initWithString:@"00:00:00"];
        if (countDownTimer)
        {
            [countDownTimer invalidate];
            countDownTimer =nil;
        }
    }
    
    [countDownDateFormatter release];
    [strTimeRemaining release];
}

- (void)setApprovedPassDetails
{
    NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
    [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
    
    NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
    [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    
    NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
    [formaterToDisplay setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
    [formaterToDisplay setDateStyle:NSDateFormatterNoStyle];
    
    NSString *fromTime = [formaterToDisplay stringFromDate:selectedDiaryItem.assignedDate];
    
    NSString *toTime = [formaterToDisplay stringFromDate:selectedDiaryItem.dueDate];

    lblFromTime.text = fromTime;
    lblToTime.text = toTime;
    lblPassDate.text = [formterDate stringFromDate:selectedDiaryItem.assignedDate];
    
    [formterDate release];
    [formaterFromDB release];
    [formaterToDisplay release];
    
    lblPassType.text = selectedDiaryItem.title;
    
    
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if([profobj.type isEqualToString:@"Teacher"])
    {
        [btn_DeletePass setHidden:NO];
        lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:selectedDiaryItem];
        
    }
    else
    {
        [btn_DeletePass setHidden:YES];
        lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[selectedDiaryItem.itemId integerValue]]];
        
        
    }
    
    [viewForApprovedPassInner setBackgroundColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:16/255.0f alpha:1]];
    
}

// -----------------------------------------------------------------------------
// Show ApprovedPass View

- (void)showApprovedViewDetails
{
    NSInteger approvedPassStatus = [self getPassStatus:selectedDiaryItem];
    
    switch (approvedPassStatus)
    {
        case passStarted:
        {
            [self setApprovedPassDetails];
            
            gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            
            countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateClock:) userInfo:nil repeats:YES];
            [countDownTimer fire];
            
            break;
        }
        case passNotStarted:
        {
            if (countDownTimer)
            {
                [countDownTimer invalidate];
                countDownTimer =nil;
            }

            
            [self setApprovedPassDetails];
            lblApprovedPassTimer.text = @"NOT STARTED";
            lblElapsedPassTimer.text = @"";
            lblElapsedPassTimer.hidden = YES;
            lblApprovedPassTimer.hidden = NO;
            break;
        }
        case passElapsed:
        {
            [self showElapsedViewDetails];
            break;
        }
        case passUnExpected:
        {
        }
    }
}

- (void)showElapsedViewDetails
{
    if (countDownTimer)
    {
        [countDownTimer invalidate];
        countDownTimer =nil;
    }

    
    NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
    [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
    
    NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
    [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    
#if DEBUG
    NSLog(@"show Elapsed Pass Detail selectedDiaryItem %@",selectedDiaryItem);
#endif
    
    NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
    [formaterToDisplay setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
    [formaterToDisplay setDateStyle:NSDateFormatterNoStyle];
    [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
    
    NSString *fromTime = [formaterToDisplay stringFromDate:selectedDiaryItem.assignedDate];
    
    NSString *toTime = [formaterToDisplay stringFromDate:selectedDiaryItem.dueDate];

    lblFromTime.text = fromTime;
    lblToTime.text = toTime;
    lblPassDate.text = [formterDate stringFromDate:selectedDiaryItem.assignedDate];
    
    [formterDate release];
    [formaterFromDB release];
    [formaterToDisplay release];
    
    lblPassType.text = selectedDiaryItem.title;
    lblApprovedPassTimer.hidden = YES;
    lblApprovedPassTimer.text = @"";
    lblElapsedPassTimer.text = @"ELAPSED";
    lblElapsedPassTimer.hidden = NO;
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    // Teacher
    if ([profobj.type isEqualToString:@"Teacher"])
    {
        lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:selectedDiaryItem];
        
        lblPassApprovedByTitle.text = @"Issued For";
        [btn_DeletePass setHidden:NO];
    }
    // Student
    else
    {
        lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[selectedDiaryItem.itemId integerValue]]];
        lblPassApprovedByTitle.text = @"Issued By";
        [btn_DeletePass setHidden:YES];
    }

    [viewForApprovedPassInner setBackgroundColor:[UIColor grayColor]];
}

#pragma mark tableLifeCycle
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==eventTableV)
    {
        return 39;//UIChanges
    }
    if(_buttonForGlance.selected)
    {
        //Cycle labels are not displaying in calendar Month and Glance views - start // glance xib is also updated
        return 70.0;
        //Cycle labels are not displaying in calendar Month and Glance views - end
    }
    else if(_buttonForWeek.selected)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        NSDate *date=[_arr objectAtIndex:indexPath.row];
        
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSString *assinegdDate=[formatter stringFromDate:date];
        
        //Important
        NSMutableArray *itemArr=[[NSMutableArray alloc]init];
        NSArray  *arrFordairyItem=nil;
        NSArray  *arrForDueDairyItem=nil;
        
        if ( ((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||
            (_buttonForFilterNotes.selected)||(_buttonFortaskFilter.selected)||
            ((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))
            )
        {
            arrFordairyItem= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate]];
            
            arrForDueDairyItem=[[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate]];
           
            [itemArr removeAllObjects];
            if ((!_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected)) {
                for (Item *itemObj in arrFordairyItem) {
                    if ([itemObj.type isEqualToString:@"Note"]) {
                        [itemArr addObject:itemObj];
                    }
                }
            }
            else if ((_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected))
            {
                for (Item *itemObj in arrFordairyItem) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        [itemArr addObject:itemObj];
                    }
                }
                for (Item *itemObj in arrForDueDairyItem) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        if (![itemArr containsObject:itemObj]) {
                            [itemArr addObject:itemObj];
                        }
                    }
                }
                
            }
            else if(((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                for (Item *itemObj in arrFordairyItem) {
                    [itemArr addObject:itemObj];
                }
                for (Item *itemObj in arrForDueDairyItem) {
                    if (![itemArr containsObject:itemObj]) {
                        [itemArr addObject:itemObj];
                    }
                }
            }
            else{
                
            }
            
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //cycle number / Cycle day display issue for week -start
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        NSArray *daySymbols = [formatter1 weekdaySymbols];
        [formatter1 setDateFormat:@"dd"];

        NSCalendar *calendar = [NSCalendar currentCalendar];

        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:date];
        NSString *weekDayStr = [NSString stringWithFormat:@"%@ %@",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:date]];
        
#if DEBUG
        NSLog(@"heightForRow");
        NSLog(@"weekDayStr : %@",weekDayStr);
#endif

        NSString *dayStr=[NSString stringWithFormat:@"%@",[self methodForGettingCalendarCycleLable:date]];
#if DEBUG
        NSLog(@"dayStr : %@",dayStr);
#endif
        
        float dayLabelHeight;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Arial" size:14.0]};
            // NSString class method: boundingRectWithSize:options:attributes:context is
            // available only on ios7.0 sdk.
            CGRect rect = [dayStr boundingRectWithSize:CGSizeMake(90.0, CGFLOAT_MAX)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
            dayLabelHeight = rect.size.height;
            
#if DEBUG
            NSLog(@"rect : %f %f %f %f",rect.origin.x,rect.origin.y, rect.size.width,rect.size.height);
#endif
            
        }
        else
        {
            CGSize sizeWithOldMethod = [dayStr sizeWithFont:[UIFont fontWithName:@"Arial" size:14.0] constrainedToSize:CGSizeMake(90, 100) lineBreakMode:NSLineBreakByWordWrapping];
#if DEBUG
            NSLog(@"before : sizeWithOldMethod : %f %f",sizeWithOldMethod.width,sizeWithOldMethod.height);
#endif
            
            dayLabelHeight = sizeWithOldMethod.height;
        }
        
        CGFloat viewContainerHeight = kGAP_BETWEEN_LABELS + kWEEK_DAY_LABEL_HEIGHT + kGAP_BETWEEN_LABELS + dayLabelHeight+kADDITIONAL_HEIGHT;

#if DEBUG
        NSLog(@"viewContainerHeight : %f",viewContainerHeight);
#endif
        
        if (itemArr.count>0) {
            itemCount=itemArr.count;
        }
        else{
            itemCount=1;
        }
        
        if (itemCount>3)
        {
            if (itemCount%3==0)
            {
#if DEBUG
                        NSLog(@"itemCount>3 if");
#endif

                
                float ht = 50*(itemCount/3);
                
#if DEBUG
                NSLog(@"ht : %f",ht);
                NSLog(@"viewContainerHeight : %f",viewContainerHeight);
#endif
                
                if(viewContainerHeight > ht)
                    return viewContainerHeight;
                else
                    return ht;
            }
            else
            {
#if DEBUG
                NSLog(@"itemCount>3 else");
#endif
      

                float ht = 50*(itemCount/3 +1);
                
#if DEBUG
                NSLog(@"ht : %f",ht);
                NSLog(@"viewContainerHeight : %f",viewContainerHeight);
#endif

                if(viewContainerHeight > ht)
                {
                    return viewContainerHeight;
                }
                else
                {
                    return ht;
                }
            }
        }
        else
        {
            if (itemCount%3==0)
            {
#if DEBUG
                NSLog(@"itemCount<3 if");
#endif
      
                
                float ht = 100*(itemCount/3);
                
#if DEBUG
                NSLog(@"ht : %f",ht);
                NSLog(@"viewContainerHeight : %f",viewContainerHeight);
#endif
      
                if(viewContainerHeight > ht)
                {
                    return viewContainerHeight;
                }
                else
                {
                    return ht;
                }
            }
            else
            {
#if DEBUG
                NSLog(@"itemCount<3 else");
#endif
                
                float ht = 100*(itemCount/3 + 1);
      
#if DEBUG
                NSLog(@"ht : %f",ht);
                NSLog(@"viewContainerHeight : %f",viewContainerHeight);
#endif
                
                if(viewContainerHeight > ht)
                {
                  return viewContainerHeight;
                }
                else
                {
                    return ht;
                }
            }
        }

        //cycle number / Cycle day display issue for week -end
        [formatter release];
        [itemArr release];
    }
    else if(_buttonForDay.selected)
    {
        NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
        NSDateFormatter *formatter1 = [[[NSDateFormatter alloc] init] autorelease];
        
        if (indexPath.row==0)
        {
            [formatter setDateFormat:@"dd/MM/yyyy"];
            
            ////////////
            //Important
            NSMutableArray *itemArr=[[NSMutableArray alloc]init];
            NSArray  *arrFordairyItem=nil;
            
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonForFilterNotes.selected)||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                arrFordairyItem= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:self.strForDayDate]];
#if DEBUG
                NSLog(@"arrFordairyItem = %@",arrFordairyItem);
#endif
                
                [itemArr removeAllObjects];
                if ((!_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))
                {
                    for (Item *itemObj in arrFordairyItem)
                    {
                        if ([itemObj.type isEqualToString:@"Note"])
                        {
                            [itemArr addObject:itemObj];
                        }
                    }
                }
                else if ((_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected))
                {
                    for (Item *itemObj in arrFordairyItem)
                    {
                        if (![itemObj.type isEqualToString:@"Note"])
                        {
                            [itemArr addObject:itemObj];
                        }
                    }
                }
                else if(((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||
                        ((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
                {
                    for (Item *itemObj in arrFordairyItem)
                    {
                        [itemArr addObject:itemObj];
                    }
                }
                else
                {
                    // Do Nothing
                }
            }
            
            //////////////////////////////////////
            
            NSMutableArray *arrForItem=[[NSMutableArray alloc]init];
            for (int i=0; i<itemArr.count; i++)
            {
                Item* itemObject=[itemArr objectAtIndex:i];
                if ([itemObject.onTheDay integerValue]==1)
                {
                    
                    [arrForItem addObject:itemObject];
                }
                
            }
            ///////////
            //Important
            NSArray  *eventItemArr=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                eventItemArr= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:self.strForDayDate]];
            }
            ///////////////////////////////////////////////////
            
            
            NSMutableArray *arrForEvent=[[NSMutableArray alloc]init];
            
            NSInteger eventTotalcount=0;
            NSInteger dairyTotalcount=0;
            
            if (eventItemArr.count>0)
            {
                for (int i=0; i<eventItemArr.count; i++)
                {
                    EventItem* eventItemObject=[eventItemArr objectAtIndex:i];
                    if ([eventItemObject.onTheDay integerValue]==1)
                    {
                        [arrForEvent addObject:eventItemObject];
                    }
                }
                
                NSInteger evevmtItemCount;
                
                if (arrForEvent.count>0) {
                    evevmtItemCount=arrForEvent.count;
                }
                else{
                    evevmtItemCount=1;
                }
                
                if (evevmtItemCount%2==0)
                {
                    eventTotalcount=31*(evevmtItemCount/2);
                    
                }
                else{
                    eventTotalcount=31*(evevmtItemCount/2 +1);
                }
            }
            
            if (arrForItem.count>0) {
                itemCount=arrForItem.count;
            }
            else{
                itemCount=1;
            }
            
            
            if (itemCount%3==0)
            {
                
                dairyTotalcount=50*(itemCount/3);
            }
            else{
                dairyTotalcount=50*(itemCount/3 +1);
                
            }
            
            
            if(arrForItem.count>0)
            {
                if(arrForEvent.count>0)
                {
                    return (dairyTotalcount+eventTotalcount);
                }
                else{
                    if (itemCount<=3)
                        return 60.0;
                    else
                        return dairyTotalcount;
                }
            }
            else if(arrForEvent.count>2)
            {
                return (eventTotalcount);
            }
            else{
                return 60.0;
            }
            
            
            [arrForEvent release];
            [arrForItem release];
            [itemArr release];
        }
        
        else
        {
            [formatter setDateFormat:@"dd/MM/yyyy"];
            NSDictionary *dict=[_arr objectAtIndex:indexPath.row-1];

            NSMutableArray *itemArr=[[[NSMutableArray alloc]init] autorelease];
            NSArray  *arrFordairyItem=nil;
            NSArray  *arrForDueDairyItem=nil;
            
            ///---- latest change to rearrange cell height to fix overlap issue.....
            [formatter1 setDateStyle:NSDateFormatterNoStyle];
            [formatter1 setTimeStyle:NSDateFormatterShortStyle];
            
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonForFilterNotes.selected)||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                arrFordairyItem= [[Service sharedInstance] getStoredAllItemDataTime:[formatter1 dateFromString:[dict objectForKey:@"start"]] toDate:[formatter1 dateFromString:[dict objectForKey:@"end"]] assign:[formatter dateFromString:self.strForDayDate]];
                
                arrForDueDairyItem= [[Service sharedInstance] getStoredAllDueItemDataTime:[formatter1 dateFromString:[dict objectForKey:@"start"]] toDate:[formatter1 dateFromString:[dict objectForKey:@"end"]] assign:[formatter dateFromString:self.strForDayDate]];
                
                [itemArr removeAllObjects];
                if ((!_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))
                {
                    for (Item *itemObj in arrFordairyItem)
                    {
                        if ([itemObj.type isEqualToString:@"Note"])
                        {
                            if ([itemObj.onTheDay integerValue]==0)
                            {
                                [itemArr addObject:itemObj];
                            }
                        }
                    }
                }
                else if ((_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected))
                {
                    for (Item *itemObj in arrFordairyItem)
                    {
                        if (![itemObj.type isEqualToString:@"Note"])
                        {
                            [itemArr addObject:itemObj];
                        }
                    }
                    
                    for (Item *itemObj in arrForDueDairyItem)
                    {
                        if (![itemObj.type isEqualToString:@"Note"])
                        {
                            if(![itemArr containsObject:itemObj])
                            {
                                [itemArr addObject:itemObj];
                            }
                        }
                    }
                }
                else if(((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
                {
                    for (Item *itemObj in arrFordairyItem)
                    {
                        if ([itemObj.type isEqualToString:@"Note"])
                        {
                            if ([itemObj.onTheDay integerValue]==0)
                            {
                                [itemArr addObject:itemObj];
                            }
                        }
                        else
                        {
                            [itemArr addObject:itemObj];
                        }
                    }
                    for (Item *itemObj in arrForDueDairyItem)
                    {
                        if(![itemArr containsObject:itemObj])
                        {
                            [itemArr addObject:itemObj];
                        }
                    }
                }
                else
                {
                    // Do Nothing
                }
                ////////////////////////////////////////////////
            }
            
            
            //Day Button If -1
            
            //Important
            NSArray  *eventItemArr=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                
                eventItemArr= [[Service sharedInstance] getStoredAllEventDairyItem:[formatter1 dateFromString:[dict objectForKey:@"start"]] toDate:[formatter1 dateFromString:[dict objectForKey:@"end"]] satrtdate:[formatter dateFromString:self.strForDayDate]];
            }
            /////////////////////////////////////////////////////
            
            NSMutableArray *arrForEvent=[[[NSMutableArray alloc]init] autorelease];
            
            NSInteger eventTotalcount=0;
            NSInteger dairyTotalcount=0;
            
            if (eventItemArr.count>0)
            {
                for (int i=0; i<eventItemArr.count; i++)
                {
                    EventItem* eventItemObject=[eventItemArr objectAtIndex:i];
                    if ([eventItemObject.onTheDay integerValue]==0)
                    {
                        [arrForEvent addObject:eventItemObject];
                    }
                }
                
                NSInteger evevmtItemCount;
                
                if (arrForEvent.count>0)
                {
                    evevmtItemCount=arrForEvent.count;
                }
                else
                {
                    evevmtItemCount=1;
                }
                if (evevmtItemCount%2==0)
                {
                    eventTotalcount=31*(evevmtItemCount/2);
                }
                else
                {
                    eventTotalcount=31*(evevmtItemCount/2 +1);
                }
            }
            
            if (itemArr.count>0) {
                itemCount=itemArr.count;
            }
            else{
                itemCount=1;
            }
            
            if (itemCount%3==0)
            {
                dairyTotalcount=50*(itemCount/3);
            }
            else{
                dairyTotalcount=50*(itemCount/3 +1);
                
            }
            if(itemArr.count>0)
            {
                if(arrForEvent.count>0)
                {
                    return (dairyTotalcount+eventTotalcount);
                }
                else
                {
                    if (itemCount<=3)
                    {
                        return 60.0;
                    }
                    else
                    {
                        return dairyTotalcount;
                    }
                }
            }
            else if(arrForEvent.count>2)
            {
                return (eventTotalcount);
            }
            else{
                return 60.0;
            }
        }
    }
    else
    {
        return 60.0;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==eventTableV)
    {
        if(_arrForEvent.count%2==0)
        {
            return _arrForEvent.count/2 ;
        }
        else
        {
            return _arrForEvent.count/2+1 ;
        }
    }
    else
    {
        if ([_arr count]>0)
        {
            if(_buttonForDay.selected)
            {
                return _arr.count+1;
            }
            else  if(_buttonForWeek.selected)
            {
                return _arr.count;
            }
            else
            {
                return _arr.count;
            }
        }
        else
        {
            return 0;
        }
    }
}

//11june
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    

    if(theTableView==eventTableV)
    {
        UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"eventCell"];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"eventCell"];
            cell.backgroundColor=[UIColor clearColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            UIImageView *cellBgImgV=[[UIImageView alloc]initWithFrame:CGRectMake(0, 1, 424, 39)];//UIChanges
            cellBgImgV.backgroundColor=[UIColor clearColor];
            cellBgImgV.tag=110;
            [cell.contentView addSubview:cellBgImgV];
            [cellBgImgV release];
            
            UIImageView *dotImgV1=[[UIImageView alloc]initWithFrame:CGRectMake(10, 11, 5, 5)];//UIChanges
            dotImgV1.backgroundColor=[UIColor clearColor];
            dotImgV1.tag=108;
            [cell.contentView addSubview:dotImgV1];
            [dotImgV1 release];
            
            
            UIImageView *dotImgV2=[[UIImageView alloc]initWithFrame:CGRectMake(230, 11, 5, 5)];//UIChanges
            dotImgV2.backgroundColor=[UIColor clearColor];
            dotImgV2.tag=109;
            [cell.contentView addSubview:dotImgV2];
            [dotImgV2 release];
            
            UILabel *label1=[[UILabel alloc]init];
            label1.frame=CGRectMake(20, 3, 180, 23);//UIChanges
            label1.tag=111;
            label1.numberOfLines=2;
            label1.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
            label1.adjustsFontSizeToFitWidth=YES;
            label1.adjustsLetterSpacingToFitWidth=YES;
            label1.backgroundColor=[UIColor clearColor];
            label1.textColor=[UIColor blackColor];
            [cell.contentView addSubview:label1];
            [label1 release];
            
            UILabel *label2=[[UILabel alloc]init];
            label2.frame=CGRectMake(20, 17, 180, 21);//UIChanges
            label2.tag=112;
            label2.font=[UIFont fontWithName:@"Arial" size:10];
            label2.backgroundColor=[UIColor clearColor];
            
            label2.textColor=[UIColor grayColor];
            [cell.contentView addSubview:label2];
            [label2 release];
            
            UILabel *label3=[[UILabel alloc]init];
            label3.frame=CGRectMake(240, 3, 180, 23);//UIChanges
            label3.tag=113;
            label3.numberOfLines=2;
            label3.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
            label3.adjustsFontSizeToFitWidth=YES;
            label3.adjustsLetterSpacingToFitWidth=YES;
            label3.backgroundColor=[UIColor clearColor];
            label3.textColor=[UIColor blackColor];
            [cell.contentView addSubview:label3];
            [label3 release];
            
            UILabel *labe4=[[UILabel alloc]init];
            labe4.frame=CGRectMake(240, 17, 180, 21);//UIChanges
            labe4.tag=114;
            labe4.font=[UIFont fontWithName:@"Arial" size:10];
            labe4.backgroundColor=[UIColor clearColor];
            
            labe4.textColor=[UIColor grayColor];
            [cell.contentView addSubview:labe4];
            [labe4 release];
            
        }
        cell.tag=indexPath.row;
        
        UIImageView *dotImgV1=(UIImageView*)[cell.contentView viewWithTag:108];
        UIImageView *dotImgV2=(UIImageView*)[cell.contentView viewWithTag:109];
        UIImageView *cellBgImgV=(UIImageView*)[cell.contentView viewWithTag:110];
        
        UILabel *firstevent=(UILabel*)[cell.contentView viewWithTag:111];
        firstevent.text=@"";
        UILabel *firstEventdesc=(UILabel*)[cell.contentView viewWithTag:112];
        firstEventdesc.text=@"";
        UILabel *secondEvent=(UILabel*)[cell.contentView viewWithTag:113];
        secondEvent.text=@"";
        UILabel *secondEventDesc=(UILabel*)[cell.contentView viewWithTag:114];
        secondEventDesc.text=@"";
        
        NSInteger k=indexPath.row * 2;
        for(int i=0 ;i<2 ;i++)
        {
            if(k+i <_arrForEvent.count)
            {
                EventItem* evntObj=[_arrForEvent objectAtIndex:k+i];
                
                if((k+i) %2==0)
                {
                    dotImgV1.backgroundColor=[UIColor blackColor];
                    firstevent.text=[NSString stringWithFormat:@"%@",evntObj.eventName];
                    if (evntObj.details)
                    {
                        NSString *str=[self stripTags:evntObj.details];
                        firstEventdesc.text=[str stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
                    }
                }
                else
                {
                    dotImgV2.backgroundColor=[UIColor blackColor];
                    secondEvent.text=[NSString stringWithFormat:@"%@",evntObj.eventName];
                    if (evntObj.details)
                    {
                        NSString *str=[self stripTags:evntObj.details];
                        secondEventDesc.text=[str stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
                    }
                }
            }
        }

        NSInteger bottomImgIndex;
        if(_arrForEvent.count%2==0)
        {
            bottomImgIndex= _arrForEvent.count/2 ;
        }
        else
        {
            bottomImgIndex= _arrForEvent.count/2+1 ;
        }
        
        
        if(indexPath.row==bottomImgIndex-1)
        {
            cellBgImgV.image=[UIImage imageNamed:@"onthisdaybuttomcell.png"];
        }
        else{
            cellBgImgV.image=[UIImage imageNamed:@"onthisdaymidcell.png"];
        }
        
        ///////////////////////////////////////////////////////
        return cell;
    }
    else{
        if(_buttonForGlance.selected){
            UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"CalenderCellForGlance"];
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CalenderCellForGlance" owner:self options:nil];
                // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                cell = [topLevelObjects objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.tag=indexPath.row;
            UIView *view1=(UIView*)[cell.contentView viewWithTag:100];
            UIView *view2=(UIView*)[cell.contentView viewWithTag:200];
            UIView *view3=(UIView*)[cell.contentView viewWithTag:300];
            UIView *view4=(UIView*)[cell.contentView viewWithTag:400];
            UILabel *monthLable1=(UILabel*)[cell.contentView viewWithTag:102];
            UILabel *monthLable2=(UILabel*)[cell.contentView viewWithTag:202];
            UILabel *monthLable3=(UILabel*)[cell.contentView viewWithTag:302];
            
            UILabel *monthLable4=(UILabel*)[cell.contentView viewWithTag:402];
            
            UILabel *dayLable1=(UILabel*)[cell.contentView viewWithTag:103];
            UILabel *dayLable2=(UILabel*)[cell.contentView viewWithTag:203];
            UILabel *dayLable3=(UILabel*)[cell.contentView viewWithTag:303];
            
            UILabel *dayLable4=(UILabel*)[cell.contentView viewWithTag:403];
            
            UIButton *btn1=(UIButton*)[cell.contentView viewWithTag:501];
            UIButton *btn2=(UIButton*)[cell.contentView viewWithTag:502];
            UIButton *btn3=(UIButton*)[cell.contentView viewWithTag:503];
            UIButton *btn4=(UIButton*)[cell.contentView viewWithTag:504];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
            
            
            [formatter setDateFormat:@"dd/MM/yyyy"];
            
            
            //for first month
            NSMutableArray *arrFordairyItem1=[[NSMutableArray alloc]init];
            NSString *assinegdDate1=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]];
            
            [btn1 addTarget:self action:@selector(goToDay:) forControlEvents:UIControlEventTouchUpInside];
            [btn1 setTitle:assinegdDate1 forState:UIControlStateNormal];
            [btn1 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [btn1 setTitleColor:[UIColor clearColor] forState:UIControlStateSelected];
            [btn1 setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            btn1.backgroundColor=[UIColor clearColor];
            
            //Important
            NSArray  *itemArr1=nil;
            NSArray  *itemDueArr1=nil;
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                
                itemArr1= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate1]];
                
                itemDueArr1= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate1]];
                
            }
            /////////////////////////////////////////////////////////////////////////
            
            [arrFordairyItem1 removeAllObjects];
            for (Item *itemObj in itemArr1) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"])
                    {
                        [arrFordairyItem1 addObject:itemObj];
                    }
                }
            }
            
            for (Item *itemObj in itemDueArr1) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        if (![arrFordairyItem1 containsObject:itemObj])
                        {
                            [arrFordairyItem1 addObject:itemObj];
                        }
                    }
                }
            }
            
            if (arrFordairyItem1.count>0)
            {
                int x=35;
                Item *itemObj=[arrFordairyItem1 objectAtIndex:0];
                
                
                UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, 2, 40, 40)];
                cellDescView.backgroundColor=[UIColor clearColor];
                
                
                UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
                cellDescBgImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                iconImgView.center=cellDescBgImgView.center;
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                if(class1.code!=nil)
                    cellDescBgImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    cellDescBgImgView.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                
                cellDescBgImgView.contentMode=UIViewContentModeScaleAspectFit;
                [cellDescBgImgView.layer setMasksToBounds:YES];
                [cellDescBgImgView.layer setCornerRadius:3.0f];
                [cellDescBgImgView.layer setBorderWidth:1.0];
                [cellDescBgImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 44, 44);
                [popUpButton setTitle:assinegdDate1 forState:UIControlStateNormal];
                [popUpButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                popUpButton.backgroundColor=[UIColor clearColor];
                [popUpButton addTarget:self action:@selector(showAllDiaryItemButtonPressedForGlance:) forControlEvents:UIControlEventTouchUpInside];
                
                //////////
                //9julyBUG
                long timeDiff1;
                NSString *isTypeStr= itemObj.type;
                
                if(![itemObj.type isEqualToString:@"Note"])
                {
                    NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                    [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                    
                    timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]]]];
                    [formeter2 release];
                }
                
                ///////////
                if (arrFordairyItem1.count<2)
                {
                    if(timeDiff1<=0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    }
                    else
                    {
                        if ([isTypeStr isEqualToString:@"Assignment"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"Homework"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            [self updatePasses:itemObj];
                            
                            if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:popUpButton];
                }
                else
                {
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    
                    UIView * viewForCountLbl=[[UIView alloc]initWithFrame:CGRectMake(22, -2, 20, 20)];
                    viewForCountLbl.backgroundColor=[UIColor clearColor];
                    
                    UIImageView * imgViewForCountLbl=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    imgViewForCountLbl.backgroundColor=[UIColor clearColor];
                    [imgViewForCountLbl setImage:[UIImage imageNamed:@"notificationdot.png"]];
                    
                    UILabel *lableForCount=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    lableForCount.backgroundColor=[UIColor clearColor];
                    lableForCount.textAlignment=NSTextAlignmentCenter;
                    lableForCount.textColor=[UIColor whiteColor];
                    lableForCount.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
                    lableForCount.center=imgViewForCountLbl.center;
                    
                    [viewForCountLbl addSubview:imgViewForCountLbl];
                    [viewForCountLbl addSubview:lableForCount];
                    
                    [imgViewForCountLbl release];
                    [lableForCount release];
                    
                    lableForCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)arrFordairyItem1.count];
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:viewForCountLbl];
                    [cellDescView addSubview:popUpButton];
                    [viewForCountLbl release];
                }
                x=x+50;
                
                cellDescView.clipsToBounds=NO;
                [view1 addSubview:cellDescView];
                [cellDescBgImgView release];
                [iconImgView release];
                [cellDescView release];
                
            }
            
            //for add Events items on Glance
            
            
            //Important
            NSArray  *eventItemArr1=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                //Aman added
                
                NSArray *arrEvents= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:assinegdDate1]];
                NSMutableArray *arrMajorEvents = [[NSMutableArray alloc]init];
                NSMutableArray *arrMinorEvents = [[NSMutableArray alloc]init];

                for (EventItem *event in arrEvents)
                {
                    if ([event.priority isEqualToString:@"1"])
                        [arrMajorEvents addObject:event];
                    else
                        [arrMinorEvents addObject:event];
                }
                
                NSSortDescriptor *recentAddedDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = @[recentAddedDescriptor];
                
                NSArray *majorEvents = [arrMajorEvents sortedArrayUsingDescriptors:sortDescriptors];
                NSArray *minorEvents = [arrMinorEvents sortedArrayUsingDescriptors:sortDescriptors];

                eventItemArr1 = [majorEvents arrayByAddingObjectsFromArray:minorEvents];
            }
            
            //////////////////////////////////////////////////////
            
            if (eventItemArr1.count>0) {
                
                [self addEventsForGlanceMethod:eventItemArr1 itemArr:arrFordairyItem1 viewForEvent:view1] ;
            }
            
            [arrFordairyItem1 release];
            
            //for second month
            NSMutableArray *arrFordairyItem2=[[NSMutableArray alloc]init];
            NSString *assinegdDate2=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]];
            
            [btn2 addTarget:self action:@selector(goToDay:) forControlEvents:UIControlEventTouchUpInside];
            [btn2 setTitle:assinegdDate2 forState:UIControlStateNormal];
            [btn2 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [btn2 setTitleColor:[UIColor clearColor] forState:UIControlStateSelected];
            [btn2 setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            btn2.backgroundColor=[UIColor clearColor];

            
            //Important
            NSArray  *itemArr2=nil;
            NSArray  *itemDueArr2=nil;
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                itemArr2= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate2]];
                
                itemDueArr2= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate2]];
            }
            ///////////////////////////////////////////////////////////
            
            [arrFordairyItem2 removeAllObjects];
            for (Item *itemObj in itemArr2) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        [arrFordairyItem2 addObject:itemObj];
                    }
                }
            }
            
            for (Item *itemObj in itemDueArr2) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        if (![arrFordairyItem2 containsObject:itemObj])
                        {
                            [arrFordairyItem2 addObject:itemObj];
                        }
                    }
                }
            }
            
            
            
            if (arrFordairyItem2.count>0) {
                int x=35;
                Item *itemObj=[arrFordairyItem2 objectAtIndex:0];
                
                
                UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, 2, 40, 40)];
                cellDescView.backgroundColor=[UIColor clearColor];
                
                
                UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
                cellDescBgImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                iconImgView.center=cellDescBgImgView.center;
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                if(class1.code!=nil)
                    cellDescBgImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    cellDescBgImgView.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                cellDescBgImgView.contentMode=UIViewContentModeScaleAspectFit;
                [cellDescBgImgView.layer setMasksToBounds:YES];
                [cellDescBgImgView.layer setCornerRadius:3.0f];
                [cellDescBgImgView.layer setBorderWidth:1.0];
                [cellDescBgImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 44, 44);
                [popUpButton setTitle:assinegdDate2 forState:UIControlStateNormal];
                [popUpButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                popUpButton.backgroundColor=[UIColor clearColor];
                [popUpButton addTarget:self action:@selector(showAllDiaryItemButtonPressedForGlance:) forControlEvents:UIControlEventTouchUpInside];
                
                //////////
                //9julyBUG
                long timeDiff2;
                NSString *isTypeStr= itemObj.type;
                if(![itemObj.type isEqualToString:@"Note"])
                {
                    NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                    [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                    
                    
                    timeDiff2=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]]]];
                    [formeter2 release];
                }
                
                if (arrFordairyItem2.count<2)
                {
                    if(timeDiff2<=0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                        
                        if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            [self updatePasses:itemObj];

                            if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    else
                    {
                        if ([isTypeStr isEqualToString:@"Assignment"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"Homework"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            [self updatePasses:itemObj];

                            if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    
                    
                    
                    
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:popUpButton];
                }
                else
                {
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        if([itemObj.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    UIView * viewForCountLbl=[[UIView alloc]initWithFrame:CGRectMake(22, -2, 20, 20)];
                    viewForCountLbl.backgroundColor=[UIColor clearColor];
                    
                    
                    UIImageView * imgViewForCountLbl=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    imgViewForCountLbl.backgroundColor=[UIColor clearColor];
                    [imgViewForCountLbl setImage:[UIImage imageNamed:@"notificationdot.png"]];
                    
                    UILabel *lableForCount=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    lableForCount.backgroundColor=[UIColor clearColor];
                    lableForCount.textAlignment=NSTextAlignmentCenter;
                    lableForCount.textColor=[UIColor whiteColor];
                    lableForCount.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
                    lableForCount.center=imgViewForCountLbl.center;
                    
                    [viewForCountLbl addSubview:imgViewForCountLbl];
                    [viewForCountLbl addSubview:lableForCount];
                    
                    [imgViewForCountLbl release];
                    [lableForCount release];
                    
                    lableForCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)arrFordairyItem2.count];
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:viewForCountLbl];
                    [cellDescView addSubview:popUpButton];
                    [viewForCountLbl release];
                }
                x=x+50;
                
                cellDescView.clipsToBounds=NO;
                [view2 addSubview:cellDescView];
                [cellDescBgImgView release];
                [iconImgView release];
                [cellDescView release];
            }
            
            //for add Events items on Glance
            
            //Important
            NSArray  *eventItemArr2=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                NSArray *arrEvents= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:assinegdDate2]];
                NSMutableArray *arrMajorEvents = [[NSMutableArray alloc]init];
                NSMutableArray *arrMinorEvents = [[NSMutableArray alloc]init];
                
                for (EventItem *event in arrEvents)
                {
                    if ([event.priority isEqualToString:@"1"])
                        [arrMajorEvents addObject:event];
                    else
                        [arrMinorEvents addObject:event];
                }
                
                NSSortDescriptor *recentAddedDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = @[recentAddedDescriptor];
                
                NSArray *majorEvents = [arrMajorEvents sortedArrayUsingDescriptors:sortDescriptors];
                NSArray *minorEvents = [arrMinorEvents sortedArrayUsingDescriptors:sortDescriptors];
                
                eventItemArr2 = [majorEvents arrayByAddingObjectsFromArray:minorEvents];
            }
            
            ////////////////////////////
            
            if (eventItemArr2.count>0)
            {
                [self addEventsForGlanceMethod:eventItemArr2 itemArr:arrFordairyItem2 viewForEvent:view2] ;
            }
            
            [arrFordairyItem2 release];
            
            //for third month
            NSMutableArray *arrFordairyItem3=[[NSMutableArray alloc]init];
            NSString *assinegdDate3=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]];
            
            [btn3 addTarget:self action:@selector(goToDay:) forControlEvents:UIControlEventTouchUpInside];
            [btn3 setTitle:assinegdDate3 forState:UIControlStateNormal];
            [btn3 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [btn3 setTitleColor:[UIColor clearColor] forState:UIControlStateSelected];
            [btn3 setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            btn3.backgroundColor=[UIColor clearColor];
            
            //Important
            NSArray  *itemArr3=nil;
            NSArray  *itemDueArr3=nil;
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                itemArr3= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate3]];
                
                itemDueArr3= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate3]];
            }
            //////////////////////////////////////////////////////////////////
            
            [arrFordairyItem3 removeAllObjects];
            for (Item *itemObj in itemArr3) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        [arrFordairyItem3 addObject:itemObj];
                    }
                }
            }
            
            for (Item *itemObj in itemDueArr3)
            {
                if (itemObj.assignedDate)
                {
                    if (![itemObj.type isEqualToString:@"Note"])
                    {
                        if (![arrFordairyItem3 containsObject:itemObj])
                        {
                            [arrFordairyItem3 addObject:itemObj];
                        }
                    }
                }
            }
            
            if (arrFordairyItem3.count>0)
            {
                int x=35;
                Item *itemObj=[arrFordairyItem3 objectAtIndex:0];
                
                UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, 2, 40, 40)];
                cellDescView.backgroundColor=[UIColor clearColor];
                
                
                UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
                cellDescBgImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                iconImgView.center=cellDescBgImgView.center;
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                if(class1.code!=nil)
                    cellDescBgImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    cellDescBgImgView.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                cellDescBgImgView.contentMode=UIViewContentModeScaleAspectFit;
                [cellDescBgImgView.layer setMasksToBounds:YES];
                [cellDescBgImgView.layer setCornerRadius:3.0f];
                [cellDescBgImgView.layer setBorderWidth:1.0];
                [cellDescBgImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];

                
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 44, 44);
                [popUpButton setTitle:assinegdDate3 forState:UIControlStateNormal];
                [popUpButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                popUpButton.backgroundColor=[UIColor clearColor];
                [popUpButton addTarget:self action:@selector(showAllDiaryItemButtonPressedForGlance:) forControlEvents:UIControlEventTouchUpInside];
                
                //9julyBUG
                long timeDiff3;
                NSString *isTypeStr= itemObj.type;
                if(![itemObj.type isEqualToString:@"Note"])
                {
                    NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                    [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                    
                    timeDiff3=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]]]];
                    [formeter2 release];
                }
                
                if (arrFordairyItem3.count<2)
                {
                    if(timeDiff3<=0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                        
                        if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    else
                    {
                        if ([isTypeStr isEqualToString:@"Assignment"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"Homework"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:popUpButton];
                }
                else
                {
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    
                    if([itemObj.isApproved integerValue] == 1)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                    }
                    else  if([itemObj.isApproved integerValue] == 2)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                    }

                    UIView * viewForCountLbl=[[UIView alloc]initWithFrame:CGRectMake(22, -2, 20, 20)];
                    viewForCountLbl.backgroundColor=[UIColor clearColor];
                    
                    
                    UIImageView * imgViewForCountLbl=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    imgViewForCountLbl.backgroundColor=[UIColor clearColor];
                    [imgViewForCountLbl setImage:[UIImage imageNamed:@"notificationdot.png"]];
                    
                    UILabel *lableForCount=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    lableForCount.backgroundColor=[UIColor clearColor];
                    lableForCount.textAlignment=NSTextAlignmentCenter;
                    lableForCount.textColor=[UIColor whiteColor];
                    lableForCount.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
                    lableForCount.center=imgViewForCountLbl.center;
                    
                    [viewForCountLbl addSubview:imgViewForCountLbl];
                    [viewForCountLbl addSubview:lableForCount];
                    
                    [imgViewForCountLbl release];
                    [lableForCount release];
                    
                    lableForCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)arrFordairyItem3.count];
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:viewForCountLbl];
                    [cellDescView addSubview:popUpButton];
                    [viewForCountLbl release];
                }
                x=x+50;
                
                cellDescView.clipsToBounds=NO;
                [view3 addSubview:cellDescView];
                [cellDescBgImgView release];
                [iconImgView release];
                [cellDescView release];
                
            }
            
            //for add Events items on Glance
            //Important
            NSArray  *eventItemArr3=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                NSArray *arrEvents= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:assinegdDate3]];
                NSMutableArray *arrMajorEvents = [[NSMutableArray alloc]init];
                NSMutableArray *arrMinorEvents = [[NSMutableArray alloc]init];
                
                for (EventItem *event in arrEvents)
                {
                    if ([event.priority isEqualToString:@"1"])
                        [arrMajorEvents addObject:event];
                    else
                        [arrMinorEvents addObject:event];
                }
                
                NSSortDescriptor *recentAddedDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = @[recentAddedDescriptor];
                
                NSArray *majorEvents = [arrMajorEvents sortedArrayUsingDescriptors:sortDescriptors];
                NSArray *minorEvents = [arrMinorEvents sortedArrayUsingDescriptors:sortDescriptors];
                
                eventItemArr3 = [majorEvents arrayByAddingObjectsFromArray:minorEvents];
            }
            /////////////////////////////////////////////////////////////
            
            if (eventItemArr3.count>0) {
                [self addEventsForGlanceMethod:eventItemArr3 itemArr:arrFordairyItem3 viewForEvent:view3] ;
            }
            
            [arrFordairyItem3 release];
            
            //for fourth month
            NSMutableArray *arrFordairyItem4=[[NSMutableArray alloc]init];
            NSString *assinegdDate4=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]];
            [btn4 addTarget:self action:@selector(goToDay:) forControlEvents:UIControlEventTouchUpInside];
            
            [btn4 setTitle:assinegdDate4 forState:UIControlStateNormal];
            [btn4 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [btn4 setTitleColor:[UIColor clearColor] forState:UIControlStateSelected];
            [btn4 setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            btn4.backgroundColor=[UIColor clearColor];
            
            //Important
            NSArray  *itemArr4=nil;
            NSArray  *itemDueArr4=nil;
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                itemArr4= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate4]];
                
                itemDueArr4= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate4]];
            }
            ///////////////////////////////////////////////
            
            [arrFordairyItem4 removeAllObjects];
            for (Item *itemObj in itemArr4) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        [arrFordairyItem4 addObject:itemObj];
                    }
                }
            }
            
            for (Item *itemObj in itemDueArr4) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        if (![arrFordairyItem4 containsObject:itemObj])
                        {
                            [arrFordairyItem4 addObject:itemObj];
                        }
                    }
                }
            }
            
            if (arrFordairyItem4.count>0) {
                int x=35;
                Item *itemObj=[arrFordairyItem4 objectAtIndex:0];
                
                UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, 2, 40, 40)];
                cellDescView.backgroundColor=[UIColor clearColor];
                
                
                UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
                cellDescBgImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                iconImgView.center=cellDescBgImgView.center;
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                if(class1.code!=nil)
                    cellDescBgImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    cellDescBgImgView.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                cellDescBgImgView.contentMode=UIViewContentModeScaleAspectFit;
                [cellDescBgImgView.layer setMasksToBounds:YES];
                [cellDescBgImgView.layer setCornerRadius:3.0f];
                [cellDescBgImgView.layer setBorderWidth:1.0];
                [cellDescBgImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 44, 44);
                [popUpButton setTitle:assinegdDate4 forState:UIControlStateNormal];
                [popUpButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                popUpButton.backgroundColor=[UIColor clearColor];
                [popUpButton addTarget:self action:@selector(showAllDiaryItemButtonPressedForGlance:) forControlEvents:UIControlEventTouchUpInside];
                
                //9julyBUG
                long timeDiff4;
                NSString *isTypeStr= itemObj.type;
                if(![itemObj.type isEqualToString:@"Note"])
                {
                    NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                    [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                    
                    timeDiff4=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]]]];
                    [formeter2 release];
                }
                
                if (arrFordairyItem4.count<2)
                {
                    if(timeDiff4<=0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];

                        if([itemObj.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else
                    {
                        if ([isTypeStr isEqualToString:@"Assignment"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"Homework"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:popUpButton];
                }
                else{
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        [self updatePasses:itemObj];

                        if([itemObj.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    
                    UIView * viewForCountLbl=[[UIView alloc]initWithFrame:CGRectMake(22, -2, 20, 20)];
                    viewForCountLbl.backgroundColor=[UIColor clearColor];
                    
                    UIImageView * imgViewForCountLbl=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    imgViewForCountLbl.backgroundColor=[UIColor clearColor];
                    [imgViewForCountLbl setImage:[UIImage imageNamed:@"notificationdot.png"]];
                    
                    UILabel *lableForCount=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
                    lableForCount.backgroundColor=[UIColor clearColor];
                    lableForCount.textAlignment=NSTextAlignmentCenter;
                    lableForCount.textColor=[UIColor whiteColor];
                    lableForCount.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
                    lableForCount.center=imgViewForCountLbl.center;
                    
                    [viewForCountLbl addSubview:imgViewForCountLbl];
                    [viewForCountLbl addSubview:lableForCount];
                    
                    [imgViewForCountLbl release];
                    [lableForCount release];
                    
                    lableForCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)arrFordairyItem4.count];
                    
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:viewForCountLbl];
                    [cellDescView addSubview:popUpButton];
                    [viewForCountLbl release];
                }
                x=x+50;
                
                cellDescView.clipsToBounds=NO;
                [view4 addSubview:cellDescView];
                [cellDescBgImgView release];
                [iconImgView release];
                [cellDescView release];
            }
            
            //for add Events items on Glance
            
            //Important
            NSArray  *eventItemArr4=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                NSArray *arrEvents= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:assinegdDate4]];
                NSMutableArray *arrMajorEvents = [[NSMutableArray alloc]init];
                NSMutableArray *arrMinorEvents = [[NSMutableArray alloc]init];
                
                for (EventItem *event in arrEvents)
                {
                        if ([event.priority isEqualToString:@"1"])
                            [arrMajorEvents addObject:event];
                        else
                            [arrMinorEvents addObject:event];
                }
                
                NSSortDescriptor *recentAddedDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = @[recentAddedDescriptor];
                
                NSArray *majorEvents = [arrMajorEvents sortedArrayUsingDescriptors:sortDescriptors];
                NSArray *minorEvents = [arrMinorEvents sortedArrayUsingDescriptors:sortDescriptors];
                
                eventItemArr4 = [majorEvents arrayByAddingObjectsFromArray:minorEvents];
            }
            /////////////////////////////////////////////
            if (eventItemArr4.count>0)
            {
                [self addEventsForGlanceMethod:eventItemArr4 itemArr:arrFordairyItem4 viewForEvent:view4] ; ;
            }
            
            [arrFordairyItem4 release];
            
            [formatter setDateFormat:@"dd"];
            monthLable1.text=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]];
            monthLable2.text=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]];
            monthLable3.text=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]];
            monthLable4.text=[formatter stringFromDate:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]];
            [formatter release];
            
            if([[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]])
            {
                [self getCureentTimeTable:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]];
                
                if(self._curentTimeTable)
                {
                    // Campus Based Start
                    /// New Code ///////
                    CalendarCycleDay *dayCycle =[self methodForCalendarCycleDay:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]];
                    /// New Code ///////
                    // Campus Based End
                    
                    if([self getCurentDayIsWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable4.text=nil;
                    }
                    else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"4month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable4.text=nil;
                    }
                    else{
                                //Cycle labels are not displaying in calendar Month and Glance views - start
                        dayLable4.text=dayCycle.formatedCycleDayLabel;
                                //Cycle labels are not displaying in calendar Month and Glance views - end
                    }
                }
                else{
                    dayLable4.text=nil;
                }
            }
            //
            if([[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]){
                [self getCureentTimeTable:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]];
                
                if(self._curentTimeTable){
                    // Campus Based Start
                    /// New Code ///////
                    CalendarCycleDay *dayCycle =[self methodForCalendarCycleDay:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]];
                    /// New Code ///////
                    // Campus Based End

                    if([self getCurentDayIsWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable3.text=nil;
                    }
                    else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"3month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable3.text=nil;
                    }
                    else{
                                //Cycle labels are not displaying in calendar Month and Glance views - start
                        dayLable3.text=dayCycle.formatedCycleDayLabel;//dayCycle.cycleDayLabel;//[NSString stringWithFormat:@"%@",[self methodForGetiingShortlable:cycleday]];
                                //Cycle labels are not displaying in calendar Month and Glance views - end
                    }
                }
                else{
                    dayLable3.text=nil;
                }
            }
            if([[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]){
                [self getCureentTimeTable:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]];
                if(self._curentTimeTable){
                    // Campus Based Start
                    // int cycleLenght=[[arrForCycle objectAtIndex:0] intValue];
//                    TableForCycle *dayCycle=[[Service sharedInstance]getlastcycleandDayTableDataInfoStored:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%d",indexPath.row+1]]];
//                    int cycleday=[dayCycle.cycleDay intValue];
                    /// New Code ///////
                    CalendarCycleDay *dayCycle =[self methodForCalendarCycleDay:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]];
                    //int cycleday=[dayCycle.cycleDay intValue];
                    /// New Code ///////
                    // Campus Based End

//                    [dayLable2 setBackgroundColor:[UIColor blackColor]];
                    
                    if([self getCurentDayIsWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable2.text=nil;
                    }
                    else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"2month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable2.text=nil;
                    }
                    else{
                                //Cycle labels are not displaying in calendar Month and Glance views - start
                        dayLable2.text=dayCycle.formatedCycleDayLabel;//dayCycle.cycleDayLabel;//[NSString stringWithFormat:@"%@",[self methodForGetiingShortlable:cycleday]];
                                //Cycle labels are not displaying in calendar Month and Glance views - end
                    }
                }
                else{
                    dayLable2.text=nil;
                }
            }
            if([[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]){
                [self getCureentTimeTable:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]];
                if(self._curentTimeTable){
                    // Campus Based Start
//                    TableForCycle *dayCycle=[[Service sharedInstance]getlastcycleandDayTableDataInfoStored:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%d",indexPath.row+1]]];
//                    int cycleday=[dayCycle.cycleDay intValue];
                    /// New Code ///////
                    CalendarCycleDay *dayCycle =[self methodForCalendarCycleDay:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]];
                    //int cycleday=[dayCycle.cycleDay intValue];
                    /// New Code ///////
                    // Campus Based End
                    [dayLable1 setBackgroundColor:[UIColor clearColor]];
                    if([self getCurentDayIsWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable1.text=nil;
                    }
                    else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[[_arr objectAtIndex:indexPath.row] objectForKey:[NSString stringWithFormat:@"1month%lu",(unsigned long)indexPath.row+1]]]){
                        dayLable1.text=nil;
                    }
                    else{
                                //Cycle labels are not displaying in calendar Month and Glance views - start
                        dayLable1.text=dayCycle.formatedCycleDayLabel;//[NSString stringWithFormat:@"%@(%@)",[self methodForGetiingShortlable:cycleday],dayCycle.ca];//dayCycle.cycleDayLabel;//[NSString stringWithFormat:@"%@",[self methodForGetiingShortlable:cycleday]];
                                //Cycle labels are not displaying in calendar Month and Glance views - end
                    }
                }
                else{
                    dayLable1.text=nil;
                }
            }
            
            return cell;
        }
        
        else if(_buttonForWeek.selected)
        {
            
            UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"CalenderCellForWeek"];
            
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CalenderCellForWeek" owner:self options:nil];
                // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                cell = [topLevelObjects objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.tag=indexPath.row;
            UIView *viewForContainer=(UIView*)[cell.contentView viewWithTag:500];
            
            UIImageView *BgImg=(UIImageView*)[cell.contentView viewWithTag:100];
            UILabel *cycle=(UILabel*)[cell.contentView viewWithTag:102];
            UILabel *weekday=(UILabel*)[cell.contentView viewWithTag:103];
            UILabel *day=(UILabel*)[cell.contentView viewWithTag:104];
            
            viewForContainer.center=CGPointMake(viewForContainer.center.x, BgImg.frame.size.height/2);
            
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSArray *daySymbols = [formatter weekdaySymbols];
            
            
            
            NSDate *date=[_arr objectAtIndex:indexPath.row];
            NSCalendar *calendar = [NSCalendar currentCalendar];
            
            [formatter setDateFormat:@"dd/MM/yyyy"];
            NSString *assinegdDate=[formatter stringFromDate:date];
            
            //Important
            
            //4julyimportant  done
            NSMutableArray *itemArr=[[NSMutableArray alloc]init];
            NSArray  *arrFordairyItem=nil;
            NSArray  *arrForDueDairyItem=nil;
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonForFilterNotes.selected)||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                
                arrFordairyItem= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate]];
                
                arrForDueDairyItem=[[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate]];
                
                [itemArr removeAllObjects];
                if ((!_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected)) {
                    for (Item *itemObj in arrFordairyItem) {
                        if ([itemObj.type isEqualToString:@"Note"]) {
                            [itemArr addObject:itemObj];
                        }
                    }
                }
                else if ((_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected))
                {
                    for (Item *itemObj in arrFordairyItem) {
                        if (![itemObj.type isEqualToString:@"Note"]) {
                            [itemArr addObject:itemObj];
                        }
                    }
                    
                    for (Item *itemObj in arrForDueDairyItem) {
                        if (![itemObj.type isEqualToString:@"Note"]) {
                            if (![itemArr containsObject:itemObj]) {
                                [itemArr addObject:itemObj];
                            }
                        }
                    }
                }
                else if(((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                    for (Item *itemObj in arrFordairyItem) {
                        [itemArr addObject:itemObj];
                    }
                    
                    for (Item *itemObj in arrForDueDairyItem) {
                        if (![itemArr containsObject:itemObj]) {
                            [itemArr addObject:itemObj];
                        }
                    }
                }
                else
                {
                    // Do Nothing
                }
            }
            
            ////////////////////////////////////////////////////////////////////////////////////////////
            
            itemCount=itemArr.count;
            
            
            if(indexPath.row ==0){
                BgImg.image=[UIImage imageNamed:@"weekdatatopcell"];
            }
            else if(indexPath.row ==_arr.count-1){
                
                BgImg.image=[UIImage imageNamed:@"weekdatabuttomcell"];
            }
            else
            {
                BgImg.image=[UIImage imageNamed:@"weekdatamidcell"];
            }
            
            int x=295;
            int y=16;
            int paddingW=10;
            int itemWidth=165;
            int itemHeight=28;
            int minX=295;
            int paddingH=14;
            int noOfColum=3;
            int variColum=1;
#if DEBUG
            NSLog(@"itemArr.count : %lu",(unsigned long)itemArr.count);
#endif

            
            for (int k=0; k<itemArr.count; k++)
            {
                Item* itemObj=[itemArr objectAtIndex:k];
                
                if (k<itemArr.count)
                {
                    UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, y, 165, 28)];
                    cellDescView.backgroundColor=[UIColor clearColor];
                    cellDescView.tag = 1100;
                    
                    UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 165, 28)];
                    cellDescBgImgView.backgroundColor=[UIColor clearColor];
                    
                    
                    UIImageView * imgViewForItemBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 38, 28)];
                    imgViewForItemBg.backgroundColor=[UIColor clearColor];
                    imgViewForItemBg.center=imgViewForItemBg.center;
                    
                    UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 4, 20, 19)];
                    iconImgView.backgroundColor=[UIColor clearColor];
                    UIImageView * imgViewForLine=[[UIImageView alloc]initWithFrame:CGRectMake(42, 13, 110, 2)];
                    imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                    imgViewForLine.backgroundColor=[UIColor grayColor];
                    
                    //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                    MyClass *class1=nil;
                    
                    class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                    if(class1.code!=nil)
                        imgViewForItemBg.backgroundColor=[AppHelper colorFromHexString:class1.code];
                    else{
                        imgViewForItemBg.backgroundColor=[UIColor grayColor];
                    }
                    
                    //--------- By Mitul ---------//
                    imgViewForItemBg.contentMode=UIViewContentModeScaleAspectFit;
                    [imgViewForItemBg.layer setMasksToBounds:YES];
                    [imgViewForItemBg.layer setCornerRadius:3.0f];
                    [imgViewForItemBg.layer setBorderWidth:1.0];
                    [imgViewForItemBg.layer setBorderColor:[[UIColor clearColor] CGColor]];
                    
                    
                    UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                    popUpButton.frame=CGRectMake(0, 0, 165, 28);
                    popUpButton.backgroundColor=[UIColor clearColor];
                    
                    //for notes item
                    NSString *isTypeStr= itemObj.type;
                    if ([isTypeStr isEqualToString:@"Note"])
                    {
                        [popUpButton addTarget:self action:@selector(showAllNotesItemButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else{
                        
                        [popUpButton addTarget:self action:@selector(showPopupButtonPressedForWeekAndMonth:) forControlEvents:UIControlEventTouchUpInside];
                        
                    }
                    
                    popUpButton.titleLabel.textColor=[UIColor clearColor];
                    if([itemObj.itemId integerValue]>0)
                    {
                        popUpButton.tag=[itemObj.itemId integerValue];
                        popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"1",assinegdDate];
                        [AppDelegate getAppdelegate]._isUniqueId=YES;
                    }
                    else
                    {
                        popUpButton.tag=[itemObj.universal_Id integerValue];
                        popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"0",assinegdDate];
                        [AppDelegate getAppdelegate]._isUniqueId=NO;
                    }
                    
                    [cellDescView addSubview:popUpButton];
                    [cellDescView addSubview:cellDescBgImgView];
                    [cellDescView addSubview:imgViewForItemBg];
                    [cellDescView addSubview:iconImgView];
                    [cellDescView addSubview:imgViewForLine];
                    [cell addSubview:cellDescView];
                    
                    cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                    
                    // check for due
                    if(![itemObj.type isEqualToString:@"Note"])
                    {
                        NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                        [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                        long timeDiff1;
                        timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[formatter dateFromString:assinegdDate]]]];
                        
                        [formeter2 release];
                        
                        if(timeDiff1<=0)
                        {
                            UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                            dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                            dulLbl.numberOfLines=1;
                            dulLbl.backgroundColor=[UIColor clearColor];
                            dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                            dulLbl.textColor=[UIColor grayColor];
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(68, 7,95, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            
                            dulLbl.text=@"Due:" ;
                            lblForTitle.text=itemObj.title ;
                            // Label dot dot added
                            NSString *temp = itemObj.title;
                            if ([temp length] > 10) {
                                NSRange range = [temp rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
                                temp = [temp substringWithRange:range];
                                temp = [temp stringByAppendingString:@"…"];
                                lblForTitle.text=temp;
                                
                            }
                            // Label dot dot added
                            
                            
                            [cellDescView addSubview:dulLbl];
                            [cellDescView addSubview:lblForTitle];
                            [dulLbl release];
                            [lblForTitle release];
                            if ([itemObj.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                            {
                                imgViewForLine.hidden=NO;
                            }
                            else
                            {
                                imgViewForLine.hidden=YES;
                            }
                            iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                            
#if DEBUG
                            NSLog(@"line number 3532 isTypeStr= %@",isTypeStr);
#endif

                            
                            
                            if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                            {
                                NSDateFormatter *formatForTimeFromDB = [[NSDateFormatter alloc] init];
                                [formatForTimeFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                                
                                NSDateFormatter *formatTimeToDisplay = [[NSDateFormatter alloc] init];
                                
                                // 24Hrs
                                
                                //added by viraj - start - issue #EZEST-2317
                                
                                [formatTimeToDisplay setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                                [formatTimeToDisplay setDateStyle:NSDateFormatterNoStyle];
                                [formatTimeToDisplay setTimeStyle:NSDateFormatterShortStyle];
                                
                                NSString *strFromTime = [formatTimeToDisplay stringFromDate:itemObj.assignedDate];
                                NSString *strToTime = [formatTimeToDisplay stringFromDate:itemObj.dueDate];
                                
                                //added by viraj - end - issue #EZEST-2317
                                
                                // 24Hrs
                                
                                lblForTitle.frame=CGRectMake(45, 3,120, 15);
                                lblForTitle.font=[UIFont fontWithName:@"Arial" size:14];
                                lblForTitle.textColor = [UIColor blackColor];
                                
                                UILabel *lblForTime=[[UILabel alloc]initWithFrame:CGRectMake(45, 19,110, 15)];
                                
                                lblForTime.font=[UIFont fontWithName:@"Arial" size:11];
                                lblForTime.backgroundColor=[UIColor clearColor];
                                lblForTime.text = [NSString stringWithFormat:@"%@ - %@",strFromTime,strToTime];
                                cellDescBgImgView.image=nil;
                                dulLbl.text=@"";
                                lblForTitle.text=itemObj.title ;// Label dot dot added
                                
                                [imgViewForItemBg setBackgroundColor:[UIColor clearColor]];
                                [cellDescView addSubview:lblForTime];
                                
                                
                                [self updatePasses:itemObj];

                                if([itemObj.isApproved integerValue] == 1)
                                {
                                    iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                                }
                                else  if([itemObj.isApproved integerValue] == 2)
                                {
                                    iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                                }

                                [lblForTime release];
                                [formatForTimeFromDB release];
                                [formatTimeToDisplay release];
                            }
                            

                            
                            else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                            {
                                dulLbl.text=@"";
                                lblForTitle.text=itemObj.title ;
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                                if (lov)
                                {
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                        iconImgView.image=[FileUtils getImageFromPath:strPath];
                                    }
                                }
                            }
                            
                            MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
#if DEBUG
                            NSLog(@"profobj %@",profobj.type);
#endif
                            
                            // Only FOr Student
                            if ([profobj.type isEqualToString:@"Student"])
                            {
                                // Fetching Diary Item Users on the bases of DI id
                                
                                NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:itemObj.itemType_id];
                                if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                                {
                                NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId];

#if DEBUG
                                NSLog(@"Before arrDIU %@",arrDIU);
#endif

                                arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                                NSLog(@"After arrDIU %@",arrDIU);
#endif

                                
                                for (DairyItemUsers *assinUserobj in arrDIU)
                                {
                                    if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                                    {
                                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                        if (lov)
                                        {
                                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                            {
                                                
                                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                                
                                                UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(138, 2, 25, 25)];
                                                [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                                                btnTaskBasedMerits.tag=[itemObj.itemId integerValue];
                                                [btnTaskBasedMerits setUserInteractionEnabled:NO];
                                                [cellDescView addSubview:btnTaskBasedMerits];
                                                [btnTaskBasedMerits release];
                                                
                                            }
                                        }
                                    }
                                }
                                }
                            }
                            // Only FOr Student

                            
                            
                        }
                        else{
                            
                            UILabel * compltedLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                            compltedLbl.font=[UIFont fontWithName:@"Arial" size:12];
                            compltedLbl.numberOfLines=1;
                            compltedLbl.backgroundColor=[UIColor clearColor];
                            compltedLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                            compltedLbl.textColor=[UIColor grayColor];
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,123, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            lblForTitle.text=itemObj.title ;
                            [cellDescView addSubview:compltedLbl];
                            [cellDescView addSubview:lblForTitle];
                            [compltedLbl release];
                            [lblForTitle release];
                            
                            
                            if ([isTypeStr isEqualToString:@"Assignment"]) {
                                iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                            }
                            else if([isTypeStr isEqualToString:@"Homework"]) {
                                iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                            }
                            if ([itemObj.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                            {
                                imgViewForLine.hidden=NO;
                            }
                            else
                            {
                                imgViewForLine.hidden=YES;
                            }
                        }
                    }
                    else
                    {
                        
                        UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                        dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                        dulLbl.numberOfLines=1;
                        dulLbl.backgroundColor=[UIColor clearColor];
                        dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                        dulLbl.textColor=[UIColor grayColor];
                        
                        UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(70, 7,93, 15)];
                        lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                        lblForTitle.backgroundColor=[UIColor clearColor];
                        dulLbl.text=@"Note: " ;
                        lblForTitle.text=itemObj.title ;
                        [cellDescView addSubview:dulLbl];
                        [cellDescView addSubview:lblForTitle];
                        [dulLbl release];
                        [lblForTitle release];
                        imgViewForLine.hidden=YES;
                        
                        iconImgView.image=[UIImage imageNamed:@"noteicon"];
                    }
                    
                    
                    [cellDescBgImgView release];
                    [iconImgView release];
                    [imgViewForItemBg release];
                    [imgViewForLine release];
                    [cellDescView release];
                    
                    if(variColum % noOfColum==0){
                        x=minX;
                        y=y+paddingH+itemHeight;
                        variColum=1;
                    }
                    else{
                        x=x+paddingW+itemWidth;
                        variColum=variColum+1;
                    }
                }
            }
            
            UIButton *addbutton=(UIButton*)[cell.contentView viewWithTag:105];
            [addbutton addTarget:self action:@selector(AddButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [addbutton setTitle:[NSString stringWithFormat:@"%@",assinegdDate] forState:UIControlStateNormal];
            [addbutton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [addbutton setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            
            BgImg.image=[UIImage imageNamed:@"allday2cell"];
            
            [formatter setDateFormat:@"dd"];
            
            NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:date];
            weekday.text=[NSString stringWithFormat:@"%@ %@",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:date]];
            [self getCureentTimeTable:date];
            if(self._curentTimeTable){
                // Campus Based Start
//                TableForCycle *dayCycle=[[Service sharedInstance]getlastcycleandDayTableDataInfoStored:date];
//                int cycleday=[dayCycle.cycleDay intValue];
//                int cycleLenght=[dayCycle.cycle intValue];
                
                // Campus Based End

                if([self getCurentDayIsWeekend:date]){
                    day.text=nil;
                    cycle.text=nil;
                }
                else  if([[Service sharedInstance]dayOfNotesHaveWeekend:date]){
                    day.text=nil;
                    cycle.text=nil;
                }
                else{
                    // Campus Based Start
                    
                    // New Code to fetch Cycle Lable From CalendarCycleDay Table
                    NSDateFormatter *formatterddMMyyyy = [[NSDateFormatter alloc] init];
                    
                    [formatterddMMyyyy setDateFormat:@"dd/MM/yyyy"];
                    //cycle number / Cycle day display issue for week -start
                    NSString *dayStr=[NSString stringWithFormat:@"%@",[self methodForGettingCalendarCycleLable:date]];

#if DEBUG
                    NSLog(@"dayStr %@",dayStr);
                    NSLog(@"day.superview : %@",day.superview);
                    NSLog(@"cell.contentView.frame.size.height : %f",cell.contentView.frame.size.height);
                    NSLog(@"day.frame.origin.y : %f",day.frame.origin.y);
#endif

                    [formatterddMMyyyy release];
                    
                    //////////
                    float dayLabelHeight;
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Arial" size:14.0]};
                        // NSString class method: boundingRectWithSize:options:attributes:context is
                        // available only on ios7.0 sdk.
                        CGRect rect = [dayStr boundingRectWithSize:CGSizeMake(90.0, CGFLOAT_MAX)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:attributes
                                                           context:nil];
                        dayLabelHeight = rect.size.height;

#if DEBUG
                        NSLog(@"rect : %f %f %f %f",rect.origin.x,rect.origin.y, rect.size.width,rect.size.height);
#endif
                    }
                    else
                    {
                        CGSize sizeWithOldMethod = [dayStr sizeWithFont:[UIFont fontWithName:@"Arial" size:14.0] constrainedToSize:CGSizeMake(90, 100) lineBreakMode:NSLineBreakByWordWrapping];
#if DEBUG
                        NSLog(@"before : sizeWithOldMethod : %f %f",sizeWithOldMethod.width,sizeWithOldMethod.height);
#endif
                        dayLabelHeight = sizeWithOldMethod.height;
                    }

                    /////////
                    
                    
                    CGRect containerFrame = viewForContainer.frame;
                    containerFrame.origin.x = 0;
                    containerFrame.origin.y = 0;
                    containerFrame.size.height = kGAP_BETWEEN_LABELS + kWEEK_DAY_LABEL_HEIGHT + kGAP_BETWEEN_LABELS + dayLabelHeight+kADDITIONAL_HEIGHT;//200;
                    [viewForContainer setFrame:containerFrame];
#if DEBUG
                    NSLog(@"before : %@",weekday);
#endif
                    [weekday setFrame:CGRectMake(2, 5, 90, kWEEK_DAY_LABEL_HEIGHT)];

#if DEBUG
                    NSLog(@"after : %@",weekday);
                    NSLog(@"before day: %@",day);
#endif

                    [day setFrame:CGRectMake(day.frame.origin.x, 30, day.frame.size.width, dayLabelHeight+kADDITIONAL_HEIGHT)];
#if DEBUG
                    NSLog(@"after day: %@",day);
#endif
                    
                    [day setNumberOfLines:0];
                    [day setLineBreakMode:NSLineBreakByWordWrapping];
                    [day setTextAlignment:NSTextAlignmentRight];

                    //cycle number / Cycle day display issue for week -end
                    day.text=dayStr;

                    cycle.text=nil;
                    // Campus Based End
                }
            }
            else{
                day.text=nil;
                cycle.text=nil;
            }
            
            //for add Events items on week
            
            [formatter setDateFormat:@"dd/MM/yyyy"];
            NSString *assinegdDateForEvent=[formatter stringFromDate:date];
            
            //Important
            NSArray  *eventItemArr=nil;
            
            //for add Events items on Glance
            
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                NSArray *arrEvents= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:assinegdDateForEvent]];
                NSMutableArray *arrMajorEvents = [[NSMutableArray alloc]init];
                NSMutableArray *arrMinorEvents = [[NSMutableArray alloc]init];
                
                for (EventItem *event in arrEvents)
                {
                    if ([event.priority isEqualToString:@"1"])
                        [arrMajorEvents addObject:event];
                    else
                        [arrMinorEvents addObject:event];
                }
                
                NSSortDescriptor *recentAddedDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = @[recentAddedDescriptor];
                
                NSArray *majorEvents = [arrMajorEvents sortedArrayUsingDescriptors:sortDescriptors];
                NSArray *minorEvents = [arrMinorEvents sortedArrayUsingDescriptors:sortDescriptors];
                
                eventItemArr = [majorEvents arrayByAddingObjectsFromArray:minorEvents];

            }
            
            //////////////////////////////////////////////
            
            if (eventItemArr.count>0) {
                
                [self addEventsForWeekMethod:eventItemArr myCell:cell] ;
            }
            
            [formatter release];
            

            return cell;
        }
        
        else if(_buttonForDay.selected){
            
            UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"CalenderCellForDay"];
            
            if (cell == nil)
            {
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CalenderCellForDay" owner:self options:nil];
                // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
                cell = [topLevelObjects objectAtIndex:0];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            }
            cell.tag=indexPath.row;
            
            UIView *viewForContainer=(UIView*)[cell.contentView viewWithTag:500];
            UIImageView *cornerImg=(UIImageView*)[cell.contentView viewWithTag:101];
            UIImageView *BgImg=(UIImageView*)[cell.contentView viewWithTag:100];
            UILabel *timelable=(UILabel*)[cell.contentView viewWithTag:102];
            UILabel *subject=(UILabel*)[cell.contentView viewWithTag:103];
            UILabel *room=(UILabel*)[cell.contentView viewWithTag:104];
            viewForContainer.center=CGPointMake(viewForContainer.center.x, BgImg.frame.size.height/2);
            
            UIButton *addbutton=(UIButton*)[cell.contentView viewWithTag:105];
            [addbutton addTarget:self action:@selector(AddButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            
            NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
            
            
            if(indexPath.row ==0)
            {
                //issues
                self.startTimeForDayView=@"AllDay";
                ////////////////////////////////////////////////////////
                
                ////////////
                //Important
                NSMutableArray *itemArr=[[NSMutableArray alloc]init];
                NSArray  *arrFordairyItem=nil;
                
                if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonForFilterNotes.selected)||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                    
                    arrFordairyItem= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:self.strForDayDate]];
#if DEBUG
                    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere @row = %d", __func__, __LINE__ );
#endif
                    
                    [itemArr removeAllObjects];
                    if ((!_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected)) {
                        for (Item *itemObj in arrFordairyItem) {
                            if ([itemObj.type isEqualToString:@"Note"]) {
                                [itemArr addObject:itemObj];
                            }
                        }
                    }
                    else if ((_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected))
                    {
                        for (Item *itemObj in arrFordairyItem) {
                            if (![itemObj.type isEqualToString:@"Note"]) {
                                [itemArr addObject:itemObj];
                            }
                        }
                        
                    }
                    else if(((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                        for (Item *itemObj in arrFordairyItem)
                        {
                            [itemArr addObject:itemObj];
                        }
                    }
                    else
                    {
                        // Do Nothing
                    }
                }
                /////////////////////////////////////////
                
                NSMutableArray *arrForItem=[[NSMutableArray alloc]init];
                for (int i=0; i<itemArr.count; i++)
                {
                    Item* itemObject=[itemArr objectAtIndex:i];
                    if ([itemObject.onTheDay integerValue]==1)
                    {
                        
                        [arrForItem addObject:itemObject];
                    }
                }
#if DEBUG
                NSLog(@"fpr All Day itemArr = %@",itemArr);
#endif
                itemCount=arrForItem.count;
                
                //for add Events items on day
                //Important
                NSArray  *eventItemArr=nil;
                if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                    eventItemArr= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:self.strForDayDate]];
                }
                ////////////////////////////////////
                NSMutableArray *arrForEvent=[[NSMutableArray alloc]init];
                
                NSInteger eventTotalcount=0;
                if (eventItemArr.count>0)
                {
                    
                    for (int i=0; i<eventItemArr.count; i++)
                    {
                        //issues
                        EventItem* eventItemObject=[eventItemArr objectAtIndex:i];
                        if (([eventItemObject.onTheDay integerValue]==1)||(([eventItemObject.startTime integerValue]==0)&&([eventItemObject.endTime integerValue]==0)))
                        {
                            //4julyimportant done
                            if ([eventItemObject.onTheDay integerValue]==1)
                            {
                                [arrForEvent addObject:eventItemObject];
                            }
                            
                        }
                        ///////////////////////////////////////////////////////////////////////////////////////////
                        
                    }
                    
                    NSInteger evevmtItemCount;
                    
                    if (arrForEvent.count>0) {
                        evevmtItemCount=arrForEvent.count;
                    }
                    else{
                        evevmtItemCount=1;
                    }
                    
                    if (evevmtItemCount%2==0)
                    {
                        eventTotalcount=31*(evevmtItemCount/2);
                    }
                    else{
                        eventTotalcount=31*(evevmtItemCount/2 +1);
                    }
                    
                    [self addEventsForDayMethod:arrForEvent myCell:cell] ;
                }
                
                cornerImg.image=[UIImage imageNamed:@"greenstrip"];
                BgImg.image=[UIImage imageNamed:@"alldaycell"];
                timelable.text=@"All Day";
                subject.text=@"";
                room.text=@" ";
                
                int x=205;
                NSInteger y;
                if (arrForEvent.count>0){
                    y=eventTotalcount;
                }
                else
                {
                    y=16;
                }
                [arrForEvent release];

                int paddingW=10;
                int itemWidth=180;
                int itemHeight=28;
                int minX=205;
                int paddingH=14;
                int noOfColum=3;
                int variColum=1;

#if DEBUG
                NSLog(@"arrForItem %@",arrForItem);
#endif
                for (int k=0; k<arrForItem.count; k++) {
                    Item* itemObj=[arrForItem objectAtIndex:k];

#if DEBUG
                    NSLog(@"itemObj - Line no:3800 %@",itemObj);
#endif
                    
                    if (k<arrForItem.count)
                    {
                        UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, y, 180, 28)];
                        cellDescView.backgroundColor=[UIColor clearColor];
                        
                        UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 28)];
                        cellDescBgImgView.backgroundColor=[UIColor clearColor];
                        
                        
                        UIImageView * imgViewForItemBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 38, 28)];
                        imgViewForItemBg.backgroundColor=[UIColor clearColor];
                        imgViewForItemBg.center=imgViewForItemBg.center;
                        
                        UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 4, 20, 19)];
                        iconImgView.backgroundColor=[UIColor clearColor];
                        UIImageView * imgViewForLine=[[UIImageView alloc]initWithFrame:CGRectMake(42, 13, 120, 2)];
                        imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                        imgViewForLine.backgroundColor=[UIColor grayColor];
                        
                        //----- Modified By Mitul To Set Class Color insted of Subject Color JIRA 1191 -----//
                        MyClass *class1=nil;
                        
                        class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                        if(class1.code!=nil)
                         imgViewForItemBg.backgroundColor=[AppHelper colorFromHexString:class1.code ];
                         else{
                         imgViewForItemBg.backgroundColor=[UIColor grayColor];
                         }

                        //--------- By Mitul ---------//
                        imgViewForItemBg.contentMode=UIViewContentModeScaleAspectFit;
                        [imgViewForItemBg.layer setMasksToBounds:YES];
                        [imgViewForItemBg.layer setCornerRadius:3.0f];
                        [imgViewForItemBg.layer setBorderWidth:1.0];
                        [imgViewForItemBg.layer setBorderColor:[[UIColor clearColor] CGColor]];
                        
                        
                        UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                        popUpButton.frame=CGRectMake(0, 0, 180, 28);
                        popUpButton.backgroundColor=[UIColor clearColor];
                        
                        //for notes item
                        NSString *isTypeStr= itemObj.type;
                        if ([isTypeStr isEqualToString:@"Note"])
                        {
                            [popUpButton addTarget:self action:@selector(showAllNotesItemButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else{
                            [popUpButton addTarget:self action:@selector(showPopupButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                        popUpButton.titleLabel.textColor=[UIColor clearColor];
                        if([itemObj.itemId integerValue]>0){
                            popUpButton.tag=[itemObj.itemId integerValue];
                            popUpButton.titleLabel.text=@"1";
                            [AppDelegate getAppdelegate]._isUniqueId=YES;
                        }
                        else{
                            popUpButton.titleLabel.text=@"0";
                            popUpButton.tag=[itemObj.universal_Id integerValue];
                            [AppDelegate getAppdelegate]._isUniqueId=NO;
                            
                        }
                        
                        [cellDescView addSubview:popUpButton];
                        [cellDescView addSubview:cellDescBgImgView];
                        [cellDescView addSubview:imgViewForItemBg];
                        [cellDescView addSubview:iconImgView];
                        [cellDescView addSubview:imgViewForLine];
                        [cell.contentView addSubview:cellDescView];
                        
                        //------------------------------------------------------
                        // Merits icon added on cell for Task Based Merits
                        
                        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
#if DEBUG
                        NSLog(@"profobj %@",profobj.type);
#endif
                        
                        // Only FOr Student
                        if ([profobj.type isEqualToString:@"Student"])
                        {
                            // Fetching Diary Item Users on the bases of DI id
                            NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:itemObj.itemType_id];
                            if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                            {
                            NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId];
#if DEBUG
                            NSLog(@"Before arrDIU %@",arrDIU);
#endif

                            arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                            NSLog(@"After arrDIU %@",arrDIU);
#endif

                            for (DairyItemUsers *assinUserobj in arrDIU)
                            {
                                if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                                {
                                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                    if (lov)
                                    {
                                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                        {
                                            
                                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
#if DEBUG
                                            NSLog(@"assinUserobj title %@",assinUserobj.meritTitle);
#endif

                                            
                                            UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(152, 2, 25, 25)];
                                            [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                                            btnTaskBasedMerits.tag=[itemObj.itemId integerValue];
#if DEBUG
                                            NSLog(@"cellDescView Frame2 %@",NSStringFromCGRect(cellDescView.frame));
#endif
                                            [btnTaskBasedMerits setUserInteractionEnabled:NO];
                                            [cellDescView addSubview:btnTaskBasedMerits];
                                            [btnTaskBasedMerits release];
                                        }
                                    }
                                }
                            }
                        }
                        }
                        //------------------------------------------------------
                        
                        [cellDescBgImgView release];
                        [iconImgView release];
                        [imgViewForItemBg release];
                        [imgViewForLine release];
                        [cellDescView release];
                        
                        // check for due
                        if(![itemObj.type isEqualToString:@"Note"]){
                            
                            iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                            UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                            dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                            dulLbl.numberOfLines=1;
                            dulLbl.backgroundColor=[UIColor clearColor];
                            dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                            dulLbl.textColor=[UIColor grayColor];
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(68, 7,110, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            
                            if ([itemObj.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                            {
                                imgViewForLine.hidden=NO;
                            }
                            else
                            {
                                imgViewForLine.hidden=YES;
                            }
                            dulLbl.text=@"Due:" ;
                            lblForTitle.text=itemObj.title ;
                            // Label dot dot added
                            NSString *temp = itemObj.title;
                            if ([temp length] > 10) {
                                NSRange range = [temp rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
                                temp = [temp substringWithRange:range];
                                temp = [temp stringByAppendingString:@"…"];
                                lblForTitle.text=temp;
                                
                            }
                            // Label dot dot added

                            
                            cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                            
                            if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                            {
                                dulLbl.text = @"";
                                 lblForTitle.text=itemObj.title ;// Label dot dot added
                                
                                [self updatePasses:itemObj];

                                if([itemObj.isApproved integerValue] == 1)
                                {
                                    iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                                }
                                else  if([itemObj.isApproved integerValue] == 2)
                                {
                                    iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                                }
                                cellDescBgImgView.image=nil;
                            }
                            else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                            {
                                dulLbl.text=@"";
                                 lblForTitle.text=itemObj.title ;// Label dot dot added
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                                if (lov)
                                {
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                        iconImgView.image=[FileUtils getImageFromPath:strPath];
                                    }
                                }
                            }
                            
                            [cellDescView addSubview:dulLbl];
                            [cellDescView addSubview:lblForTitle];
                            [dulLbl release];
                            [lblForTitle release];
                        }
                        else
                        {
                            iconImgView.image=[UIImage imageNamed:@"noteicon"];
                            
                            UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                            dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                            dulLbl.numberOfLines=1;
                            dulLbl.backgroundColor=[UIColor clearColor];
                            dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                            dulLbl.textColor=[UIColor grayColor];
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(70, 7,110, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            dulLbl.text=@"Note:" ;
                            imgViewForLine.hidden=YES;
                            lblForTitle.text=itemObj.title ;
                            [cellDescView addSubview:dulLbl];
                            [cellDescView addSubview:lblForTitle];
                            [dulLbl release];
                            [lblForTitle release];
                            cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                        }
                        
                        if(variColum % noOfColum==0){
                            x=minX;
                            y=y+paddingH+itemHeight;
                            variColum=1;
                        }
                        else{
                            x=x+paddingW+itemWidth;
                            variColum=variColum+1;
                        }
                    }
                }
                
                [arrForItem release];
                [itemArr release];
                
            }
            
            else
            {
#if DEBUG
                NSLog(@"inside else for periods... ");
#endif

                addbutton.hidden=NO;
                NSDictionary *dict=[_arr objectAtIndex:indexPath.row-1];
                
                if(self._curentTimeTable){
                    if(_isCycleday){
                        timelable.text=[NSString stringWithFormat:@"%@ \n %@ \n %@",[dict objectForKey:@"Period"],[dict objectForKey:@"start"],[dict objectForKey:@"end"]];
                        // Campus Based Start
                        CalendarCycleDay *dayCycle =[self methodForCalendarCycleDay:[formatter dateFromString:self.strForDayDate]];
                        NSInteger cycleday=[dayCycle.cycleDay integerValue];
                        // Campus Based End

                        MyClass *class1=nil;
                        if([dict objectForKey:@"Period_id"])
                        {
                            //-------------------   Class is on cycleDay 0   -------------------//
                            
                            class1=[[Service sharedInstance]getStoredFilterClass:[NSNumber numberWithInteger:[[dict objectForKey:@"Period_id"]integerValue]]cycle:@"0" timeTable:[NSNumber numberWithInteger:[[dict objectForKey:@"timeTable"]integerValue]]] ;
                            
                            if (!class1)
                            {
                                class1=[[Service sharedInstance]getStoredFilterClass:[NSNumber numberWithInteger:[[dict objectForKey:@"Period_id"]integerValue]]cycle:[NSString stringWithFormat:@"%d",cycleday] timeTable:[NSNumber numberWithInteger:[[dict objectForKey:@"timeTable"]integerValue]]] ;
                            }
                            
                            //-------------------   Class is on cycleDay 0   -------------------//
                        }
                        if(class1){
                            
                            subject.text=class1.class_name;
                            addbutton.tag=[class1.class_id integerValue];
                            room.text=class1.room_name;
                            
                            //Save subject for add Item on DayView
                            if (subject.text.length>0) {
                                self.strForSub=[NSString stringWithFormat:@"%@",subject.text];
                            }
                            
                            // 17 july work
                            Subjects *sub=[[Service sharedInstance]getSubjectsDataInfoStored:class1.subject_name  postNotification:NO];
                            
                            if(sub.subject_color!=nil){
                                if (class1.code.length != 0) {
                                    cornerImg.backgroundColor=[AppHelper colorFromHexString:class1.code];

                                }else{
                                    cornerImg.backgroundColor=[AppHelper colorFromHexString:@"#FFFFFF"];

                                }
                            }
                            else{
                                cornerImg.backgroundColor=[AppHelper colorFromHexString:@"#FFFFFF"];
                            }
                            
                        }
                        else{
                            self.strForSub=@"";
                            cornerImg.backgroundColor=[UIColor whiteColor];
                        }
                    }
                    else{
                        cornerImg.backgroundColor=[UIColor whiteColor];
                        
                        timelable.text=[NSString stringWithFormat:@" %@ \n %@",[dict objectForKey:@"start"],[dict objectForKey:@"end"]];
                        room.text=nil;
                        subject.text=nil;
                    }
                }
                else{
                    timelable.text=[NSString stringWithFormat:@" %@ \n %@",[dict objectForKey:@"start"],[dict objectForKey:@"end"]];
                    room.text=nil;
                    subject.text=nil;
                    cornerImg.backgroundColor=[UIColor whiteColor];
                }
                
                BgImg.image=[UIImage imageNamed:@"allday2cell"];
                
                //Important
                NSMutableArray *itemArr=[[NSMutableArray alloc]init];
                NSArray  *arrFordairyItem=nil;
                NSArray  *arrForDueDairyItem=nil;

                [formatter1 setTimeStyle:NSDateFormatterShortStyle];
                [formatter1 setDateStyle:NSDateFormatterNoStyle];
                
                // 24Hrs
                if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonForFilterNotes.selected)||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
                {
                    //bugissue30june
                    
                    arrFordairyItem= [[Service sharedInstance] getStoredAllItemDataTime:[formatter1 dateFromString:[dict objectForKey:@"start"]] toDate:[formatter1 dateFromString:[dict objectForKey:@"end"]] assign:[formatter dateFromString:self.strForDayDate]];
                    
                    arrForDueDairyItem= [[Service sharedInstance] getStoredAllDueItemDataTime:[formatter1 dateFromString:[dict objectForKey:@"start"]] toDate:[formatter1 dateFromString:[dict objectForKey:@"end"]] assign:[formatter dateFromString:self.strForDayDate]];
                    
#if DEBUG
                    NSLog(@"at Line 4100 - arrForDueDairyItem count %lu",(unsigned long)arrForDueDairyItem.count);
#endif
                    [itemArr removeAllObjects];
                    if ((!_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected)) {
                        for (Item *itemObj in arrFordairyItem) {
                            if ([itemObj.type isEqualToString:@"Note"]) {
                                if ([itemObj.onTheDay integerValue]==0)
                                {
                                    [itemArr addObject:itemObj];
                                }
                            }
                        }
                    }
                    else if ((_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected))
                    {
                        for (Item *itemObj in arrFordairyItem) {
                            if (![itemObj.type isEqualToString:@"Note"]) {
                                
                                [itemArr addObject:itemObj];
                            }
                        }
                        
                        for (Item *itemObj in arrForDueDairyItem) {
                            if (![itemObj.type isEqualToString:@"Note"]) {
                                if(![itemArr containsObject:itemObj])
                                    [itemArr addObject:itemObj];
                            }
                        }
                    }
                    else if(((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
                    {
                    
#if DEBUG
                        NSLog(@"else arrFordairyItem %@",arrFordairyItem);
#endif

                        for (Item *itemObj in arrFordairyItem)
                        {
#if DEBUG
                        NSLog(@"else itemObj %@",itemObj);
#endif
                            
                            if ([itemObj.type isEqualToString:@"Note"])
                            {
                                if ([itemObj.onTheDay integerValue]==0)
                                {
                                    [itemArr addObject:itemObj];
                                }
                            }
                            else{
                                [itemArr addObject:itemObj];
                            }
                        }
                        
                        for (Item *itemObj in arrForDueDairyItem)
                        {
                            if(![itemArr containsObject:itemObj])
                                [itemArr addObject:itemObj];
                        }
                    }
                    else
                    {
                        // Do Nothing
                    }
                    ////////////////////////////////////////////////
                }
                
                //issues
                self.startTimeForDayView=[NSString stringWithFormat:@"%@     %@",[dict objectForKey:@"start"],[dict objectForKey:@"end"]];
                
                itemCount=itemArr.count;
                
                //for add Events items on day//////
                //Important
                
                //Day Button If -2
                
                NSArray  *eventItemArr=nil;
                if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected))){
                    
                    eventItemArr= [[Service sharedInstance] getStoredAllEventDairyItem:[formatter1 dateFromString:[dict objectForKey:@"start"]] toDate:[formatter1 dateFromString:[dict objectForKey:@"end"]] satrtdate:[formatter dateFromString:self.strForDayDate]];
                }
                /////////////////////////////////////////////////////
                NSMutableArray *arrForEvent=[[NSMutableArray alloc]init];
                NSInteger eventTotalcount=0;
                
                if (eventItemArr.count>0)
                {
                    for (int i=0; i<eventItemArr.count; i++)
                    {
                        EventItem* eventItemObject=[eventItemArr objectAtIndex:i];
                        if ([eventItemObject.onTheDay integerValue]==0)
                        {
                            [arrForEvent addObject:eventItemObject];
                        }
                    }
                    
                    NSInteger evevmtItemCount;
                    if (arrForEvent.count>0) {
                        evevmtItemCount=arrForEvent.count;
                    }
                    else{
                        evevmtItemCount=1;
                    }
                    
                    if (evevmtItemCount%2==0)
                    {
                        eventTotalcount=31*(evevmtItemCount/2);
                        
                    }
                    else{
                        eventTotalcount=31*(evevmtItemCount/2 +1);
                    }
                    
                    [self addEventsForDayMethod:arrForEvent myCell:cell] ;
                }
                
                int x=205;
                NSInteger y;
                if (arrForEvent.count>0){
                    y=eventTotalcount;
                }
                else
                {
                    y=16;
                }
                [arrForEvent release];
                
                int paddingW=10;
                int itemWidth=180;
                int itemHeight=28;
                int minX=205;
                int paddingH=14;
                int noOfColum=3;
                int variColum=1;
                
#if DEBUG
                NSLog(@" at 4241 Line itemArr Count %lu",(unsigned long)itemArr.count);
#endif

                for (int k=0; k<itemArr.count; k++)
                {
                    Item* itemObj=[itemArr objectAtIndex:k];
                    if (k<itemArr.count)
                    {
                        UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, y, 180, 28)];
                        cellDescView.backgroundColor=[UIColor clearColor];
                        
                        UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 180, 28)];
                        cellDescBgImgView.backgroundColor=[UIColor clearColor];
                        
                        
                        UIImageView * imgViewForItemBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 38, 28)];
                        imgViewForItemBg.backgroundColor=[UIColor clearColor];
                        imgViewForItemBg.center=imgViewForItemBg.center;
                        
                        UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 4, 20, 19)];
                        iconImgView.backgroundColor=[UIColor clearColor];
                        UIImageView * imgViewForLine=[[UIImageView alloc]initWithFrame:CGRectMake(42, 13, 120, 2)];
                        imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                        imgViewForLine.backgroundColor=[UIColor grayColor];
                        
                        //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                        MyClass *class1=nil;
                        
                        class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
                        if(class1.code!=nil)
                            imgViewForItemBg.backgroundColor=[AppHelper colorFromHexString:class1.code ];
                        else{
                            imgViewForItemBg.backgroundColor=[UIColor grayColor];
                        }
                        
                        //--------- By Mitul ---------//
                        
                        imgViewForItemBg.contentMode=UIViewContentModeScaleAspectFit;
                        [imgViewForItemBg.layer setMasksToBounds:YES];
                        [imgViewForItemBg.layer setCornerRadius:3.0f];
                        [imgViewForItemBg.layer setBorderWidth:1.0];
                        [imgViewForItemBg.layer setBorderColor:[[UIColor clearColor] CGColor]];
                        
                        
                        UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                        popUpButton.frame=CGRectMake(0, 0, 180, 28);
                        popUpButton.backgroundColor=[UIColor clearColor];
                        
                        //for notes item
                        NSString *isTypeStr= itemObj.type;
                        if ([isTypeStr isEqualToString:@"Note"])
                        {
                            [popUpButton addTarget:self action:@selector(showAllNotesItemButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        else{
                            [popUpButton addTarget:self action:@selector(showPopupButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        }
                        
                        popUpButton.titleLabel.textColor=[UIColor clearColor];
                        if([itemObj.itemId integerValue]>0){
                            popUpButton.tag=[itemObj.itemId integerValue];
                            popUpButton.titleLabel.text=@"1";
                            [AppDelegate getAppdelegate]._isUniqueId=YES;
                        }
                        else{
                            popUpButton.titleLabel.text=@"0";
                            popUpButton.tag=[itemObj.universal_Id integerValue];
                            [AppDelegate getAppdelegate]._isUniqueId=NO;
                        }
                        
                        [cellDescView addSubview:popUpButton];
                        [cellDescView addSubview:cellDescBgImgView];
                        [cellDescView addSubview:imgViewForItemBg];
                        [cellDescView addSubview:iconImgView];
                        [cellDescView addSubview:imgViewForLine];
                        [cell.contentView addSubview:cellDescView];
                        
                        // Merits icon added on cell for Task Based Merits
                        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                        
                        //------------------------------------------------------
                        // Only FOr Student
                        if ([profobj.type isEqualToString:@"Student"])
                        {
                            // Fetching Diary Item Users on the bases of DI id
                            
                            NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:itemObj.itemType_id];
                            if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                            {
                            NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId];
                            
#if DEBUG
                                NSLog(@"Before arrDIU %@",arrDIU);
#endif
                            arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];

#if DEBUG
                                NSLog(@"After arrDIU %@",arrDIU);
#endif

                            for (DairyItemUsers *assinUserobj in arrDIU)
                            {
                                if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                                {
                                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                    if (lov)
                                    {
                                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                        {
                                            
                                            
                                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                            
                                            UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(152, 2, 25, 25)];
                                            [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                                            btnTaskBasedMerits.tag=[itemObj.itemId integerValue];
                                            [btnTaskBasedMerits setUserInteractionEnabled:NO];
                                            
                                            [cellDescView addSubview:btnTaskBasedMerits];
                                            [btnTaskBasedMerits release];
                                            
                                            
                                        
                                            
                                        }
                                    }
                                }
                            }
                            }
                        }
                        //------------------------------------------------------
                        
                        [cellDescBgImgView release];
                        [iconImgView release];
                        [imgViewForItemBg release];
                        [imgViewForLine release];
                        [cellDescView release];
                        
                        
                        // check for due
                        //------- Mitul modified for 12/24 Hr Format --------//
                        // New not working for Due Copy - 2305
                       
                        ///----- Mitul Added to fix 2305 (when two due copies are in same cell the it gives dd/mm/yyyy Date format
                        // so it should be time format only -- Start ---
                        [formatter1 setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                        [formatter1 setDateStyle:NSDateFormatterNoStyle];
                        [formatter1 setTimeStyle:NSDateFormatterShortStyle];
                        ///------ End
                        
                        // timeDiff = Period Start Time
                        int timeDiff=(int)[itemObj.dueTimeDate timeIntervalSinceDate:[formatter1 dateFromString:[dict objectForKey:@"start"]]];
                        
                        // timeDiff1 = Period end Time
                        int timeDiff1=(int)[itemObj.dueTimeDate timeIntervalSinceDate:[formatter1 dateFromString:[dict objectForKey:@"end"]]];
                        //------- Mitul modified for 12/24 Hr Format --------//
                        
                        if(![itemObj.type isEqualToString:@"Note"])
                        {
#if DEBUG
                            NSLog(@"Inside ! Note Condition");
#endif
                            if (([itemObj.onTheDay integerValue]==0)&& timeDiff >=0 &&timeDiff1<0 )
                            {
                                [formatter1 setDateFormat:@"dd/MM/yyyy"];

                                // Changed to fix 2305 issue: Start ======>
                                int timeDiff2=[[formatter1 dateFromString:[formatter1 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formatter1 dateFromString:self.strForDayDate]];

                                //================================== End =======
                                if(timeDiff2>0)
                                {
#if DEBUG
                                    NSLog(@"inside if of timeDiff2>0 %d ",timeDiff2);
#endif
                                    
                                    UILabel * completedLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                                    completedLbl.font=[UIFont fontWithName:@"Arial" size:12];
                                    completedLbl.numberOfLines=1;
                                    completedLbl.backgroundColor=[UIColor clearColor];
                                    completedLbl.textColor=[UIColor grayColor];
                                    
                                    UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,125, 15)];
                                    lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                                    lblForTitle.backgroundColor=[UIColor clearColor];
                                    lblForTitle.text=itemObj.title ;
                                    [cellDescView addSubview:lblForTitle];
                                    [cellDescView addSubview:completedLbl];
                                    [lblForTitle release];
                                    [completedLbl release];
                                    
                                    if ([isTypeStr isEqualToString:@"Assignment"]) {
                                        
                                        iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                                    }
                                    else if([isTypeStr isEqualToString:@"Homework"]) {
                                        iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                                    }
                                    if ([itemObj.progress isEqualToString:@"100"])
                                    {
                                        imgViewForLine.hidden=NO;
                                    }
                                    else
                                    {
                                        imgViewForLine.hidden=YES;
                                    }
                                }
                                else
                                {
                                    
                                    UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                                    dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                                    dulLbl.numberOfLines=1;
                                    dulLbl.backgroundColor=[UIColor clearColor];
                                    dulLbl.textColor=[UIColor grayColor];
                                    
                                    UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(68, 7,110, 15)];
                                    lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                                    lblForTitle.backgroundColor=[UIColor clearColor];
                                    
                                    if ([itemObj.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                                    {
                                        imgViewForLine.hidden=NO;
                                    }
                                    else
                                    {
                                        imgViewForLine.hidden=YES;
                                    }
                                    dulLbl.text=@"Due:";
                                    lblForTitle.text=itemObj.title ;
                                    // Label dot dot added
                                    NSString *temp = itemObj.title;
                                    if ([temp length] > 10) {
                                        NSRange range = [temp rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
                                        temp = [temp substringWithRange:range];
                                        temp = [temp stringByAppendingString:@"…"];
                                        lblForTitle.text=temp;

                                    }
                                    // Label dot dot added
                                    
                                    
                                    [cellDescView addSubview:dulLbl];
                                    [cellDescView addSubview:lblForTitle];
                                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                                    cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                                    
                                    // OutOfClass
                                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                                    {
                                        NSDateFormatter *formatForTimeFromDB = [[NSDateFormatter alloc] init];
                                        [formatForTimeFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                                        
                                        NSDateFormatter *formatTimeToDisplay = [[NSDateFormatter alloc] init];
                                        // 24Hrs
                                        
                                        //added by viraj - start - issue #EZEST-2317
                                        
                                        [formatTimeToDisplay setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                                        [formatTimeToDisplay setDateStyle:NSDateFormatterNoStyle];
                                        [formatTimeToDisplay setTimeStyle:NSDateFormatterShortStyle];
                                        
                                        NSString *strFromTime = [formatTimeToDisplay stringFromDate:itemObj.assignedDate];
                                        NSString *strToTime = [formatTimeToDisplay stringFromDate:itemObj.dueDate];
                                        
                                        //added by viraj - end - issue #EZEST-2317
                                        
                                        // 24Hrs
                                        
                                        lblForTitle.frame=CGRectMake(45, 3,120, 15);
                                        lblForTitle.font=[UIFont fontWithName:@"Arial" size:14];
                                        lblForTitle.textColor = [UIColor blackColor];
                                        
                                        UILabel *lblForTime=[[UILabel alloc]initWithFrame:CGRectMake(45, 19,110, 15)];
                                        
                                        lblForTime.font=[UIFont fontWithName:@"Arial" size:11];
                                        lblForTime.backgroundColor=[UIColor clearColor];
                                        lblForTime.text = [NSString stringWithFormat:@"%@ - %@",strFromTime,strToTime];
                                        
                                        [self updatePasses:itemObj];
                                        
                                        if([itemObj.isApproved integerValue] == 1)
                                        {
                                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                                        }
                                        else  if([itemObj.isApproved integerValue] == 2)
                                        {
                                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                                        }
                                        
                                        cellDescBgImgView.image=nil;
                                        dulLbl.text=@"";
                                        lblForTitle.text=itemObj.title ;// Label dot dot added


                                        [imgViewForItemBg setBackgroundColor:[UIColor clearColor]];
                                        [cellDescView addSubview:lblForTime];
                                        [lblForTime release];
                                        [formatForTimeFromDB release];
                                        [formatTimeToDisplay release];
                                    }
                                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                                    {
                                        dulLbl.text=@"";
                                         lblForTitle.text=itemObj.title ; // Label dot dot added
                                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                                        if (lov)
                                        {
                                            
                                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                            {
                                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                                            }
                                        }
                                    }
                                    [dulLbl release];
                                    [lblForTitle release];
                                }
                            }
                            else
                            {
                                UILabel * completedLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                                completedLbl.font=[UIFont fontWithName:@"Arial" size:12];
                                completedLbl.numberOfLines=1;
                                completedLbl.backgroundColor=[UIColor clearColor];
                                completedLbl.textColor=[UIColor grayColor];
                                
                                UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,125, 15)];
                                lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                                lblForTitle.backgroundColor=[UIColor clearColor];
                                lblForTitle.text=itemObj.title ;
                                // Label dot dot added
                                NSString *temp = itemObj.title;
                                if ([temp length] > 10) {
                                    NSRange range = [temp rangeOfComposedCharacterSequencesForRange:(NSRange){0, 10}];
                                    temp = [temp substringWithRange:range];
                                    temp = [temp stringByAppendingString:@"…"];
                                    lblForTitle.text=temp;
                                    
                                }
                                // Label dot dot added

                                
                                
                                [cellDescView addSubview:lblForTitle];
                                [cellDescView addSubview:completedLbl];
                                [lblForTitle release];
                                [completedLbl release];
                                
#if DEBUG
                                NSLog(@"inside else of timeDiff1<0 %d",timeDiff1);
                                NSLog(@"itemObj.title %@",itemObj.title);
#endif

                                if ([isTypeStr isEqualToString:@"Assignment"])
                                {
                                    iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                                }
                                else if([isTypeStr isEqualToString:@"Homework"])
                                {
                                    iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                                }
                                if ([itemObj.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                                {
                                    imgViewForLine.hidden=NO;
                                }
                                else
                                {
                                    imgViewForLine.hidden=YES;
                                }
                                cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                                
                                if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                                {
                                    NSDateFormatter *formatForTimeFromDB = [[NSDateFormatter alloc] init];
                                    [formatForTimeFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                                    
                                    NSDateFormatter *formatTimeToDisplay = [[NSDateFormatter alloc] init];
                                    // 24Hrs
                                    
                                    //added by viraj - start - issue #EZTEST-2317
                                    //                                    [formatTimeToDisplay setDateFormat:[formatTimeToDisplay dateFormat]];
                                    //[formatTimeToDisplay setDateFormat:kDateFormatToShowOnUI_hhmma];
                                    
                                    [formatTimeToDisplay setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                                    [formatTimeToDisplay setDateStyle:NSDateFormatterNoStyle];
                                    [formatTimeToDisplay setTimeStyle:NSDateFormatterShortStyle];

                                    NSString *strFromTime = [formatTimeToDisplay stringFromDate:itemObj.assignedDate];
                                    NSString *strToTime = [formatTimeToDisplay stringFromDate:itemObj.dueDate];
                                    //added by viraj - end - issue #EZTEST-2317

                                    // 24Hrs
                                
                                    lblForTitle.frame=CGRectMake(45, 3,120, 15);
                                    lblForTitle.font=[UIFont fontWithName:@"Arial" size:14];
                                    lblForTitle.textColor = [UIColor blackColor];
                                    
                                    UILabel *lblForTime=[[UILabel alloc]initWithFrame:CGRectMake(45, 19,110, 15)];
                                    
                                    lblForTime.font=[UIFont fontWithName:@"Arial" size:11];
                                    lblForTime.backgroundColor=[UIColor clearColor];
                                    lblForTime.text = [NSString stringWithFormat:@"%@ - %@",strFromTime,strToTime];
                                    
                                    [self updatePasses:itemObj];

                                    if([itemObj.isApproved integerValue] == 1)
                                    {
                                        iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                                    }
                                    else  if([itemObj.isApproved integerValue] == 2)
                                    {
                                        iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                                    }
                                    cellDescBgImgView.image=nil;
                                    [imgViewForItemBg setBackgroundColor:[UIColor clearColor]];
                                    [cellDescView addSubview:lblForTime];
                                    [lblForTime release];
                                    [formatForTimeFromDB release];
                                    [formatTimeToDisplay release];
                                    lblForTitle.text=itemObj.title ; // Label dot dot added
                                   


                                }
                                else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                                {
                                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                                    if (lov)
                                    {
                                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                        {
                                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                            iconImgView.image=[FileUtils getImageFromPath:strPath];
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
#if DEBUG
                            NSLog(@"Inside ! Note Else Condition");
#endif
                            UILabel * dulLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 7,50, 15)];
                            dulLbl.font=[UIFont fontWithName:@"Arial" size:12];
                            dulLbl.numberOfLines=1;
                            dulLbl.backgroundColor=[UIColor clearColor];
                            dulLbl.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                            dulLbl.textColor=[UIColor grayColor];
                            iconImgView.image=[UIImage imageNamed:@"noteicon"];
                            
                            UILabel * lblForTitle=[[UILabel alloc]initWithFrame:CGRectMake(70, 7,110, 15)];
                            lblForTitle.font=[UIFont fontWithName:@"Arial" size:12];
                            lblForTitle.backgroundColor=[UIColor clearColor];
                            dulLbl.text=@"Note:" ;
                            lblForTitle.text=itemObj.title ;
                            [cellDescView addSubview:dulLbl];
                            [cellDescView addSubview:lblForTitle];
                            [dulLbl release];
                            [lblForTitle release];
                            imgViewForLine.hidden=YES;
                            
                            cellDescBgImgView.image=[UIImage imageNamed:@"notificationcellblue"];
                        }
                        
                        if(variColum % noOfColum==0){
                            x=minX;
                            y=y+paddingH+itemHeight;
                            variColum=1;
                        }
                        else{
                            x=x+paddingW+itemWidth;
                            variColum=variColum+1;
                        }
                    }
                }
                [itemArr release];
            }
            
            [addbutton setTitle:[NSString stringWithFormat:@"%@     %@     %@",self.strForDayDate,self.startTimeForDayView,self.strForSub] forState:UIControlStateNormal];
            [addbutton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
            [addbutton setTitleColor:[UIColor clearColor] forState:UIControlStateHighlighted];
            
            [formatter release];
            [formatter1 release];
            
            return cell;
        }
    }
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //14AugChange
    if (_buttonForWeek.selected)
    {
        // TODO: Set Flag if Button Pressed to fix calendar picker issue 2565
        isSpecificDateSelected = YES;
        NSDate *serchDate=[_arr objectAtIndex:indexPath.row];
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        self.strForDayDate=[formatter stringFromDate:serchDate];
        
        [self dayButtonAction:nil];
        [formatter release];
    }
}

#pragma mark buttonActioLifeCycle
-(void)goToDay:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    
    if(btn.titleLabel.text.length>0)
    {
        // TODO: Set Flag if Button Pressed to fix calendar picker issue 2565
        isSpecificDateSelected = YES;
        NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSDate *serchDate=[formatter dateFromString:btn.titleLabel.text];
        
        self.strForDayDate=[formatter stringFromDate:serchDate];
        
        [self dayButtonAction:nil];
        [formatter release];
    }
}

- (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}

- (void)setFilterButtonsEnableOrDisable
{
    // --------- Filter button Disable Logic Starts -----------
    [_buttonFortaskFilter setUserInteractionEnabled:NO];
    [_buttonForFilterEvent setUserInteractionEnabled:NO];
    [_buttonForFilterNotes setUserInteractionEnabled:NO];
    
    NSArray *activeEvents = [[Service sharedInstance] getStoreDAllActiveDiaryItemTypes];
    BOOL isTheObjectAssignment = [activeEvents containsObject: @"Assignment"];
    BOOL isTheObjectHomework = [activeEvents containsObject: @"Homework"];
    NSMutableArray *arryState = [[NSMutableArray alloc] init];
    [arryState addObject:[NSNumber numberWithBool:isTheObjectAssignment]];
    [arryState addObject:[NSNumber numberWithBool:isTheObjectHomework]];
    
    BOOL isTheObjectNote = [activeEvents containsObject: @"Note"];
    BOOL isTheObjectEvent = [activeEvents containsObject: @"Event"];

    if([arryState containsObject:[NSNumber numberWithBool:YES]])
    {
        [_buttonFortaskFilter setUserInteractionEnabled:YES];
    }
    if(isTheObjectEvent)
    {

        [_buttonForFilterEvent setUserInteractionEnabled:YES];
    }
    
    if(isTheObjectNote)
    {
        [_buttonForFilterNotes setUserInteractionEnabled:YES];
    }
    
    [arryState release];
    arryState = nil;
    // --------- Filter button Disable Logic Ends -----------
}

- (IBAction)dateButtonIconAction:(id)sender
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif

    if(_buttonForDateIcon.selected){
        _buttonForDateIcon.selected=NO;
        _viewForFilter.hidden=YES;
        _viewForMonthweek.hidden=YES;
        _viewForDay.hidden=NO;
        [self customizepikerView];

        [eventTableV reloadData];
    }
    else{
        [pickerViewDay reloadAllComponents];
        
        [self customizepikerView];
        _viewForFilter.hidden=NO;
        _viewForMonthweek.hidden=NO;
        _viewForDay.hidden=YES;
        _buttonForDateIcon.selected=YES;
        
        [self setFilterButtonsEnableOrDisable];

        for(int i=0;i< pickerViewDay.subviews.count;i++)
        {
            //Aman added -- iOS7 optimisation --------------------------------------
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [(UIView*)[[ pickerViewDay subviews] objectAtIndex:i] setAlpha:1.0];
                ((UIView *)[pickerViewDay.subviews objectAtIndex:1]).backgroundColor = [UIColor clearColor];
                ((UIView *)[pickerViewDay.subviews objectAtIndex:2]).backgroundColor = [UIColor clearColor];
            }
            else
            {
                [(UIView*)[[ pickerViewDay subviews] objectAtIndex:i] setAlpha:0.0];
            }
            //Aman added -- iOS7 optimisation --------------------------------------
            
        }
        
        [(UIView*)[[ pickerViewDay subviews] objectAtIndex:2] setAlpha:1.0];
        if([ pickerViewDay subviews].count>5)
            [(UIView*)[[ pickerViewDay subviews] objectAtIndex:5] setAlpha:1.0];
        
    }
    
}

-(void)AddButtonAction:(id)sender{
    
    UIButton *btn=(UIButton*)sender;
    
    if(_buttonForDay.selected){
        
        [[AppDelegate getAppdelegate]._currentViewController addDiaryItemButtonPressedOnCalender:btn];
        
    }
    else{
        [[AppDelegate getAppdelegate]._currentViewController addDiaryItemBtnPressedOnCalenderForWeek:btn];
    }
}

- (void)editTaskBasedMerits:(id)sender
{
    [[AppDelegate getAppdelegate]._currentViewController editAddedTaskBasedMerit:[sender tag]];
    
    if([self._popoverControler isPopoverVisible])
    {
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler.contentViewController.view = nil;
        return;
    }
}

-(void)edtiButtunPessed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    btn.selected=YES;
    [[AppDelegate getAppdelegate]._currentViewController editAddedDiaryItem:btn.tag];
    
    if([self._popoverControler isPopoverVisible])
    {
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler.contentViewController.view = nil;
        return;
    }
}

#pragma mark
#pragma mark Show DairyItem Details methods

-(void)showAllDiaryItemButtonPressedForGlance:(id)sender {
    
    UIButton *btn=(UIButton*)sender;
    btn_ToHoldClickMonthDate = btn;

    NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
    
    [formeter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *assignDate=[NSString stringWithFormat:@"%@",btn.titleLabel.text];
    
    ShowAllDairyItemViewController *viewpopup=[[ShowAllDairyItemViewController alloc]initWithNibName:@"ShowAllDairyItemViewController" bundle:nil];
    [self._viewForShowAllDairyItem.view removeFromSuperview];

    self._viewForShowAllDairyItem=viewpopup;
    [viewpopup release];
    [self._viewForShowAllDairyItem getAllDairyItemMethod:[formeter dateFromString:assignDate] selectedButton:@"Glance"];
    
    
    
    self._popoverControler = nil;
    // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
    self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:self._viewForShowAllDairyItem];
    self._popoverControler.delegate = self;
    
    // Need to set PreferredContentSize for iOS 7 and above and
    //             ContentSizeForViewInPopover for below iOS 7
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(self._viewForShowAllDairyItem.view.frame.size.width, self._viewForShowAllDairyItem.view.frame.size.height)];
    }
    else
    {
        [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._viewForShowAllDairyItem.view.frame.size.width, self._viewForShowAllDairyItem.view.frame.size.height)];
    }
    
    [self._popoverControler setPopoverContentSize:CGSizeMake(self._viewForShowAllDairyItem.view.frame.size.width, self._viewForShowAllDairyItem.view.frame.size.height) animated:NO];
    
    [self._popoverControler presentPopoverFromRect:CGRectMake(25 ,0,10,10 )inView:btn.superview
                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
    [formeter release];
    
}



-(void)showAllDiaryItemButtonPressedForMonth:(id)sender
{
#if DEBUG
    NSLog(@"showAllDiaryItemButtonPressedForMonth");
#endif
    
    UIButton *btn=(UIButton*)sender;
    btn_ToHoldClickMonthDate = btn;

    NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
    [formeter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *assignDate=[formeter stringFromDate:[self.datesIndex objectAtIndex:[sender tag]]];
    
    ShowAllDairyItemViewController *viewpopup=[[ShowAllDairyItemViewController alloc]initWithNibName:@"ShowAllDairyItemViewController" bundle:nil];
    [self._viewForShowAllDairyItem.view removeFromSuperview];

    self._viewForShowAllDairyItem=viewpopup;
    
    [self._viewForShowAllDairyItem getAllDairyItemMethod:[formeter dateFromString:assignDate] selectedButton:@"Month"];
    
    
#if DEBUG
    NSLog(@"Before : self._popoverControler.contentViewController : %@",self._popoverControler.contentViewController);
#endif
    

    self._popoverControler = nil;
    // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
    self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:self._viewForShowAllDairyItem];
    self._popoverControler.delegate = self;

    [viewpopup release];

#if DEBUG
    NSLog(@"After : self._popoverControler.contentViewController : %@",self._popoverControler.contentViewController);
#endif
    
    // Need to set PreferredContentSize for iOS 7 and above and
    //             ContentSizeForViewInPopover for below iOS 7
    
#if DEBUG
    NSLog(@"showAllDiaryItemButtonPressedForMonth 11");
#endif

        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
#if DEBUG
            NSLog(@"showAllDiaryItemButtonPressedForMonth 12");
#endif

            [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(self._viewForShowAllDairyItem.view.frame.size.width, self._viewForShowAllDairyItem.view.frame.size.height)];
#if DEBUG
            NSLog(@"showAllDiaryItemButtonPressedForMonth 13");
#endif
        }
        else
        {
            [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._viewForShowAllDairyItem.view.frame.size.width, self._viewForShowAllDairyItem.view.frame.size.height)];
        }
    
#if DEBUG
    NSLog(@"showAllDiaryItemButtonPressedForMonth 14");
#endif

    [self._popoverControler setPopoverContentSize:CGSizeMake(self._viewForShowAllDairyItem.view.frame.size.width, self._viewForShowAllDairyItem.view.frame.size.height) animated:NO];
#if DEBUG
    NSLog(@"showAllDiaryItemButtonPressedForMonth 15");
    NSLog(@"btn %@",btn.superview.superview);
    
    NSLog(@"btn %@",btn.superview.superview.superview.superview);
#endif

    [self._popoverControler presentPopoverFromRect:CGRectMake(25 ,0,10,10 ) inView:btn.superview
                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
    
    
#if DEBUG
    NSLog(@"showAllDiaryItemButtonPressedForMonth 16");
#endif
    
    [formeter release];
    
}

# pragma mark - UIPopoverController Delegate Methods

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [self.view setUserInteractionEnabled:YES];

    if(self._popoverControler)
        self._popoverControler.contentViewController.view = nil;
}



- (void)dismissPopoverAnimated:(BOOL)animated
{

}

- (void)dismisPopover
{
#if DEBUG
    NSLog(@"Calender dismisPopover");
#endif
    
    [self.view setUserInteractionEnabled:YES];

    if([self._popoverControler isPopoverVisible])
    {
        
        [self._popoverControler dismissPopoverAnimated:NO];
        self._popoverControler.contentViewController.view =nil;
        
    
        if(_buttonForDay.selected ||_buttonForWeek.selected)
            [_table reloadData];
        
        return;
    }
}





-(void)showPopupButtonPressedForWeekAndMonth:(id)sender
{
#if DEBUG
    NSLog(@"showPopupButtonPressedForWeekAndMonth");
#endif
    
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
#if DEBUG
    NSLog(@"profobj %@",profobj.type);
#endif
    
    UIButton *btn=(UIButton*)sender;
    
    NSString *cehckstrForId=[[btn.titleLabel.text componentsSeparatedByString:@"    " ]objectAtIndex:0];
    NSString *dateStr=[[btn.titleLabel.text componentsSeparatedByString:@"    " ]objectAtIndex:1];
    
    
    Item *diarItem=nil;
    if([cehckstrForId isEqualToString:@"1"]){
        diarItem =[[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:btn.tag] postNotification:NO];
        [AppDelegate getAppdelegate]._isUniqueId =YES;
    }
    else{
        diarItem =[[Service sharedInstance]getItemDataInfoStoredForParseDate:[NSNumber numberWithInteger:btn.tag] appid:nil];
        [AppDelegate getAppdelegate]._isUniqueId =NO;
    }
    
    NSString *isTypeTemplateStr=  diarItem.type;
    selectedDiaryItem = diarItem;
    
    if([isTypeTemplateStr isEqualToString:@"OutOfClass"] || [isTypeTemplateStr isEqualToString:@"Out Of Class"])
    {
        NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
        [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
        
        NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
        [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        
        NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
        // 24Hrs
        [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
        [formaterToDisplay setDateFormat:[formaterToDisplay dateFormat]];
        //[formaterToDisplay setDateFormat:kDateFormatToShowOnUI_hhmma];
        
        // 24Hrs
        
        NSString *fromTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:diarItem.assignedTime]];
        
        NSString *toTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:diarItem.dueTime]];
        
        viewForApprovedPass.hidden = NO;
        lblPassType.text = diarItem.title;
        
        lblFromTime.text = fromTime;
        lblToTime.text = toTime;
        lblPassDate.text = [formterDate stringFromDate:diarItem.assignedDate];
        
        [formterDate release];
        [formaterFromDB release];
        [formaterToDisplay release];
        
        
        if([diarItem.isApproved integerValue] == 0)
        {
            [self edtiButtunPessed:btn];
        }
        else  if([diarItem.isApproved integerValue] == 1)
        {
            MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
            // Teacher
            if ([profobj.type isEqualToString:@"Teacher"])
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:diarItem];
                
                lblPassApprovedByTitle.text = @"Issued For";
                [btn_DeletePass setHidden:NO];
                
            }
            // Student
            else
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[diarItem.itemId integerValue]]];
                lblPassApprovedByTitle.text = @"Issued By";
                [btn_DeletePass setHidden:YES];
                
                
            }
            
            [self showApprovedViewDetails];
            
            [viewForApprovedPassInner setBackgroundColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:16/255.0f alpha:1]];
            
            UIViewController* popoverContent = [[UIViewController alloc]init];
            popoverContent.view = viewForApprovedPass;
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;
            
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [popoverContent release]; popoverContent = nil;
        }
        else  if([diarItem.isApproved integerValue] == 2)
        {
#if DEBUG
            NSLog(@"showPopupButtonPressedForWeekAndMonth 1");
#endif
            

            [self showElapsedViewDetails];
            [viewForApprovedPassInner setBackgroundColor:[UIColor grayColor]];

            UIViewController* popoverContent = [[UIViewController alloc]init];
#if DEBUG
            NSLog(@"viewForApprovedPass = %p",[viewForApprovedPass description]);
#endif
            popoverContent.view = viewForApprovedPass;
            
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;
            
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [popoverContent release]; popoverContent = nil;
        }
    }
    else
    {
        
        if ([profobj.type isEqualToString:@"Teacher"])
        {
            UIView *popUpView=[[UIView alloc]init];
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popUpView setFrame:CGRectMake(0, 0, 240, 170)];
                popUpView.backgroundColor=[UIColor blackColor];
                
            }
            else
            {
                [popUpView setFrame:CGRectMake(0, 0, 230, 160)];
                popUpView.backgroundColor=[UIColor clearColor];
                
            }
            
            
            UIImageView *popUpBgImgView=[[UIImageView alloc]init];
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popUpBgImgView setFrame:CGRectMake(5, 5, 230, 160)];
            }
            else
            {
                [popUpBgImgView setFrame:CGRectMake(0, 0, 230, 150)];
            }
            
           // popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
            [popUpBgImgView setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
            [popUpBgImgView.layer setCornerRadius:5.0];
            [popUpView addSubview:popUpBgImgView];
            [popUpBgImgView release];
            
            UIImageView * iconImgView=[[UIImageView alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [iconImgView setFrame:CGRectMake(7, 10, 20, 19)];
            }
            else
            {
                [iconImgView setFrame:CGRectMake(2, 5, 20, 19)];
            }
            
            iconImgView.backgroundColor=[UIColor clearColor];
            
            UIImageView * imgViewForLine=[[UIImageView alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [imgViewForLine setFrame:CGRectMake(40, 18, 150, 2)];
            }
            else
            {
                [imgViewForLine setFrame:CGRectMake(35, 13, 150, 2)];
            }
            
            imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
            imgViewForLine.backgroundColor=[UIColor grayColor];
            
            
            NSString *isTypeStr=  diarItem.type;
            
            NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
            [formeter2 setDateFormat:@"dd/MM/yyyy"];
            NSDate *currentDate=[formeter2 dateFromString:dateStr];
            
            [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
            long timeDiff1;
            timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:diarItem.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:currentDate]]];
            
            [formeter2 release];
            
            
            if(timeDiff1<=0){
                iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                {
                    if([diarItem.isApproved integerValue] == 0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 1)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 2)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                    }
                    
                }
                else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                {
                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                    if (lov)
                    {
                        
                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                        {
                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                            iconImgView.image=[FileUtils getImageFromPath:strPath];
                        }
                    }
                }
                
                
                
                
                
                
            }
            else{
                if ([isTypeStr isEqualToString:@"Assignment"]) {
                    
                    iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                }
                else if([isTypeStr isEqualToString:@"Homework"]) {
                    iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                }
                else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                    if([diarItem.isApproved integerValue] == 0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 1)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 2)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                    }
                    
                }
                else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                {
                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                    if (lov)
                    {
                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                        {
                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                            iconImgView.image=[FileUtils getImageFromPath:strPath];
                        }
                    }
                }
                else{
                }
                
            }
            
            
            //    if ([diarItem.isCompelete intValue]==1)
            if ([diarItem.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
            {
                imgViewForLine.hidden=NO;
                //dulLbl.text=@"" ;
            }
            else
            {
                imgViewForLine.hidden=YES;
                //dulLbl.text=@"Due:" ;
            }
            
            
            
            
            UIImageView *imgViewForBgHeader=[[UIImageView alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [imgViewForBgHeader setFrame:CGRectMake(6, 5, 227, 26)];
            }
            else
            {
                [imgViewForBgHeader setFrame:CGRectMake(1, 0, 227, 26)];
            }
            
            //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
            MyClass *class1=nil;
            
            class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diarItem.class_id]];
            if(class1.code!=nil)
                imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
            else{
                imgViewForBgHeader.backgroundColor=[UIColor grayColor];
            }
            
            //--------- By Mitul ---------//
            
            imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
            [imgViewForBgHeader.layer setMasksToBounds:YES];
            [imgViewForBgHeader.layer setCornerRadius:3.0f];
            [imgViewForBgHeader.layer setBorderWidth:1.0];
            [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
            
            [popUpView addSubview:imgViewForBgHeader];
            [imgViewForBgHeader release];
            
            UILabel *popUpHeaderLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popUpHeaderLbl setFrame:CGRectMake(35, 7,180, 25)];
            }
            else
            {
                [popUpHeaderLbl setFrame:CGRectMake(30, 2,180, 25)];
            }
            
            popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popUpHeaderLbl.numberOfLines=1;
            popUpHeaderLbl.backgroundColor=[UIColor clearColor];
            popUpHeaderLbl.textColor=[UIColor whiteColor];
            if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
            {
                popUpHeaderLbl.textColor=[UIColor blackColor];
                
            }
            [popUpView addSubview:popUpHeaderLbl];
            [popUpHeaderLbl release];
            
            
            [popUpView addSubview:iconImgView];
            [iconImgView release];
            
            
            [popUpView addSubview:imgViewForLine];
            [imgViewForLine release];
            
            
            UILabel *popSubNameLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popSubNameLbl setFrame:CGRectMake(25, 35,200, 15)];
            }
            else
            {
                [popSubNameLbl setFrame:CGRectMake(20, 30,200, 15)];
            }
            
            popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popSubNameLbl.numberOfLines=1;
            popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popSubNameLbl.backgroundColor=[UIColor clearColor];
            [popUpView addSubview:popSubNameLbl];
            [popSubNameLbl release];
            
            UILabel *popUDescLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popUDescLbl setFrame:CGRectMake(25, 53,200, 15)];
            }
            else
            {
                [popUDescLbl setFrame:CGRectMake(20, 48,200, 15)];
            }
            
            popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popUDescLbl.numberOfLines=1;
            popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popUDescLbl.backgroundColor=[UIColor clearColor];
            [popUpView addSubview:popUDescLbl];
            [popUDescLbl release];
            
            
            popUpHeaderLbl.text=diarItem.title ;
            if(diarItem.subject_name!=nil){
                popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
            }
            else{
                popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
            }
            //  popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
            popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",diarItem.itemDescription] ;
            if (diarItem.itemDescription!=nil && diarItem.itemDescription!=nil) {
                popUDescLbl.hidden=NO;
            }
            else{
                popUDescLbl.hidden=YES;
            }
            
            
            
            
            
            
            UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
            [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                editBtn.frame=CGRectMake(205,6,26, 26);
            }
            else
            {
                editBtn.frame=CGRectMake(200,1,26, 26);
            }
            
            editBtn.tag=btn.tag;
            [popUpView addSubview:editBtn];
            
            
            
            /***********************************************************/
            
            UILabel *popAssignedDateLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popAssignedDateLbl setFrame:CGRectMake(25, 71,200, 15)];
            }
            else
            {
                [popAssignedDateLbl setFrame:CGRectMake(20, 66,200, 15)];
            }
            
            popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popAssignedDateLbl.numberOfLines=1;
            popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popAssignedDateLbl.backgroundColor=[UIColor clearColor];
            
            NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
            [formeter setTimeStyle:NSDateFormatterNoStyle];
            [formeter setDateStyle:NSDateFormatterShortStyle];
            [formeter setDateFormat:kDATEFORMAT_Slash];
            
            NSString *strAssignedDate = [formeter stringFromDate:diarItem.assignedDate];
            
            popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
            
            
            
            
            [popUpView addSubview:popAssignedDateLbl];
            [popAssignedDateLbl release];
            
            
            
            
            if(!([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))
            {
                UILabel *popDueDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popDueDateLbl setFrame:CGRectMake(25, 89,200, 15)];
                }
                else
                {
                    [popDueDateLbl setFrame:CGRectMake(20, 84,200, 15)];
                }
                
                popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popDueDateLbl.numberOfLines=1;
                popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popDueDateLbl.backgroundColor=[UIColor clearColor];
                
                NSString *strDueDate = [formeter stringFromDate:diarItem.dueDate];
                
                popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
                
                
                [popUpView addSubview:popDueDateLbl];
                [popDueDateLbl release];
                
                UILabel *popProgressLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popProgressLbl setFrame:CGRectMake(25, 107,200, 15)];
                }
                else
                {
                    [popProgressLbl setFrame:CGRectMake(20, 102,200, 15)];
                }
                
                popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popProgressLbl.numberOfLines=1;
                popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popProgressLbl.backgroundColor=[UIColor clearColor];
                
                
                if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                }
                // For Prime Only
                else
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                    
                }
                
                [popUpView addSubview:popProgressLbl];
                [popProgressLbl release];
            }
            
            
            /***********************************************************/

            
            
            
            if(_buttonFormonth.selected)
            {
                
                
                // Only FOr Student
                if ([profobj.type isEqualToString:@"Student"])
                {
                    // Fetching Diary Item Users on the bases of DI id
                    NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:diarItem.itemType_id];
                    if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                    {
                        NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:diarItem.itemId];
                        
#if DEBUG
                        NSLog(@"Before arrDIU %@",arrDIU);

#endif
                        arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                        NSLog(@"After arrDIU %@",arrDIU);
#endif
                        
                        for (DairyItemUsers *assinUserobj in arrDIU)
                        {
                            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                            {
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                if (lov)
                                {
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                        
                                        UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(180, 6, 25, 25)];
                                        [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                                        [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                                        btnTaskBasedMerits.tag=[diarItem.itemId integerValue];
                                        
                                        [popUpView addSubview:btnTaskBasedMerits];
                                        [btnTaskBasedMerits release];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Only FOr Student
            
            
            
            
            
            
            
            UIViewController* popoverContent = [[UIViewController alloc]init];
            popoverContent.view = popUpView;
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;
            
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(240, 170)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(240, 170) animated:YES];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(230, 160)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(230, 160) animated:YES];
            }
            
            if(_buttonFormonth.selected){
                
                [self._popoverControler presentPopoverFromRect:CGRectMake(5 ,0,10,10 )inView:btn.superview
                                      permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            }
            else{
                [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,10,10 )inView:btn.superview
                                      permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            }
            [popoverContent release]; popoverContent = nil;
            [popUpView release];
  
            
        }
        
        else // if student login
        {
            
            NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:diarItem.itemId];
            arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
            
            if([arrDIU count]>0 && ![diarItem.type  isEqualToString:k_MERIT_CHECK] && ([[[arrDIU firstObject]listofvaluesTypeid] integerValue] != 0 && [[[[arrDIU firstObject] meritTitle] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )) // If given merit is  active in student login
            {
                UIView *popUpView=[[UIView alloc]init];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpView setFrame:CGRectMake(0, 0, 440, 180)];
                    popUpView.backgroundColor=[UIColor blackColor];
                    
                }
                else
                {
                    [popUpView setFrame:CGRectMake(0, 0, 430, 170)];
                    popUpView.backgroundColor=[UIColor clearColor];
                    
                }
                
                
                UIImageView *popUpBgImgView=[[UIImageView alloc]init];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpBgImgView setFrame:CGRectMake(5, 5, 430, 170)];
                }
                else
                {
                    [popUpBgImgView setFrame:CGRectMake(0, 0, 227, 160)];
                }
                
                // popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
                [popUpBgImgView setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
                [popUpBgImgView.layer setCornerRadius:5.0];
                [popUpView addSubview:popUpBgImgView];
                [popUpBgImgView release];
                
                UIImageView * iconImgView=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [iconImgView setFrame:CGRectMake(7, 10, 20, 19)];
                }
                else
                {
                    [iconImgView setFrame:CGRectMake(2, 5, 20, 19)];
                }
                
                iconImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView * imgViewForLine=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForLine setFrame:CGRectMake(40, 18, 150, 2)];
                }
                else
                {
                    [imgViewForLine setFrame:CGRectMake(35, 13, 150, 2)];
                }
                
                imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                imgViewForLine.backgroundColor=[UIColor grayColor];
                
                
                NSString *isTypeStr=  diarItem.type;
                
                NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                [formeter2 setDateFormat:@"dd/MM/yyyy"];
                NSDate *currentDate=[formeter2 dateFromString:dateStr];
                
                [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                long timeDiff1;
                timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:diarItem.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:currentDate]]];
                
                [formeter2 release];
                
                
                if(timeDiff1<=0){
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    
                    
                    
                    
                    
                    
                }
                else{
                    if ([isTypeStr isEqualToString:@"Assignment"]) {
                        
                        iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"Homework"]) {
                        iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    else{
                    }
                    
                }
                
                
                //    if ([diarItem.isCompelete intValue]==1)
                if ([diarItem.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                {
                    imgViewForLine.hidden=NO;
                    //dulLbl.text=@"" ;
                }
                else
                {
                    imgViewForLine.hidden=YES;
                    //dulLbl.text=@"Due:" ;
                }
                
                
                
                
                UIImageView *imgViewForBgHeader=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForBgHeader setFrame:CGRectMake(6, 5, 430, 26)];
                }
                else
                {
                    [imgViewForBgHeader setFrame:CGRectMake(1, 0, 430, 26)];
                }
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diarItem.class_id]];
                if(class1.code!=nil)
                    imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    imgViewForBgHeader.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                
                imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
                [imgViewForBgHeader.layer setMasksToBounds:YES];
                [imgViewForBgHeader.layer setCornerRadius:3.0f];
                [imgViewForBgHeader.layer setBorderWidth:1.0];
                [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                [popUpView addSubview:imgViewForBgHeader];
                [imgViewForBgHeader release];
                
                UILabel *popUpHeaderLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpHeaderLbl setFrame:CGRectMake(35, 7,180, 25)];
                }
                else
                {
                    [popUpHeaderLbl setFrame:CGRectMake(30, 2,180, 25)];
                }
                
                popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUpHeaderLbl.numberOfLines=1;
                popUpHeaderLbl.backgroundColor=[UIColor clearColor];
                popUpHeaderLbl.textColor=[UIColor whiteColor];
                if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
                {
                    popUpHeaderLbl.textColor=[UIColor blackColor];
                    
                }
                [popUpView addSubview:popUpHeaderLbl];
                [popUpHeaderLbl release];
                
                
                [popUpView addSubview:iconImgView];
                [iconImgView release];
                
                
                [popUpView addSubview:imgViewForLine];
                [imgViewForLine release];
                
                
                UILabel *popSubNameLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popSubNameLbl setFrame:CGRectMake(25, 35,200, 15)];
                }
                else
                {
                    [popSubNameLbl setFrame:CGRectMake(20, 30,200, 15)];
                }
                
                popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popSubNameLbl.numberOfLines=1;
                popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popSubNameLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popSubNameLbl];
                [popSubNameLbl release];
                
                UILabel *popUDescLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUDescLbl setFrame:CGRectMake(25, 53,200, 15)];
                }
                else
                {
                    [popUDescLbl setFrame:CGRectMake(20, 48,200, 15)];
                }
                
                popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUDescLbl.numberOfLines=1;
                popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popUDescLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popUDescLbl];
                [popUDescLbl release];
                
                
                
                
                popUpHeaderLbl.text=diarItem.title ;
                
                
                UILabel *popAssignedDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popAssignedDateLbl setFrame:CGRectMake(25, 71,200, 15)];
                }
                else
                {
                    [popAssignedDateLbl setFrame:CGRectMake(20, 66,200, 15)];
                }
                
                popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popAssignedDateLbl.numberOfLines=1;
                popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popAssignedDateLbl.backgroundColor=[UIColor clearColor];
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                [formeter setTimeStyle:NSDateFormatterNoStyle];
                [formeter setDateStyle:NSDateFormatterShortStyle];
                [formeter setDateFormat:kDATEFORMAT_Slash];
                
                NSString *strAssignedDate = [formeter stringFromDate:diarItem.assignedDate];
                
                popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
                
                
                
                
                [popUpView addSubview:popAssignedDateLbl];
                [popAssignedDateLbl release];
                
                
               
                if(!([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))

                {
                    
                    UILabel *popDueDateLbl=[[UILabel alloc]init];
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popDueDateLbl setFrame:CGRectMake(25, 89,200, 15)];
                    }
                    else
                    {
                        [popDueDateLbl setFrame:CGRectMake(20, 84,200, 15)];
                    }
                    
                    popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popDueDateLbl.numberOfLines=1;
                    popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popDueDateLbl.backgroundColor=[UIColor clearColor];
                    
                    NSString *strDueDate = [formeter stringFromDate:diarItem.dueDate];
                    
                    popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
                    
                    
                    [popUpView addSubview:popDueDateLbl];
                    [popDueDateLbl release];
                
                UILabel *popProgressLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popProgressLbl setFrame:CGRectMake(25, 107,200, 15)];
                }
                else
                {
                    [popProgressLbl setFrame:CGRectMake(20, 102,200, 15)];
                }
                
                popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popProgressLbl.numberOfLines=1;
                popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popProgressLbl.backgroundColor=[UIColor clearColor];
                
                
                if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                }
                // For Prime Only
                else
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                    
                }
                
                
                
                
                [popUpView addSubview:popProgressLbl];
                [popProgressLbl release];
                
                
            }
            
                
                
                
                
                if(diarItem.subject_name!=nil){
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                }
                else{
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
                }
                //  popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",diarItem.itemDescription] ;
                if (diarItem.itemDescription!=nil) {
                    popUDescLbl.hidden=NO;
                }
                else{
                    popUDescLbl.hidden=YES;
                }
                
                
                UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
                [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    editBtn.frame=CGRectMake(410,6,26, 26);
                }
                else
                {
                    editBtn.frame=CGRectMake(415,1,26, 26);
                }
                
                editBtn.tag=btn.tag;
                [popUpView addSubview:editBtn];
                
                UILabel *popMeritTitleLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popMeritTitleLbl setFrame:CGRectMake(330, 50,90, 30)];
                }
                else
                {
                    [popMeritTitleLbl setFrame:CGRectMake(330, 50,90, 30)];
                }
                
                popMeritTitleLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popMeritTitleLbl.numberOfLines=0;
                popMeritTitleLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popMeritTitleLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popMeritTitleLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popMeritTitleLbl];
                [popMeritTitleLbl release];
                
                
                
                UILabel *popMeritAssignedBy=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popMeritAssignedBy setFrame:CGRectMake(330, 125,90, 30)];
                }
                else
                {
                    [popMeritAssignedBy setFrame:CGRectMake(330, 125,90, 30)];
                }
                
                popMeritAssignedBy.font=[UIFont fontWithName:@"Arial" size:13];
                popMeritAssignedBy.numberOfLines=0;
                popMeritAssignedBy.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popMeritAssignedBy.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popMeritAssignedBy.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popMeritAssignedBy];
                [popMeritAssignedBy release];
                
                
                
                
                UILabel *popMeritLblForSeprationline=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popMeritLblForSeprationline setFrame:CGRectMake(230, 31,1, 140)];
                }
                else
                {
                    [popMeritLblForSeprationline setFrame:CGRectMake(230, 31,1, 140)];
                }
                
                popMeritLblForSeprationline.backgroundColor=[UIColor lightGrayColor];
                [popUpView addSubview:popMeritLblForSeprationline];
                [popMeritLblForSeprationline release];
                
                
                
                
                
                
                // Only FOr Student
                if ([profobj.type isEqualToString:@"Student"])
                {
                    // Fetching Diary Item Users on the bases of DI id
                    NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:diarItem.itemType_id];
                    if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                    {
                        NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:diarItem.itemId];
                        
#if DEBUG
                        NSLog(@"Before arrDIU %@",arrDIU);

#endif
                        arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                        NSLog(@"After arrDIU %@",arrDIU);
#endif
                        
                        for (DairyItemUsers *assinUserobj in arrDIU)
                        {
                            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                            {
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                if (lov)
                                {
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.icon];
                                        
                                        UIImageView *imgViewTaskBasedMerits = [[UIImageView alloc]initWithFrame:CGRectMake(235, 50, 85, 85)];
                                        [imgViewTaskBasedMerits setImage:[FileUtils getImageFromPath:strPath] ];
                                        imgViewTaskBasedMerits.tag=[diarItem.itemId integerValue];
                                        
                                        [popUpView addSubview:imgViewTaskBasedMerits];
                                        [imgViewTaskBasedMerits release];
                                        
                                        
                                        UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(317, 86, 90, 13)];
                                        btnTaskBasedMerits.titleLabel.font = [UIFont fontWithName:@"Arial" size:13];
                                        [btnTaskBasedMerits.titleLabel setTextAlignment:NSTextAlignmentLeft];
                                        
                                        [btnTaskBasedMerits setTitle:@"View Merit" forState:UIControlStateNormal];
                                        [btnTaskBasedMerits setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                                        [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                                        btnTaskBasedMerits.tag=[diarItem.itemId integerValue];
                                        
                                        [popUpView addSubview:btnTaskBasedMerits];
                                        [btnTaskBasedMerits release];
                                        popMeritTitleLbl.text = assinUserobj.meritTitle;
                                        popMeritAssignedBy.text = assinUserobj.approverName;
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                
                // Only FOr Student
                
                
                
                UIViewController* popoverContent = [[UIViewController alloc]init];
                popoverContent.view = popUpView;
                
                self._popoverControler = nil;
                // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
                self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
                self._popoverControler.delegate = self;
                
                // Need to set PreferredContentSize for iOS 7 and above and
                //             ContentSizeForViewInPopover for below iOS 7
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(440, 180)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(440, 180) animated:YES];
                }
                else
                {
                    [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(430, 170)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(430, 170) animated:YES];
                }
                
                if(_buttonFormonth.selected){
                    
                    [self._popoverControler presentPopoverFromRect:CGRectMake(5 ,0,10,10 )inView:btn.superview
                                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                else{
                    [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,10,10 )inView:btn.superview
                                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                [popoverContent release]; popoverContent = nil;
                [popUpView release];

            }
            else // If given merit is Not active in student login
            {
                UIView *popUpView=[[UIView alloc]init];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpView setFrame:CGRectMake(0, 0, 240, 150)];
                    popUpView.backgroundColor=[UIColor blackColor];
                    
                }
                else
                {
                    [popUpView setFrame:CGRectMake(0, 0, 230, 140)];
                    popUpView.backgroundColor=[UIColor clearColor];
                    
                }
                
                
                UIImageView *popUpBgImgView=[[UIImageView alloc]init];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpBgImgView setFrame:CGRectMake(5, 5, 230, 140)];
                }
                else
                {
                    [popUpBgImgView setFrame:CGRectMake(0, 0, 230, 130)];
                }
                
                //popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
                [popUpBgImgView setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
                [popUpBgImgView.layer setCornerRadius:5.0];
                [popUpView addSubview:popUpBgImgView];
                [popUpBgImgView release];
                
                UIImageView * iconImgView=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [iconImgView setFrame:CGRectMake(7, 10, 20, 19)];
                }
                else
                {
                    [iconImgView setFrame:CGRectMake(2, 5, 20, 19)];
                }
                
                iconImgView.backgroundColor=[UIColor clearColor];
                
                UIImageView * imgViewForLine=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForLine setFrame:CGRectMake(40, 18, 150, 2)];
                }
                else
                {
                    [imgViewForLine setFrame:CGRectMake(35, 13, 150, 2)];
                }
                
                imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                imgViewForLine.backgroundColor=[UIColor grayColor];
                
                
                NSString *isTypeStr=  diarItem.type;
                
                NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                [formeter2 setDateFormat:@"dd/MM/yyyy"];
                NSDate *currentDate=[formeter2 dateFromString:dateStr];
                
                [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                long timeDiff1;
                timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:diarItem.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:currentDate]]];
                
                [formeter2 release];
                
                
                if(timeDiff1<=0){
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    
                    
                    
                    
                    
                    
                }
                else{
                    if ([isTypeStr isEqualToString:@"Assignment"]) {
                        
                        iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"Homework"]) {
                        iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    else{
                    }
                    
                }
                
                
                //    if ([diarItem.isCompelete intValue]==1)
                if ([diarItem.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                {
                    imgViewForLine.hidden=NO;
                    //dulLbl.text=@"" ;
                }
                else
                {
                    imgViewForLine.hidden=YES;
                    //dulLbl.text=@"Due:" ;
                }
                
                
                
                
                UIImageView *imgViewForBgHeader=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForBgHeader setFrame:CGRectMake(6, 5, 227, 26)];
                }
                else
                {
                    [imgViewForBgHeader setFrame:CGRectMake(1, 0, 227, 26)];
                }
                
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diarItem.class_id]];
                if(class1.code!=nil)
                    imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    imgViewForBgHeader.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                
                imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
                [imgViewForBgHeader.layer setMasksToBounds:YES];
                [imgViewForBgHeader.layer setCornerRadius:3.0f];
                [imgViewForBgHeader.layer setBorderWidth:1.0];
                [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                [popUpView addSubview:imgViewForBgHeader];
                [imgViewForBgHeader release];
                
                UILabel *popUpHeaderLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpHeaderLbl setFrame:CGRectMake(35, 7,180, 25)];
                }
                else
                {
                    [popUpHeaderLbl setFrame:CGRectMake(30, 2,180, 25)];
                }
                
                popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUpHeaderLbl.numberOfLines=1;
                popUpHeaderLbl.backgroundColor=[UIColor clearColor];
                popUpHeaderLbl.textColor=[UIColor whiteColor];
                if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
                {
                    popUpHeaderLbl.textColor=[UIColor blackColor];
                    
                }
                [popUpView addSubview:popUpHeaderLbl];
                [popUpHeaderLbl release];
                
                
                [popUpView addSubview:iconImgView];
                [iconImgView release];
                
                
                [popUpView addSubview:imgViewForLine];
                [imgViewForLine release];
                
                
                UILabel *popSubNameLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popSubNameLbl setFrame:CGRectMake(25, 35,200, 15)];
                }
                else
                {
                    [popSubNameLbl setFrame:CGRectMake(20, 30,200, 15)];
                }
                
                popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popSubNameLbl.numberOfLines=1;
                popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popSubNameLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popSubNameLbl];
                [popSubNameLbl release];
                
                UILabel *popUDescLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUDescLbl setFrame:CGRectMake(25, 53,200, 15)];
                }
                else
                {
                    [popUDescLbl setFrame:CGRectMake(20, 48,200, 15)];
                }
                
                popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUDescLbl.numberOfLines=1;
                popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popUDescLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popUDescLbl];
                [popUDescLbl release];
                
                
                popUpHeaderLbl.text=diarItem.title ;
                if(diarItem.subject_name!=nil){
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                }
                else{
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
                }
                //  popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",diarItem.itemDescription] ;
                if (diarItem.itemDescription!=nil) {
                    popUDescLbl.hidden=NO;
                }
                else{
                    popUDescLbl.hidden=YES;
                }
                
                
                UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
                [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    editBtn.frame=CGRectMake(205,6,26, 26);
                }
                else
                {
                    editBtn.frame=CGRectMake(200,1,26, 26);
                }
                
                editBtn.tag=btn.tag;
                [popUpView addSubview:editBtn];
                
                
                if(_buttonFormonth.selected)
                {
                    
                    
                    // Only FOr Student
                    if ([profobj.type isEqualToString:@"Student"])
                    {
                        // Fetching Diary Item Users on the bases of DI id
                        NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:diarItem.itemType_id];
                        if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                        {
                            NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:diarItem.itemId];
                            
#if DEBUG
                            NSLog(@"Before arrDIU %@",arrDIU);
#endif
                            arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                            NSLog(@"After arrDIU %@",arrDIU);
#endif
                            
                            for (DairyItemUsers *assinUserobj in arrDIU)
                            {
                                if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                                {
                                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                    if (lov)
                                    {
                                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.icon])
                                        {
                                            
                                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                            
                                            UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(180, 6, 25, 25)];
                                            [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                                            [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                                            btnTaskBasedMerits.tag=[diarItem.itemId integerValue];
                                            
                                            [popUpView addSubview:btnTaskBasedMerits];
                                            [btnTaskBasedMerits release];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // Only FOr Student
                
                /***********************************************************/
                
                UILabel *popAssignedDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popAssignedDateLbl setFrame:CGRectMake(25, 74,200, 15)];
                }
                else
                {
                    [popAssignedDateLbl setFrame:CGRectMake(20, 69,200, 15)];
                }
                
                popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popAssignedDateLbl.numberOfLines=1;
                popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popAssignedDateLbl.backgroundColor=[UIColor clearColor];
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                [formeter setTimeStyle:NSDateFormatterNoStyle];
                [formeter setDateStyle:NSDateFormatterShortStyle];
                [formeter setDateFormat:kDATEFORMAT_Slash];
                
                NSString *strAssignedDate = [formeter stringFromDate:diarItem.assignedDate];
                
                popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
                
                
                
                
                [popUpView addSubview:popAssignedDateLbl];
                [popAssignedDateLbl release];
                
                MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                
                
                
                if ([profobj.type isEqualToString:@"Student"] && ([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))
                {
                    DairyItemUsers *itemuser = [[Service sharedInstance]getAssigneStudentDataInfoStored:profobj.profile_id itemId:[diarItem.itemId stringValue]  postNotification:NO] ;
                    
                    UILabel *popAssignedby=[[UILabel alloc]init];
                    
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popAssignedby setFrame:CGRectMake(25, 92,200, 15)];
                    }
                    else
                    {
                        [popAssignedby setFrame:CGRectMake(20, 87,200, 15)];
                    }
                    
                    popAssignedby.font=[UIFont fontWithName:@"Arial" size:13];
                    popAssignedby.numberOfLines=1;
                    popAssignedby.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popAssignedby.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popAssignedby.backgroundColor=[UIColor clearColor];
                    popAssignedby.text= [NSString stringWithFormat:@"Assigned By: %@",itemuser.approverName];
                    
                    [popUpView addSubview:popAssignedby];
                    [popAssignedby release];
                    
                    
                    
                }

                
                
                if(!([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))
                {
                    UILabel *popDueDateLbl=[[UILabel alloc]init];
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popDueDateLbl setFrame:CGRectMake(25, 92,200, 15)];
                    }
                    else
                    {
                        [popDueDateLbl setFrame:CGRectMake(20, 87,200, 15)];
                    }
                    
                    popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popDueDateLbl.numberOfLines=1;
                    popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popDueDateLbl.backgroundColor=[UIColor clearColor];
                    
                    NSString *strDueDate = [formeter stringFromDate:diarItem.dueDate];
                    
                    popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
                    
                    
                    [popUpView addSubview:popDueDateLbl];
                    [popDueDateLbl release];
                    
                    UILabel *popProgressLbl=[[UILabel alloc]init];
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popProgressLbl setFrame:CGRectMake(25, 110,200, 15)];
                    }
                    else
                    {
                        [popProgressLbl setFrame:CGRectMake(20, 105,200, 15)];
                    }
                    
                    popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popProgressLbl.numberOfLines=1;
                    popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popProgressLbl.backgroundColor=[UIColor clearColor];
                    
                    
                    if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                    {
                        popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                    }
                    // For Prime Only
                    else
                    {
                        popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                        
                    }
                    
                    
                    
                    
                    [popUpView addSubview:popProgressLbl];
                    [popProgressLbl release];
                }
                
                
                
                /***********************************************************/

                
                
                
                UIViewController* popoverContent = [[UIViewController alloc]init];
                popoverContent.view = popUpView;
                
                self._popoverControler = nil;
                // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
                self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
                self._popoverControler.delegate = self;
                
                // Need to set PreferredContentSize for iOS 7 and above and
                //             ContentSizeForViewInPopover for below iOS 7
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(240, 150)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(240, 150) animated:YES];
                }
                else
                {
                    [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(230, 140)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(230, 140) animated:YES];
                }
                
                if(_buttonFormonth.selected){
                    
                    [self._popoverControler presentPopoverFromRect:CGRectMake(5 ,0,10,10 )inView:btn.superview
                                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                else{
                    [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,10,10 )inView:btn.superview
                                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
                [popoverContent release];
                popoverContent = nil;
                [popUpView release];
                
                
            }
            
        
        
        }
        
            }
}



-(void)showPopupButtonPressed:(id)sender
{
#if DEBUG
    NSLog(@"showPopupButtonPressed");
#endif


    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    UIButton *btn=(UIButton*)sender;
#if DEBUG
    NSLog(@"btn.titleLabel.text===%@",btn.titleLabel.text);
#endif
    
    Item *diarItem=nil;
    if([ btn.titleLabel.text isEqualToString:@"1"]){
        diarItem =[[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:btn.tag] postNotification:NO];
        [AppDelegate getAppdelegate]._isUniqueId =YES;
    }
    else{
        diarItem =[[Service sharedInstance]getItemDataInfoStoredForParseDate:[NSNumber numberWithInteger:btn.tag] appid:nil];
        [AppDelegate getAppdelegate]._isUniqueId =NO;
    }
#if DEBUG
    NSLog(@"diarItem %@",diarItem);
#endif
    
    
    NSString *isTypeTemplateStr=  diarItem.type;
    
    if([isTypeTemplateStr isEqualToString:@"OutOfClass"] || [isTypeTemplateStr isEqualToString:@"Out Of Class"]) {
        
        if (countDownTimer)
        {
            [countDownTimer invalidate];
            countDownTimer =nil;
        }
        viewForApprovedPass.hidden = NO;
        lblPassType.text = diarItem.title;
        
        NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
        [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];
        
        NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
        [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        
       
        
        NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
        [formaterToDisplay setDateFormat:NSDateFormatterNoStyle];
        [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
        
        NSString *fromTime = [formaterToDisplay stringFromDate:diarItem.assignedDate];
        
        NSString *toTime = [formaterToDisplay stringFromDate:diarItem.dueDate];
        
        
        lblFromTime.text = fromTime;
        lblToTime.text = toTime;
        lblPassDate.text = [formterDate stringFromDate:diarItem.assignedDate];
        
        [formterDate release];
        [formaterFromDB release];
        [formaterToDisplay release];
        
        selectedDiaryItem = diarItem;
        
#if DEBUG
        NSLog(@"[diarItem.isApproved integerValue] %d %@",[diarItem.isApproved integerValue],diarItem.subject_name);
#endif
        
        if([diarItem.isApproved integerValue] == 0)
        {
            [self edtiButtunPessed:btn];
        }
        else  if([diarItem.isApproved integerValue] == 1)
        {
            if ([profobj.type isEqualToString:@"Teacher"])
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:diarItem];
                
                lblPassApprovedByTitle.text = @"Issued For";
                [btn_DeletePass setHidden:NO];
                
            }
            // Student
            else
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[diarItem.itemId integerValue]]];
                lblPassApprovedByTitle.text = @"Issued By";
                [btn_DeletePass setHidden:YES];
                
            }
            
            [self showApprovedViewDetails];
            
            [viewForApprovedPassInner setBackgroundColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:16/255.0f alpha:1]];
            
            UIViewController* popoverContent = [[UIViewController alloc]init];
            popoverContent.view =viewForApprovedPass;
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;
            
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [popoverContent release]; popoverContent = nil;
            
        }
        else  if([diarItem.isApproved integerValue] == 2)
        {
            // Teacher
            if ([profobj.type isEqualToString:@"Teacher"])
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:diarItem];
                lblPassApprovedByTitle.text = @"Issued For";
                [btn_DeletePass setHidden:NO];
            }
            // Student
            else
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[diarItem.itemId integerValue]]];
                lblPassApprovedByTitle.text = @"Issued By";
                [btn_DeletePass setHidden:YES];
            }
            [self showElapsedViewDetails];
            
            [viewForApprovedPassInner setBackgroundColor:[UIColor grayColor]];
            
            UIViewController* popoverContent = [[UIViewController alloc]init];

// Code commeted to Fix 2774 - This code was added to fix sync crash issue - 2568 on 9th April 2015 but this was not the actual fix
//        if(viewForApprovedPass)
//        {
//            [viewForApprovedPass removeFromSuperview];
//            viewForApprovedPass=nil;
//            viewForApprovedPass = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 400)];
//            [self.view addSubview:viewForApprovedPass];
//
//        }
#if DEBUG
            NSLog(@"viewForApprovedPass %@",viewForApprovedPass);

#endif
            popoverContent.view =viewForApprovedPass;

#if DEBUG
            NSLog(@"popoverContent %@",popoverContent);

#endif
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;
            
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                [self._popoverControler setPopoverContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

            [popoverContent release];
            popoverContent = nil;
        }
        
    }
    else
    {
        
        
        if ([profobj.type isEqualToString:@"Teacher"])
        {
            //UITableViewCell*cell=(UITableViewCell*) btn.superview.superview.superview;
            //int row=[_table indexPathForCell:cell].row;
            UIView *popUpView=nil;
            UIImageView *popUpBgImgView = nil;
            if([diarItem.weblink length]>0)
            {
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popUpView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, 240, 180)];
                }
                else
                {
                    popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 230, 170)];
                }
                
            }
            else
            {
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popUpView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, 240, 162)];
                }
                else
                {
                    popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 230, 152)];
                }
                
                
            }
            
            popUpView.backgroundColor=[UIColor clearColor];
            
            if([diarItem.weblink length]>0)
            {
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 5, 227,170)];
                }
                else
                {
                    popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -8, 227,179)];
                }
            }
            else
            {
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 5, 227,152)];
                }
                else
                {
                    popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 227,152)];
                }
            }
            // popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
            [popUpBgImgView setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
            [popUpBgImgView.layer setCornerRadius:5.0];
            [popUpView addSubview:popUpBgImgView];
            [popUpBgImgView release];
            
            UIImageView * iconImgView=[[UIImageView alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [iconImgView setFrame:CGRectMake(7, 10, 20, 19)];
                popUpView.backgroundColor=[UIColor blackColor];
                
            }
            else
            {
                [iconImgView setFrame:CGRectMake(2, 5, 20, 19)];
                popUpView.backgroundColor=[UIColor clearColor];
                
            }
            
            iconImgView.backgroundColor=[UIColor clearColor];
            
            //20AugChange //line for complete
            UIImageView * imgViewForLine=[[UIImageView alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [imgViewForLine setFrame:CGRectMake(40, 18, 150, 2)];
            }
            else
            {
                [imgViewForLine setFrame:CGRectMake(35, 13, 150, 2)];
            }
            
            imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
            imgViewForLine.backgroundColor=[UIColor grayColor];
            
            NSString *isTypeStr=  diarItem.type;
            
            NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
            [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
            long timeDiff1;
            timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:diarItem.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[NSDate date]]]];
            
            [formeter2 release];
            
            UIImageView *imgViewForBgHeader=[[UIImageView alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [imgViewForBgHeader setFrame:CGRectMake(6, 5, 227, 26)];
            }
            else
            {
                [imgViewForBgHeader setFrame:CGRectMake(0, 0, 227, 26)];
            }
            
#if DEBUG
            NSLog(@"diarItem Subect Name %@",diarItem.subject_name);
#endif
            //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
            MyClass *class1=nil;
            
            class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diarItem.class_id]];
            if(class1.code!=nil)
                imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
            else{
                imgViewForBgHeader.backgroundColor=[UIColor grayColor];
            }
            
            //--------- By Mitul ---------//
            imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
            [imgViewForBgHeader.layer setMasksToBounds:YES];
            [imgViewForBgHeader.layer setCornerRadius:3.0f];
            [imgViewForBgHeader.layer setBorderWidth:1.0];
            [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
            
            [popUpView addSubview:imgViewForBgHeader];
            [imgViewForBgHeader release];
            
            UILabel *popUpHeaderLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popUpHeaderLbl setFrame:CGRectMake(35, 7,180, 25)];
            }
            else
            {
                [popUpHeaderLbl setFrame:CGRectMake(30, 2,180, 25)];
            }
            
            popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popUpHeaderLbl.numberOfLines=1;
            popUpHeaderLbl.backgroundColor=[UIColor clearColor];
            popUpHeaderLbl.textColor=[UIColor whiteColor];

            // if class color is white we need to set class name color as black
            
            if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
            {
                popUpHeaderLbl.textColor=[UIColor blackColor];

            }
            
            
            [popUpView addSubview:popUpHeaderLbl];
            [popUpHeaderLbl release];
            
            
            [popUpView addSubview:iconImgView];
            [iconImgView release];
            
            [popUpView addSubview:imgViewForLine];
            [imgViewForLine release];
            
            
            
            
            UILabel *popSubNameLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popSubNameLbl setFrame:CGRectMake(25, 35,200, 15)];
            }
            else
            {
                [popSubNameLbl setFrame:CGRectMake(20, 30,200, 15)];
            }
            
            popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popSubNameLbl.numberOfLines=1;
            popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popSubNameLbl.backgroundColor=[UIColor clearColor];
            [popUpView addSubview:popSubNameLbl];
            [popSubNameLbl release];
            
            UILabel *popUDescLbl = nil;
            UILabel *popwebLinklbl = nil;
            UIButton *popwebLinkURllbl = nil;
            
            popUpHeaderLbl.text=diarItem.title ;
            if(diarItem.subject_name!=nil){
                popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
            }
            else{
                popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
            }
            popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
            
            
            
            
            if (diarItem.itemDescription!=nil) {
                popUDescLbl.hidden=NO;
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 56,200, 15)];
                }
                else
                {
                    popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 51,200, 15)];
                }
                
                popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",diarItem.itemDescription] ;
                
                popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUDescLbl.numberOfLines=1;
                popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popUDescLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popUDescLbl];
                [popUDescLbl release];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(25, 87,60, 15)];
                    popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(83, 87,130, 15)];
                }
                else
                {
                    popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(20, 82,60, 15)];
                    popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(78, 82,130, 15)];
                }
            }
            else{
                popUDescLbl.hidden=YES;
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(25, 53,60, 15)];
                    
                    popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(85,53,130, 15)];
                }
                else
                {
                    popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(20, 48,60, 15)];
                    
                    popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(80,48,130, 15)];
                }
            }
            
            if (diarItem.weblink!=nil && [diarItem.weblink length]>0) {
                popwebLinklbl.hidden = NO;
                popwebLinkURllbl.hidden = NO;
            }
            else{
                popwebLinklbl.hidden = YES;
                popwebLinkURllbl.hidden = YES;
            }
            
            popwebLinklbl.font=[UIFont fontWithName:@"Arial" size:13];
            popwebLinklbl.numberOfLines=1;
            popwebLinklbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popwebLinklbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popwebLinklbl.backgroundColor=[UIColor clearColor];
            popwebLinklbl.text=@"WebLink:";
            
            [popUpView addSubview:popwebLinklbl];
            
            [popwebLinklbl release];
            
            [popwebLinkURllbl.titleLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
            [popwebLinkURllbl setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [popwebLinkURllbl setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
            [popwebLinkURllbl setTitle:diarItem.weblink forState:UIControlStateNormal];
            [popwebLinkURllbl addTarget:self action:@selector(popupWebLinkclickToOpen:) forControlEvents:UIControlEventTouchUpInside];
            [popUpView addSubview:popwebLinkURllbl];
            [popwebLinkURllbl release];
            
            UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
            [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                editBtn.frame=CGRectMake(205,6,26, 26);
            }
            else
            {
                editBtn.frame=CGRectMake(200,1,26, 26);
            }
            
            editBtn.tag=btn.tag;
            [popUpView addSubview:editBtn];
            
            /***********************************************************/
            
            UILabel *popAssignedDateLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popAssignedDateLbl setFrame:CGRectMake(25, 74,200, 15)];
            }
            else
            {
                [popAssignedDateLbl setFrame:CGRectMake(20, 69,200, 15)];
            }
            
            popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popAssignedDateLbl.numberOfLines=1;
            popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popAssignedDateLbl.backgroundColor=[UIColor clearColor];
            
            NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
            [formeter setTimeStyle:NSDateFormatterNoStyle];
            [formeter setDateStyle:NSDateFormatterShortStyle];
            [formeter setDateFormat:kDATEFORMAT_Slash];
            
            NSString *strAssignedDate = [formeter stringFromDate:diarItem.assignedDate];
            
            popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
            
            
            
            
            [popUpView addSubview:popAssignedDateLbl];
            [popAssignedDateLbl release];
            
            
            
            
            if(!([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))
            {
                UILabel *popDueDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popDueDateLbl setFrame:CGRectMake(25, 92,200, 15)];
                }
                else
                {
                    [popDueDateLbl setFrame:CGRectMake(20, 87,200, 15)];
                }
                
                popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popDueDateLbl.numberOfLines=1;
                popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popDueDateLbl.backgroundColor=[UIColor clearColor];
                
                NSString *strDueDate = [formeter stringFromDate:diarItem.dueDate];
                
                popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
                
                
                [popUpView addSubview:popDueDateLbl];
                [popDueDateLbl release];
                
                UILabel *popProgressLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popProgressLbl setFrame:CGRectMake(25, 110,200, 15)];
                }
                else
                {
                    [popProgressLbl setFrame:CGRectMake(20, 105,200, 15)];
                }
                
                popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popProgressLbl.numberOfLines=1;
                popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popProgressLbl.backgroundColor=[UIColor clearColor];
                
                
                if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                }
                // For Prime Only
                else
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                    
                }
                
                
                
                
                [popUpView addSubview:popProgressLbl];
                [popProgressLbl release];
            }
         

            if([diarItem.weblink length]>0 && diarItem.itemDescription!=nil )
            {
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popwebLinklbl setFrame: CGRectMake(25, 128,60, 15)];
                    [popwebLinkURllbl setFrame:CGRectMake(83, 128,130, 15)];
                }
                else
                {
                    [popwebLinklbl setFrame:CGRectMake(20, 123,60, 15)];
                    [popwebLinkURllbl setFrame:CGRectMake(78, 123,130, 15)];
                    
                }
            
            }
            
            
            
            /***********************************************************/
            
            UIViewController* popoverContent = [[UIViewController alloc]init];
            popoverContent.view = popUpView;
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;
            
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(240, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(240, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(230, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(230, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
            }
            
            if(timeDiff1<0)
            {
                iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                
                if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                {
                    if([diarItem.isApproved integerValue] == 0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 1)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 2)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                    }
                    
                }
                else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                {
                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                    if (lov)
                    {
                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                        {
                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                            iconImgView.image=[FileUtils getImageFromPath:strPath];
                        }
                    }
                }
            }
            else{
                if ([isTypeStr isEqualToString:@"Assignment"]) {
                    
                    iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                }
                else if([isTypeStr isEqualToString:@"Homework"]) {
                    iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                }
                else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                    
                    if([diarItem.isApproved integerValue] == 0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 1)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                    }
                    else  if([diarItem.isApproved integerValue] == 2)
                    {
                        iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                    }
                }
                else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                {
                    ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                    if (lov)
                    {
                        if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                        {
                            NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                            iconImgView.image=[FileUtils getImageFromPath:strPath];
                        }
                    }
                }
                else
                {
                    // Do Nothing
                }
            }
            
            if ([diarItem.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
            {
                imgViewForLine.hidden=NO;
            }
            else
            {
                imgViewForLine.hidden=YES;
            }
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,10,10 )inView:btn.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            
            [popoverContent release]; popoverContent = nil;
            [popUpView release];

        }
        // Student
        else
            
        {
            
            NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:diarItem.itemId];
            arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
            
            if([arrDIU count]>0 && ![diarItem.type  isEqualToString:k_MERIT_CHECK] && ([[[arrDIU firstObject]listofvaluesTypeid] integerValue] != 0 && [[[[arrDIU firstObject] meritTitle] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )) // If given merit is  active in student login
            {
                UIView *popUpView=nil;
                UIImageView *popUpBgImgView = nil;
                if([diarItem.weblink length]>0)
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, 440, 180)];
                    }
                    else
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 440, 170)];
                    }
                    
                }
                else
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, 440, 162)];
                    }
                    else
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 440, 152)];
                    }
                    
                    
                }
                
                popUpView.backgroundColor=[UIColor clearColor];
                
                if([diarItem.weblink length]>0)
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 5, 430,170)];
                    }
                    else
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -8, 227,179)];
                    }
                }
                else
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 5, 430,152)];
                    }
                    else
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 227,152)];
                    }
                }
                // popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
                [popUpBgImgView setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
                [popUpBgImgView.layer setCornerRadius:5.0];
                [popUpView addSubview:popUpBgImgView];
                [popUpBgImgView release];
                
                UIImageView * iconImgView=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [iconImgView setFrame:CGRectMake(7, 10, 20, 19)];
                    popUpView.backgroundColor=[UIColor blackColor];
                    
                }
                else
                {
                    [iconImgView setFrame:CGRectMake(2, 5, 20, 19)];
                    popUpView.backgroundColor=[UIColor clearColor];
                    
                }
                
                iconImgView.backgroundColor=[UIColor clearColor];
                
                //20AugChange //line for complete
                UIImageView * imgViewForLine=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForLine setFrame:CGRectMake(40, 18, 150, 2)];
                }
                else
                {
                    [imgViewForLine setFrame:CGRectMake(35, 13, 150, 2)];
                }
                
                imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                imgViewForLine.backgroundColor=[UIColor grayColor];
                
                NSString *isTypeStr=  diarItem.type;
                
                NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                long timeDiff1;
                timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:diarItem.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[NSDate date]]]];
                
                [formeter2 release];
                
                UIImageView *imgViewForBgHeader=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForBgHeader setFrame:CGRectMake(6, 5, 430, 26)];
                }
                else
                {
                    [imgViewForBgHeader setFrame:CGRectMake(0, 0, 430, 26)];
                }
                
#if DEBUG
                NSLog(@"diarItem Subect Name %@",diarItem.subject_name);
#endif
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diarItem.class_id]];
                if(class1.code!=nil)
                    imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    imgViewForBgHeader.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
                [imgViewForBgHeader.layer setMasksToBounds:YES];
                [imgViewForBgHeader.layer setCornerRadius:3.0f];
                [imgViewForBgHeader.layer setBorderWidth:1.0];
                [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                [popUpView addSubview:imgViewForBgHeader];
                [imgViewForBgHeader release];
                
                UILabel *popUpHeaderLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpHeaderLbl setFrame:CGRectMake(35, 7,180, 25)];
                }
                else
                {
                    [popUpHeaderLbl setFrame:CGRectMake(30, 2,180, 25)];
                }
                
                popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUpHeaderLbl.numberOfLines=1;
                popUpHeaderLbl.backgroundColor=[UIColor clearColor];
                popUpHeaderLbl.textColor=[UIColor whiteColor];
                
                if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
                {
                    popUpHeaderLbl.textColor=[UIColor blackColor];
                    
                }
                
                
                [popUpView addSubview:popUpHeaderLbl];
                [popUpHeaderLbl release];
                
                
                [popUpView addSubview:iconImgView];
                [iconImgView release];
                
                [popUpView addSubview:imgViewForLine];
                [imgViewForLine release];
                
                
                
                
                UILabel *popSubNameLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popSubNameLbl setFrame:CGRectMake(25, 37,200, 15)];
                }
                else
                {
                    [popSubNameLbl setFrame:CGRectMake(20, 32,200, 15)];
                }
                
                popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popSubNameLbl.numberOfLines=1;
                popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popSubNameLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popSubNameLbl];
                [popSubNameLbl release];
                
                UILabel *popAssignedDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popAssignedDateLbl setFrame:CGRectMake(25, 74,200, 15)];
                }
                else
                {
                    [popAssignedDateLbl setFrame:CGRectMake(20, 69,200, 15)];
                }
                
                popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popAssignedDateLbl.numberOfLines=1;
                popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popAssignedDateLbl.backgroundColor=[UIColor clearColor];
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                [formeter setTimeStyle:NSDateFormatterNoStyle];
                [formeter setDateStyle:NSDateFormatterShortStyle];
                [formeter setDateFormat:kDATEFORMAT_Slash];
                
                NSString *strAssignedDate = [formeter stringFromDate:diarItem.assignedDate];
                
                popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
                
                
                
                
                [popUpView addSubview:popAssignedDateLbl];
                [popAssignedDateLbl release];

                
               
                if(!([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))

                {
                    
                    UILabel *popDueDateLbl=[[UILabel alloc]init];
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popDueDateLbl setFrame:CGRectMake(25, 92,200, 15)];
                    }
                    else
                    {
                        [popDueDateLbl setFrame:CGRectMake(20, 87,200, 15)];
                    }
                    
                    popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popDueDateLbl.numberOfLines=1;
                    popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popDueDateLbl.backgroundColor=[UIColor clearColor];
                    
                    NSString *strDueDate = [formeter stringFromDate:diarItem.dueDate];
                    
                    popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
                    
                    
                    [popUpView addSubview:popDueDateLbl];
                    [popDueDateLbl release];

                    
                UILabel *popProgressLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popProgressLbl setFrame:CGRectMake(25, 110,200, 15)];
                }
                else
                {
                    [popProgressLbl setFrame:CGRectMake(20, 105,200, 15)];
                }
                
                popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popProgressLbl.numberOfLines=1;
                popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popProgressLbl.backgroundColor=[UIColor clearColor];
                
                
                if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                }
                // For Prime Only
                else
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];

                }
                
                
                
                
                [popUpView addSubview:popProgressLbl];
                [popProgressLbl release];
                }
                
                
                
                UILabel *popUDescLbl = nil;
                UILabel *popwebLinklbl = nil;
                UIButton *popwebLinkURllbl = nil;
                
                popUpHeaderLbl.text=diarItem.title ;
                if(diarItem.subject_name!=nil){
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                }
                else{
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
                }
                popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                
                
                if (diarItem.itemDescription!=nil) {
                    popUDescLbl.hidden=NO;
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 56,200, 15)];
                    }
                    else
                    {
                        popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 51,200, 15)];
                    }
                    
                    popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",diarItem.itemDescription] ;
                    
                    popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popUDescLbl.numberOfLines=1;
                    popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popUDescLbl.backgroundColor=[UIColor clearColor];
                    [popUpView addSubview:popUDescLbl];
                    [popUDescLbl release];
                    
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(25, 87,60, 15)];
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(83, 87,130, 15)];
                    }
                    else
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(20, 82,60, 15)];
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(78, 82,130, 15)];
                    }
                }
                else{
                    popUDescLbl.hidden=YES;
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(25, 53,60, 15)];
                        
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(85,53,130, 15)];
                    }
                    else
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(20, 48,60, 15)];
                        
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(80,48,130, 15)];
                    }
                }
                
                if (diarItem.weblink!=nil && [diarItem.weblink length]>0) {
                    popwebLinklbl.hidden = NO;
                    popwebLinkURllbl.hidden = NO;
                }
                else{
                    popwebLinklbl.hidden = YES;
                    popwebLinkURllbl.hidden = YES;
                }
                
                popwebLinklbl.font=[UIFont fontWithName:@"Arial" size:13];
                popwebLinklbl.numberOfLines=1;
                popwebLinklbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popwebLinklbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popwebLinklbl.backgroundColor=[UIColor clearColor];
                popwebLinklbl.text=@"WebLink:";
                
                [popUpView addSubview:popwebLinklbl];
                
                [popwebLinklbl release];
                
                [popwebLinkURllbl.titleLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
                [popwebLinkURllbl setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [popwebLinkURllbl setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
                [popwebLinkURllbl setTitle:diarItem.weblink forState:UIControlStateNormal];
                [popwebLinkURllbl addTarget:self action:@selector(popupWebLinkclickToOpen:) forControlEvents:UIControlEventTouchUpInside];
                [popUpView addSubview:popwebLinkURllbl];
                [popwebLinkURllbl release];
                
                UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
                [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    editBtn.frame=CGRectMake(410,6,26, 26);
                }
                else
                {
                    editBtn.frame=CGRectMake(415,1,26, 26);
                }
                
                editBtn.tag=btn.tag;
                [popUpView addSubview:editBtn];
                
                
                
                
                UILabel *popMeritTitleLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popMeritTitleLbl setFrame:CGRectMake(330, 50,90, 30)];
                }
                else
                {
                    [popMeritTitleLbl setFrame:CGRectMake(330, 50,90, 30)];
                }
                
                popMeritTitleLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popMeritTitleLbl.numberOfLines=0;
                popMeritTitleLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popMeritTitleLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popMeritTitleLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popMeritTitleLbl];
                [popMeritTitleLbl release];
                
                
                
                UILabel *popMeritAssignedBy=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popMeritAssignedBy setFrame:CGRectMake(330, 125,90, 30)];
                }
                else
                {
                    [popMeritAssignedBy setFrame:CGRectMake(330, 125,90, 30)];
                }
                
                popMeritAssignedBy.font=[UIFont fontWithName:@"Arial" size:13];
                popMeritAssignedBy.numberOfLines=0;
                popMeritAssignedBy.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popMeritAssignedBy.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popMeritAssignedBy.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popMeritAssignedBy];
                [popMeritAssignedBy release];
                
                
                
                if([diarItem.weblink length]>0 && diarItem.itemDescription!=nil )
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popwebLinklbl setFrame: CGRectMake(25, 128,60, 15)];
                        [popwebLinkURllbl setFrame:CGRectMake(83, 128,130, 15)];
                    }
                    else
                    {
                        [popwebLinklbl setFrame:CGRectMake(20, 123,60, 15)];
                        [popwebLinkURllbl setFrame:CGRectMake(78, 123,130, 15)];

                    }
                }
                
                
                
                
                
                
                UILabel *popMeritLblForSeprationline=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popMeritLblForSeprationline setFrame:CGRectMake(230, 31,1, 140)];
                }
                else
                {
                    [popMeritLblForSeprationline setFrame:CGRectMake(230, 31,1, 140)];
                }
                
                popMeritLblForSeprationline.backgroundColor=[UIColor lightGrayColor];
                [popUpView addSubview:popMeritLblForSeprationline];
                [popMeritLblForSeprationline release];
                
                
                
                
                MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
#if DEBUG
                NSLog(@"profobj %@",profobj.type);
#endif
                
                // Only FOr Student
                if ([profobj.type isEqualToString:@"Student"])
                {
                    // Fetching Diary Item Users on the bases of DI id
                    NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:diarItem.itemType_id];
                    if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                    {
                        NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:diarItem.itemId];
                        
#if DEBUG
                        NSLog(@"Before arrDIU %@",arrDIU);
#endif
                        arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
#if DEBUG
                        NSLog(@"After arrDIU %@",arrDIU);
#endif
                        
                        for (DairyItemUsers *assinUserobj in arrDIU)
                        {
                            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                            {
                                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                                if (lov)
                                {
                                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                    {
                                        
                                        
                                        
                                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.icon];
                                        
                                        UIImageView *imgViewTaskBasedMerits = [[UIImageView alloc]initWithFrame:CGRectMake(235, 50, 85, 85)];
                                        [imgViewTaskBasedMerits setImage:[FileUtils getImageFromPath:strPath] ];
                                        imgViewTaskBasedMerits.tag=[diarItem.itemId integerValue];
                                        
                                        [popUpView addSubview:imgViewTaskBasedMerits];
                                        [imgViewTaskBasedMerits release];
                                        
                                        
                                        UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(317, 86, 90, 13)];
                                        btnTaskBasedMerits.titleLabel.font = [UIFont fontWithName:@"Arial" size:13];
                                        [btnTaskBasedMerits.titleLabel setTextAlignment:NSTextAlignmentLeft];
                                        
                                        [btnTaskBasedMerits setTitle:@"View Merit" forState:UIControlStateNormal];
                                        [btnTaskBasedMerits setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                                        [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                                        btnTaskBasedMerits.tag=[diarItem.itemId integerValue];
                                        
                                        [popUpView addSubview:btnTaskBasedMerits];
                                        [btnTaskBasedMerits release];
                                        popMeritTitleLbl.text = assinUserobj.meritTitle;
                                        popMeritAssignedBy.text = assinUserobj.approverName;
                                    }
                                }
                            }
                        }
                    }
                }
                
                
                
                
                
                
                
                
                UIViewController* popoverContent = [[UIViewController alloc]init];
                popoverContent.view = popUpView;
                
                self._popoverControler = nil;
                // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
                self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
                self._popoverControler.delegate = self;
                
                // Need to set PreferredContentSize for iOS 7 and above and
                //             ContentSizeForViewInPopover for below iOS 7
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(440, self._popoverControler.contentViewController.view.frame.size.height)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(440, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
                }
                else
                {
                    [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(430, self._popoverControler.contentViewController.view.frame.size.height)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(430, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
                }
                
                if(timeDiff1<0)
                {
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                }
                else{
                    if ([isTypeStr isEqualToString:@"Assignment"]) {
                        
                        iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"Homework"]) {
                        iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                        
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    else
                    {
                        // Do Nothing
                    }
                }
                
                if ([diarItem.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                {
                    imgViewForLine.hidden=NO;
                }
                else
                {
                    imgViewForLine.hidden=YES;
                }
                
                [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,10,10 )inView:btn.superview
                                      permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                
                [popoverContent release]; popoverContent = nil;
                [popUpView release];
                
                
                

            }
            else // If given merit is Not active in student login
            {
                //UITableViewCell*cell=(UITableViewCell*) btn.superview.superview.superview;
                //int row=[_table indexPathForCell:cell].row;
                UIView *popUpView=nil;
                UIImageView *popUpBgImgView = nil;
                if([diarItem.weblink length]>0)
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, 240, 180)];
                    }
                    else
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 230, 170)];
                    }
                    
                }
                else
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(5, 5, 240, 162)];
                    }
                    else
                    {
                        popUpView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 230, 152)];
                    }
                    
                    
                }
                
                popUpView.backgroundColor=[UIColor clearColor];
                
                if([diarItem.weblink length]>0)
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 5, 227,170)];
                    }
                    else
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, -8, 227,179)];
                    }
                }
                else
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(6, 5, 227,152)];
                    }
                    else
                    {
                        popUpBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 227,152)];
                    }
                }
                // popUpBgImgView.image=[UIImage imageNamed:@"yellowpopup.png"];
                [popUpBgImgView setBackgroundColor:[UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]];
                [popUpBgImgView.layer setCornerRadius:5.0];
                [popUpView addSubview:popUpBgImgView];
                [popUpBgImgView release];
                
                UIImageView * iconImgView=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [iconImgView setFrame:CGRectMake(7, 10, 20, 19)];
                    popUpView.backgroundColor=[UIColor blackColor];
                    
                }
                else
                {
                    [iconImgView setFrame:CGRectMake(2, 5, 20, 19)];
                    popUpView.backgroundColor=[UIColor clearColor];
                    
                }
                
                iconImgView.backgroundColor=[UIColor clearColor];
                
                //20AugChange //line for complete
                UIImageView * imgViewForLine=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForLine setFrame:CGRectMake(40, 18, 150, 2)];
                }
                else
                {
                    [imgViewForLine setFrame:CGRectMake(35, 13, 150, 2)];
                }
                
                imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
                imgViewForLine.backgroundColor=[UIColor grayColor];
                
                NSString *isTypeStr=  diarItem.type;
                
                NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                long timeDiff1;
                timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:diarItem.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:[NSDate date]]]];
                
                [formeter2 release];
                
                UIImageView *imgViewForBgHeader=[[UIImageView alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [imgViewForBgHeader setFrame:CGRectMake(6, 5, 227, 26)];
                }
                else
                {
                    [imgViewForBgHeader setFrame:CGRectMake(0, 0, 227, 26)];
                }
                
#if DEBUG
                NSLog(@"diarItem Subect Name %@",diarItem.subject_name);
#endif
                //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
                MyClass *class1=nil;
                
                class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",diarItem.class_id]];
                if(class1.code!=nil)
                    imgViewForBgHeader.backgroundColor=[AppHelper colorFromHexString:class1.code];
                else{
                    imgViewForBgHeader.backgroundColor=[UIColor grayColor];
                }
                
                //--------- By Mitul ---------//
                imgViewForBgHeader.contentMode=UIViewContentModeScaleAspectFit;
                [imgViewForBgHeader.layer setMasksToBounds:YES];
                [imgViewForBgHeader.layer setCornerRadius:3.0f];
                [imgViewForBgHeader.layer setBorderWidth:1.0];
                [imgViewForBgHeader.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                [popUpView addSubview:imgViewForBgHeader];
                [imgViewForBgHeader release];
                
                UILabel *popUpHeaderLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popUpHeaderLbl setFrame:CGRectMake(35, 7,180, 25)];
                }
                else
                {
                    [popUpHeaderLbl setFrame:CGRectMake(30, 2,180, 25)];
                }
                
                popUpHeaderLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popUpHeaderLbl.numberOfLines=1;
                popUpHeaderLbl.backgroundColor=[UIColor clearColor];
                popUpHeaderLbl.textColor=[UIColor whiteColor];
                
                if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
                {
                    popUpHeaderLbl.textColor=[UIColor blackColor];
                    
                }
                [popUpView addSubview:popUpHeaderLbl];
                [popUpHeaderLbl release];
                
                
                [popUpView addSubview:iconImgView];
                [iconImgView release];
                
                [popUpView addSubview:imgViewForLine];
                [imgViewForLine release];
                
                
                
                
                UILabel *popSubNameLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popSubNameLbl setFrame:CGRectMake(25, 35,200, 15)];
                }
                else
                {
                    [popSubNameLbl setFrame:CGRectMake(20, 30,200, 15)];
                }
                
                popSubNameLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popSubNameLbl.numberOfLines=1;
                popSubNameLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popSubNameLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popSubNameLbl.backgroundColor=[UIColor clearColor];
                [popUpView addSubview:popSubNameLbl];
                [popSubNameLbl release];
                
                UILabel *popUDescLbl = nil;
                UILabel *popwebLinklbl = nil;
                UIButton *popwebLinkURllbl = nil;
                
                popUpHeaderLbl.text=diarItem.title ;
                if(diarItem.subject_name!=nil){
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                }
                else{
                    popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:",@""];
                }
                popSubNameLbl.text= [NSString stringWithFormat:@"%@ %@",@"Subject:", diarItem.subject_name];
                
                 if (diarItem.itemDescription!=nil ) {
                    popUDescLbl.hidden=NO;
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(25, 56,200, 15)];
                    }
                    else
                    {
                        popUDescLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 51,200, 15)];
                    }
                    
                    popUDescLbl.text=[NSString stringWithFormat:@"%@ %@",@"Description:",diarItem.itemDescription] ;
                    
                    popUDescLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popUDescLbl.numberOfLines=1;
                    popUDescLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popUDescLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popUDescLbl.backgroundColor=[UIColor clearColor];
                    [popUpView addSubview:popUDescLbl];
                    [popUDescLbl release];
                    
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(25, 87,60, 15)];
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(83, 87,130, 15)];
                    }
                    else
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(20, 82,60, 15)];
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(78, 82,130, 15)];
                    }
                }
                else{
                    popUDescLbl.hidden=YES;
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(25, 53,60, 15)];
                        
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(85,53,130, 15)];
                    }
                    else
                    {
                        popwebLinklbl =[[UILabel alloc]initWithFrame:CGRectMake(20, 48,60, 15)];
                        
                        popwebLinkURllbl=[[UIButton alloc]initWithFrame:CGRectMake(80,48,130, 15)];
                    }
                }
                
                if (diarItem.weblink!=nil && [diarItem.weblink length]>0) {
                    popwebLinklbl.hidden = NO;
                    popwebLinkURllbl.hidden = NO;
                }
                else{
                    popwebLinklbl.hidden = YES;
                    popwebLinkURllbl.hidden = YES;
                }
                
                popwebLinklbl.font=[UIFont fontWithName:@"Arial" size:13];
                popwebLinklbl.numberOfLines=1;
                popwebLinklbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popwebLinklbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popwebLinklbl.backgroundColor=[UIColor clearColor];
                popwebLinklbl.text=@"WebLink:";
                
                [popUpView addSubview:popwebLinklbl];
                
                [popwebLinklbl release];
                
                [popwebLinkURllbl.titleLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
                [popwebLinkURllbl setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [popwebLinkURllbl setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
                [popwebLinkURllbl setTitle:diarItem.weblink forState:UIControlStateNormal];
                [popwebLinkURllbl addTarget:self action:@selector(popupWebLinkclickToOpen:) forControlEvents:UIControlEventTouchUpInside];
                [popUpView addSubview:popwebLinkURllbl];
                [popwebLinkURllbl release];
                
                UIButton *editBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                [editBtn setImage:[UIImage imageNamed:@"inboxshortbydateiconselected.png"] forState:UIControlStateNormal];
                [editBtn addTarget:self action:@selector(edtiButtunPessed:) forControlEvents:UIControlEventTouchUpInside];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    editBtn.frame=CGRectMake(205,6,26, 26);
                }
                else
                {
                    editBtn.frame=CGRectMake(200,1,26, 26);
                }
                
                editBtn.tag=btn.tag;
                [popUpView addSubview:editBtn];
                
                /***********************************************************/
                
                UILabel *popAssignedDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popAssignedDateLbl setFrame:CGRectMake(25, 74,200, 15)];
                }
                else
                {
                    [popAssignedDateLbl setFrame:CGRectMake(20, 69,200, 15)];
                }
                
                popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popAssignedDateLbl.numberOfLines=1;
                popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popAssignedDateLbl.backgroundColor=[UIColor clearColor];
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                [formeter setTimeStyle:NSDateFormatterNoStyle];
                [formeter setDateStyle:NSDateFormatterShortStyle];
                [formeter setDateFormat:kDATEFORMAT_Slash];
                
                NSString *strAssignedDate = [formeter stringFromDate:diarItem.assignedDate];
                
                popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
                
                
                
                
                [popUpView addSubview:popAssignedDateLbl];
                [popAssignedDateLbl release];
                
                
                
                MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];

              
                
                if ([profobj.type isEqualToString:@"Student"] && ([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))
                {
                    DairyItemUsers *itemuser = [[Service sharedInstance]getAssigneStudentDataInfoStored:profobj.profile_id itemId:[diarItem.itemId stringValue]  postNotification:NO] ;
                    
                    UILabel *popAssignedby=[[UILabel alloc]init];
                    
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popAssignedby setFrame:CGRectMake(25, 92,200, 15)];
                    }
                    else
                    {
                        [popAssignedby setFrame:CGRectMake(20, 87,200, 15)];
                    }
                    
                    popAssignedby.font=[UIFont fontWithName:@"Arial" size:13];
                    popAssignedby.numberOfLines=1;
                    popAssignedby.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popAssignedby.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popAssignedby.backgroundColor=[UIColor clearColor];
                    popAssignedby.text= [NSString stringWithFormat:@"Assigned By: %@",itemuser.approverName];
                    
                    [popUpView addSubview:popAssignedby];
                    [popAssignedby release];

                    
                    
                }
                
                
                
                
                
                
                if(!([diarItem.type isEqualToString:k_MERIT_CHECK] || [diarItem.type isEqualToString:@"Merits"]))

                {
                    UILabel *popDueDateLbl=[[UILabel alloc]init];
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popDueDateLbl setFrame:CGRectMake(25, 92,200, 15)];
                    }
                    else
                    {
                        [popDueDateLbl setFrame:CGRectMake(20, 87,200, 15)];
                    }
                    
                    popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                    popDueDateLbl.numberOfLines=1;
                    popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                    popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                    popDueDateLbl.backgroundColor=[UIColor clearColor];
                    
                    NSString *strDueDate = [formeter stringFromDate:diarItem.dueDate];
                    
                    popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
                    
                    
                    [popUpView addSubview:popDueDateLbl];
                    [popDueDateLbl release];

                    
                UILabel *popProgressLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popProgressLbl setFrame:CGRectMake(25, 110,200, 15)];
                }
                else
                {
                    [popProgressLbl setFrame:CGRectMake(20, 105,200, 15)];
                }
                
                popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popProgressLbl.numberOfLines=1;
                popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popProgressLbl.backgroundColor=[UIColor clearColor];
                
                
                if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                }
                // For Prime Only
                else
                {
                    popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[diarItem.progress integerValue]],@"%"];
                    
                }
                
                
                
                
                [popUpView addSubview:popProgressLbl];
                [popProgressLbl release];
            }
            
                if([diarItem.weblink length]>0 && diarItem.itemDescription!=nil )
                {
                    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                    {
                        [popwebLinklbl setFrame: CGRectMake(25, 128,60, 15)];
                        [popwebLinkURllbl setFrame:CGRectMake(83, 128,130, 15)];
                    }
                    else
                    {
                        [popwebLinklbl setFrame:CGRectMake(20, 123,60, 15)];
                        [popwebLinkURllbl setFrame:CGRectMake(78, 123,130, 15)];
                        
                    }
                    
                }
                
                
                
                /***********************************************************/

                
                
                
                UIViewController* popoverContent = [[UIViewController alloc]init];
                popoverContent.view = popUpView;
                
                self._popoverControler = nil;
                // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
                self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
                self._popoverControler.delegate = self;
                
                // Need to set PreferredContentSize for iOS 7 and above and
                //             ContentSizeForViewInPopover for below iOS 7
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(240, self._popoverControler.contentViewController.view.frame.size.height)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(240, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
                }
                else
                {
                    [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(230, self._popoverControler.contentViewController.view.frame.size.height)];
                    
                    [self._popoverControler setPopoverContentSize:CGSizeMake(230, self._popoverControler.contentViewController.view.frame.size.height) animated:YES];
                }
                
                if(timeDiff1<0)
                {
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                        
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                }
                else{
                    if ([isTypeStr isEqualToString:@"Assignment"]) {
                        
                        iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"Homework"]) {
                        iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                        
                        if([diarItem.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([diarItem.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:diarItem.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                    else
                    {
                        // Do Nothing
                    }
                }
                
                if ([diarItem.progress isEqualToString:@"100"]) //Aman added -- EZTEST 683
                {
                    imgViewForLine.hidden=NO;
                }
                else
                {
                    imgViewForLine.hidden=YES;
                }
                
                [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,10,10 )inView:btn.superview
                                      permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                
                [popoverContent release]; popoverContent = nil;
                [popUpView release];
                
            }
            
                  
        }
        
        
    }
}

-(void)showPassesInMonthviewWhenclickFromShowAllDairyItem:(id)sender
{
     MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    UIButton *btn=(UIButton*)sender;

    Item *diarItem=nil;
    if([ btn.titleLabel.text isEqualToString:@"1"]){
        diarItem =[[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:btn.tag] postNotification:NO];
        [AppDelegate getAppdelegate]._isUniqueId =YES;
    }
    else{
        diarItem =[[Service sharedInstance]getItemDataInfoStoredForParseDate:[NSNumber numberWithInteger:btn.tag] appid:nil];
        [AppDelegate getAppdelegate]._isUniqueId =NO;
    }
    
    NSString *isTypeTemplateStr=  diarItem.type;
    
    selectedDiaryItem = diarItem;

    if([isTypeTemplateStr isEqualToString:@"OutOfClass"] || [isTypeTemplateStr isEqualToString:@"Out Of Class"]) {
        
        viewForApprovedPass.hidden = NO;
        lblPassType.text = diarItem.title;

        NSDateFormatter *formterDate=[[NSDateFormatter alloc]init];
        [formterDate setDateFormat:kDATEFORMAT_ddMMMyyyy];

        NSDateFormatter *formaterFromDB = [[NSDateFormatter alloc] init];
        [formaterFromDB setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        
        NSDateFormatter *formaterToDisplay = [[NSDateFormatter alloc] init];
        // 24Hrs
        [formaterToDisplay setTimeStyle:NSDateFormatterShortStyle];
        [formaterToDisplay setDateFormat:[formaterToDisplay dateFormat]];
        //[formaterToDisplay setDateFormat:kDateFormatToShowOnUI_hhmma];
        
        // 24Hrs
        NSString *fromTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:diarItem.assignedTime]];
        
        NSString *toTime = [formaterToDisplay stringFromDate:[formaterFromDB dateFromString:diarItem.dueTime]];
        
        lblFromTime.text = fromTime;
        lblToTime.text = toTime;
        lblPassDate.text = [formterDate stringFromDate:diarItem.assignedDate];
        
        [formterDate release];
        [formaterFromDB release];
        [formaterToDisplay release];
        
        if([diarItem.isApproved integerValue] == 0)
        {
            [self edtiButtunPessed:btn];
        }
        else  if([diarItem.isApproved integerValue] == 1)
        {
            // Teacher
            if ([profobj.type isEqualToString:@"Teacher"])
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getStudentName:diarItem];
               
                    lblPassApprovedByTitle.text = @"Issued For";
                [btn_DeletePass setHidden:NO];

            }
            // Student
            else
            {
                lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[diarItem.itemId integerValue]]];
                lblPassApprovedByTitle.text = @"Issued By";
                [btn_DeletePass setHidden:YES];

            }
            
            [self showApprovedViewDetails];
            
            [viewForApprovedPassInner setBackgroundColor:[UIColor colorWithRed:18/255.0f green:123/255.0f blue:16/255.0f alpha:1]];

            UIViewController* popoverContent = [[UIViewController alloc]init];

#if DEBUG
            NSLog(@"viewForApprovedPass = %p",[viewForApprovedPass description]);
#endif

            @try {
                [popoverContent.view  removeFromSuperview];
                popoverContent.view = viewForApprovedPass;
            }
            @catch (NSException *exception)
            {
#if DEBUG
                NSLog(@"approved passes exception =%@",[exception description]);
#endif
                
            }
            @finally {
                
                
            }
            
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;

            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake( self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:NO];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];

                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:NO];
            }
            
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn_ToHoldClickMonthDate.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
            
            [popoverContent release]; popoverContent = nil;
            
        }
        else  if([diarItem.isApproved integerValue] == 2)
        {
            // Teacher
            if ([profobj.type isEqualToString:@"Teacher"])
            {
                
                lblPassApprovedByTitle.text = @"Issued For";
            }
            // Student
            else
            {
                lblPassApprovedByTitle.text = @"Issued By";
                
            }
            
            lblPassAppovedBy.text = [[Service sharedInstance]getApprovalNameFromLocalDB:[NSString stringWithFormat:@"%d",[diarItem.itemId integerValue]]];
            
            [self showElapsedViewDetails];
            [viewForApprovedPassInner setBackgroundColor:[UIColor grayColor]];

            UIViewController* popoverContent = [[UIViewController alloc]init];

            @try {
                [popoverContent.view  removeFromSuperview];

                popoverContent.view = viewForApprovedPass;
                
                
            }
            @catch (NSException *exception)
            {
#if DEBUG
                NSLog(@"elapsed passes exception =%@",[exception description]);
#endif
                
            }
            @finally {
                
                
            }
#if DEBUG
            NSLog(@"popover crash 1");
#endif
            self._popoverControler = nil;
            // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
            self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler.delegate = self;

#if DEBUG
            NSLog(@"popover crash 2");
#endif
            // Need to set PreferredContentSize for iOS 7 and above and
            //             ContentSizeForViewInPopover for below iOS 7
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:NO];
            }
            else
            {
                [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height)];
                
                [self._popoverControler setPopoverContentSize:CGSizeMake(self._popoverControler.contentViewController.view.frame.size.width, self._popoverControler.contentViewController.view.frame.size.height) animated:NO];
            }
#if DEBUG
            NSLog(@"popover crash 3");
#endif
            [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,0,30,30 )inView:btn_ToHoldClickMonthDate.superview
                                  permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
#if DEBUG
            NSLog(@"popover crash 4");
#endif
            [popoverContent release];
            popoverContent = nil;
#if DEBUG
            NSLog(@"popover crash 5");
#endif
        }
    }
}

- (IBAction)deletePassAction:(id)sender {

    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:selectedDiaryItem.type forKey:@"type"];
    NSInteger itemId=[selectedDiaryItem.itemId integerValue];
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if( itemId>0)
    {
        [dict setObject:[NSNumber numberWithInteger:itemId] forKey:@"id"];
        
        if (networkReachable)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(diaryItemDidDelet:) name:NOTIFICATION_DeleteDairyItem object:nil];
            [[Service sharedInstance]deleteDairyItems:dict];
        }
        else{
            selectedDiaryItem.isDelete=[NSNumber numberWithBool:YES];
            selectedDiaryItem.isSync=[NSNumber numberWithBool:NO];
            NSError *error=nil;
            
            if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
        }
    }
    else
    {
        NSManagedObject *eventToDelete = selectedDiaryItem;
        if (eventToDelete)
        {
            [[AppDelegate getAppdelegate].managedObjectContext deleteObject:eventToDelete];
        }
        
        
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
    }
    [dict release];
}

-(void)diaryItemDidDelet:(NSNotification*)note
{
    if (note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_DeleteDairyItem object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            [[Service sharedInstance]deleteObjectOfDairyItem:selectedDiaryItem.itemId];
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
            if([self._popoverControler isPopoverVisible]){
                [self._popoverControler dismissPopoverAnimated:NO];
                self._popoverControler.contentViewController.view = nil;

                if(_buttonForDay.selected ||_buttonForWeek.selected)
                    [_table reloadData];
                
                // --------- 2095 ------ DI list count update
                MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;

                if([mainVC._currentViewController.nibName isEqualToString:@"CalenderViewController"])
                {
                    CalenderViewController *cal=(CalenderViewController*)mainVC._currentViewController;
                    [cal updateCalenderView];
                }
                // --------- DI List count update
            }
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
    }
}

#pragma mark - method To Open Link ON WebView when user select on small popup link
- (IBAction)popupWebLinkclickToOpen:(id)sender {

    //added by Viraj - Start - issue #EZTEST-2351
    if([self._popoverControler isPopoverVisible])
    {
        [self._popoverControler dismissPopoverAnimated:NO];
        self._popoverControler.contentViewController.view = nil;
    }
    //added by Viraj - End - issue #EZTEST-2351
    
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        ShowAttachmentViewController *showAttachmentVC = [[ShowAttachmentViewController alloc] init];
            //added by Viraj - Start - issue #EZTEST-2351
//        [self presentViewController:showAttachmentVC animated:YES completion:nil];
            //added by Viraj - End - issue #EZTEST-2351
        [[AppDelegate getAppdelegate]._navi presentViewController:showAttachmentVC animated:YES completion:nil];

        [showAttachmentVC openLinkOnWebView:[sender currentTitle]];
        [showAttachmentVC release];
    }
    else
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
   
}

#pragma mark ends dairyitem details methods
#pragma mark

- (IBAction)detailsDescriptionButtonClick:(id)sender {
    
    
    if(_buttonForDiscrption.selected){
        [UIView beginAnimations:nil context:NULL];
        
        [UIView setAnimationDuration:0.30];

        _viewForItemDetails.frame=CGRectMake( _viewForItemDetails.frame.origin.x, 560,  _viewForItemDetails.frame.size.width,  _viewForItemDetails.frame.size.height);

        [UIView commitAnimations];
        
        _buttonForDiscrption.selected=NO;
    }
    else{
        
        [UIView beginAnimations:nil context:NULL];
        
        [UIView setAnimationDuration:0.30];
        _viewForItemDetails.frame=CGRectMake( _viewForItemDetails.frame.origin.x, 520,  _viewForItemDetails.frame.size.width,  _viewForItemDetails.frame.size.height);
        [UIView commitAnimations];
        _buttonForDiscrption.selected=YES;
        
        NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];

        if (count>self._arrForCalenderData.count) {
            count=0;
            School_Information *about=[self._arrForCalenderData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
        }
        else
        {
            School_Information *about=[self._arrForCalenderData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            count++;
        }
    }
}

-(NSString*)methodForGetiingShortlable:(int)value
{
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableCycleData];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"timeTableCycle_id" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    if(value>0)
    {
        TimeTableCycle *tabCycle=[arr objectAtIndex:value-1];

#if DEBUG
    NSLog(@"tabCycle.cycle_day %@",tabCycle.cycle_day);
#endif

        return tabCycle.short_lable;
    }
    else
    {
        return @" ";
    }
}

// Campus Based Start

-(NSString*)methodForGettingCalendarCycleLable:(NSDate *)date
{
    
    NSArray *arr=[[Service sharedInstance]getStoredCalendarCycleDayForGivenDate:date];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dateRef" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    NSString *strCycleDayLable = @" ";
    
    for(CalendarCycleDay *cycle in arr)
    {
        strCycleDayLable = cycle.cycleDayLabel;
#if DEBUG
        NSLog(@"cycleDayLabel %@",strCycleDayLable);
#endif
    }
    
    return strCycleDayLable;
    
}

-(CalendarCycleDay*)methodForCalendarCycleDay:(NSDate *)date
{
    
    NSArray *arr=[[Service sharedInstance]getStoredCalendarCycleDayForGivenDate:date];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"dateRef" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    CalendarCycleDay *cc=nil;
    
    if([arr count]>0)
    {
        cc = [arr objectAtIndex:0];
    }
    
    return cc;
    
}
// Campus Based End

- (IBAction)openCalendarAccessURL:(id)sender
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if ([profobj.type isEqualToString:@"Teacher"])
    {
        School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
        
        //TODO: Change URL While creating build...
        
        // For Daisy CALENDAR ACCESS URL
        NSURL *calendarURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@.e-planner.com.au/calendar/?pop=1&popOpt=link&userID=%@&platform=Mobile",schoolDetailObj.domainName,[AppHelper userDefaultsForKey:@"user_id"]]];
        
        // For Prime CALENDAR ACCESS URL
//        NSURL *calendarURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@.prime-planner.com/calendar/?pop=1&popOpt=link&userID=%@&platform=Mobile",schoolDetailObj.domainName,[AppHelper userDefaultsForKey:@"user_id"]]];
        
        // For Honeycomb CALENDAR ACCESS URL
//        NSURL *calendarURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@.digiplanner.co.uk/calendar/?pop=1&popOpt=link&userID=%@&platform=Mobile",schoolDetailObj.domainName,[AppHelper userDefaultsForKey:@"user_id"]]];

//        [[UIApplication sharedApplication] openURL:calendarURL];
//        [dateIconButton addTarget:self action:@selector(openCalendarAccessURL:) forControlEvents:UIControlEventTouchUpInside];
        BOOL networkReachable = [[DELEGATE netManager]networkReachable];
        if (networkReachable)
        {
            ShowAttachmentViewController *showAttachmentVC = [[ShowAttachmentViewController alloc] init];
            [[DELEGATE _currentViewController] presentViewController:showAttachmentVC animated:YES completion:nil];
            [showAttachmentVC openCalendarAccessOnWebView:calendarURL];
            [showAttachmentVC release];
            showAttachmentVC = nil;
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
    }
    else
    {
        [dateIconButton addTarget:self action:@selector(dateButtonIconAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
   
}


- (IBAction)dayButtonAction:(id)sender
{
#if DEBUG
    NSLog(@"dayButtonAction Start...");
#endif
    
    
    [Flurry logEvent:FLURRY_EVENT_Day_View_Cilcked withParameters:dictFlurryParameter];
    
    if(_buttonForDay.selected==NO){
        _buttonForDateIcon.enabled=YES;
        
        //TODO:Frame Changed
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            _viewForItemDetails.frame=CGRectMake( _viewForItemDetails.frame.origin.x, 560,  _viewForItemDetails.frame.size.width,  125);
        }
        else
        {
            _viewForItemDetails.frame=CGRectMake( _viewForItemDetails.frame.origin.x, 560,  _viewForItemDetails.frame.size.width,  115);
        }

        _buttonForDiscrption.selected=NO;
        
        // New code added to Show/Hide Content Panel
        NSString *contentType = [[Service sharedInstance] getContentType];
        if ([contentType isEqualToString:@"None"])
        {
            // hide Content panel
            _viewForItemDetails.hidden = YES;
        }
        else
        {
            // Show Content panel as before
            _viewForItemDetails.hidden = NO;
        }
        
        _lableForMonthHeading.text=@"Select Day";
        _table.tableHeaderView=nil;
        
        _buttonForDay.selected=YES;
        _buttonForGlance.selected=NO;
        _buttonFormonth.selected=NO;
        _buttonForWeek.selected=NO;
        _viewForWeek.hidden=NO;
        [self.calendarView removeFromSuperview];
        [_arr removeAllObjects];
        _viewForFilter.hidden=YES;
        _viewForMonthweek.hidden=YES;
        _viewForDay.hidden=NO;
        _lableForDay.hidden=NO;
        _lableForDate.hidden=NO;
        _lableForMonthName.hidden=YES;
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		NSArray *daySymbols = [formatter weekdaySymbols];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        // TODO: Need to Add condition to set current date if user come back to day view from Month/Week/Glance
#if DEBUG
        NSLog(@"Before self.strForDayDate %@",self.strForDayDate);
#endif
      
        NSDate* currentDate = nil;
        if (isSpecificDateSelected)
        {
            // Current logic to display existing/ previously selected date
            currentDate=[formatter dateFromString:self.strForDayDate];
            isSpecificDateSelected = NO;
        }
        else
        {
            // New logic added to display current/ System date
            self.strForDayDate=[formatter stringFromDate:[NSDate date]];
            currentDate=[formatter dateFromString:self.strForDayDate];
        }

#if DEBUG
        NSLog(@"After self.strForDayDate %@",self.strForDayDate);
#endif
        
        
        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:currentDate];
        
        _lableForDay.text=[NSString stringWithFormat:@"%ld",(long)[components day]];
        [formatter setDateFormat:@"MMM dd, yyyy"];
        [self getCureentTimeTable:[formatter dateFromString:[formatter stringFromDate:currentDate]]];
        NSString *str=nil;
        if(self._curentTimeTable)
        {
            
#if DEBUG
            NSLog(@"currentDate %@",currentDate);
#endif
            
//            self._dateForPicker=[calendar dateFromComponents:components];
            
            self._dateForPicker=currentDate;
            [pickerViewDay reloadAllComponents];
            [formatter setDateFormat:@"MMMM"];
#if DEBUG
            NSLog(@"[_arrForMonth indexOfObject:[formatter stringFromDate:currentDate]] %lu",(unsigned long)[_arrForMonth indexOfObject:[formatter stringFromDate:currentDate]]);
#endif
            
            [pickerViewDay selectRow:[_arrForMonth indexOfObject:[formatter stringFromDate:currentDate]] inComponent:0 animated:YES];
            
            // Code added to highlight the selected date number in picker - 2721
            //------------------------------------------------------------------
            NSInteger dayNumber = [components day] - 1;
            [pickerViewDay selectRow:dayNumber inComponent:1 animated:YES];
            //------------------------------------------------------------------
            
            [formatter setDateFormat:@"MMM dd, yyyy"];
            // Campus Based Start
#if DEBUG
            NSLog(@"dayButtonAction Before selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
            
            
            selectedMonthIndex = [pickerViewDay selectedRowInComponent:0];

#if DEBUG
            NSLog(@"dayButtonAction After selectedMonthIndex %ld",(long)selectedMonthIndex);
            NSLog(@"[pickerViewDay selectedRowInComponent:1] %ld",(long)[pickerViewDay selectedRowInComponent:1]);
#endif
            
            NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
            
            [formatter1 setDateFormat:@"dd/MM/yyyy"];
            
            // Current logic to display existing/ previously selected date
            NSString *dayStr=[NSString stringWithFormat:@"%@",[self methodForGettingCalendarCycleLable:[formatter1 dateFromString:self.strForDayDate]]];

//            // New logic added to display current/ System date
//            NSString *strCurrentDate = [formatter1 stringFromDate:[NSDate date]];
//            NSString *dayStr=[NSString stringWithFormat:@"%@",[self methodForGettingCalendarCycleLable:[formatter1 dateFromString:strCurrentDate]]];
            
#if DEBUG
            NSLog(@"dayStr %@",dayStr);
#endif
            // Campus Based End
            
            if([self getCurentDayIsWeekend:currentDate]){
                str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:currentDate]];
            }
            else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[formatter dateFromString:[formatter stringFromDate:currentDate]]]){
                str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:currentDate]];
            }
            else{
                str=[NSString stringWithFormat:@"%@ \n %@ \n %@",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:currentDate],dayStr];
            }
            
        }
        else
        {
            // New code added to load picker view components to fix 2644 issue
            //------------------------------------------------------------------
            self._dateForPicker=currentDate;
            [pickerViewDay reloadAllComponents];
            
            // Code added to highlight the selected date number in picker - 2721
            //------------------------------------------------------------------
            NSInteger dayNumber = [components day] - 1;
            [pickerViewDay selectRow:dayNumber inComponent:1 animated:YES];
            //------------------------------------------------------------------

            //------------------------------------------------------------------
            
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:currentDate]];
        }
        
        [formatter release];
        NSArray *arrItem=[str componentsSeparatedByString:@"\n"];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                             initWithString:str];
#if DEBUG
        NSLog(@"dayButtonAction : string : %@",string);
#endif
        NSString *str1=[arrItem objectAtIndex:0];
        NSString *str2=[arrItem objectAtIndex:1];
        NSString *str3=[arrItem objectAtIndex:2];
#if DEBUG
        NSLog(@"str3: %@",str3);
#endif

        //Cycle day labels are not fully displayed in calendar day view. - start
        CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 28.0, NULL);
        CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 18.0, NULL);
        CTFontRef helveticaBold2 = CTFontCreateWithName(CFSTR("Arial"), 13.0, NULL);
        //Cycle day labels are not fully displayed in calendar day view. - end
        
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold1
                       range:[str rangeOfString:str2]];
        
        // Adding Underline to date i.e. str2 9 June 2015 , EZTEST-1966
        //----------------------------------------------------------------------
        [string addAttribute:NSUnderlineStyleAttributeName
                       value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                       range:[str rangeOfString:str2]];
        //----------------------------------------------------------------------

        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold2
                       range:[str rangeOfString:str3]];
        
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor darkGrayColor].CGColor
                       range:[str rangeOfString:str2]];
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor lightGrayColor].CGColor
                       range:[str rangeOfString:str3]];
        
        if (self._testlayer!=nil){
            
            [self._testlayer removeFromSuperlayer];
        }
        
        CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
        normalTextLayer_.string =string;
        normalTextLayer_.wrapped = YES;
        normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
        normalTextLayer_.alignmentMode = kCAAlignmentCenter;
        normalTextLayer_.frame = CGRectMake(0, 0, _lableForDate.frame.size.width,_lableForDate.frame.size.height);
        self._testlayer=normalTextLayer_;
        [_lableForDate.layer addSublayer: self._testlayer];
        [_lableForDate setNeedsDisplay];
        [string release];
        [normalTextLayer_ release];
        
        [self createDataForDayView];
        [_table reloadData];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        // TODO: This condition is applied because Client needs to display only Current date events . If removed will display event according to date
        if ([[dateFormat stringFromDate:currentDate] isEqualToString:[dateFormat stringFromDate:[NSDate date]]])
        {
            //It's the same day
            [self showevent];

        }
        [dateFormat release];
        
    }
    
    // Calling method to update Clanedar Puulbar View Content on the bases of Current / Selected Date
    [self updateCalenderlDaisy];
    
#if DEBUG
    NSLog(@"dayButtonAction End...");
#endif
    
}
-(void)createDataForDayView{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    [_arr removeAllObjects];
    
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
    // Campus Based End

    
    NSDateFormatter *dtFormatForPeriods = [[NSDateFormatter alloc] init];
    [dtFormatForPeriods setTimeStyle:NSDateFormatterShortStyle];
#if DEBUG
    NSLog(@"dtFormatForPeriods format%@",[dtFormatForPeriods dateFormat]);
#endif
    
    [dtFormatForPeriods setDateFormat:[dtFormatForPeriods dateFormat]];
    
#if DEBUG
    NSLog(@"[dtFormatForPeriods dateFormat] : %@",[dtFormatForPeriods dateFormat]);
#endif
    
    
    if ([[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS8] || [[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS7])
    {
        NSDate *dtStart = [dtFormatForPeriods dateFromString:@"12:00 AM"];
        
        NSString *strDate = [dtFormatForPeriods stringFromDate:dtStart];
        
#if DEBUG
        NSLog(@"12 Hr Format --> \n dtStart %@ \n strDate %@",dtStart, strDate);
#endif
      
        
        for(int i=0;i<9;i++)
        {
            NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
            if(i==0)
            {
                
                [dict setObject:@"12:00 AM" forKey:@"start"];
                [dict setObject:@"08:00 AM" forKey:@"end"];
                
                [dict setObject:@"Before School" forKey:@"Period"];
            }
            else  if(i==1){
                [dict setObject:@"08:00 AM" forKey:@"start"];
                [dict setObject:@"10:00 AM" forKey:@"end"];
                [dict setObject:@" Home Group" forKey:@"Period"];
            }
            else  if(i==2){
                [dict setObject:@"10:00 AM" forKey:@"start"];
                [dict setObject:@"12:00 PM" forKey:@"end"];
                [dict setObject:@"Period 1" forKey:@"Period"];
                
            }
            else  if(i==3){
                [dict setObject:@"12:00 PM" forKey:@"start"];
                [dict setObject:@"02:00 PM" forKey:@"end"];
                
                [dict setObject:@"Period 2" forKey:@"Period"];
                
            }
            else  if(i==4){
                [dict setObject:@"02:00 PM" forKey:@"start"];
                [dict setObject:@"04:00 PM" forKey:@"end"];
                
                [dict setObject:@"Recess" forKey:@"Period"];
                
            }
            else  if(i==5){
                [dict setObject:@"04:00 PM" forKey:@"start"];
                [dict setObject:@"06:00 PM" forKey:@"end"];
                
                [dict setObject:@"Period 3" forKey:@"Period"];
                
            }
            else  if(i==6){
                [dict setObject:@"06:00 PM" forKey:@"start"];
                [dict setObject:@"08:00 PM" forKey:@"end"];
                
                [dict setObject:@"Period 4" forKey:@"Period"];
                
            }
            else  if(i==7){
                [dict setObject:@"08:00 PM" forKey:@"start"];
                [dict setObject:@"10:00 PM" forKey:@"end"];
                [dict setObject:@"Lunch" forKey:@"Period"];
                
            }
            else  if(i==8){
                [dict setObject:@"10:00 PM" forKey:@"start"];
                [dict setObject:@"11:59 PM" forKey:@"end"];
                [dict setObject:@"Period 5" forKey:@"Period"];
                
            }
            [_arr addObject:dict];
            
            [dict release];
        }

    }
    else
    {
        NSDate *dtStart = [dtFormatForPeriods dateFromString:@"12:00"];
        
        NSString *strDate = [dtFormatForPeriods stringFromDate:dtStart];
        
#if DEBUG
        NSLog(@"24 Hr Format --> \n dtStart %@ \n strDate %@",dtStart, strDate);
#endif
        
        for(int i=0;i<9;i++)
        {
            NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
            if(i==0)
            {
                [dict setObject:@"00:00" forKey:@"start"];
                [dict setObject:@"08:00" forKey:@"end"];
                
                [dict setObject:@"Before School" forKey:@"Period"];
            }
            else  if(i==1){
                
                [dict setObject:@"08:00" forKey:@"start"];
                [dict setObject:@"10:00" forKey:@"end"];
                [dict setObject:@" Home Group" forKey:@"Period"];
                
            }
            else  if(i==2){
                [dict setObject:@"10:00" forKey:@"start"];
                [dict setObject:@"12:00" forKey:@"end"];
                [dict setObject:@"Period 1" forKey:@"Period"];
                
            }
            else  if(i==3){
                [dict setObject:@"12:00" forKey:@"start"];
                [dict setObject:@"14:00" forKey:@"end"];
                
                [dict setObject:@"Period 2" forKey:@"Period"];
                
            }
            else  if(i==4){
                [dict setObject:@"14:00" forKey:@"start"];
                [dict setObject:@"16:00" forKey:@"end"];
                
                [dict setObject:@"Recess" forKey:@"Period"];
                
            }
            else  if(i==5){
                [dict setObject:@"16:00" forKey:@"start"];
                [dict setObject:@"18:00" forKey:@"end"];
                
                [dict setObject:@"Period 3" forKey:@"Period"];
                
            }
            else  if(i==6){
                [dict setObject:@"18:00" forKey:@"start"];
                [dict setObject:@"20:00" forKey:@"end"];
                
                [dict setObject:@"Period 4" forKey:@"Period"];
                
            }
            else  if(i==7){
                [dict setObject:@"20:00" forKey:@"start"];
                [dict setObject:@"22:00" forKey:@"end"];
                [dict setObject:@"Lunch" forKey:@"Period"];
                
            }
            else  if(i==8){
                [dict setObject:@"22:00" forKey:@"start"];
                [dict setObject:@"23:59" forKey:@"end"];
                [dict setObject:@"Period 5" forKey:@"Period"];
                
            }
            [_arr addObject:dict];
            
            [dict release];
        }
        
    }
    
    NSMutableArray *m_Arr = [[NSMutableArray alloc] init];
    // Campus Based Start
    if([schoolDetailObj.noOfCampus integerValue]>1)
    {
        for(NSMutableDictionary *dictNr in _arr)
        {
            [dictNr setObject:@"" forKey:@"Period"];
            [m_Arr addObject:dictNr];
        }
        [_arr removeAllObjects];
        [_arr setArray:(NSMutableArray*)m_Arr];
    }
    [m_Arr release];
    m_Arr = nil;
    // Campus Based End

#if DEBUG
    NSLog(@"Create Data for Day View_arr = %@",_arr);
#endif
    

    
    NSArray *arrOfp=nil;
    if(self._curentTimeTable){
        if([self getCurentDayIsWeekend:[formatter dateFromString:self.strForDayDate]]){
            _isCycleday=NO;
        }
        else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[formatter dateFromString:self.strForDayDate]]){
            _isCycleday=NO;
        }
        else{
            // Campus Based Start
            _isCycleday=YES;

            CalendarCycleDay *dayCycle2 = nil;
            int cycleday=100000;
            NSArray *arrcalenderCycleDay=[[Service sharedInstance]getStoredCalendarCycleDayForGivenDate:[formatter dateFromString:self.strForDayDate]];
            for(CalendarCycleDay *dayCycle in arrcalenderCycleDay)
            {
                dayCycle2 = dayCycle;
                if([dayCycle.cycleDayLabel isEqualToString:@""])
                {
                _isCycleday=NO;
                }
            }
#if DEBUG
            NSLog(@"dayCycle2 %@",dayCycle2);
#endif
            if(dayCycle2!=nil)
            {
                cycleday=[dayCycle2.cycleDay integerValue];
            }
#if DEBUG
            NSLog(@"cycleday %d",cycleday);
#endif
            
            NSArray *arr1=[[Service sharedInstance]getStoredAllTimeTableData];
            NSMutableArray *arrofTimeTabelID = [[[NSMutableArray alloc] init] autorelease];
            for(TimeTable *timetable in arr1)
            {
                if([DELEGATE isDateInBetweenGivenDate:[formatter dateFromString:self.strForDayDate] firstDate:timetable.startDate  lastDate:timetable. end_date])
                {
                    [arrofTimeTabelID addObject:timetable];
                }
            }
            
#if DEBUG
            NSLog(@"createDataForDayView arrofTimeTabelID Crash 1");
            NSLog(@"arrofTimeTabelID %@",arrofTimeTabelID);
            NSLog(@"createDataForDayView arrofTimeTabelID Crash 2");
#endif
            
            NSArray *arrOfAllClasses = [[Service sharedInstance]getStoredAllClassData];
            TimeTable *timetableObj=nil ;
#if DEBUG
            NSLog(@"totalCampusFromDB... %ld",(long)totalCampusFromDB);
#endif
            // New condition added to fix TC_01 i.e. previous periods not displaying...
            if(totalCampusFromDB == 1 && [arrofTimeTabelID count]>0 )
                timetableObj = [arrofTimeTabelID objectAtIndex:0];
            
            for(TimeTable *timetable in arrofTimeTabelID)
            {
                for(MyClass *classes in arrOfAllClasses)
                {
                    if(([timetable.timeTable_id integerValue]== [classes.timeTable_id integerValue]) && [classes.cycle_day integerValue] == cycleday)
                    {
#if DEBUG
                        NSLog(@"Timetable found for Classes");
#endif
                        timetableObj = timetable;
                        //break;
                    }
                    else
                    {
#if DEBUG
                        NSLog(@"Timtable not found for classes...");
#endif
                    }
                }
            }
#if DEBUG
            NSLog(@"timetableObj %@",timetableObj);
#endif
            
            if(timetableObj!=nil)
            {
                arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:timetableObj.timeTable_id cycle:[NSString stringWithFormat:@"%d",cycleday]];
            }
            ///_isCycleday=YES;Code move to up because to show timtable like holiday on calender day view

            if(!arrOfp.count>0)
            {
                // Multi Campus
                if (totalCampusFromDB > 1 )
                {
                    arrOfp =[[Service sharedInstance]getStoredAllPeriodTableData:timetableObj.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];
                }
                // single Campus
                else
                {
                    if ([arrofTimeTabelID count] > 0 )
                    {
#if DEBUG
                        NSLog(@"[[arrofTimeTabelID objectAtIndex:0] timeTable_id] %@",[[arrofTimeTabelID objectAtIndex:0] timeTable_id]);
#endif
                        arrOfp =[[Service sharedInstance]getStoredAllPeriodTableData:[[arrofTimeTabelID objectAtIndex:0] timeTable_id] cycle:[NSString stringWithFormat:@"%d",0]];
                    }
                }
            }
#if DEBUG
            NSLog(@"createDataForDayView arrOfp %@",arrOfp);
#endif
            // Campus Based End
        }
    }
    else{
        _isCycleday=NO;
        
    }
    
#if DEBUG
    NSLog(@"Crash arrOfp Count = %lu",(unsigned long)[arrOfp count] );
#endif
    

    if(arrOfp.count>0)
    {
        NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
        [_arr removeAllObjects];
        // 24Hrs
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateFormat:[formatter dateFormat]];
        
        for(Period *perObj in arrOfp)
        {

            NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
            [dict setObject:[[formatter stringFromDate:perObj.start_time] uppercaseString] forKey:@"start"];
            [dict setObject:[[formatter stringFromDate:perObj.end_time] uppercaseString] forKey:@"end"];
            [dict setObject:perObj.title forKey:@"Period"];
            [dict setObject:[NSString stringWithFormat:@"%d",[perObj.period_id integerValue]] forKey:@"Period_id"];
            [dict setObject:[NSString stringWithFormat:@"%d",[perObj.timeTable_id integerValue]] forKey:@"timeTable"];
            
            [_arr addObject:dict];
            [dict release];
            
        }
        
        Period *firstPeriod=[arrOfp objectAtIndex:0];
        Period *lastObj=[arrOfp objectAtIndex:arrOfp.count-1];

#if DEBUG
        NSLog(@"[dtFormatForPeriods dateFormat] %@",[dtFormatForPeriods dateFormat]);
#endif
        
        NSArray *arr=[[NSArray alloc]initWithObjects:firstPeriod, nil];
        NSString *str_12Or24Before = nil;
         NSString *str_12Or24After = nil;
        if ([[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS8] || [[dtFormatForPeriods dateFormat] isEqualToString:kDateFormatForiOS7] )
        {
            str_12Or24Before = @"12:00 AM";
            str_12Or24After = @"11:59 PM" ;
        }
        
        else
        {
            str_12Or24Before = @"00:00";
            str_12Or24After = @"23:59" ;

        }
        
        NSPredicate *predicate=  [NSPredicate predicateWithFormat:@"start_time > %@ ",[formatter dateFromString:str_12Or24Before]];
#if DEBUG
        NSLog(@"[formatter dateFromString:str_12Or24Before] %@",[formatter dateFromString:str_12Or24Before]);
        NSLog(@"predicate %@",predicate);
#endif
        
        NSArray *tempArray = [arr  filteredArrayUsingPredicate:predicate];
        if(tempArray.count>0){
            NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
            [dict1 setObject:str_12Or24Before forKey:@"start"];
            [dict1 setObject:[[formatter stringFromDate:firstPeriod.start_time] uppercaseString] forKey:@"end"];
            [dict1 setObject:@"Before School" forKey:@"Period"];
            [_arr insertObject:dict1 atIndex:0];
            [dict1 release];
            NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
            [dict2 setObject:str_12Or24After forKey:@"end"];
            [dict2 setObject:[[formatter stringFromDate:lastObj.end_time] uppercaseString] forKey:@"start"];
            [dict2 setObject:@"After School" forKey:@"Period"];
            [_arr addObject:dict2];
            [dict2 release];
            
        }
    }

#if DEBUG
    NSLog(@"Create Data for Day View_arr 222 = %@",_arr);
#endif

    [formatter release];
    
}

// Campus Based Start
- (void)createLogicalPeriods
{
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"hh:mm a"];
    NSMutableArray *marrForNewPeiods = [[NSMutableArray alloc] init];
    if ([_arr count]>0)
    {
        for ( int i=0; i< [_arr count] - 1; i++ )
        {
            NSMutableDictionary *periodobj = [_arr objectAtIndex:i] ;
            
#if DEBUG
            NSLog(@"Start %@ ",[[_arr objectAtIndex:(i+1)] valueForKey:@"start"]);
            NSLog(@"End %@ ",[[_arr objectAtIndex:i] valueForKey:@"end"]);
#endif
            
            // if start Time is later in time than end Time
            if(!([[[_arr objectAtIndex:(i+1)] valueForKey:@"start"] isEqualToString:[[_arr objectAtIndex:i] valueForKey:@"end"]]))
            {
                [marrForNewPeiods addObject:periodobj];
                
                // Add logical period
                NSMutableDictionary *dicts = [[[NSMutableDictionary alloc] init] autorelease];
                
                [dicts setObject:[[_arr objectAtIndex:i] valueForKey:@"end"] forKey:@"start"];
                [dicts setObject:[[_arr objectAtIndex:(i+1)] valueForKey:@"start"] forKey:@"end"];
                [dicts setObject:@"" forKey:@"Period"];
                
                [marrForNewPeiods addObject:dicts];
            }
            else
            {
                [marrForNewPeiods addObject:periodobj];
            }
        }
        [marrForNewPeiods addObject:[_arr lastObject]];
    }
    [_arr removeAllObjects];
    _arr= marrForNewPeiods;
    
#if DEBUG
    NSLog(@"Create Data for Day View_arr = %@",marrForNewPeiods);
    
    NSLog(@"Create Data for Day View_arr = %@",_arr);
#endif
    
}
// Campus Based End

- (IBAction)filterButtonAction:(id)sender
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    

    //Important  done
    UIButton* btn=(UIButton*)sender;
    if(btn.tag==1){
        if(btn.selected){
            btn.selected=NO;
            _buttonFortaskFilter.selected=NO;
        }
        else{
            btn.selected=YES;
            _buttonFortaskFilter.selected=YES;
        }
    }
    else if(btn.tag==2){
        if(btn.selected){
            btn.selected=NO;
            _buttonForFilterEvent.selected=NO;
        }
        else{
            btn.selected=YES;
            _buttonForFilterEvent.selected=YES;
        }
        
    }
    else{
        if(btn.selected){
            btn.selected=NO;
            _buttonForFilterNotes.selected=NO;
        }
        else{
            btn.selected=YES;
            _buttonForFilterNotes.selected=YES;
        }
        
    }
    
    
    if (_buttonFormonth.selected) {
        
        //19AugChange done
        
        for(UIView *view1 in self.calendarView.subviews){
            [view1 removeFromSuperview];
        }
        [self createViewForCalender];
    }
    else{
        [_table reloadData];
    }
    
    //////////////////////////////////////////
}

- (IBAction)weekButtonAction:(id)sender {
    
    [Flurry logEvent:FLURRY_EVENT_Week_View_Cilcked withParameters:dictFlurryParameter];
    if(_buttonForWeek.selected==NO){
        
        _buttonForFilterEvent.enabled = NO ;
        _buttonForFilterEvent.alpha = 0.5 ;
        _buttonForFilterNotes.enabled = NO ;
        _buttonForFilterNotes.alpha = 0.5 ;
        
        // Aman GP
        //NSArray *arr = [[Service sharedInstance] getStoreDAllActiveDiaryItemTypes];
        NSArray *arr = [[Service sharedInstance] getStoredAllActiveDiaryItemTypesWithFormTemplate];
        for (DiaryItemTypes *activeItemTypes in arr) {
            
            if ([activeItemTypes.formtemplate isEqualToString:@"Event"]) {
                _buttonForFilterEvent.enabled = YES ;
                _buttonForFilterEvent.alpha = 1.0 ;
                
            }else if ([activeItemTypes.formtemplate isEqualToString:@"Note"]){
                _buttonForFilterNotes.enabled = YES ;
                _buttonForFilterNotes.alpha = 1.0 ;
            }else{
                
            }
        }


        
        _buttonForDateIcon.selected=NO;
        _viewForItemDetails.hidden=YES;
        _buttonForDateIcon.enabled=NO;
        _lableForMonthHeading.text=@"Select Week";
        
        _table.tableHeaderView=nil;
        _buttonForDay.selected=NO;
        _buttonForGlance.selected=NO;
        _buttonFormonth.selected=NO;
        _buttonForWeek.selected=YES;
        _viewForWeek.hidden=NO;
        [pickerViewDay reloadAllComponents];
        
        [self.calendarView removeFromSuperview];
        [_arr removeAllObjects];
        [_table reloadData];
        _viewForFilter.hidden=NO;
        _viewForMonthweek.hidden=NO;
        _viewForDay.hidden=YES;
        _lableForDay.hidden=YES;
        _lableForDate.hidden=YES;
        _lableForMonthName.hidden=NO;
        
        
        [self pickerView:pickerViewDay didSelectRow:[self getCurrentWeek] inComponent:0];
        [pickerViewDay selectRow:[self getCurrentWeek] inComponent:0 animated:YES];
        
        [self setSelectedMonth:[pickerViewDay selectedRowInComponent:0]];
        
        // Campus Based Start
        //-----
#if DEBUG
        NSLog(@"weekButtonAction Before selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
      
        selectedMonthIndex = [self getCurrentWeek];
#if DEBUG
        NSLog(@"weekButtonAction After selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
      
        //-----
        // Campus Based End
        
        [self customizepikerView];
        // [formatter release];
    }
    
}
-(NSInteger)getCurrentWeek{
    NSDateFormatter *formatter = [[[NSDateFormatter alloc]init] autorelease];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    // Current logic to display existing/ previously selected date
    //NSDate* currentDate=[formatter dateFromString:self.strForDayDate];
    // New logic added to display current/ System date
    NSDate* currentDate=[NSDate date];
    
#if DEBUG
    NSLog(@"getCurrentWeek currentDate  %@",currentDate);
    
    NSLog(@"_arrForweek %@",_arrForweek);
#endif
    
    for(NSDictionary *dict in _arrForweek)
    {
        if((int)[currentDate timeIntervalSinceDate:[dict objectForKey:@"start"]]>=0 &&([currentDate timeIntervalSinceDate:[dict objectForKey:@"end"]]<=0))
        {

#if DEBUG
            NSLog(@"dict %@",dict);
#endif
            return [_arrForweek indexOfObject:dict];
        }
    }
    return nil;
}
-(void)setWeekName :(NSInteger )row{
    NSString *mainStr=nil;
    [_globlDateFormat setDateFormat:@"dd"];
    mainStr=[NSString stringWithFormat:@"%@ to %@",[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"start"]],[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"end"]]];
    [_globlDateFormat setDateFormat:@"MMM"];
    NSString *firstMonth=[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"start"]];
    NSString *secondMonth=[_globlDateFormat stringFromDate:[[_arrForweek objectAtIndex:row] objectForKey:@"end"]];
    if([secondMonth isEqualToString:firstMonth]){
        
        mainStr=[NSString stringWithFormat:@"%@ %@",mainStr,firstMonth];
    }
    else{
        mainStr=[NSString stringWithFormat:@"%@ %@-%@",mainStr,firstMonth,secondMonth];
    }
    
    
    NSArray *arrItem=[mainStr componentsSeparatedByString:@" "];
    NSString *str1=[arrItem objectAtIndex:0];
    NSString *str2=[arrItem objectAtIndex:1];
    NSString *str3=[arrItem objectAtIndex:2];
    NSString *str4=[arrItem objectAtIndex:3];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                         initWithString:mainStr];
    CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial-BoldMT"), 60.0, NULL);
    CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 35.0, NULL);
    //  CTFontRef helveticaBold2 = CTFontCreateWithName(CFSTR("Arial-BoldMT"), 15.0, NULL);
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold
                   range:[mainStr rangeOfString:str1]];
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold1
                   range:[mainStr rangeOfString:str2]];
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold
                   range:[mainStr rangeOfString:str3]];
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold1
                   range:[mainStr rangeOfString:str4]];
    
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                   range:[mainStr rangeOfString:str1]];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                   range:[mainStr rangeOfString:str3]];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor darkGrayColor].CGColor
                   range:[mainStr rangeOfString:str2]];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor darkGrayColor].CGColor
                   range:[mainStr rangeOfString:str4]];
    
    
    if (self._testlayer!=nil){
        
        [self._testlayer removeFromSuperlayer];
    }
    CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
    normalTextLayer_.string =string;
    normalTextLayer_.wrapped = YES;
    normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
    normalTextLayer_.alignmentMode = kCAAlignmentCenter;
    normalTextLayer_.frame = CGRectMake(0, 0, _lableForMonthName.frame.size.width,_lableForMonthName.frame.size.height);
    self._testlayer=normalTextLayer_;
    [_lableForMonthName.layer addSublayer: self._testlayer];
    [_lableForMonthName setNeedsDisplay];
    [string release];
    [normalTextLayer_ release];
    
}
- (IBAction)monthButtonAction:(id)sender {
 
     [Flurry logEvent:FLURRY_EVENT_Month_View_Cilcked withParameters:dictFlurryParameter];
    
    if(_buttonFormonth.selected==NO){
        
        
        _buttonForFilterEvent.enabled = NO ;
        _buttonForFilterEvent.alpha = 0.5 ;
        _buttonForFilterNotes.enabled = NO ;
        _buttonForFilterNotes.alpha = 0.5 ;
        
        // Aman GP
        //NSArray *arr = [[Service sharedInstance] getStoreDAllActiveDiaryItemTypes];
        NSArray *arr = [[Service sharedInstance] getStoredAllActiveDiaryItemTypesWithFormTemplate];
        for (DiaryItemTypes *activeItemTypes in arr) {
            
            if ([activeItemTypes.formtemplate isEqualToString:@"Event"]) {
                _buttonForFilterEvent.enabled = YES ;
                _buttonForFilterEvent.alpha = 1.0 ;
                
            }else if ([activeItemTypes.formtemplate isEqualToString:@"Note"]){
                _buttonForFilterNotes.enabled = YES ;
                _buttonForFilterNotes.alpha = 1.0 ;
            }else{
                
            }
        }
        _buttonForDateIcon.selected=NO;
        _viewForItemDetails.hidden=YES;
        _lableForMonthHeading.text=@"Select Month";
        _buttonForDateIcon.enabled=NO;
        _viewForWeek.hidden=YES;
        [self.calendarView removeFromSuperview];
        _viewForFilter.hidden=NO;
        _viewForMonthweek.hidden=NO;
        _viewForDay.hidden=YES;
        _lableForDay.hidden=YES;
        _lableForDate.hidden=YES;
        _lableForMonthName.hidden=NO;
        _buttonForDay.selected=NO;
        _buttonForGlance.selected=NO;
        _buttonFormonth.selected=YES;
        _buttonForWeek.selected=NO;
        [pickerViewDay reloadAllComponents];
        self.calendarView=nil;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"dd/MM/yyyy"];
        // Current logic to display existing/ previously selected date
        //CalendarLogic *aCalendarLogic = [[CalendarLogic alloc] initWithDelegate:self referenceDate:[formatter dateFromString:self.strForDayDate]];
        // New logic added to display current/ System date
        CalendarLogic *aCalendarLogic = [[CalendarLogic alloc] initWithDelegate:self referenceDate:[NSDate date]];
        
        self.calendarLogic = aCalendarLogic;
        [aCalendarLogic release];
        //Cycle labels are not displaying in calendar Month and Glance views - start
        UIView *aCalendarView = [[UIView alloc] initWithFrame:CGRectMake(5, 150, 875, 512) ];
        //Cycle labels are not displaying in calendar Month and Glance views - end
        aCalendarView.backgroundColor=[UIColor clearColor];
        self.calendarView = aCalendarView;
		
        [formatter setDateFormat:@"MMMM"];
        // [pickerViewDay selectRow: inComponent:0 animated:YES];
        [pickerViewDay selectRow:[_arrForMonth indexOfObject:[formatter stringFromDate:self.calendarLogic.referenceDate]] inComponent:0 animated:YES];
        [pickerViewDay reloadAllComponents];
        [self createViewForCalender];
        [self selectButtonForDate:selectedDate];
        [self.view addSubview:aCalendarView];
        
        [formatter release];
        self.calendarView = aCalendarView;
        [aCalendarView release];
        [self setSelectedMonth:[pickerViewDay selectedRowInComponent:0]];
        
        UIView *view1=(UIView*)[pickerViewDay viewForRow:[pickerViewDay selectedRowInComponent:0] forComponent:0];
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
        [self customizepikerView];
    }
    
}

- (IBAction)glanceButtonAction:(id)sender {
    
    [Flurry logEvent:FLURRY_EVENT_Glance_View_Cilcked withParameters:dictFlurryParameter];
    
    
    if(_buttonForGlance.selected==NO){
        
        _buttonForFilterEvent.enabled = NO ;
        _buttonForFilterEvent.alpha = 0.5 ;
        _buttonForFilterNotes.enabled = NO ;
        _buttonForFilterNotes.alpha = 0.5 ;
        
        // Aman GP
        //NSArray *arr = [[Service sharedInstance] getStoreDAllActiveDiaryItemTypes];
        NSArray *arr = [[Service sharedInstance] getStoredAllActiveDiaryItemTypesWithFormTemplate];
        for (DiaryItemTypes *activeItemTypes in arr) {
            
            if ([activeItemTypes.formtemplate isEqualToString:@"Event"]) {
                _buttonForFilterEvent.enabled = YES ;
                _buttonForFilterEvent.alpha = 1.0 ;
                
            }else if ([activeItemTypes.formtemplate isEqualToString:@"Note"]){
                _buttonForFilterNotes.enabled = YES ;
                _buttonForFilterNotes.alpha = 1.0 ;
            }else{
                
            }
        }

        _buttonForDateIcon.selected=NO;
        _buttonForDateIcon.enabled=NO;
        _viewForItemDetails.hidden=YES;
        _lableForMonthHeading.text=@"Select Month";
        _buttonForDay.selected=NO;
        _viewForWeek.hidden=YES;
        _viewForFilter.hidden=NO;
        _viewForMonthweek.hidden=NO;
        _viewForDay.hidden=YES;
        _lableForDay.hidden=YES;
        _lableForDate.hidden=YES;
        _lableForMonthName.hidden=NO;
        _buttonForGlance.selected=YES;
        _buttonFormonth.selected=NO;
        _buttonForWeek.selected=NO;
        _viewForWeek.hidden=NO;
        _table.tableHeaderView=_viewForGlanceTable;
        [self.calendarView removeFromSuperview];
        [_arr removeAllObjects];
        [_table reloadData];
        [pickerViewDay reloadAllComponents];
#if DEBUG
        NSLog(@"_arrForMonth %@",_arrForMonth);
#endif
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        
        // Current logic to display existing/ previously selected date
        //NSDate *currenDate=[formatter dateFromString:self.strForDayDate];
        // New logic added to display current/ System date
        NSDate *currenDate=[NSDate date];

        [formatter setDateFormat:@"MMMM"];
        
        // Code added to set current month for glance view
        //-----------------------------
#if DEBUG
        NSLog(@"glanceButtonAction Before selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        
        if([_arrForMonth count]>0)
        {
#if DEBUG
            NSLog(@"[formatter stringFromDate:self.calendarLogic.referenceDate] %@",[formatter stringFromDate:self.calendarLogic.referenceDate]);
#endif
            selectedMonthIndex = (NSInteger)[_arrForMonth indexOfObject:[formatter stringFromDate:self.calendarLogic.referenceDate]];
        }

#if DEBUG
        NSLog(@"glanceButtonAction After selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        //-----------------------------
        
        [self getdateOfamonth:[_arrForMonth indexOfObject:[formatter stringFromDate:currenDate]]];

        [pickerViewDay selectRow:[_arrForMonth indexOfObject:[formatter stringFromDate:currenDate]] inComponent:0 animated:YES];
        [pickerViewDay reloadAllComponents];
        [formatter release];

        [self setSelectedMonth:[pickerViewDay selectedRowInComponent:0]];
        
        
        
        UIView *view1=(UIView*)[pickerViewDay viewForRow:[pickerViewDay selectedRowInComponent:0] forComponent:0];
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
        
        [self customizepikerView];
    }
}

#pragma mark CalenderLogic

- (NSUInteger)iso8601WeeksForYear:(NSUInteger)year {
    // Campus Based Start
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    TimeTable *tble=[arr firstObject];
    TimeTable *tbleLastYear=[arr lastObject];
    
NSInteger months = [self getNumberOfMonthsinBetweenStartDate:tble.startDate andEndDate:tbleLastYear.end_date];
    
#if DEBUG
    NSLog(@"Total months(for Week) %ld",(long)months);
#endif
    
    // Campus Based End

    
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *firstThursdayOfYearComponents = [[NSDateComponents alloc] init];
    [firstThursdayOfYearComponents setWeekday:5]; // Thursday
    [firstThursdayOfYearComponents setWeekdayOrdinal:1]; // The first Thursday of the month
    [firstThursdayOfYearComponents setMonth:1]; // January
    [firstThursdayOfYearComponents setYear:year];
    NSDate *firstThursday = [calendar dateFromComponents:firstThursdayOfYearComponents];
    
    NSDateComponents *lastDayOfYearComponents = [[NSDateComponents alloc] init];
    [lastDayOfYearComponents setDay:31];
    
    // Campus Based Start
    [lastDayOfYearComponents setMonth:months];
    // Campus Based End

    [lastDayOfYearComponents setYear:year];
    NSDate *lastDayOfYear = [calendar dateFromComponents:lastDayOfYearComponents];
    
    NSDateComponents *result = [calendar components:NSWeekCalendarUnit fromDate:firstThursday toDate:lastDayOfYear options:0];
    
    return result.week + 1;
}

-(void)getlistofWeek
{
#if DEBUG
    NSLog(@"getlistofWeek Start...");
#endif
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    TimeTable *tble=[arr firstObject];

    // Week View issue
    NSCalendar *currentCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    NSDateComponents *components = [[[NSDateComponents alloc] init] autorelease];
    [_globlDateFormat setDateFormat:@"yyyy"];
    [components setYear:[[_globlDateFormat stringFromDate:tble.startDate] integerValue]];
    [_globlDateFormat setDateFormat:@"MM"];
    [components setMonth:[[_globlDateFormat stringFromDate:tble.startDate] integerValue]];
    
    [_globlDateFormat setDateFormat:@"MM-yyyy"];

    NSDateComponents *components1 = [currentCalendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:[currentCalendar dateFromComponents:components]];
    [_globlDateFormat setDateFormat:@"dd-MM-yyyy"];
    
    NSInteger weeks = components1.week;
    
    NSInteger totalWeeks = [self iso8601WeeksForYear:components.year];
    for(int i=0; i<totalWeeks;i++)
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [components setWeek:weeks+i];
        [components setWeekday:2];
        
        [dict setObject:[currentCalendar dateFromComponents:components]
                 forKey:@"start"];

        [components setWeek:weeks+i+1];
        [components setWeekday:1];

        [dict setObject:[currentCalendar dateFromComponents:components]
                 forKey:@"end"];
        [_arrForweek addObject:dict];

        [dict release];
    }
    // Week View issue
    [currentCalendar release];
    
#if DEBUG
    NSLog(@"getlistofWeek End...");
#endif
    
}

// Campus Based Start

-(NSInteger)returnDays:(NSDate*)date
{
    //Get a date object for today's date
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:date];
    return days.length;
}

-(NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

- (NSInteger)getNumberOfMonthsinBetweenStartDate:(NSDate*)startDate andEndDate:(NSDate*)endDate
{
#if DEBUG
    NSLog(@"startDate %@ And endDate %@", startDate,endDate );
#endif
    
    if ( startDate != nil && endDate != nil )
    {
        NSInteger numberOFDays = [self daysBetweenDate:startDate andDate:endDate];
#if DEBUG
        NSLog(@"numberOFDays =%ld",(long)numberOFDays);
#endif
      
        
        NSInteger daysinStartDate = [self returnDays:startDate];
#if DEBUG
        NSLog(@"daysinStartDate =%ld", (long)daysinStartDate);
#endif
        
        NSInteger daysinEndDate = [self returnDays:endDate];
#if DEBUG
        NSLog(@"daysinEndDate =%ld", (long)daysinEndDate);
#endif
      
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd"];
        
        NSInteger startDD = [[formatter stringFromDate:startDate] integerValue];
        NSInteger endDD = [[formatter stringFromDate:endDate] integerValue];

#if DEBUG
        NSLog(@"Numberofmonths =%lu",(unsigned long)(numberOFDays+startDD+(daysinEndDate - endDD))/30);
#endif
        
        return (numberOFDays+startDD+(daysinEndDate - endDD))/30;
    }
    else
    {
        return 0;
    }
    
    
}
// Campus Based End




-(void)getlistofMonth{
    
    // Campus Based Start
    marrOFPickerMonthYears = nil;
    marrOFPickerMonthYears = [[NSMutableArray alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    // Campus Based End


    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
#if DEBUG
    NSLog(@"arr %@",arr);
#endif
    
    TimeTable *tble=[arr firstObject];
    // Campus Based Start

    TimeTable *tbleLastYear=[arr lastObject];
    
#if DEBUG
    NSLog(@"timetable FirstRec StartDate %@ and LastRec EndDate %@",[formatter stringFromDate:tble.startDate],[formatter stringFromDate:tbleLastYear.end_date]);
#endif
    
    
    NSInteger months = [self getNumberOfMonthsinBetweenStartDate:tble.startDate andEndDate:tbleLastYear.end_date];
    
    // Campus Based End
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[[NSDateComponents alloc] init] autorelease];
    [_globlDateFormat setDateFormat:@"yyyy"];
    [components setYear:[[_globlDateFormat stringFromDate:tble.startDate] integerValue]];
    [_globlDateFormat setDateFormat:@"MM"];
    [components setMonth:[[_globlDateFormat stringFromDate:tble.startDate] integerValue]];
    
    // Campus Based Start
    //int k=[[_globlDateFormat stringFromDate:tble.startDate] intValue]+12;
    NSInteger k=[[_globlDateFormat stringFromDate:tble.startDate] integerValue]+months;

    // Campus Based End
    int m=[[_globlDateFormat stringFromDate:tble.startDate] integerValue];
    [_globlDateFormat setDateFormat:@"MMMM"];
    
    // Campus Based Start
    
    for(int i=m+1;i<=k;i++){
        [_globlDateFormat setDateFormat:@"MMMM"];
        
        [_arrForMonth addObject:[_globlDateFormat stringFromDate:[calendar dateFromComponents:components]]];
        [_globlDateFormat setDateFormat:@"YYYY"];
        
        [marrOFPickerMonthYears addObject:[_globlDateFormat stringFromDate:[calendar dateFromComponents:components]]];
        
        [components setMonth:i];
    }
    [_globlDateFormat setDateFormat:@"MMMM"];
    
#if DEBUG
    NSLog(@"_arrForMonth %@",_arrForMonth);
    NSLog(@"marrOFPickerMonthYears %@",marrOFPickerMonthYears);
#endif
    // Campus Based End
    
}
-(BOOL)getCurentDayIsWeekend:(NSDate*)fromdate{
    [_globlDateFormat setDateFormat:@"E"];
    if([[_globlDateFormat stringFromDate:fromdate] isEqualToString:@"Sun"] || [[_globlDateFormat stringFromDate:fromdate] isEqualToString:@"Sat"]){
        return YES;
    }
    else{
        return NO;
    }
}

-(void)getCureentTimeTable:(NSDate*)fromDate{
    [_globlDateFormat setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    
    for(TimeTable *time in arr)
    {
        // Method call changed to compare/include given start and End Dates to get current timetable to Fix issue No - 2772
        //-----------------------------------------------------------------------------------------------------------------
//        if([DELEGATE isDate:fromDate inRangeFirstDate:[ _globlDateFormat dateFromString:[_globlDateFormat stringFromDate:time.startDate ]] lastDate:[ _globlDateFormat dateFromString:[_globlDateFormat stringFromDate:time.end_date ]]])
//        {
        if([self isDate:fromDate inRangeFirstDate:[ _globlDateFormat dateFromString:[_globlDateFormat stringFromDate:time.startDate ]] lastDate:[ _globlDateFormat dateFromString:[_globlDateFormat stringFromDate:time.end_date ]]])
        {
            _isFirstSem=YES;
            self._curentTimeTable=time;
            break;
        }
    }
}
- (void)setSelectedDate:(NSDate *)aDate
{
}

-(NSString*)getCurrentYearForMonth:(int)month
{
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    
    NSString *monthYear;
    
    if ([arr count] > 0)
    {
        TimeTable *tble=[arr objectAtIndex:0];
        TimeTable *tbleSem2=[arr lastObject];
        //
        [_globlDateFormat setDateFormat:@"yyyy"];
        NSString *yearStr=[_globlDateFormat stringFromDate:tble.startDate];
        NSString *yearEnd=[_globlDateFormat stringFromDate:tbleSem2.end_date];
        
        [_globlDateFormat setDateFormat:@"MM"];
        if(month<[[_globlDateFormat stringFromDate:tble.startDate] integerValue])
        {
            // Month Lie in Next Year i.e. Sem 2 - End Date Year
            monthYear=yearEnd;
        }
        else
        {
            // Month Lie in Current Year i.e. Sem 1 - Start Date Year
            monthYear=yearStr;
        }
    }
    else
    {
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
        
        // [components year]; // gives you year
        monthYear = [NSString stringWithFormat:@"%ld",(long)[components year]];
    }
    // Campus Based Start
    monthYear = [marrOFPickerMonthYears objectAtIndex:selectedMonthIndex];
    
#if DEBUG
    NSLog(@"getCurrentYearForMoth selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
    
    // Campus Based End

    
    return monthYear;
}
-(NSString *)getCurrentYear:(NSInteger)month{
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    NSString *monthYear;
    
    if ([arr count] > 0)
    {
        TimeTable *tble=[arr objectAtIndex:0];
        TimeTable *tbleSem2=[arr lastObject];
        
        [_globlDateFormat setDateFormat:@"yyyy"];
        //  BOOL isdiferent=NO;
        NSString *yearStr=[_globlDateFormat stringFromDate:tble.startDate];
        NSString *yearEnd=[_globlDateFormat stringFromDate:tbleSem2.end_date];
        
        
        
        [_globlDateFormat setDateFormat:@"MM"];
        if(month<[[_globlDateFormat stringFromDate:tble.startDate] integerValue])
        {
            // Month Lie in Next Year i.e. Sem 2 - End Date Year
#if DEBUG
            NSLog(@"Next Year = %@",yearEnd);
#endif
            monthYear=yearEnd;
        }
        else
        {
            // Month Lie in Current Year i.e. Sem 1 - Start Date Year
#if DEBUG
            NSLog(@"Current Year = %@",yearStr);
#endif
            monthYear=yearStr;
        }
    }
    else
    {
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate]; // Get necessary date components
        
        // [components year]; // gives you year
        monthYear = [NSString stringWithFormat:@"%ld",(long)[components year]];
    }
    
    // Campus Based Start

#if DEBUG
    NSLog(@"getCurrentYear: selectedMonthIndex ===%ld  monthYear = %@",(long)selectedMonthIndex,monthYear);
#endif
    
    
    if(_buttonForGlance.selected){
        
        //--***********************************************
        NSArray *arr1=[[Service sharedInstance]getStoredAllTimeTableData];
        TimeTable *tble=[arr1 firstObject];
        TimeTable *tbleLastYear=[arr1 lastObject];
        
        NSInteger months = [self getNumberOfMonthsinBetweenStartDate:tble.startDate andEndDate:tbleLastYear.end_date];
#if DEBUG
        NSLog(@"Total months (getCurrentYear) %ld",(long)months);
        NSLog(@"getCurrentYear ForGlance selectedMonthIndex %ld",(long)selectedMonthIndex);
#endif
        //--***********************************************
        
        NSInteger newIndex = 0;
        if ((selectedMonthIndex - 4) > month)
        {
            newIndex = 12 + month;
        }
        
        if (newIndex <= months && newIndex > 0 )
        {
            monthYear = [marrOFPickerMonthYears objectAtIndex:newIndex-1];
        }
        else if (month>(months-4))
        {
            monthYear = [marrOFPickerMonthYears objectAtIndex:month-1];
        }
#if DEBUG
        NSLog(@"monthYear %@",monthYear);
#endif
    }

    // Campus Based End
    
    return monthYear;
}

#pragma mark CalendarLogic delegatet
-(void)getNewDateOfamonth:(NSInteger)month day:(NSInteger)day
{
#if DEBUG
    NSLog(@"getNewDateOfamonth");
    NSLog(@"day===%li",(long)day);
#endif
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [[[NSDateComponents alloc] init] autorelease];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSArray *daySymbols = [formatter weekdaySymbols];

    [formatter setDateFormat:@"MMM dd, yyyy"];
    
    // Campus Based Start

    NSString *yearStr=@"";
    
    if(selectedMonthIndex< [marrOFPickerMonthYears count])
    {
        yearStr=[marrOFPickerMonthYears objectAtIndex:selectedMonthIndex];
    }
    else
    {
    }
    // Campus Based End
    
    [components1 setYear:[yearStr integerValue]];
    [components1 setMonth:month];
    [components1 setDay:day];
    self._dateForPicker=[calendar dateFromComponents:components1];
    [pickerViewDay reloadAllComponents];
    
    // Code added to highlight the selected date number in picker - 2721
    //------------------------------------------------------------------
    NSInteger dayNumber = [components1 day] - 1;
    [pickerViewDay selectRow:dayNumber inComponent:1 animated:YES];
    //------------------------------------------------------------------
#if DEBUG
    NSLog(@"selected Day %ld",(long)[pickerViewDay selectedRowInComponent:1]);
#endif
    
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit) fromDate:[calendar dateFromComponents:components1]];
    
    _lableForDay.text=[NSString stringWithFormat:@"%ld",(long)[components day]];
    
    NSString *str=nil;
    [self getCureentTimeTable:[calendar dateFromComponents:components]];
    
    if(self._curentTimeTable){
        // Campus Based Start
        //---------------Block Start---------- 13 Aug modified by Mitul---------

        // New Code to fetch Cycle Lable From CalendarCycleDay Table
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        
        [formatter1 setDateFormat:@"dd/MM/yyyy"];
        NSDate *dtSelectedDate = [formatter1 dateFromString:[formatter1 stringFromDate:[calendar dateFromComponents:components]]];
#if DEBUG
        NSLog(@"date %@",dtSelectedDate);
#endif
        
        NSString *dayStr=[NSString stringWithFormat:@"%@",[self methodForGettingCalendarCycleLable:dtSelectedDate]];
        
        [formatter1 release];
        //-------------Block End------------ 13 Aug modified by Mitul-----------
        // Campus Based End

        
        if([self getCurentDayIsWeekend:[calendar dateFromComponents:components]]){
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
            
        }
        else  if([[Service sharedInstance]dayOfNotesHaveWeekend:[calendar dateFromComponents:components]])
        {
            
            
            str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
           
        }
        else{
            
            
            /**************  if condition added due to wrong data display issue week 0,null  ******/
            
            // Campus Based Start
//            if (cycleday == 0 && cycleLenght == 0) {
//                  str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
//            }
//            else
//            {
            

            str=[NSString stringWithFormat:@"%@ \n %@ \n %@",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]],dayStr];
             //}
            // Campus Based End
        }
        
    }
    else{
        str=[NSString stringWithFormat:@"%@ \n %@ \n",[daySymbols objectAtIndex:[components weekday]-1],[formatter stringFromDate:[calendar dateFromComponents:components]]];
    }
    
#if DEBUG
    NSLog(@"str %@",str);
#endif
    
    
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    self.strForDayDate=[formatter stringFromDate:[calendar dateFromComponents:components]];
    
    // TODO: [self showevent] this method calling is commented because Client needs to display only Current date events . If uncommented will display event according to date
    //[self showevent];
    [self updateCalenderlDaisy];
    
    NSArray *arrItem=[str componentsSeparatedByString:@"\n"];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                         initWithString:str];
    
    NSString *str1=[arrItem objectAtIndex:0];
    NSString *str2=[arrItem objectAtIndex:1];
    NSString *str3=[arrItem objectAtIndex:2];
    //Cycle day labels are not fully displayed in calendar day view. - start
    CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 28.0, NULL);
    CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 18.0, NULL);
    CTFontRef helveticaBold2 = CTFontCreateWithName(CFSTR("Arial"), 13.0, NULL);
    //Cycle day labels are not fully displayed in calendar day view. - end
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold
                   range:[str rangeOfString:str1]];
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold1
                   range:[str rangeOfString:str2]];
    
    // Adding Underline to date i.e. str2 9 June 2015 , EZTEST-1966
    //----------------------------------------------------------------------
    [string addAttribute:NSUnderlineStyleAttributeName
                   value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                   range:[str rangeOfString:str2]];
    //----------------------------------------------------------------------

    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold2
                   range:[str rangeOfString:str3]];
    
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                   range:[str rangeOfString:str1]];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor darkGrayColor].CGColor
                   range:[str rangeOfString:str2]];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor lightGrayColor].CGColor
                   range:[str rangeOfString:str3]];

    if (self._testlayer!=nil){
        
        [self._testlayer removeFromSuperlayer];
    }
    CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
    normalTextLayer_.string =string;
    normalTextLayer_.wrapped = YES;
    normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
    normalTextLayer_.alignmentMode = kCAAlignmentCenter;
    normalTextLayer_.frame = CGRectMake(0, 0, _lableForDate.frame.size.width,_lableForDate.frame.size.height);

    self._testlayer=normalTextLayer_;
    [_lableForDate.layer addSublayer: self._testlayer];
    [_lableForDate setNeedsDisplay];

    [string release];
    [normalTextLayer_ release];
    [formatter release];

    [self createDataForDayView];

    [_table reloadData];
    
}

-(void)getdateOfamonth:(NSInteger)month
{
#if DEBUG
    NSLog(@"getdateOfamonth month=%ld",(long)month);
#endif
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[[NSDateComponents alloc] init] autorelease];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSArray *arr=[formatter monthSymbols];
    [formatter setDateFormat:@"yyyy"];
    [_arr removeAllObjects];
    
    
    [_globlDateFormat setDateFormat:@"MM"];
    // Campus Based Start

    //--***********************************************
    NSArray *arr1=[[Service sharedInstance]getStoredAllTimeTableData];
    TimeTable *tble=[arr1 firstObject];
    TimeTable *tbleLastYear=[arr1 lastObject];
    
    NSInteger months = [self getNumberOfMonthsinBetweenStartDate:tble.startDate andEndDate:tbleLastYear.end_date];
    
#if DEBUG
    NSLog(@"Total months (getDateOfamonth) %ld",(long)months);
    NSLog(@"month %ld",(long)month);
#endif
    
    //--***********************************************
    
    if(month>(months-4))
    {
        NSInteger first,second,third,forth;
        NSInteger month1=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-4)]]] integerValue];
        NSInteger month2=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-3)]]] integerValue];
        NSInteger month3=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-2)]]] integerValue];
        NSInteger month4=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-1)]]] integerValue];
        _lableForGlancemonth1.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-4)]]] integerValue]-1];
        _lableForGlancemonth2.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-3)]]] integerValue]-1];
        _lableForGlancemonth3.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-2)]]] integerValue]-1];
        _lableForGlancemonth4.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:(months-1)]]] integerValue]-1];
        // Campus Based End
        
        
        [components setMonth:month1];
        [components setYear:[[self getCurrentYear:month1] integerValue]];
        NSRange range = [calendar rangeOfUnit:NSDayCalendarUnit
                                       inUnit:NSMonthCalendarUnit
                                      forDate:[calendar dateFromComponents:components]];
        first=range.length;
        [components setMonth:month2];
        [components setYear:[[self getCurrentYear:month2] integerValue]];
        NSRange range2 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        second=range2.length;
        [components setMonth:month3];
        [components setYear:[[self getCurrentYear:month3] integerValue]];
        NSRange range3 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        third=range3.length;
        [components setMonth:month4];
        [components setYear:[[self getCurrentYear:month4] integerValue]];
        NSRange range4 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        forth=range4.length;
        for(int i=1;i<=31;i++){
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            if(i<=first){
                [components setMonth:month1];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month1] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"1month%d",i]];
            }
            if(i<=second){
                [components setMonth:month2];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month2] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"2month%d",i]];
            }
            if(i<=third){
                [components setMonth:month3];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month3] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"3month%d",i]];
            }
            if(i<=forth){
                [components setMonth:month4];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month4] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"4month%d",i]];
            }
            [_arr addObject:dict];
            [dict release];
            
        }
    }
    else if(month<2)
    {
#if DEBUG
        NSLog(@"_arrForMonth %@",_arrForMonth);
#endif
        NSInteger first,second,third,forth;
        NSInteger month1=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:0]]] integerValue];
        NSInteger month2=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:1]]] integerValue];
        NSInteger month3=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:2]]] integerValue];
        NSInteger month4=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:3]]] integerValue];
        _lableForGlancemonth1.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:0]]] integerValue]-1];
        _lableForGlancemonth2.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:1]]] integerValue]-1];
        _lableForGlancemonth3.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:2]]] integerValue]-1];
        _lableForGlancemonth4.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:3]]] integerValue]-1];
        
        [components setMonth:month1];
        [components setYear:[[self getCurrentYear:month1] integerValue]];
        NSRange range = [calendar rangeOfUnit:NSDayCalendarUnit
                                       inUnit:NSMonthCalendarUnit
                                      forDate:[calendar dateFromComponents:components]];
        first=range.length;
        [components setMonth:month2];
        [components setYear:[[self getCurrentYear:month2] integerValue]];
        NSRange range2 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        second=range2.length;
        [components setMonth:month3];
        [components setYear:[[self getCurrentYear:month3] integerValue]];
        NSRange range3 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        third=range3.length;
        [components setMonth:month4];
        [components setYear:[[self getCurrentYear:month4] integerValue]];
        NSRange range4 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        forth=range4.length;
        for(int i=1;i<=31;i++){
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            if(i<=first){
                [components setMonth:month1];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month1] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"1month%d",i]];
            }
            if(i<=second){
                [components setMonth:month2];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month2] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"2month%d",i]];
            }
            if(i<=third){
                [components setMonth:month3];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month3] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"3month%d",i]];
            }
            if(i<=forth){
                [components setMonth:month4];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month4] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"4month%d",i]];
            }
            [_arr addObject:dict];
            [dict release];
            
        }
#if DEBUG
        NSLog(@"getDateOfaMonth _arr = %@",_arr);
#endif
        
    }
    else
    {
        NSInteger first,second,third,forth;
        NSInteger month1=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month-1]]] integerValue];
        NSInteger month2=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month]]] integerValue];
        NSInteger month3=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month+1]]] integerValue];
        NSInteger month4=[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month+2]]] integerValue];
        _lableForGlancemonth1.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month-1]]] integerValue]-1];
        _lableForGlancemonth2.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month]]] integerValue]-1];
        _lableForGlancemonth3.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month+1]]] integerValue]-1];
        _lableForGlancemonth4.text=[arr objectAtIndex:[[_globlDateFormat stringFromDate:[_globlDateFormat dateFromString:[_arrForMonth objectAtIndex:month+2]]] integerValue]-1];
        
        [components setMonth:month1];
        [components setYear:[[self getCurrentYear:month1] integerValue]];
        NSRange range = [calendar rangeOfUnit:NSDayCalendarUnit
                                       inUnit:NSMonthCalendarUnit
                                      forDate:[calendar dateFromComponents:components]];
        first=range.length;
        [components setMonth:month2];
        [components setYear:[[self getCurrentYear:month2] integerValue]];
        NSRange range2 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        second=range2.length;
        [components setMonth:month3];
        [components setYear:[[self getCurrentYear:month3] integerValue]];
        NSRange range3 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        third=range3.length;
        [components setMonth:month4];
        [components setYear:[[self getCurrentYear:month4] integerValue]];
        NSRange range4 = [calendar rangeOfUnit:NSDayCalendarUnit
                                        inUnit:NSMonthCalendarUnit
                                       forDate:[calendar dateFromComponents:components]];
        forth=range4.length;
        for(int i=1;i<=31;i++){
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            if(i<=first){
                [components setMonth:month1];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month1] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"1month%d",i]];
            }
            if(i<=second){
                [components setMonth:month2];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month2] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"2month%d",i]];
            }
            if(i<=third){
                [components setMonth:month3];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month3] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"3month%d",i]];
            }
            if(i<=forth){
                [components setMonth:month4];
                [components setDay:i];
                [components setYear:[[self getCurrentYear:month4] integerValue]];
                
                [dict setObject:[calendar dateFromComponents:components] forKey:[NSString stringWithFormat:@"4month%d",i]];
            }
            [_arr addObject:dict];
            [dict release];
            
        }
    }
    
    
    [_table reloadData];
    
    [formatter setDateFormat:@"MMMM"];
    NSDate *date1=[formatter dateFromString:_lableForGlancemonth1.text];
    NSDate *date2=[formatter dateFromString:_lableForGlancemonth4.text];
    [formatter setDateFormat:@"MM"];
    
    
    NSString *str=[NSString stringWithFormat:@"%@ %@ to %@ %@",_lableForGlancemonth1.text,[self getCurrentYear:[[formatter stringFromDate:date1] integerValue]],_lableForGlancemonth4.text,[self getCurrentYear:[[formatter stringFromDate:date2] integerValue]]];
    
#if DEBUG
    NSLog(@"str length %lu",(unsigned long)str.length);
#endif
    
    NSString *str1=_lableForGlancemonth1.text;
    NSString *str3=_lableForGlancemonth4.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                         initWithString:str];
    CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 30.0, NULL);
    CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 20.0, NULL);

    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold1
                   range:NSMakeRange(0, str.length)];
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold
                   range:[str rangeOfString:str1]];
    [string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold
                   range:[str rangeOfString:str3]];
    
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor darkGrayColor].CGColor
                   range:NSMakeRange(0, str.length)];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                   range:[str rangeOfString:str3]];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                   range:[str rangeOfString:str1]];
    
    
    
    if (self._testlayer!=nil){
        
        [self._testlayer removeFromSuperlayer];
    }
    CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
    normalTextLayer_.string =string;
    normalTextLayer_.wrapped = YES;
    normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
    normalTextLayer_.alignmentMode = kCAAlignmentCenter;
    normalTextLayer_.frame = CGRectMake(0, 0, _lableForMonthName.frame.size.width,_lableForMonthName.frame.size.height);
    self._testlayer=normalTextLayer_;
    [_lableForMonthName.layer addSublayer: self._testlayer];
    [_lableForMonthName setNeedsDisplay];
    [normalTextLayer_ release];
    [formatter release];
    
    // 2: monday
    // return _arr;
}


- (void)calendarLogic:(CalendarLogic *)aLogic dateSelected:(NSDate *)aDate {
	[selectedDate autorelease];
	selectedDate = [aDate retain];
	
	if ([calendarLogic distanceOfDateFromCurrentMonth:selectedDate] == 0) {
		[self selectButtonForDate:selectedDate];
	}
	
	//[calendarViewControllerDelegate calendarViewController:self dateDidChange:aDate];
}
- (void)calendarLogic:(CalendarLogic *)aLogic monthChangeDirection:(NSInteger)aDirection {
	
	CGFloat distance = 320;
	if (aDirection < 0) {
		distance = -distance;
	}
	
    //CalendarMonth *aCalendarView = [[CalendarMonth alloc] initWithFrame:CGRectMake(distance, 0, 320, 308) logic:aLogic];
	for(UIView *view1 in self.calendarView.subviews){
        [view1 removeFromSuperview];
    }
	self.calendarLogic=aLogic;
    [self createViewForCalender];
	if ([calendarLogic distanceOfDateFromCurrentMonth:selectedDate] == 0) {
		[self selectButtonForDate:selectedDate];
	}
	//[self.view insertSubview:aCalendarView belowSubview:calendarView];
	
	//self.calendarViewNew = aCalendarView;
	//[aCalendarView release];
	
    //	if (animate) {
    //		[UIView beginAnimations:NULL context:nil];
    //		[UIView setAnimationDelegate:self];
    //		[UIView setAnimationDidStopSelector:@selector(animationMonthSlideComplete)];
    //		[UIView setAnimationDuration:0.3];
    //		[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //	}
    //
    //	self.calendarView.frame = CGRectOffset(calendarView.frame, -distance, 0);
    //
    //	if (animate) {
    //		[UIView commitAnimations];
    //
    //	} else {
    //		[self animationMonthSlideComplete];
    //	}
}

- (void)animationMonthSlideComplete {
	// Get rid of the old one.
	//[calendarView removeFromSuperview];
	
	// replace
	//self.calendarView = calendarViewNew;
	//self.calendarViewNew = nil;
    
	
	calendarView.userInteractionEnabled = YES;
}

- (IBAction)viewAllButtonAction:(id)sender {
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Daisy" message:@"In Progress" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
    [alert release];
}
- (void)dayOfWeekFromDate:(NSInteger)week {
    NSDate *startDate=[[_arrForweek objectAtIndex:week] objectForKey:@"start"];
    if (_arr)
    {
        [_arr removeAllObjects];
    }
    
    for(int i=1;i<=7;i++){
        
        [_arr addObject:startDate];
        startDate = [startDate dateByAddingTimeInterval:(60 * 60 * 24)];
    }
    [_table reloadData];
    [self setWeekName:week];
    
}

#pragma mark calenderView
-(void)setSelectedMonth:(NSInteger)value
{
    int k=0;
    if(_buttonFormonth.selected || _buttonForGlance.selected)
    {
        for( int i=0;i<12;i++)
        {
            UIView *view1=(UIView*)[pickerViewDay viewForRow:i forComponent:0];
            UILabel *name=(UILabel*)[view1 viewWithTag:1];
            name.textColor=[UIColor darkGrayColor];
            name.alpha=1;
        }
        for(NSInteger i=value+1;i<12;i++)
        {
            UIView *view1=(UIView*)[pickerViewDay viewForRow:i forComponent:0];
            UILabel *name=(UILabel*)[view1 viewWithTag:1];
            
            k=k+1;
            if(name){
                if(k==1){
                    name.alpha=1;
                }
                else  if(k==2){
                    name.alpha=0.6;
                }
                else  if(k==3){
                    name.alpha=0.3;
                }
            }
            
        }
        k=0;
        for(NSInteger i=value-1;i>=0;i--){
            UIView *view1=(UIView*)[pickerViewDay viewForRow:i forComponent:0];
            UILabel *name=(UILabel*)[view1 viewWithTag:1];
            k=k+1;
            
            if(name){
                if(k==1){
                    name.alpha=1;
                }
                else  if(k==2){
                    name.alpha=0.6;
                }
                else  if(k==3){
                    name.alpha=0.3;
                }
            }
        }
    }
    else    if(_buttonForWeek.selected)
    {
        
        
        value=[pickerViewDay selectedRowInComponent:0];
        for( int i=0;i<[pickerViewDay numberOfRowsInComponent:0];i++){
            UIView *view1=(UIView*)[pickerViewDay viewForRow:i forComponent:0];
            UILabel *name=(UILabel*)[view1 viewWithTag:1];
            name.textColor=[UIColor darkGrayColor];
            name.alpha=1;
            UILabel *name2=(UILabel*)[view1 viewWithTag:2];
            name2.textColor=[UIColor darkGrayColor];
            name2.alpha=1;
        }
        for(NSInteger i=value+1;i<[pickerViewDay numberOfRowsInComponent:0];i++){
            UIView *view1=(UIView*)[pickerViewDay viewForRow:i forComponent:0];
            UILabel *name=(UILabel*)[view1 viewWithTag:1];
            UILabel *name2=(UILabel*)[view1 viewWithTag:2];
            
            k=k+1;
            if(name){
                if(k==1){
                    name.alpha=1;
                    name2.alpha=1;
                }
                else  if(k==2){
                    name.alpha=0.6;
                    name2.alpha=0.6;
                }
                else  if(k==3){
                    name.alpha=0.3;
                    name2.alpha=0.3;
                }
            }
            
        }
        k=0;
        for(NSInteger i=value-1;i>=0;i--){
            UIView *view1=(UIView*)[pickerViewDay viewForRow:i forComponent:0];
            UILabel *name=(UILabel*)[view1 viewWithTag:1];
            k=k+1;
            UILabel *name2=(UILabel*)[view1 viewWithTag:2];
            
            if(k==1){
                name.alpha=1;
                name2.alpha=1;
            }
            else  if(k==2){
                name.alpha=0.6;
                name2.alpha=0.6;
            }
            else  if(k==3){
                name.alpha=0.3;
                name2.alpha=0.3;
                
            }
        }
        UIView *view1=(UIView*)[pickerViewDay viewForRow:[pickerViewDay selectedRowInComponent:0] forComponent:0];
        
        UILabel *name=(UILabel*)[view1 viewWithTag:1];
        UILabel *name2=(UILabel*)[view1 viewWithTag:2];
        name.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
        name2.textColor=[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1];
    }
    
}
-(void)setupWeekname{
    // Setup weekday names
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSArray *daySymbols = [formatter weekdaySymbols];
    [calendar setFirstWeekday:1];
    NSInteger firstWeekday = [calendar firstWeekday] ;

    for (NSInteger aWeekday = 0; aWeekday < numberOfDaysInWeek; aWeekday ++)
    {
        NSInteger symbolIndex = aWeekday + firstWeekday;
        if (symbolIndex >= numberOfDaysInWeek)
        {
            symbolIndex -= numberOfDaysInWeek;
        }
        
        NSString *symbol = [daySymbols objectAtIndex:symbolIndex];
        CGFloat positionX = (aWeekday * kCalendarDayWidth) - 1;
        CGRect aFrame = CGRectMake(positionX, 0, kCalendarDayWidth, 20);
        UIView *dayView=[[UIView alloc]initWithFrame:aFrame];
        dayView.backgroundColor=[UIColor clearColor];
        UIImageView *dayImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, aFrame.size.width-1, aFrame.size.height)];
        dayImgView.backgroundColor=[UIColor clearColor];
        if(aWeekday==0)
        {
            dayImgView.image=[UIImage imageNamed:@"monthscreendaysnamebase1"];
        }
        else if(aWeekday==6)
        {
            dayImgView.image=[UIImage imageNamed:@"monthscreendaysnamebase3"];
        }
        else
        {
            dayImgView.image=[UIImage imageNamed:@"monthscreendaysnamebase2"];
        }
        
        UILabel  *aLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, aFrame.size.width, aFrame.size.height)];
        aLabel.backgroundColor = [UIColor clearColor];
        
        aLabel.textAlignment = NSTextAlignmentCenter;
        aLabel.text = symbol;
        aLabel.textColor = [UIColor darkGrayColor];
        aLabel.font = [UIFont systemFontOfSize:12];
        aLabel.shadowColor = [UIColor whiteColor];
        aLabel.shadowOffset = CGSizeMake(0, 1);
        [dayView addSubview:dayImgView];
        [dayView addSubview:aLabel];
        
        [self.calendarView addSubview:dayView];
        [dayImgView release];
        [aLabel release];
        [dayView release];
    }
    [formatter release];
    
}
-(void)createViewForCalender{
	
	// Size is static
	NSInteger numberOfWeeks = 5;
	selectedButton = -1;
    NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit |NSWeekCalendarUnit |NSWeekdayCalendarUnit |NSWeekdayOrdinalCalendarUnit |NSWeekOfMonthCalendarUnit |NSWeekOfYearCalendarUnit |NSYearForWeekOfYearCalendarUnit) fromDate:[NSDate date]];
    
	NSDate *todayDate = [calendar dateFromComponents:components];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSArray *daySymbols = [formatter weekdaySymbols];
    self.numberOfDaysInWeek = [daySymbols count];
    [formatter setDateFormat:@"MM"];
    
    [formatter setDateFormat:@"MMMM yyyy"];

    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                         initWithString:[formatter stringFromDate:self.calendarLogic .referenceDate]];
	// make a few words bold
	CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial-BoldMT"), 35.0, NULL);
    NSArray *arr=[[formatter stringFromDate:self.calendarLogic .referenceDate] componentsSeparatedByString:@" "];

    NSString *str1=[arr objectAtIndex:0];
    NSString *str2=[arr objectAtIndex:1];
    
	[string addAttribute:(id)kCTFontAttributeName
                   value:(id)helveticaBold
                   range:NSMakeRange(0, [string length])];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor colorWithRed:85.0/255.0 green:120.0/255.0 blue:200.0/255.0 alpha:1].CGColor
                   range:NSMakeRange(0, str1.length)];
    [string addAttribute:(id)kCTForegroundColorAttributeName
                   value:(id)[UIColor darkGrayColor].CGColor
                   range:NSMakeRange(str1.length, str2.length+1)];
    if (self._testlayer!=nil){
        
        [self._testlayer removeFromSuperlayer];
    }
    CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
    normalTextLayer_.string =string;
    
    normalTextLayer_.wrapped = YES;
    normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
    normalTextLayer_.alignmentMode = kCAAlignmentCenter;
    normalTextLayer_.frame = CGRectMake(_lableForMonthName.frame.origin.x, _lableForMonthName.frame.origin.x, _lableForMonthName.frame.size.width-5,_lableForMonthName.frame.size.height-8);
    self._testlayer=normalTextLayer_;
    [_lableForMonthName.layer addSublayer: self._testlayer];
    [_lableForMonthName setNeedsDisplay];
    [string release];
    [normalTextLayer_ release];
    //set
    [self setupWeekname];
    
    // Build calendar buttons (6 weeks of 7 days)
    NSMutableArray *aDatesIndex = [[[NSMutableArray alloc] init] autorelease];
    NSMutableArray *aButtonsIndex = [[[NSMutableArray alloc] init] autorelease];
    
    for (NSInteger aWeek = 0; aWeek <=numberOfWeeks; aWeek ++) {
        //Cycle labels are not displaying in calendar Month and Glance views - start
        CGFloat positionY = (aWeek * kCalendarDayHeight) + 20;
        //Cycle labels are not displaying in calendar Month and Glance views - end
        
        for (NSInteger aWeekday = 1; aWeekday <= numberOfDaysInWeek; aWeekday ++) {
            CGFloat positionX = ((aWeekday - 1) * kCalendarDayWidth) - 1;
            CGRect dayFrame = CGRectMake(positionX, positionY, kCalendarDayWidth, kCalendarDayHeight);
            
            // Modified By Mitul to Fix 2299 - on 1 Nov(Sat) === Start ======
            // MonthView Day Column Date Entries issue because of adding 1 day in aWeekday
            // checked/Fixed for Honeycomb Build Need to uncomment the code for Daisy With +1 And Check/Fix for Prime
            // Problem is with UK Region Only
            // Code Commented for ref and added new code with only aWeeday
            
            // For Daisy - Supprting Australia Region
            NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday+1
                                                     onWeek:aWeek-1
                                              referenceDate:[self.calendarLogic referenceDate]];
            
            
            // For honeyComb - Supprting UK Region
           
            /*NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday
                                                     onWeek:aWeek-1
                                              referenceDate:[self.calendarLogic referenceDate]];*/
           
            //=== End of Modification of Code ======
            
            NSDateFormatter *dayDateFrmt = [[[NSDateFormatter alloc] init] autorelease];
            [dayDateFrmt setDateFormat:@"yyyy-MM-dd"];
            
            
            
            NSDateComponents *dayComponents = [calendar
                                               components:NSDayCalendarUnit fromDate:dayDate];
            
            
            
            UIColor *titleColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CalendarTitleColor.png"]];
            if ([self.calendarLogic distanceOfDateFromCurrentMonth:dayDate] != 0) {
                titleColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"CalendarTitleDimColor.png"]];
            }
            
            UIButton *dayButton = [UIButton buttonWithType:UIButtonTypeCustom];
            dayButton.opaque = YES;
            dayButton.clipsToBounds = NO;
            dayButton.clearsContextBeforeDrawing = NO;
            dayButton.frame = CGRectMake(0, 0, dayFrame.size.width, dayFrame.size.height);
            dayButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
            dayButton.titleLabel.font = [UIFont boldSystemFontOfSize:20];
            dayButton.tag = [aDatesIndex count];
            dayButton.adjustsImageWhenHighlighted = NO;
            dayButton.adjustsImageWhenDisabled = NO;
            dayButton.showsTouchWhenHighlighted = YES;
            
            
            [dayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            [dayButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateSelected];
            
            if ([dayDate compare:todayDate] != NSOrderedSame) {
                // Normal
                [dayButton setTitleColor:titleColor forState:UIControlStateNormal];
                [dayButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
                if(aWeekday<numberOfDaysInWeek){
                    [dayButton setBackgroundImage:[UIImage imageNamed:@"monthscreendaysbox"] forState:UIControlStateNormal];
                }
                else{
					[dayButton setBackgroundImage:[UIImage imageNamed:@"monthscreendaysboxsunday"] forState:UIControlStateNormal];
                }
                
            }
            else
            {
                // Normal
                [dayButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [dayButton setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
                [dayButton setBackgroundImage:[UIImage imageNamed:@"monthscreenselector"] forState:UIControlStateNormal];
                
				
            }
            
            
            [dayButton addTarget:self action:@selector(dayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self.calendarView addSubview:dayButton];
            //customization
            UIView *view1 = [[UIView alloc] init] ;
            [view1 setFrame:dayFrame];
            view1.backgroundColor=[UIColor clearColor];
            // Comment code after debug
            view1.backgroundColor = [UIColor lightGrayColor];
            
            UILabel *englishDate =[[UILabel alloc] initWithFrame:CGRectMake(2, 2, 25, 25)];;
            [englishDate setFont:[UIFont fontWithName:@"Arial" size:12]];
            englishDate.backgroundColor = [UIColor clearColor];
            englishDate.textAlignment = NSTextAlignmentCenter;
            if ([self.calendarLogic distanceOfDateFromCurrentMonth:dayDate] != 0)
            {
                englishDate.textColor = [UIColor grayColor];
                // dayButton.enabled=NO;
            }
            else
            {
                if ([dayDate compare:todayDate] == NSOrderedSame)
                {
                    englishDate.textColor = [UIColor redColor];
                }else
                {
                    englishDate.textColor = [UIColor blackColor];
                }
                
            }
            
            englishDate.text=[NSString stringWithFormat:@"%ld", (long)[dayComponents day]];
            view1.clipsToBounds=YES;
            [view1 addSubview:dayButton];
            [view1 addSubview:englishDate];
            [englishDate release];
            
            //for add dairy items on month
            NSMutableArray *arrFordairyItem=[[NSMutableArray alloc]init];
            [formatter setDateFormat:@"dd/MM/yyyy"];
            NSString *assinegdDate=[formatter stringFromDate:dayDate];
            //Important
            //4julyimportant done
            NSArray  *itemArr=nil;
            NSArray  *itemDueArr=nil;
            if (((_buttonFortaskFilter.selected)&&(_buttonForFilterNotes.selected))||(_buttonFortaskFilter.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
#if DEBUG
                NSLog(@"inside if To get Item Array...");
#endif
                itemArr= [[Service sharedInstance] getStoredAllItemData:[formatter dateFromString:assinegdDate]];
#if DEBUG
                NSLog(@"itemArr = %@",itemArr);
#endif
                itemDueArr= [[Service sharedInstance] getStoredAllDueItemData:[formatter dateFromString:assinegdDate]];
#if DEBUG
                NSLog(@"itemDueArr = %@",itemDueArr);
#endif

            }
            //////////////////////////////////////////////
            
            [arrFordairyItem removeAllObjects];
            for (Item *itemObj in itemArr)
            {
                if (itemObj.assignedDate)
                {
                    if (![itemObj.type isEqualToString:@"Note"])
                    {
                        [arrFordairyItem addObject:itemObj];
                    }
                }
            }
            
            for (Item *itemObj in itemDueArr) {
                if (itemObj.assignedDate) {
                    if (![itemObj.type isEqualToString:@"Note"]) {
                        if (![arrFordairyItem containsObject:itemObj]) {
                            [arrFordairyItem addObject:itemObj];
                        }
                    }
                }
            }
            
            ///////////////////////////////////////////////////////////////////////////////
#if DEBUG
            NSLog(@"arrFordairyItem = %@",arrFordairyItem);
#endif
            if (arrFordairyItem.count>0) {
                aDatesIndexForMonth=[aDatesIndex count];
                
                [self addDairyItemForMonthMethod:arrFordairyItem  dateIndex:aDatesIndexForMonth currDate:dayDate view:view1];
            }
            
            //for add Events items on month
            
            //Important  done
            NSArray  *eventItemArr=nil;
            if ((_buttonForFilterEvent.selected)||((!_buttonFortaskFilter.selected)&&(!_buttonForFilterNotes.selected)&&(!_buttonForFilterEvent.selected)))
            {
                NSArray *arrEvents= [[Service sharedInstance] getStoredAllEventItemData:[formatter dateFromString:assinegdDate]];
                NSMutableArray *arrMajorEvents = [[NSMutableArray alloc]init];
                NSMutableArray *arrMinorEvents = [[NSMutableArray alloc]init];
                
                for (EventItem *event in arrEvents)
                {
                    if ([event.priority isEqualToString:@"1"])
                        [arrMajorEvents addObject:event];
                    else
                        [arrMinorEvents addObject:event];
                }
                
                NSSortDescriptor *recentAddedDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
                NSArray *sortDescriptors = @[recentAddedDescriptor];
                
                NSArray *majorEvents = [arrMajorEvents sortedArrayUsingDescriptors:sortDescriptors];
                NSArray *minorEvents = [arrMinorEvents sortedArrayUsingDescriptors:sortDescriptors];
                
                eventItemArr = [majorEvents arrayByAddingObjectsFromArray:minorEvents];

            }
            //////////////////////////////////////////////////
            
            if (eventItemArr.count>0) {
                
                [self addEventsForMonthMethod:eventItemArr itemArr:arrFordairyItem  view:view1] ;
                //  [view1 addSubview:cellDescView];
            }
            
            [arrFordairyItem release];
            
            NSDateFormatter *dateFrmt = [[[NSDateFormatter alloc] init] autorelease];
            [dateFrmt setDateFormat:@"yyyy-MM-dd"];
            
            
            [self addDayonMonthScreen:dayDate view:view1];
            
            [self.calendarView addSubview:view1];
            [view1 release];
          

            // Save
            [aDatesIndex addObject:dayDate];
            [aButtonsIndex addObject:dayButton];
        }
    }
    
    [formatter release];
    

    // save
    self.datesIndex = [[aDatesIndex copy] autorelease];
    self.buttonsIndex = [[aButtonsIndex copy] autorelease];
    
}
-(void)addDayonMonthScreen:(NSDate*)date view:(UIView*)view
{
        //Cycle labels are not displaying in calendar Month and Glance views - start
    UILabel *dayLable=[[UILabel alloc]initWithFrame:CGRectMake(2, 71, 120, 14)];
        //Cycle labels are not displaying in calendar Month and Glance views - end
    dayLable.backgroundColor=[UIColor clearColor];
    dayLable.textColor=[UIColor lightGrayColor];
    [dayLable setFont:[UIFont fontWithName:@"Arial-BoldMT" size:9]];
    
#if DEBUG
    NSLog(@"addDayonMonthScreen dayDate %@",date);
#endif

    
    [self getCureentTimeTable:date];
    if(self._curentTimeTable)
    {
#if DEBUG
        NSLog(@"self._curentTimeTable addDayonMonthScreen dayDate %@",date);
#endif
      
        // Campus Based Start
        CalendarCycleDay *dayCycle =[self methodForCalendarCycleDay:date];
        // Campus Based End

        if([self getCurentDayIsWeekend:date]){
            dayLable.text=nil;
        }
        else  if([[Service sharedInstance]dayOfNotesHaveWeekend:date]){
            dayLable.text=nil;
        }
        else{
                    //Cycle labels are not displaying in calendar Month and Glance views - start
            dayLable.text=dayCycle.formatedCycleDayLabel;//[NSString stringWithFormat:@"%@",[self methodForGetiingShortlable:cycleday]];
                    //Cycle labels are not displaying in calendar Month and Glance views - end
        }
    }
    else{
        dayLable.text=nil;
    }
#if DEBUG
    NSLog(@"addDayonMonthScreen dayLable.text %@",dayLable.text);
#endif

    [view addSubview:dayLable];
    [dayLable release];
}

#pragma  mark Add Dairy Item For Month Method
//9julyBUG
-(void)addDairyItemForMonthMethod:(NSArray*)itemArr dateIndex:(NSInteger)aDatesIndex currDate:(NSDate*)dayDate view:(UIView*)view1
{
    
    //22AUG2013
#if DEBUG
    NSLog(@"addDairyItemForMonthMethod dayDate=%@",dayDate);
#endif
    
    int x=25;
    for(int i=0; i<itemArr.count;i++)
    {
        Item *itemObj=[itemArr objectAtIndex:i];
        if (itemArr.count<3) {
            
            UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, 30, 40, 40)];
            cellDescView.backgroundColor=[UIColor clearColor];
            
            
            UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
            cellDescBgImgView.backgroundColor=[UIColor clearColor];
            
            UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            iconImgView.center=cellDescBgImgView.center;
            
            //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
            MyClass *class1=nil;
            
            class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
            if(class1.code!=nil)
                cellDescBgImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
            else{
                cellDescBgImgView.backgroundColor=[UIColor grayColor];
            }
            
            //--------- By Mitul ---------//

            cellDescBgImgView.contentMode=UIViewContentModeScaleAspectFit;
            [cellDescBgImgView.layer setMasksToBounds:YES];
            [cellDescBgImgView.layer setCornerRadius:3.0f];
            [cellDescBgImgView.layer setBorderWidth:1.0];
            [cellDescBgImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];
            
            UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
            popUpButton.frame=CGRectMake(0, 0, 44, 44);
            popUpButton.backgroundColor=[UIColor clearColor];
            [popUpButton addTarget:self action:@selector(showPopupButtonPressedForWeekAndMonth:) forControlEvents:UIControlEventTouchUpInside];
            
            popUpButton.titleLabel.textColor=[UIColor clearColor];
            
            
            NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
            NSDateFormatter *formeterWithDateAndTime = [[[NSDateFormatter alloc] init] autorelease];
            
            NSString *isTypeStr= itemObj.type;
            if(![itemObj.type isEqualToString:@"Note"])
            {
                
                [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                [formeterWithDateAndTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                
                long timeDiff1;
                
                timeDiff1=(int)[[formeter2 dateFromString:[formeterWithDateAndTime stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:dayDate]]];
                
                if(timeDiff1<=0)
                {
                    iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                    if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                    {
                        
                        if([itemObj.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                }
                else{
                    if ([isTypeStr isEqualToString:@"Assignment"]) {
                        
                        iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"Homework"]) {
                        iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                    }
                    else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                        
                        if([itemObj.isApproved integerValue] == 0)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 1)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                        }
                        else  if([itemObj.isApproved integerValue] == 2)
                        {
                            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                        }
                    }
                    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                        if (lov)
                        {
                            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                iconImgView.image=[FileUtils getImageFromPath:strPath];
                            }
                        }
                    }
                }
            }
            
            
            
            if([itemObj.itemId integerValue]>0){
                [formeter2 setDateFormat:@"dd/MM/yyyy"];
                NSString *curentDate=[formeter2 stringFromDate:dayDate];
                
                popUpButton.tag=[itemObj.itemId integerValue];
                //popUpButton.titleLabel.text=@"1";
                popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"1",curentDate];
                [AppDelegate getAppdelegate]._isUniqueId=YES;
            }
            else{
                [formeter2 setDateFormat:@"dd/MM/yyyy"];
                NSString *curentDate=[formeter2 stringFromDate:dayDate];
                
                popUpButton.tag=[itemObj.universal_Id integerValue];
                //popUpButton.titleLabel.text=@"0";
                popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"0",curentDate];
                [AppDelegate getAppdelegate]._isUniqueId=NO;
                
            }
            
            [formeter2 release];
            
            
            
            [cellDescView addSubview:popUpButton];
            
            x=x+50;
            
            [cellDescView addSubview:cellDescBgImgView];
            [cellDescView addSubview:iconImgView];
            
            
            cellDescView.clipsToBounds=YES;
            [view1 addSubview:cellDescView];
            [cellDescBgImgView release];
            [iconImgView release];
            [cellDescView release];
            
            
        }
        else
        {
            
            UIView * cellDescView=[[UIView alloc]initWithFrame:CGRectMake(x, 30, 40, 40)];
            cellDescView.backgroundColor=[UIColor clearColor];
            
            UIView * viewForCountLbl=[[UIView alloc]initWithFrame:CGRectMake(22, -4, 20, 20)];
            viewForCountLbl.backgroundColor=[UIColor clearColor];
            
            
            UIImageView * imgViewForCountLbl=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            imgViewForCountLbl.backgroundColor=[UIColor clearColor];
            [imgViewForCountLbl setImage:[UIImage imageNamed:@"notificationdot.png"]];
            
            UILabel *lableForCount=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            lableForCount.backgroundColor=[UIColor clearColor];
            lableForCount.textAlignment=NSTextAlignmentCenter;
            lableForCount.textColor=[UIColor whiteColor];
            lableForCount.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
            lableForCount.center=imgViewForCountLbl.center;
            
            [viewForCountLbl addSubview:imgViewForCountLbl];
            [viewForCountLbl addSubview:lableForCount];
            
            [imgViewForCountLbl release];
            [lableForCount release];
            
            
            
            UIImageView * cellDescBgImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
            cellDescBgImgView.backgroundColor=[UIColor clearColor];
            
            UIImageView * iconImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
            iconImgView.center=cellDescBgImgView.center;
            
            //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
            MyClass *class1=nil;
            
            class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
            if(class1.code!=nil)
                cellDescBgImgView.backgroundColor=[AppHelper colorFromHexString:class1.code];
            else{
                cellDescBgImgView.backgroundColor=[UIColor grayColor];
            }
            
            //--------- By Mitul ---------//

            cellDescBgImgView.contentMode=UIViewContentModeScaleAspectFit;
            [cellDescBgImgView.layer setMasksToBounds:YES];
            [cellDescBgImgView.layer setCornerRadius:3.0f];
            [cellDescBgImgView.layer setBorderWidth:1.0];
            [cellDescBgImgView.layer setBorderColor:[[UIColor clearColor] CGColor]];
            
            if (i==0)
            {
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 44, 44);
                popUpButton.backgroundColor=[UIColor clearColor];
                [popUpButton addTarget:self action:@selector(showPopupButtonPressedForWeekAndMonth:) forControlEvents:UIControlEventTouchUpInside];
                
                popUpButton.titleLabel.textColor=[UIColor clearColor];
                
                NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                NSDateFormatter *formeterWithDateAndTime = [[NSDateFormatter alloc] init];
                
                NSString *isTypeStr= itemObj.type;
                if(![itemObj.type isEqualToString:@"Note"])
                {
                    
                    [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                    [formeterWithDateAndTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                    
                    long timeDiff1;
                    
                    timeDiff1=(int)[[formeter2 dateFromString:[formeterWithDateAndTime stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:dayDate]]];
                    
                    
                    if(timeDiff1<=0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                        
                        if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            if([itemObj.isApproved integerValue] == 0)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    else{
                        if ([isTypeStr isEqualToString:@"Assignment"])
                        {
                            iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"Homework"]) {
                            iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"]) {
                            
                            if([itemObj.isApproved integerValue] == 0)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                }
                
                
                if([itemObj.itemId integerValue]>0){
                    [formeter2 setDateFormat:@"dd/MM/yyyy"];
                    NSString *curentDate=[formeter2 stringFromDate:dayDate];
                    
                    popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"1",curentDate];
                    popUpButton.tag=[itemObj.itemId integerValue];
                    //popUpButton.titleLabel.text=@"1";
                    [AppDelegate getAppdelegate]._isUniqueId=YES;
                }
                else{
                    [formeter2 setDateFormat:@"dd/MM/yyyy"];
                    NSString *curentDate=[formeter2 stringFromDate:dayDate];
                    
                    popUpButton.tag=[itemObj.universal_Id integerValue];
                    popUpButton.titleLabel.text=[NSString stringWithFormat:@"%@    %@",@"0",curentDate];
                    //popUpButton.titleLabel.text=@"0";
                    [AppDelegate getAppdelegate]._isUniqueId=NO;
                    
                }
                
                [formeter2 release];
                
                
                [cellDescView addSubview:popUpButton];
                
                x=x+50;
                
                [cellDescView addSubview:cellDescBgImgView];
                [cellDescView addSubview:iconImgView];
                
                cellDescView.clipsToBounds=YES;
                [view1 addSubview:cellDescView];
            }
            else if (i==1)
            {
                NSString *isTypeStr= itemObj.type;
                if(![itemObj.type isEqualToString:@"Note"])
                {
                    NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
                    NSDateFormatter *formeterWithDateAndTime = [[NSDateFormatter alloc] init];
                    
                    
                    [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                    [formeterWithDateAndTime setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                    
                    long timeDiff1;
                    
                    timeDiff1=(int)[[formeter2 dateFromString:[formeterWithDateAndTime stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:dayDate]]];
                    [formeter2 release];
                    
                    if(timeDiff1<=0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                        
                        if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            if([itemObj.isApproved integerValue] == 0)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                    else{
                        if ([isTypeStr isEqualToString:@"Assignment"]) {
                            
                            iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"Homework"]) {
                            iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
                        }
                        else if([isTypeStr isEqualToString:@"OutOfClass"] || [isTypeStr isEqualToString:@"Out Of Class"])
                        {
                            if([itemObj.isApproved integerValue] == 0)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 1)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
                            }
                            else  if([itemObj.isApproved integerValue] == 2)
                            {
                                iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
                            }
                        }
                        else if([isTypeStr isEqualToString:k_MERIT_CHECK])
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                                    iconImgView.image=[FileUtils getImageFromPath:strPath];
                                }
                            }
                        }
                    }
                }
                
                UIButton *popUpButton=[UIButton buttonWithType:UIButtonTypeCustom];
                popUpButton.frame=CGRectMake(0, 0, 44, 44);
                popUpButton.backgroundColor=[UIColor clearColor];
                popUpButton.tag = aDatesIndex;
                popUpButton.titleLabel.text=@"more";
                [popUpButton addTarget:self action:@selector(showAllDiaryItemButtonPressedForMonth:) forControlEvents:UIControlEventTouchUpInside];
                
                lableForCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)(itemArr.count-1)];
                
                
                
                x=x+50;
                
                [cellDescView addSubview:cellDescBgImgView];
                [cellDescView addSubview:iconImgView];
                [cellDescView addSubview:viewForCountLbl];
                [cellDescView addSubview:popUpButton];
                
                cellDescView.clipsToBounds=NO;
                [view1 addSubview:cellDescView];
            }
            
            
            
            [viewForCountLbl release];
            [cellDescBgImgView release];
            [iconImgView release];
            [cellDescView release];
            
            
            
        }
        
        
    }
    
}

#pragma  mark Add Events Item For Month
-(void)addEventsForMonthMethod:(NSArray*)eventsArr itemArr:(NSArray*)itemArray view:(UIView*)view1
{
    int x=10;
    int y=5;
    for(int i=0; i<eventsArr.count;i++)
    {
        int noOfEventCount=0;
        
        EventItem *evensItemObj=[eventsArr objectAtIndex:i];
        
        if(itemArray.count==0)
        {
            
            if (i<3)
            {
                UIView * viewForEvent=[[[UIView alloc]initWithFrame:CGRectMake(x, y, 100, 30)]autorelease];
                viewForEvent.backgroundColor=[UIColor clearColor];
                
                UIImageView * imgViewForIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
                imgViewForIcon.backgroundColor=[UIColor clearColor];
                
                UILabel * lblForEventName=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 95, 20)];
                lblForEventName.backgroundColor=[UIColor clearColor];
                lblForEventName.textColor=[UIColor blueColor];
                lblForEventName.font=[UIFont fontWithName:@"Arial" size:11.0];
                lblForEventName.numberOfLines=1;
                
                //Aman added
                if ([evensItemObj.priority isEqualToString:@"1"])
                {
                    lblForEventName.font = [UIFont fontWithName:@"Arial-BoldMT" size:11.0];
                }
                
                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                
                [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                
                // 24Hrs
                [formeter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                [formeter setTimeStyle:NSDateFormatterShortStyle];
                [formeter setDateFormat:NSDateFormatterNoStyle];
                
                // 24Hrs
                
                if ([evensItemObj.onTheDay integerValue]==0)
                {
                    // Code modified to implement UK + 12 Hr Format
                    lblForEventName.text=[NSString stringWithFormat:@"%@ (%@ to %@)",evensItemObj.eventName,[formeter stringFromDate:evensItemObj.startDate],[formeter stringFromDate:evensItemObj.endDate]];
                  
                }
                else{
                    lblForEventName.text=evensItemObj.eventName;
                }
                
                [formeter release];
                
                UIButton *btnForEvents=[UIButton buttonWithType:UIButtonTypeCustom];
                btnForEvents.frame=CGRectMake(10, 0, 100, 20);
                btnForEvents.backgroundColor=[UIColor clearColor];
                [btnForEvents addTarget:self action:@selector(editEventMethod:) forControlEvents:UIControlEventTouchUpInside];

                if([evensItemObj.item_Id integerValue]>0){
                    btnForEvents.tag=[evensItemObj.item_Id integerValue];
                    btnForEvents.titleLabel.text=@"1";
                    [AppDelegate getAppdelegate]._isUniqueId=YES;
                }
                else{
                    btnForEvents.tag=[evensItemObj.universal_id integerValue];
                    btnForEvents.titleLabel.text=@"0";
                    [AppDelegate getAppdelegate]._isUniqueId=NO;
                }
                
                [btnForEvents addSubview:lblForEventName];
                [viewForEvent addSubview:btnForEvents];
                
                
                y=y+20;
                
                [viewForEvent addSubview:imgViewForIcon];
                
                viewForEvent.clipsToBounds=NO;
                [view1 addSubview:viewForEvent];
                
                [imgViewForIcon release];
                [lblForEventName release];
                noOfEventCount++;
            }
        }
        else
        {
            if (i<1) {
                UIView * viewForEvent=[[[UIView alloc]initWithFrame:CGRectMake(x, y, 100, 30)]autorelease];
                viewForEvent.backgroundColor=[UIColor clearColor];
                
                UIImageView * imgViewForIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
                imgViewForIcon.backgroundColor=[UIColor clearColor];
                
                UILabel * lblForEventName=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 95, 20)];
                lblForEventName.backgroundColor=[UIColor clearColor];
                lblForEventName.textColor=[UIColor blueColor];
                lblForEventName.font=[UIFont fontWithName:@"Arial" size:11.0];
                lblForEventName.numberOfLines=1;
                
                //Aman added
                if ([evensItemObj.priority isEqualToString:@"1"])
                {
                    lblForEventName.font = [UIFont fontWithName:@"Arial-BoldMT" size:11.0];
                }

                NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
                [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                
                [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
                
                // 24Hrs
                [formeter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
                [formeter setTimeStyle:NSDateFormatterShortStyle];
                [formeter setDateFormat:NSDateFormatterNoStyle];
                
                // 24Hrs
                
                if ([evensItemObj.onTheDay integerValue]==0)
                {
                    if ((evensItemObj.startTime)&&(evensItemObj.endTime)) {
                        lblForEventName.text=[NSString stringWithFormat:@"%@ (%@ to %@)",evensItemObj.eventName,[formeter stringFromDate:evensItemObj.startDate],[formeter stringFromDate:evensItemObj.endDate]];
                    }
                    else{
                        lblForEventName.text=[NSString stringWithFormat:@"%@",evensItemObj.eventName];
                    }
                }
                else{
                    lblForEventName.text=evensItemObj.eventName;
                }
                
                [formeter release];
                
                UIButton *btnForEvents=[UIButton buttonWithType:UIButtonTypeCustom];
                btnForEvents.frame=CGRectMake(10, 0, 100, 20);
                btnForEvents.backgroundColor=[UIColor clearColor];
                [btnForEvents addTarget:self action:@selector(editEventMethod:) forControlEvents:UIControlEventTouchUpInside];
                if([evensItemObj.item_Id integerValue]>0){
                    btnForEvents.tag=[evensItemObj.item_Id integerValue];
                    btnForEvents.titleLabel.text=@"1";
                    [AppDelegate getAppdelegate]._isUniqueId=YES;
                }
                else{
                    btnForEvents.tag=[evensItemObj.universal_id integerValue];
                    [AppDelegate getAppdelegate]._isUniqueId=NO;
                    btnForEvents.titleLabel.text=@"0";
                    
                }
                [btnForEvents addSubview:lblForEventName];
                [viewForEvent addSubview:btnForEvents];
                
                
                y=y+20;
                
                [viewForEvent addSubview:imgViewForIcon];
                
                viewForEvent.clipsToBounds=NO;
                [view1 addSubview:viewForEvent];
                
                [imgViewForIcon release];
                [lblForEventName release];
                noOfEventCount++;
            }
            
        }
    }
    
    
}

#pragma  mark Add Events Item For Week
-(void)addEventsForWeekMethod:(NSArray*)eventsArr myCell:(UITableViewCell*)myCell
{
    UIView *cellDescView =nil;
    cellDescView = [[UIView alloc]init];
    
    int x=90;
    int y=1;
    for(int i=0; i<eventsArr.count;i++)
    {
        int noOfEventCount=0;
        
        EventItem *evensItemObj=[eventsArr objectAtIndex:i];
        
        if (i<5)
        {
            UIView * viewForEvent=[[[UIView alloc]initWithFrame:CGRectMake(x, y, 200, 30)]autorelease];
            viewForEvent.backgroundColor=[UIColor clearColor];
            
            UIImageView * imgViewForIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
            imgViewForIcon.backgroundColor=[UIColor clearColor];
            
            UILabel * lblForEventName=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 170, 20)];
            lblForEventName.backgroundColor=[UIColor clearColor];
            lblForEventName.textColor=[UIColor blueColor];
            //12AugChange
            lblForEventName.font=[UIFont fontWithName:@"Arial" size:11.0];
            lblForEventName.numberOfLines=1;
            
            NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
            [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
            
            [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
            
            // 24Hrs
            // 24Hrs modified to fix event null time on Calendar Glance and Month Section
            [formeter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
            [formeter setTimeStyle:NSDateFormatterShortStyle];
            [formeter setDateFormat:NSDateFormatterNoStyle];
            
            // 24Hrs
            
            if ([evensItemObj.onTheDay integerValue]==0) {
                
                if ((evensItemObj.startTime)&&(evensItemObj.endTime)) {
                    lblForEventName.text=[NSString stringWithFormat:@"%@ (%@ to %@)",evensItemObj.eventName,[formeter stringFromDate:evensItemObj.startDate],[formeter stringFromDate:evensItemObj.endDate]];
                }
                else{
                    lblForEventName.text=[NSString stringWithFormat:@"%@",evensItemObj.eventName];
                }
            }
            else{
                lblForEventName.text=evensItemObj.eventName;
            }
            
            //Aman added
            if ([evensItemObj.priority isEqualToString:@"1"])
            {
                lblForEventName.font = [UIFont fontWithName:@"Arial-BoldMT" size:11.0];
            }
            
            [formeter release];
            
            UIButton *btnForEvents=[UIButton buttonWithType:UIButtonTypeCustom];
            btnForEvents.frame=CGRectMake(10, 0, 200, 20);
            btnForEvents.backgroundColor=[UIColor clearColor];
            [btnForEvents addTarget:self action:@selector(editEventMethod:) forControlEvents:UIControlEventTouchUpInside];
            if([evensItemObj.item_Id integerValue]>0){
                btnForEvents.tag=[evensItemObj.item_Id integerValue];
                [AppDelegate getAppdelegate]._isUniqueId=YES;
                btnForEvents.titleLabel.text=@"1";
            }
            else{
                btnForEvents.tag=[evensItemObj.universal_id integerValue];
                btnForEvents.titleLabel.text=@"0";
                [AppDelegate getAppdelegate]._isUniqueId=NO;
                
            }
            [btnForEvents addSubview:lblForEventName];
            [viewForEvent addSubview:btnForEvents];
            
            
            y=y+19;
            
            [viewForEvent addSubview:imgViewForIcon];
            
            viewForEvent.clipsToBounds=NO;
            [myCell.contentView addSubview:viewForEvent];
            //[view1 addSubview:viewForEvent];
            
            [imgViewForIcon release];
            [lblForEventName release];
            noOfEventCount++;
            
        }
    }
}

#pragma  mark Add Events Item For Day
-(void)addEventsForDayMethod:(NSArray*)eventsArr myCell:(UITableViewCell*)myCell
{
    //int x=305;
    int x=200;
    int y=7;
    int paddingW=10;
    int itemWidth=300;
    int itemHeight=21;
    int minX=200;
    int paddingH=10;
    int noOfColum=2;
    int variColum=1;
    
    for(int i=0; i<eventsArr.count;i++)
    {
        int noOfEventCount=0;
        
        EventItem *evensItemObj=[eventsArr objectAtIndex:i];
        
        
        UIView * viewForEvent=[[[UIView alloc]initWithFrame:CGRectMake(x, y, 300, 21)]autorelease];
        viewForEvent.backgroundColor=[UIColor clearColor];
        
        UIImageView * imgViewForIcon=[[UIImageView alloc]initWithFrame:CGRectMake(8, 6, 7, 7)];
        imgViewForIcon.backgroundColor=[UIColor blueColor];
        
        UILabel * lblForEventName=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 290, 20)];
        lblForEventName.backgroundColor=[UIColor clearColor];
        lblForEventName.textColor=[UIColor blueColor];
        //12AugChange
        lblForEventName.font=[UIFont fontWithName:@"Arial" size:11.0];
        lblForEventName.numberOfLines=1;
        
        //Aman added temporary to be removed
        if ([evensItemObj.priority isEqualToString:@"1"])
        {
            lblForEventName.font = [UIFont fontWithName:@"Arial-BoldMT" size:11.0];
        }
        
        NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
        //[formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        NSDate *startTime=evensItemObj.startDate;
        
        //[formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        NSDate *endTime=evensItemObj.endDate;
        
        // 24Hrs
        [formeter setTimeStyle:NSDateFormatterShortStyle];
        [formeter setDateFormat:NSDateFormatterNoStyle];
        //[formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
        
        // 24Hrs
        
        if ([evensItemObj.onTheDay integerValue]==0) {
            //    lblForEventName.text=[NSString stringWithFormat:@"%@ (%@ to %@)",evensItemObj.eventName,[formeter stringFromDate:startTime],[formeter stringFromDate:endTime]];
            if ((evensItemObj.startTime)&&(evensItemObj.endTime)) {
                lblForEventName.text=[NSString stringWithFormat:@"%@ (%@ to %@)",evensItemObj.eventName,[formeter stringFromDate:startTime],[formeter stringFromDate:endTime]];
            }
            else{
                lblForEventName.text=[NSString stringWithFormat:@"%@",evensItemObj.eventName];
            }
            
        }
        else{
            lblForEventName.text=evensItemObj.eventName;
        }
        
        [formeter release];
        
        UIButton *btnForEvents=[UIButton buttonWithType:UIButtonTypeCustom];
        btnForEvents.frame=CGRectMake(10, 0, 200, 20);
        btnForEvents.backgroundColor=[UIColor clearColor];
        [btnForEvents addTarget:self action:@selector(editEventMethod:) forControlEvents:UIControlEventTouchUpInside];
        if([evensItemObj.item_Id integerValue]>0){
            btnForEvents.tag=[evensItemObj.item_Id integerValue];
            [AppDelegate getAppdelegate]._isUniqueId=YES;
            btnForEvents.titleLabel.text=@"1";
        }
        else{
            btnForEvents.tag=[evensItemObj.universal_id integerValue];
            [AppDelegate getAppdelegate]._isUniqueId=NO;
            btnForEvents.titleLabel.text=@"0";
            
        }
        [btnForEvents addSubview:lblForEventName];
        [viewForEvent addSubview:btnForEvents];
        
        [viewForEvent addSubview:imgViewForIcon];
        
        viewForEvent.clipsToBounds=NO;
        [myCell.contentView addSubview:viewForEvent];
        
        [imgViewForIcon release];
        [lblForEventName release];
        noOfEventCount++;
        
        
        
        
        if(variColum % noOfColum==0){
            x=minX;
            y=y+paddingH+itemHeight;
            variColum=1;
        }
        else{
            x=x+paddingW+itemWidth;
            variColum=variColum+1;
        }
        
    }
    
    
}

////////
#pragma  mark Add Events Item For Glance
-(void)addEventsForGlanceMethod:(NSArray*)eventsArr itemArr:(NSArray*)itemArr viewForEvent:(UIView*)myView
{
    int x;
    int y=2;
    int z;
    if(itemArr.count>0)
    {
        x=70;
        z=130;
    }
    else{
        x=20;
        z=175;
    }
    
    
    for(int i=0; i<eventsArr.count;i++)
    {
        int noOfEventCount=0;
        
        EventItem *evensItemObj=[eventsArr objectAtIndex:i];
        
        if (i<2) {
            UIView * viewForEvent=[[[UIView alloc]initWithFrame:CGRectMake(x, y, z, 30)]autorelease];
            viewForEvent.backgroundColor=[UIColor clearColor];
            
            UIImageView * imgViewForIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
            imgViewForIcon.backgroundColor=[UIColor clearColor];
            
            UILabel * lblForEventName=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, z, 20)];
            lblForEventName.backgroundColor=[UIColor clearColor];
            lblForEventName.textColor=[UIColor blueColor];
            lblForEventName.font=[UIFont fontWithName:@"Arial" size:11.0];
            lblForEventName.numberOfLines=1;
            
            //Aman added
            if ([evensItemObj.priority isEqualToString:@"1"])
            {
                lblForEventName.font = [UIFont fontWithName:@"Arial-BoldMT" size:11.0];
            }
            
            NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
            [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
            
            [formeter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
            
            // 24Hrs modified to fix event null time on Calendar Glance and Month Section
            [formeter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
            [formeter setTimeStyle:NSDateFormatterShortStyle];
            [formeter setDateFormat:NSDateFormatterNoStyle];
            
            // 24Hrs
            
            if ([evensItemObj.onTheDay integerValue]==0) {
                if ((evensItemObj.startTime)&&(evensItemObj.endTime))
                {
                    // Code modified to implement UK + 12 Hr Format
                    
                    lblForEventName.text=[NSString stringWithFormat:@"%@ (%@ to %@)",evensItemObj.eventName,[formeter stringFromDate:evensItemObj.startDate],[formeter stringFromDate:evensItemObj.endDate]];
                }
                else{
                    lblForEventName.text=[NSString stringWithFormat:@"%@",evensItemObj.eventName];
                }
                
            }
            else{
                lblForEventName.text=evensItemObj.eventName;
            }
            
            [formeter release];
            
            UIButton *btnForEvents=[UIButton buttonWithType:UIButtonTypeCustom];
            btnForEvents.frame=CGRectMake(10, 0, z, 20);
            btnForEvents.backgroundColor=[UIColor clearColor];
            [btnForEvents addTarget:self action:@selector(editEventMethod:) forControlEvents:UIControlEventTouchUpInside];
            if([evensItemObj.item_Id integerValue]>0){
                btnForEvents.tag=[evensItemObj.item_Id integerValue];
                [AppDelegate getAppdelegate]._isUniqueId=YES;
                btnForEvents.titleLabel.text=@"1";
            }
            else{
                btnForEvents.tag=[evensItemObj.universal_id integerValue];
                [AppDelegate getAppdelegate]._isUniqueId=NO;
                btnForEvents.titleLabel.text=@"0";
                
            }
            [btnForEvents addSubview:lblForEventName];
            [viewForEvent addSubview:btnForEvents];
            
            
            y=y+19;
            
            [viewForEvent addSubview:imgViewForIcon];
            
            viewForEvent.clipsToBounds=NO;
            
            [myView addSubview:viewForEvent];
            
            [imgViewForIcon release];
            [lblForEventName release];
            noOfEventCount++;
        }
    }
}

////////


- (void)editEventMethod:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    btn.selected=YES;
    if([btn.titleLabel.text isEqualToString:@"1"])
    {
        [AppDelegate getAppdelegate]._isUniqueId =YES;
    }
    else
    {
        [AppDelegate getAppdelegate]._isUniqueId =NO;
    }
    [[AppDelegate getAppdelegate]._currentViewController editAddedEventDiaryItem:btn.tag];
}

-(void)configureFlurryParameterDictionary
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    dictFlurryParameter = [[NSMutableDictionary alloc]init];

    [dictFlurryParameter setValue:profobj.profile_id forKey:@"userid"];
    [dictFlurryParameter setValue:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"schoolId"];
    [dictFlurryParameter setValue:profobj.type forKey:@"Role"];
    if ([profobj.type isEqualToString:@"Teacher"])
        [dictFlurryParameter setValue:@"0" forKey:@"grade"];
    else
        [dictFlurryParameter setValue:[NSString stringWithFormat:@"%@",profobj.year] forKey:@"grade"];


}


- (void)dayButtonPressed:(id)sender
{
    // TODO: Set Flag if Button Pressed to fix calendar picker issue 2565
    isSpecificDateSelected = YES;
    NSDate *serchDate=[self.datesIndex objectAtIndex:[sender tag]];
    
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    self.strForDayDate=[formatter stringFromDate:serchDate];
    
    [self dayButtonAction:nil];
    [formatter release];
}

- (void)selectButtonForDate:(NSDate *)aDate {
	if (selectedButton >= 0) {
		NSDate *todayDate = [CalendarLogic dateForToday];
		UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
		
		CGRect selectedFrame = button.frame;
		if ([selectedDate compare:todayDate] != NSOrderedSame) {
			selectedFrame.origin.y = selectedFrame.origin.y + 1;
			selectedFrame.size.width = kCalendarDayWidth;
			selectedFrame.size.height = kCalendarDayHeight;
		}
		
		button.selected = NO;
		button.frame = selectedFrame;
		
		self.selectedButton = -1;
		self.selectedDate = nil;
	}
	
	if (aDate != nil)
    {
		// Save
		self.selectedButton = [calendarLogic indexOfCalendarDate:aDate];
		self.selectedDate = aDate;
		
		NSDate *todayDate = [CalendarLogic dateForToday];
		UIButton *button = [buttonsIndex objectAtIndex:selectedButton];
		
		CGRect selectedFrame = button.frame;
		if ([aDate compare:todayDate] != NSOrderedSame) {
			selectedFrame.origin.y = selectedFrame.origin.y - 1;
			selectedFrame.size.width = kCalendarDayWidth + 1;
			selectedFrame.size.height = kCalendarDayHeight + 1;
		}
		
		button.selected = YES;
		button.frame = selectedFrame;
		[self.calendarView bringSubviewToFront:button];
	}
}



#pragma  mark Show notes method
-(void)showAllNotesItemButtonPressed:(id)sender {
    
    UIButton *btn=(UIButton*)sender;
    
    
    if (_buttonForWeek.selected) {
        NSString *cehckstrForId=[[btn.titleLabel.text componentsSeparatedByString:@"    " ]objectAtIndex:0];
        if([cehckstrForId isEqualToString:@"1"]){
            
            [AppDelegate getAppdelegate]._isUniqueId =YES;
        }
        else{
            
            [AppDelegate getAppdelegate]._isUniqueId =NO;
        }
    }
    else{
        if([ btn.titleLabel.text isEqualToString:@"1"]){
            
            [AppDelegate getAppdelegate]._isUniqueId =YES;
        }
        else{
            
            [AppDelegate getAppdelegate]._isUniqueId =NO;
        }
    }
    
    ShowNotesViewController *viewpopup=[[ShowNotesViewController alloc]initWithNibName:@"ShowNotesViewController" bundle:nil];
    
    viewpopup.NoteItemId  =  [NSNumber numberWithInteger:btn.tag];
    self._viewForShowAllNotesItem=viewpopup;
    
    
    [viewpopup release];
    
    [self._viewForShowAllNotesItem getAllNotesDairyItemMethod:[NSNumber numberWithInteger:btn.tag] ];
    Item *objItem = [viewpopup getItemObjectForNoteAttachment];
    
    [self._viewForShowAllNotesItem.view removeFromSuperview];
    
    self._popoverControler = nil;
    // Every time allocation to self._popoverControler with initWithContentViewController is important as we can not change the contentViewController property successfully
    self._popoverControler = [[UIPopoverController alloc] initWithContentViewController:self._viewForShowAllNotesItem];
    self._popoverControler.delegate = self;
    
    
    [DELEGATE setGlb_isDiaryItemEditForAttachment:YES];
    [DELEGATE setGlb_Button_Attachment:nil];
    [DELEGATE setGlb_Button_Attachment:viewpopup.btnNoteEditViewAttachment];
    
    NSArray *tempArray = [[Service sharedInstance]getAttachmentsForItemId:objItem.itemId withType:[[Service sharedInstance]getFormTemplateOnItemIDBasis:objItem.itemType_id]] ;
    [DELEGATE setMarrListedAttachments:nil];
    [DELEGATE setMarrListedAttachments:[[NSMutableArray alloc] initWithArray:tempArray]] ;
    
    NSInteger count = [[DELEGATE marrListedAttachments]count];
    
    if (count>0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %ld",(long)count]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if (count > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }

    
    
    
    // Need to set PreferredContentSize for iOS 7 and above and
    //             ContentSizeForViewInPopover for below iOS 7
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        [self._popoverControler.contentViewController setPreferredContentSize:CGSizeMake(self._viewForShowAllNotesItem.view.frame.size.width, self._viewForShowAllNotesItem.view.frame.size.height)];
    }
    else
    {
        [self._popoverControler.contentViewController setContentSizeForViewInPopover:CGSizeMake(self._viewForShowAllNotesItem.view.frame.size.width, self._viewForShowAllNotesItem.view.frame.size.height)];
    }
    
    [self._popoverControler setPopoverContentSize:CGSizeMake(self._viewForShowAllNotesItem.view.frame.size.width, self._viewForShowAllNotesItem.view.frame.size.height) animated:YES];
    
    [self._popoverControler presentPopoverFromRect:CGRectMake(25 ,0,10,10 )inView:btn.superview
                          permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
#if DEBUG
    NSLog(@"After : self._popoverControler.contentViewController : %@",self._popoverControler.contentViewController);
#endif

}

@end
