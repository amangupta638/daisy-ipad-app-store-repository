//
//  SchoolInfoViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "SchoolInfoViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "School_Information.h"
#import "Service.h"
@interface SchoolInfoViewController ()

@end

@implementation SchoolInfoViewController
@synthesize _tableView;
@synthesize _arrForSchoolInfoData;
@synthesize _testlayer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    count=0;
    
    
    
    [_webView.layer setMasksToBounds:YES];
    [_webView.layer setCornerRadius:5.0f];
    [_webView.layer setBorderWidth:1.0];
    [_webView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    _infomationView.frame=CGRectMake(900, _infomationView.frame.origin.y, _infomationView.frame.size.width, _infomationView.frame.size.height);
    
    [self updateSchoolInfoDaisy];
       
    
}


-(void)updateSchoolInfoDaisy{
       
    self._arrForSchoolInfoData=[[Service sharedInstance]getStoredDaisyAboutData:@"SchoolContent"];
    
    NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    count=0;
    if(_arrForSchoolInfoData.count>0){
        School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
        
        [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
        [_tableView reloadData];
    }
    
    
    if(_arrForSchoolInfoData.count>0){
        School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-1];
        School_Information *about3=nil;
        if (_arrForSchoolInfoData.count>1) {
            about3=[self._arrForSchoolInfoData objectAtIndex:1];
        }
        else if (_arrForSchoolInfoData.count==1){
             about3=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-1];
        }
        
        _lableForPrev.text=about2.title;
              
        NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
        NSArray *arrItem=[str componentsSeparatedByString:@"  "];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                             initWithString:str];
        NSString *str1=[arrItem objectAtIndex:0];
        NSString *str2=[arrItem objectAtIndex:1];
        CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
        CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold1
                       range:[str rangeOfString:str2]];
        
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor darkGrayColor].CGColor
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                       range:[str rangeOfString:str2]];
        
        if (self._testlayer!=nil){
            
            [self._testlayer removeFromSuperlayer];
        }
        CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
        normalTextLayer_.string =string;
        normalTextLayer_.wrapped = YES;
        normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
        normalTextLayer_.alignmentMode = kCAAlignmentRight;
        normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
        self._testlayer=normalTextLayer_;
        [_lableForNext.layer addSublayer: self._testlayer];
        [_lableForNext setNeedsDisplay];
        [normalTextLayer_ release];
        [string release];
    }
    
    
}

-(void)searchSchoolContent:(School_Information*)infoObj
{
    self._arrForSchoolInfoData=[[Service sharedInstance]getStoredDaisyAboutData:@"SchoolContent"];
    
       if(self._arrForSchoolInfoData.count>0){
        NSInteger Aindex = [self._arrForSchoolInfoData indexOfObject:infoObj];
        count=Aindex;
        
        NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        if(_arrForSchoolInfoData.count>0){
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:Aindex];
            
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            [_tableView reloadData];
        }
        
        
        if(_arrForSchoolInfoData.count>0)
        {
            
            School_Information *about2=nil;
            School_Information *about3=nil;
            if (count==0)
            {
                
                about2=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-1];
                about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
               
            }
            else if (count==self._arrForSchoolInfoData.count-1)
            {
                about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
                about3=[self._arrForSchoolInfoData objectAtIndex:0];
            }
            else{
                about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
                about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            }
          
                _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
        }

    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 1;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return height for rows.
    return 45;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self._arrForSchoolInfoData.count;
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0;
    
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //
    //        UIView *headerView=[[[UIView alloc] init] autorelease];
    //        headerView.backgroundColor=[UIColor clearColor];
    //        UIImageView *sectionImage=[[UIImageView alloc] init];
    //
    //        sectionImage.backgroundColor=[UIColor clearColor];
    //        [headerView addSubview:sectionImage];
    //
    //        headerView.frame=CGRectMake(0, 0,220, 45);
    //        sectionImage.frame=CGRectMake(0, 0,220, 45);
    //
    //        [sectionImage release];
    //
    //
    //        UILabel* sectionLabel=[[UILabel alloc] init];
    //        sectionLabel.frame=CGRectMake(10, 0,240, 30);
    //        sectionLabel.font=[UIFont boldSystemFontOfSize:14.0f];
    //        sectionLabel.backgroundColor=[UIColor clearColor];
    //        sectionLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
    //        [headerView addSubview:sectionLabel];
    //        [sectionLabel release];
    //
    //        if (section==0) {
    //             sectionImage.image=[UIImage imageNamed:@"topcell.png"];
    //            sectionLabel.text=@"Student Profile";
    //        }
    //        else{
    //             sectionImage.image=[UIImage imageNamed:@"profilemidcell.png"];
    //
    //            sectionLabel.text=@"About Empower";
    //        }
    return nil;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
    UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (myCell == nil){
        myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
        myCell.backgroundColor=[UIColor clearColor ];
        myCell.selectionStyle = UITableViewCellSelectionStyleGray;
        myCell.accessoryType=UITableViewCellAccessoryNone;
        
        UILabel * lblForTitle=[[UILabel alloc]init];
        lblForTitle.numberOfLines=2;
        lblForTitle.tag=1000;
        lblForTitle.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
        lblForTitle.backgroundColor=[UIColor clearColor];
        
        UIImageView * cellBgImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 220, 45)];
        cellBgImg.backgroundColor=[UIColor clearColor];
        cellBgImg.tag=89;
        [myCell.contentView addSubview:cellBgImg];
        
        [myCell.contentView addSubview:lblForTitle];
        [cellBgImg release];
        [lblForTitle release];
    }
    
    
    UILabel * lblForTitle=(UILabel*)[myCell.contentView viewWithTag:1000];
    //UIImageView *cellBgImg=(UIImageView*)[myCell.contentView viewWithTag:89];
    
    //cellBgImg.image=[UIImage imageNamed:@"noteEnterBgCell.png"];
    
    

    
    School_Information *about=[self._arrForSchoolInfoData objectAtIndex:indexPath.row];
    
    if ([about.parent_id integerValue]==0) {
        lblForTitle.frame=CGRectMake(10, 7,200, 30);
        lblForTitle.font=[UIFont boldSystemFontOfSize:12.0f];
        lblForTitle.textColor=[UIColor darkGrayColor];
    }
    else{
        
        lblForTitle.frame=CGRectMake(30, 7,190, 30);
        lblForTitle.font=[UIFont systemFontOfSize:12.0f];
        lblForTitle.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
    }
    lblForTitle.text=about.title;
    
    
    return myCell;
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(count!=indexPath.row){
        count=indexPath.row;
        
        if (indexPath.row==0)
        {
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-1];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else if(indexPath.row==self._arrForSchoolInfoData.count-1){
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:0];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-2];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else{
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        
        
    }
    
    
}
#pragma mark
#pragma mark Button Action
- (IBAction)informationButtonAction:(id)sender {
    
    if (_informationBtn.frame.origin.x==861) {
        [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_blackImgView];
        [UIView animateWithDuration:0.3
                         animations:^{
                             _informationBtn.frame=CGRectMake(746, _informationBtn.frame.origin.y, _informationBtn.frame.size.width, _informationBtn.frame.size.height);
                             _infomationView.frame=CGRectMake(775, _infomationView.frame.origin.y, _infomationView.frame.size.width, _infomationView.frame.size.height);
                             
                         }];
        [_informationBtn setImage:[UIImage imageNamed:@"popuparrow.png"] forState:UIControlStateNormal];
    }
    else{
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             _informationBtn.frame=CGRectMake(861, _informationBtn.frame.origin.y, _informationBtn.frame.size.width, _informationBtn.frame.size.height);
                             _infomationView.frame=CGRectMake(900, _infomationView.frame.origin.y, _infomationView.frame.size.width, _infomationView.frame.size.height);
                         }];
        [_blackImgView removeFromSuperview];
        [_informationBtn setImage:[UIImage imageNamed:@"informationpopup.png"] forState:UIControlStateNormal];
        
    }
    
    
}

- (IBAction)previousButtonAction:(id)sender {
    if (self._arrForSchoolInfoData.count>0) {
        if (count>1) {
            count=count-1;
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            
            
            _lableForPrev.text=about2.title;
            
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else if(count==1){
            count=count-1;
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-1];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
            
            
        }
        else if(count==0){
            count=self._arrForSchoolInfoData.count-1;
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            
            //21AugChange done
            School_Information *about2=nil;
            if (count-1==-1) {
                about2=[self._arrForSchoolInfoData objectAtIndex:0];
            }
            else{
                about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
            }
            
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:0];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
            
            
        }

    }
   
    
}

- (IBAction)nextButtonAction:(id)sender {
    if (self._arrForSchoolInfoData.count>0) {
        if (count==self._arrForSchoolInfoData.count-1) {
            count=0;
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:self._arrForSchoolInfoData.count-1];
            
            //21AugChange
            School_Information *about3=nil;
            if (self._arrForSchoolInfoData.count==1) {
                about3=[self._arrForSchoolInfoData objectAtIndex:count];
            }
            else{
                about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            }
            
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else if (count==self._arrForSchoolInfoData.count-2) {
            count=count+1;
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:0];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else {
            count=count+1;
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about=[self._arrForSchoolInfoData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:dataPath1 isDirectory:YES]];
            School_Information *about2=[self._arrForSchoolInfoData objectAtIndex:count-1];
            School_Information *about3=[self._arrForSchoolInfoData objectAtIndex:count+1];
            
            
            _lableForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _lableForNext.frame.size.width,_lableForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_lableForNext.layer addSublayer: self._testlayer];
            [_lableForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }

    }
    
       
}

- (void)dealloc {
    [_testlayer release];
    [_arrForSchoolInfoData release];
    [_tableView release];
    [_informationBtn release];
    [_infomationView release];
    [_blackImgView release];
    [_lableForPrev release];
    [_btnForPrev release];
    
    [_lableForNext release];
    [_btnForNext release];
    [_webView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self set_tableView:nil];
    [_informationBtn release];
    _informationBtn = nil;
    [_infomationView release];
    _infomationView = nil;
    [_blackImgView release];
    _blackImgView = nil;
    [_lableForPrev release];
    _lableForPrev = nil;
    [_btnForPrev release];
    _btnForPrev = nil;
    [_lableForNext release];
    _lableForNext = nil;
    [_btnForNext release];
    _btnForNext = nil;
    [_webView release];
    _webView = nil;
    [super viewDidUnload];
}
@end
