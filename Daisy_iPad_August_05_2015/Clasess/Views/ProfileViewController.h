//
//  ProfileViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "OperationQueue.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
   
@interface ProfileViewController : UIViewController<UITextFieldDelegate,UIPopoverControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    OperationQueue *queueOperation;
    
    IBOutlet UIButton *_profileBtn;
    IBOutlet UIButton *_parentsBtn;
    IBOutlet UIButton *_teacherBtn;
    IBOutlet UITableView *_lefTableView;
    IBOutlet UITableView *_rightTableView;
    IBOutlet UIView *_inviteParentsView;
    IBOutlet UIView *_headerView;
    IBOutlet UIButton *_editBtn;
    NSMutableArray *_selectedArrIndex;
    IBOutlet UITextField *_txtFielldForInvitParetns;
    IBOutlet UIView *_viewForDatePicker;
    IBOutlet UIDatePicker *_datePicker;
    IBOutlet UIImageView *_imgVForTeacher;
    IBOutlet UILabel *_lableforTecherName;
    IBOutlet UIView *_ViewForImageOption;
    NSMutableArray *_arrForParentData;
    NSMutableArray *_arrForTeacherData;
    UITextField *_dateTextField;
    IBOutlet UILabel *_lableForUsername;
    
    //for state picker
    IBOutlet UIView *_pickerViewForState;
    IBOutlet UIPickerView *_pickerForState;
    NSMutableArray *_arrForState;
    UITextField *_textFieldForState;
    
    //for profile image;
    IBOutlet UIImageView *_imgViewForBanner;
    
    UIImagePickerController  *imagePickerController;
    IBOutlet UIButton *_btnForProfilImg;
    IBOutlet UIImageView *ImaVgforUserProfile;
    
    IBOutlet UIView *_indicatorView;
    BOOL _isImageChange;
    
    NSInteger selectedRowIndex;
    NSTimer* myTimer;
    IBOutlet UIButton *btnSendInvitation;
    IBOutlet UILabel *lblInviteYourParent;
}
@property(nonatomic,retain) UIPopoverController *_popoverControler;
@property(nonatomic,retain) UITextField *_temTextField;

- (IBAction)myProfileButtonAction:(id)sender;
- (IBAction)myParentsButtonAction:(id)sender;
- (IBAction)teacherButtonAction:(id)sender;
- (IBAction)editButtonAction:(id)sender;
- (IBAction)sendButtonAction:(id)sender;
- (IBAction)datePickerCancelButtonAction:(id)sender;
- (IBAction)datePickerDoneButtonAction:(id)sender;
- (IBAction)userProfileImageButtonAction:(id)sender;
- (IBAction)photoOptionButtonAction:(id)sender;
- (IBAction)cameraOptionButtonAction:(id)sender;
- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType;
-(void) animateView :(UIView*)aView  xCoordinate:(CGFloat)dx  yCoordinate :(CGFloat) dy;

@end
