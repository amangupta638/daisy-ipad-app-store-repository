//
//  ShowAllDairyItemViewController.m
//  StudyPlaner
//
//  Created by Dhirendra on 24/05/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "ShowAllDairyItemViewController.h"
#import "Item.h"
#import "Subjects.h"
#import "Service.h"
#import "MainViewController.h"
#import "CalenderViewController.h"
#import "MyProfile.h"

#import "FileUtils.h"

@interface ShowAllDairyItemViewController ()

@end

@implementation ShowAllDairyItemViewController
@synthesize assignDateForDiryItem;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark view cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        //[self.view setBackgroundColor:[UIColor blackColor]];
        [self.view setFrame:CGRectMake(0, 0, 250, 360)];
        [_table setFrame:CGRectMake(5, 5, 240, 350)];
    }
    else
    {
        //[self.view setBackgroundColor:[UIColor clearColor]];
        [self.view setFrame:CGRectMake(0, 0, 240, 350)];
        [_table setFrame:CGRectMake(0, 0, 240, 360)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [assignDateForDiryItem release];
    [_table release];
    [_arrForDairyItems release];
    [super dealloc];
}
- (void)viewDidUnload {
    [_table release];
    _table = nil;
    [super viewDidUnload];
    
    
}

-(void)getAllDairyItemMethod:(NSDate*)assignedDate selectedButton:(NSString*)selectBtn
{
    self.assignDateForDiryItem=assignedDate;
    
    //4julyimportant done
    _arrForDairyItems =[[NSMutableArray alloc]init];
    NSMutableArray *arrFordairyItem=[[NSMutableArray alloc]init];
    NSArray  *itemArr= [[Service sharedInstance] getStoredAllItemData:assignedDate];
    
    NSArray  *itemDueArr= [[Service sharedInstance] getStoredAllDueItemData:assignedDate];
    
    [arrFordairyItem removeAllObjects];
    for (Item *itemObj in itemArr) {
        if (itemObj.assignedDate) {
            if (![itemObj.type isEqualToString:@"Note"]) {
                [arrFordairyItem addObject:itemObj];
            }
            
        }
        
    }
    
    for (Item *itemObj in itemDueArr) {
        if (itemObj.assignedDate) {
            if (![itemObj.type isEqualToString:@"Note"]) {
                if (![arrFordairyItem containsObject:itemObj]) {
                    [arrFordairyItem addObject:itemObj];
                }
                
            }
            
        }
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    [_arrForDairyItems removeAllObjects];
    for (Item *itemObj in arrFordairyItem) {
        [_arrForDairyItems addObject:itemObj];
    }
    if ([selectBtn isEqualToString:@"Month"]) {
        [_arrForDairyItems removeObjectAtIndex:0];
    }
    else{
    }
    
    [arrFordairyItem release];
    
    if (_arrForDairyItems.count==1) {
        
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, 160);
        [_table setScrollEnabled:NO];
        
        
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
#if DEBUG
        NSLog(@"profobj %@",profobj.type);
#endif
      
        
        Item *itemObj = [_arrForDairyItems firstObject];
        // Only FOr Student
        if ([profobj.type isEqualToString:@"Student"])
        {
            // Fetching Diary Item Users on the bases of DI id
            NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:itemObj.itemType_id];
            if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
            {
                
                NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId];
                
#if DEBUG
                NSLog(@"Before arrDIU %@",arrDIU);
#endif

                arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
                
#if DEBUG
                NSLog(@"After arrDIU %@",arrDIU);
#endif
                
                for (DairyItemUsers *assinUserobj in arrDIU)
                {
                    if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                    {
                        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                        if (lov)
                        {
                            self.view.frame=CGRectMake(0, 0, 450, 160);
                            /*if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                            {
                                
                                
                                
//                                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
//                                
//                                UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(185, 0, 25, 25)];
//                                [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
//                                [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
//                                btnTaskBasedMerits.tag=[itemObj.itemId intValue];
//                                
//                                [cell.contentView addSubview:btnTaskBasedMerits];
//                                [btnTaskBasedMerits release];
                            }*/
                        }
                    }
                }
            }
        }
        // Only FOr Student
        
        
        
      
    }
    else if (_arrForDairyItems.count==2) {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, 176);
        [_table setScrollEnabled:YES];

        //[_table setScrollEnabled:NO];
    }
    else if (_arrForDairyItems.count==3)
    {
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, 264);
        [_table setScrollEnabled:YES];

        //[_table setScrollEnabled:NO];
    }
    else{
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, 350);
        [_table setScrollEnabled:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
  
    return 1;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return height for rows.
    if([_arrForDairyItems count]==1 ||[_arrForDairyItems count]>1 )
    {
//    if ([_arrForDairyItems count]==1 ) {
        return 160;

    }
    else
    {
    return 88;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrForDairyItems.count;
}



-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ShowAllDairyItemCell"];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ShowAllDairyItemCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        UIImageView * imgViewForLine=[[UIImageView alloc]initWithFrame:CGRectMake(30, 13, 164, 2)];
        imgViewForLine.contentMode=UIViewContentModeScaleAspectFit;
        imgViewForLine.tag=201;
        imgViewForLine.backgroundColor=[UIColor grayColor];
        
        [cell.contentView addSubview:imgViewForLine];
        
        [imgViewForLine release];
        
        /***********************************************************/
        if([_arrForDairyItems count]==1 ||[_arrForDairyItems count]>1 )
        //if([_arrForDairyItems count]==1)
        {
            NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
            [formeter setTimeStyle:NSDateFormatterNoStyle];
            [formeter setDateStyle:NSDateFormatterShortStyle];
            [formeter setDateFormat:kDATEFORMAT_Slash];
            Item *itemObj=[_arrForDairyItems objectAtIndex:indexPath.row];
            NSString *strAssignedDate = [formeter stringFromDate:itemObj.assignedDate];
            
            if (!([itemObj.type isEqualToString:@"OutOfClass"] || [itemObj.type isEqualToString:@"Out Of Class"]))
            {
                UILabel *popAssignedDateLbl=[[UILabel alloc]init];
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popAssignedDateLbl setFrame:CGRectMake(8, 70,200, 15)];
                }
                else
                {
                    [popAssignedDateLbl setFrame:CGRectMake(3, 65,200, 15)];
                }
                
                popAssignedDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
                popAssignedDateLbl.numberOfLines=1;
                popAssignedDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popAssignedDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popAssignedDateLbl.backgroundColor=[UIColor clearColor];
                
               
                
                
                
                popAssignedDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Assigned Date:",strAssignedDate];
                
                
                
                
                [cell.contentView addSubview:popAssignedDateLbl];
                [popAssignedDateLbl release];
  
            }
            
            
            MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
            
            
            
            if ([profobj.type isEqualToString:@"Student"] && ([itemObj.type isEqualToString:k_MERIT_CHECK] || [itemObj.type isEqualToString:@"Merits"]))
            {
                DairyItemUsers *itemuser = [[Service sharedInstance]getAssigneStudentDataInfoStored:profobj.profile_id itemId:[itemObj.itemId stringValue]  postNotification:NO] ;
                
                UILabel *popAssignedby=[[UILabel alloc]init];
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    [popAssignedby setFrame:CGRectMake(8, 89,200, 15)];
                }
                else
                {
                    [popAssignedby setFrame:CGRectMake(3, 84,200, 15)];
                }
                
                popAssignedby.font=[UIFont fontWithName:@"Arial" size:13];
                popAssignedby.numberOfLines=1;
                popAssignedby.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
                popAssignedby.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
                popAssignedby.backgroundColor=[UIColor clearColor];
                popAssignedby.text= [NSString stringWithFormat:@"Assigned By: %@",itemuser.approverName];
                
                [cell.contentView addSubview:popAssignedby];
                [popAssignedby release];
                
            }
            
            
            UILabel *popMeritTitleLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popMeritTitleLbl setFrame:CGRectMake(340, 48,90, 30)];
            }
            else
            {
                [popMeritTitleLbl setFrame:CGRectMake(340, 48,90, 30)];
            }
            
            popMeritTitleLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popMeritTitleLbl.numberOfLines=0;
            popMeritTitleLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popMeritTitleLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popMeritTitleLbl.backgroundColor=[UIColor clearColor];
            [cell.contentView addSubview:popMeritTitleLbl];
            [popMeritTitleLbl release];
            
            
            
            UILabel *popMeritAssignedBy=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popMeritAssignedBy setFrame:CGRectMake(340, 120,90, 15)];
            }
            else
            {
                [popMeritAssignedBy setFrame:CGRectMake(340, 120,90, 15)];
            }
            
            popMeritAssignedBy.font=[UIFont fontWithName:@"Arial" size:13];
            popMeritAssignedBy.numberOfLines=2;
            popMeritAssignedBy.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popMeritAssignedBy.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popMeritAssignedBy.backgroundColor=[UIColor clearColor];
            [cell.contentView addSubview:popMeritAssignedBy];
            [popMeritAssignedBy release];
           
            UILabel *popMeritLblForSeprationline=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popMeritLblForSeprationline setFrame:CGRectMake(240, 25,1, 140)];
            }
            else
            {
                [popMeritLblForSeprationline setFrame:CGRectMake(240, 25,1, 140)];
            }
            
            popMeritLblForSeprationline.backgroundColor=[UIColor lightGrayColor];
            [cell.contentView addSubview:popMeritLblForSeprationline];
            [popMeritLblForSeprationline release];
            
            
            
            
            // Only FOr Student
            if ([profobj.type isEqualToString:@"Student"])
            {
                // Fetching Diary Item Users on the bases of DI id
                NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:itemObj.itemType_id];
                if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
                {
                    NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId];
                    
#if DEBUG
                    NSLog(@"Before arrDIU %@",arrDIU);
#endif

                    arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
                    
#if DEBUG
                    NSLog(@"After arrDIU %@",arrDIU);
#endif

                    
                    for (DairyItemUsers *assinUserobj in arrDIU)
                    {
                        if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
                        {
                            ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                            if (lov)
                            {
                                if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                                {
                                    
                                    
                                    
                                    NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.icon];
                                    
                                    UIImageView *imgViewTaskBasedMerits = [[UIImageView alloc]initWithFrame:CGRectMake(245, 45, 85, 85)];
                                    [imgViewTaskBasedMerits setImage:[FileUtils getImageFromPath:strPath] ];
                                    imgViewTaskBasedMerits.tag=[itemObj.itemId integerValue];
                                    
                                    [cell.contentView addSubview:imgViewTaskBasedMerits];
                                    [imgViewTaskBasedMerits release];
                                    
                                    
                                    UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(327, 83, 90, 13)];
                                    btnTaskBasedMerits.titleLabel.font = [UIFont fontWithName:@"Arial" size:13];
                                    [btnTaskBasedMerits.titleLabel setTextAlignment:NSTextAlignmentLeft];
                                    
                                    [btnTaskBasedMerits setTitle:@"View Merit" forState:UIControlStateNormal];
                                    [btnTaskBasedMerits setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                                    [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                                    btnTaskBasedMerits.tag=[itemObj.itemId integerValue];
                                    
                                    [cell.contentView addSubview:btnTaskBasedMerits];
                                    [btnTaskBasedMerits release];
                                    popMeritTitleLbl.text = assinUserobj.meritTitle;
                                    popMeritAssignedBy.text = assinUserobj.approverName;
                                }
                            }
                        }
                    }
                }
            }
        
        
        
        if(!(([itemObj.type isEqualToString:k_MERIT_CHECK] || [itemObj.type isEqualToString:@"Merits"])|| ([itemObj.type isEqualToString:@"OutOfClass"] || [itemObj.type isEqualToString:@"Out Of Class"])))
        {
            UILabel *popDueDateLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popDueDateLbl setFrame:CGRectMake(8, 89,200, 15)];
            }
            else
            {
                [popDueDateLbl setFrame:CGRectMake(3, 84,200, 15)];
            }
            
            popDueDateLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popDueDateLbl.numberOfLines=1;
            popDueDateLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popDueDateLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popDueDateLbl.backgroundColor=[UIColor clearColor];
            
            NSString *strDueDate = [formeter stringFromDate:itemObj.dueDate];
            
            popDueDateLbl.text= [NSString stringWithFormat:@"%@ %@",@"Due Date:",strDueDate];
            
            
            [cell.contentView addSubview:popDueDateLbl];
            [popDueDateLbl release];
            
            UILabel *popProgressLbl=[[UILabel alloc]init];
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
            {
                [popProgressLbl setFrame:CGRectMake(8, 106,200, 15)];
            }
            else
            {
                [popProgressLbl setFrame:CGRectMake(3, 101,200, 15)];
            }
            
            popProgressLbl.font=[UIFont fontWithName:@"Arial" size:13];
            popProgressLbl.numberOfLines=1;
            popProgressLbl.textColor=[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1];
            popProgressLbl.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
            popProgressLbl.backgroundColor=[UIColor clearColor];
            
            
            if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
            {
                popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Weight Complete:",[NSString stringWithFormat:@"%d",[itemObj.progress integerValue]],@"%"];
            }
            // For Prime Only
            else
            {
                popProgressLbl.text= [NSString stringWithFormat:@"%@ %@ %@",@"Percent Complete:",[NSString stringWithFormat:@"%d",[itemObj.progress integerValue]],@"%"];
                
            }
            
            
            
            
            [cell.contentView addSubview:popProgressLbl];
            [popProgressLbl release];
//            [cell.contentView setFrame:CGRectMake(cell.contentView.frame.origin.x , cell.contentView.frame.origin.y, cell.contentView.frame.size.width, 180)];
        }
        
        
    }
    
        /***********************************************************/

        
    }
    cell.tag=indexPath.row;
    UIImageView *imgViewForHeaderBg=(UIImageView*)[cell.contentView viewWithTag:99];
    UIButton *buttonForEdit=(UIButton*)[cell.contentView viewWithTag:100];
    UILabel *lblForHeader=(UILabel*)[cell.contentView viewWithTag:101];
    UILabel *lblForDesc=(UILabel*)[cell.contentView viewWithTag:102];
    UIImageView *iconImgView=(UIImageView*)[cell.contentView viewWithTag:103];
    UILabel *lblForSub=(UILabel*)[cell.contentView viewWithTag:200];
    UIImageView *imgViewForLine=(UIImageView*)[cell.contentView viewWithTag:201];
    
    
    //29maychange
    
    
    Item *itemObj=[_arrForDairyItems objectAtIndex:indexPath.row];
    
    [buttonForEdit addTarget:self action:@selector(edtiButtonPessed:) forControlEvents:UIControlEventTouchUpInside];
    
    buttonForEdit.titleLabel.textColor=[UIColor clearColor];
    if([itemObj.itemId integerValue]>0){
        buttonForEdit.tag=[itemObj.itemId integerValue];
        buttonForEdit.titleLabel.text=@"1";
        [AppDelegate getAppdelegate]._isUniqueId=YES;
    }
    else{
        buttonForEdit.titleLabel.text=@"0";
        buttonForEdit.tag=[itemObj.universal_Id integerValue];
        [AppDelegate getAppdelegate]._isUniqueId=NO;
        
    }
    //4julyimportant done
    NSString *isTypeStr= itemObj.type;
    if(![itemObj.type isEqualToString:@"Note"])
    {
        NSDateFormatter *formeter2 = [[NSDateFormatter alloc] init];
        [formeter2 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        long timeDiff1;
        
        timeDiff1=(int)[[formeter2 dateFromString:[formeter2 stringFromDate:itemObj.dueDate]] timeIntervalSinceDate:[formeter2 dateFromString:[formeter2 stringFromDate:self.assignDateForDiryItem]]];
        
        
        
        [formeter2 release];
        
        
        
        if(timeDiff1<=0){
            iconImgView.image=[UIImage imageNamed:@"overdueicon.png"];
            
            if ([itemObj.isCompelete integerValue]==1) {
                imgViewForLine.hidden=NO;
            }
            else
            {
                imgViewForLine.hidden=YES;
            }
            
        }
        else{
            if ([isTypeStr isEqualToString:@"Assignment"]) {
                
                iconImgView.image=[UIImage imageNamed:@"dairygreenicon.png"];
            }
            else if([isTypeStr isEqualToString:@"Homework"]) {
                iconImgView.image=[UIImage imageNamed:@"homeredicon.png"];
            }
            
            if ([itemObj.isCompelete integerValue]==1) {
                imgViewForLine.hidden=NO;
            }
            else
            {
                imgViewForLine.hidden=YES;
            }
            
            
            
        }
    }
    ///////////////////////////////////////////////////////////////////////
    
    Subjects *sub=[[Service sharedInstance]getSubjectsDataInfoStored:itemObj.subject_name  postNotification:NO];
    //----- Modified By Mitul To set Class Color insted of Subject Color JIRA 1191 -----//
    MyClass *class1=nil;
    
    class1 = [[Service sharedInstance] getStoredClassDatawithClassId:[NSString stringWithFormat:@"%@",itemObj.class_id]];
    if(class1.code!=nil)
        imgViewForHeaderBg.backgroundColor=[AppHelper colorFromHexString:class1.code];
    else{
        imgViewForHeaderBg.backgroundColor=[UIColor grayColor];
    }
    
    if(class1.code!=nil && [class1.code isEqualToString:@"#FFFFFF"])
    {
        lblForHeader.textColor=[UIColor blackColor];
        
    }
    
    imgViewForHeaderBg.contentMode=UIViewContentModeScaleAspectFit;
    [imgViewForHeaderBg.layer setMasksToBounds:YES];
    [imgViewForHeaderBg.layer setCornerRadius:2.0f];
    [imgViewForHeaderBg.layer setBorderWidth:1.0];
    [imgViewForHeaderBg.layer setBorderColor:[[UIColor clearColor] CGColor]];
    
    
    lblForHeader.text=itemObj.title;
    if(sub!=nil){
        lblForSub.text=[NSString stringWithFormat:@"%@ %@",@"Subject:", sub.subject_name];
    }
    else{
       lblForSub.text=[NSString stringWithFormat:@"%@ %@",@"Subject:", @""];
    }
    //lblForSub.text=[NSString stringWithFormat:@"%@ %@",@"Subject:", sub.subject_name];
    lblForDesc.text=[NSString stringWithFormat:@"%@ %@",@"Description:",itemObj.itemDescription] ;

    if (itemObj.itemDescription!=nil) {
        lblForDesc.hidden=NO;
    }
    else{
        lblForDesc.hidden=YES;
    }
    
    
    
    
    if ([itemObj.type isEqualToString:@"OutOfClass"] || [itemObj.type isEqualToString:@"Out Of Class"])
    {
        
        if ([itemObj.isApproved integerValue] == 0)
        {
            iconImgView.image=[UIImage imageNamed:@"passPendingicon.png"];
            imgViewForHeaderBg.backgroundColor=[UIColor colorWithRed:127.0/255.0f green:127.0/255.0f blue:127.0/255.0f alpha:1];
            //imgViewForHeaderBg.backgroundColor=[UIColor colorWithRed:248.0/255.0f green:139.0/255.0f blue:5.0/255.0f alpha:1];
        }
        else if ( [itemObj.isApproved integerValue] == 1 )
        {
            iconImgView.image=[UIImage imageNamed:@"passApprovedicon.png"];
            imgViewForHeaderBg.backgroundColor=[UIColor colorWithRed:127.0/255.0f green:127.0/255.0f blue:127.0/255.0f alpha:1];
            //imgViewForHeaderBg.backgroundColor=[UIColor colorWithRed:18.0/255.0f green:123.0/255.0f blue:16.0/255.0f alpha:1];
        }
        else
        {
            iconImgView.image=[UIImage imageNamed:@"passElapsedicon.png"];
            imgViewForHeaderBg.backgroundColor=[UIColor colorWithRed:127.0/255.0f green:127.0/255.0f blue:127.0/255.0f alpha:1];
            //imgViewForHeaderBg.backgroundColor=[UIColor colorWithRed:230/255.0f green:37/255.0f blue:37/255.0f alpha:1]; // Orange
            
        }
    }
    else if([isTypeStr isEqualToString:k_MERIT_CHECK])
    {
        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:itemObj.passTypeId];
        if (lov)
        {
            if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
            {
                NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                iconImgView.image=[FileUtils getImageFromPath:strPath];
            }
        }
    }

    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];

#if DEBUG
    NSLog(@"profobj %@",profobj.type);
#endif

    
    // Only FOr Student
    if ([profobj.type isEqualToString:@"Student"])
    {
        // Fetching Diary Item Users on the bases of DI id
        NSString *strFormtemplateName =[[Service sharedInstance] getFormTemplateOnItemIDBasis:itemObj.itemType_id];
        if([strFormtemplateName isEqualToString:@"Assignment"] || [strFormtemplateName isEqualToString:@"Homework"])
        {
        
        NSArray *arrDIU = [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId];
        
#if DEBUG
        NSLog(@"Before arrDIU %@",arrDIU);
#endif

        arrDIU = [[Service sharedInstance] getActiveLOVWithDIU:arrDIU];
            
#if DEBUG
        NSLog(@"After arrDIU %@",arrDIU);
#endif
      
        
        for (DairyItemUsers *assinUserobj in arrDIU)
        {
            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
            {
                ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:assinUserobj.listofvaluesTypeid];
                if (lov)
                {
                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                    {
                        if([_arrForDairyItems count]==1)
                        {
                        [imgViewForHeaderBg setFrame:CGRectMake(imgViewForHeaderBg.frame.origin.x, imgViewForHeaderBg.frame.origin.y, 440, imgViewForHeaderBg.frame.size.height)];
                            
                         [buttonForEdit setFrame:CGRectMake(410, buttonForEdit.frame.origin.y, buttonForEdit.frame.size.width, buttonForEdit.frame.size.height)];
                            
                        }
                        
                        if([_arrForDairyItems count]>1)
                        {
                            
                        
                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                        
                        UIButton *btnTaskBasedMerits = [[UIButton alloc]initWithFrame:CGRectMake(190, 3, 20, 19)];
                        [btnTaskBasedMerits setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                        
                        [btnTaskBasedMerits addTarget:self action:@selector(editTaskBasedMerits:) forControlEvents:UIControlEventTouchUpInside];
                        
                        btnTaskBasedMerits.tag=[itemObj.itemId integerValue];
                        
                        [cell.contentView addSubview:btnTaskBasedMerits];
                        [btnTaskBasedMerits release];
                            }
                    }
                }
            }
        }
        }
    }
    // Only FOr Student

    
    
    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}

#pragma mark Button actions
-(void)edtiButtonPessed:(id)sender
{
    UIButton *btn=(UIButton*)sender;

    btn.selected=YES;
    if([ btn.titleLabel.text isEqualToString:@"1"]){
        
        [AppDelegate getAppdelegate]._isUniqueId =YES;
    }
    else{
        
        [AppDelegate getAppdelegate]._isUniqueId =NO;
    }
    
    Item *diarItem=nil;
    diarItem =[[Service sharedInstance]getItemDataInfoStored:[NSNumber numberWithInteger:btn.tag] postNotification:NO];
    if([diarItem.type isEqualToString:@"OutOfClass"] || [diarItem.type isEqualToString:@"Out Of Class"])
    {
        // code added to fix issue number EZTEST - 2568
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
            
        });
        dispatch_async(dispatch_get_main_queue(), ^{
            MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
            CalenderViewController *calObje = (CalenderViewController*)[mainVC _currentViewController];
            [calObje.view setUserInteractionEnabled:NO];
            [calObje showPassesInMonthviewWhenclickFromShowAllDairyItem:sender];
        });

        return;
    }
    
    [[AppDelegate getAppdelegate]._currentViewController editAddedDiaryItem:btn.tag];
    
    [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];
    
}



- (void)editTaskBasedMerits:(id)sender
{
    [[AppDelegate getAppdelegate]._currentViewController editAddedTaskBasedMerit:[sender tag]];
    
    [[AppDelegate getAppdelegate]._currentViewController dissmisCalenderPopover];

}

@end
