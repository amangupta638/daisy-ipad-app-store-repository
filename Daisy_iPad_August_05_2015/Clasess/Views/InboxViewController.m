//
//  InboxViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "InboxViewController.h"
#import "Service.h"
#import "MyProfile.h"
#import "MyTeacher.h"
#include "Inbox.h"
#import "Annoncements.h"
#import "News.h"
#import <QuartzCore/QuartzCore.h>
#import "News_Attachment.h"
#import "NSString+HTML.h"
#import "Flurry.h"


#define HeightForListCell 40

@interface InboxViewController ()

{
    NSMutableArray *arrStudentList ;
    NSMutableArray *arrTeacherList ;
    NSMutableArray *arrParentsList ;
    NSMutableArray *arrAllList ;
    NSMutableArray *arrClassList ;
    NSMutableArray *arrSelectedParticipernts ;
    NSMutableArray *arrSearchedList ;
    NSArray *arrForList ;
    NSMutableDictionary *dictForParticipent ;
    BOOL isSpecificStudentTapped ;
    BOOL isListDisplayed ;
    NSInteger currentSelectedButton ;
    NSInteger insertedInboxCount ;
    BOOL isSearchOn ;
    CGRect originalListTableViewHeight ;
    NSInteger selectedMsgRow ;
    
    BOOL isAnnouncementSelected;
    BOOL isNewsSelected;
    BOOL isMessageSelected;
    BOOL isArrowUP;
    
    NSString *strRecivers;
    
}

@end

@implementation InboxViewController
@synthesize _tableView,_popoverControler,_arrformsz;
@synthesize btnSend ;
@synthesize btnDiscardChanges ;
@synthesize btnSelectAll ;
@synthesize lblSelectAll;
@synthesize btnSelectSpecificStudents ;
@synthesize btnAll,ParentButton,ParentLabel,ClassButton,ClassLabel,AllLabel;

@synthesize arrForNews,arrForAnnouncements,arrForInboxMessagesOnly,arrForAllMessages,arrOfSelectedMessages;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma  mark Service
-(void)inboxMessgesDidDeleted:(NSNumber *)msgAppId iPAdd:(NSString *)msgIpAdd msgId:(NSNumber *)msgId
{
    self._arrformsz=nil;
    [self getInboxMessage];
    
    id model=nil;
    if((privousForBgImgV<_arrForInbox.count && selectedIndexForFiltrationInbox == 0) || ((privousForBgImgV<arrForInboxMessagesOnly.count && selectedIndexForFiltrationInbox == 0) && privousForBgImgV>=0) )
    {
        switch (selectedIndexForFiltrationInbox)
        {
            case 0:
                model = [_arrForInbox objectAtIndex:privousForBgImgV];
                break;
            case 1:
                model = [arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                break;
            case 2:
                model = [arrForAnnouncements objectAtIndex:privousForBgImgV];
                break;
            case 3:
                model = [arrForNews objectAtIndex:privousForBgImgV];
                break;
            case 4:
                model = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                break;
            default:
                model = [_arrForInbox objectAtIndex:privousForBgImgV];
                break;
        }
    }
    else
    {
        if((_arrForInbox.count>0 && selectedIndexForFiltrationInbox == 0) || (arrForInboxMessagesOnly.count>0 && selectedIndexForFiltrationInbox == 1))
        {
            privousForBgImgV=0;
            
            switch (selectedIndexForFiltrationInbox)
            {
                case 0:
                    model = [_arrForInbox objectAtIndex:0];
                    break;
                case 1:
                    model = [arrForInboxMessagesOnly objectAtIndex:0];
                    break;
                case 2:
                    model = [arrForAnnouncements objectAtIndex:0];
                    break;
                case 3:
                    model = [arrForNews objectAtIndex:0];
                    break;
                case 4:
                    model = [arrOfSelectedMessages objectAtIndex:0];
                    break;
                    
                default:
                    model = [_arrForInbox objectAtIndex:0];
                    break;
            }
        }
        else
        {
            privousForBgImgV=-1;
        }
    }
    if(model){
        if([model isKindOfClass:[Inbox class]])
        {
            Inbox *msz=(Inbox*)model;
            if([msz.message_id integerValue]>0){
                self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.message_id];
            }
            else{
                self._arrformsz=[[Service sharedInstance]getStoredMessageForLocal:msz.appId];
            }
            
            // Sorting Implementation
            NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
            NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
            self._arrformsz =[NSMutableArray arrayWithArray:[self._arrformsz sortedArrayUsingDescriptors:sortDescriptorArr]];
            _isShowOther=NO;
            [_tablViewForMsz reloadData];
        }
        else  if([model isKindOfClass:[Annoncements class]])
        {
            _isShowOther=YES;
            _tablViewForMsz.hidden=NO;
            self._arrformsz=nil;
        }
        else  if([model isKindOfClass:[News class]])
        {
            _isShowOther=YES;
            _tablViewForMsz.hidden=NO;
            self._arrformsz=nil;
        }
    }
    else
    {
        self._arrformsz=nil;
    }
    [_tableView reloadData];
    [_tablViewForMsz reloadData];
}

-(void)inboxMessgesDidSend:(NSNotification*)note
{
    if(_textFieldSubject.tag==1){
        if (insertedInboxCount == arrSelectedParticipernts.count) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Inbox Message Save" object:nil];
        }
    }else{
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Inbox Message Save" object:nil];
    }
    
    Inbox* newMsg  = note.object;
    
    if([newMsg.local_parent_id integerValue]==0)
    {
        NSArray *arr1;
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
            {
                if(![_arrForInbox containsObject:newMsg])
                {
                    [_arrForInbox addObject:newMsg];
                }
                arr1=_arrForInbox;
            }
                break;
            case 1:
            {
                if(![arrForInboxMessagesOnly containsObject:newMsg])
                {
                    [arrForInboxMessagesOnly addObject:newMsg];
                }
                arr1=arrForInboxMessagesOnly;
            }
                break;
            case 2:
            {
                if(![arrForAnnouncements containsObject:newMsg])
                {
                    [arrForAnnouncements addObject:newMsg];
                }
                arr1=arrForAnnouncements;
            }
                break;
            case 3:
                
            {
                if(![arrForNews containsObject:newMsg])
                {
                    [arrForNews addObject:newMsg];
                }
                arr1=arrForNews;
            }
                
                
                break;
                
            case 4:
                
            {
                if(![arrOfSelectedMessages containsObject:newMsg])
                {
                    [arrOfSelectedMessages addObject:newMsg];
                }
                arr1=arrOfSelectedMessages;
            }
                
                
                break;
                
                
            default:
            {
                if(![_arrForInbox containsObject:newMsg])
                {
                    [_arrForInbox addObject:newMsg];
                }
                arr1=_arrForInbox;
            }
                
                break;
        }
        
        
        // Sorting Implementation
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                [_arrForInbox removeAllObjects];
                break;
            case 1:
                [arrForInboxMessagesOnly removeAllObjects];
                break;
            case 2:
                [arrForAnnouncements removeAllObjects];
                break;
            case 3:
                [arrForNews removeAllObjects];
                break;
            case 4:
                [arrOfSelectedMessages removeAllObjects];
                break;
                
            default:
                [_arrForInbox removeAllObjects];
                break;
        }
        
        for(id obj in arr1)
        {
            
            switch (selectedIndexForFiltrationInbox) {
                case 0:
                    [_arrForInbox addObject:obj];
                    break;
                case 1:
                    [arrForInboxMessagesOnly addObject:obj];
                    break;
                case 2:
                    [arrForAnnouncements addObject:obj];
                    break;
                case 3:
                    [arrForNews addObject:obj];
                    break;
                case 4:
                    [arrOfSelectedMessages addObject:obj];
                    break;
                default:
                    [_arrForInbox addObject:obj];
                    break;
            }
        }
    }
    
    id model =nil;
    if(privousForBgImgV>=0){
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                model = [_arrForInbox objectAtIndex:privousForBgImgV];
                break;
            case 1:
                model = [arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                break;
            case 2:
                model = [arrForAnnouncements objectAtIndex:privousForBgImgV];
                break;
            case 3:
                model = [arrForNews objectAtIndex:privousForBgImgV];
                break;
            case 4:
                model = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                break;
            default:
                model = [_arrForInbox objectAtIndex:privousForBgImgV];
                break;
        }
        
    }
    else{
        privousForBgImgV=0;
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                model = [_arrForInbox objectAtIndex:0];
                break;
            case 1:
                model = [arrForInboxMessagesOnly objectAtIndex:0];
                break;
            case 2:
                model = [arrForAnnouncements objectAtIndex:0];
                break;
            case 3:
                model = [arrForNews objectAtIndex:0];
                break;
            case 4:
                model = [arrOfSelectedMessages objectAtIndex:0];
                break;
                
            default:
                model = [_arrForInbox objectAtIndex:0];
                break;
        }
        
    }
    
    
    
    
    
    
    [self refreshTableView:model];
    _tablViewForMsz.hidden=NO;
    [_tablViewForMsz reloadData];
    [_tableView reloadData];
    
}

#pragma mark -
#pragma mark Sorting Methods -

-(void)getInboxMessage
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    
    [arrForNews removeAllObjects];
    [arrForAnnouncements removeAllObjects];
    [arrForInboxMessagesOnly removeAllObjects];
    
    [_arrForInbox removeAllObjects];
    NSArray *arrA=[[Service sharedInstance]getStoredAllIAnnouncementForInbox];
    
    for(Annoncements *anc in arrA)
    {
        if(![_arrForInbox containsObject:anc])
        {
            [_arrForInbox addObject:anc];
            [arrForAnnouncements addObject:anc];
        }
    }
    
    NSArray *arrB=[[Service sharedInstance]getStoredAllINewsForInbox];
    for(News *anc in arrB)
    {
        if(![_arrForInbox containsObject:anc])
        {
            [_arrForInbox addObject:anc];
            [arrForNews addObject:anc];
        }
    }
    
    NSArray *arr=[[Service sharedInstance]getStoredMessage:[NSNumber numberWithInteger:0]];
    
    for(Inbox *msz  in arr)
    {
        if(![_arrForInbox containsObject:msz])
        {
            [_arrForInbox addObject:msz];
            [arrForInboxMessagesOnly addObject:msz];
        }
    }
    
    NSArray *arr1=_arrForInbox;
    
    // Sorting Implementation
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
    
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    
    arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [_arrForInbox removeAllObjects];
    for(id obj in arr1)
    {
        [_arrForInbox addObject:obj];
    }
    
    // Sorting Announcement Array
    NSArray *arrAnn=arrForAnnouncements;
    arrAnn =[NSMutableArray arrayWithArray:[arrAnn sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrForAnnouncements removeAllObjects];
    for(id obj in arrAnn)
    {
        [arrForAnnouncements addObject:obj];
    }
    
    // Sorting News Array
    NSArray *arrNews=arrForNews;
    arrNews =[NSMutableArray arrayWithArray:[arrNews sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrForNews removeAllObjects];
    for(id obj in arrNews)
    {
        [arrForNews addObject:obj];
    }
    
    // Sorting Message Array
    NSArray *arrMsg=arrForInboxMessagesOnly;
    arrMsg =[NSMutableArray arrayWithArray:[arrMsg sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrForInboxMessagesOnly removeAllObjects];
    for(id obj in arrMsg)
    {
        [arrForInboxMessagesOnly addObject:obj];
    }
    
    
#if DEBUG
    NSLog(@"%@",_arrForInbox);
#endif
    
    [_tableView reloadData];
}


-(IBAction)filterMessages:(UIButton *)sender
{
    [_createNewMsgView removeFromSuperview];
    UIButton *btn = sender;
    selectedIndexForFiltrationInbox = btn.tag;
    
    switch (selectedIndexForFiltrationInbox) {
        case 1:
        {
            if (sender.isSelected)
            {
                sender.selected = NO;
                isMessageSelected = NO;
            }
            else
            {
                sender.selected = YES;
                isMessageSelected = YES;
            }
        }
            break;
        case 2:
        {
            if (sender.isSelected)
            {
                sender.selected = NO;
                isAnnouncementSelected = NO;
            }
            else
            {
                sender.selected = YES;
                isAnnouncementSelected =YES;
            }
        }
            break;
        case 3:
        {
            if (sender.isSelected)
            {
                sender.selected = NO;
                isNewsSelected = NO;
            }
            else
            {
                sender.selected = YES;
                isNewsSelected = YES;
            }
        }
            
            break;
            
        default:
            break;
    }
    [self mergeInboxMessageArray];
    
#if DEBUG
    NSLog(@"News %d Message %d  Announcement %d",isNewsSelected,isMessageSelected,isAnnouncementSelected);
#endif
    
    /*******  if we dont reload _tablViewForMsz then its woking but if we reload i get crashed.  ***/
    //if (_tablViewForMsz != nil)
    // [_tablViewForMsz reloadData];
    
    [_tableView reloadData];
    self._tableView.scrollsToTop = YES ;
    
}

-(void)mergeInboxMessageArray
{
    NSArray *arr1 = nil;
    [arrOfSelectedMessages removeAllObjects];
    
    if ((isAnnouncementSelected == YES && isMessageSelected == YES && isNewsSelected == YES) || (isAnnouncementSelected == NO && isMessageSelected == NO && isNewsSelected == NO))
    {
        selectedIndexForFiltrationInbox = 0;
        
        [arrOfSelectedMessages addObjectsFromArray:arrForInboxMessagesOnly];
        [arrOfSelectedMessages addObjectsFromArray:arrForAnnouncements];
        [arrOfSelectedMessages addObjectsFromArray:arrForNews];
    }
    else if(isNewsSelected == YES && isMessageSelected == YES)
    {
        selectedIndexForFiltrationInbox = 4;
        
        [arrOfSelectedMessages addObjectsFromArray:arrForInboxMessagesOnly];
        [arrOfSelectedMessages addObjectsFromArray:arrForNews];
        
    }
    else if(isNewsSelected == YES && isAnnouncementSelected == YES)
    {
        selectedIndexForFiltrationInbox = 4;
        [arrOfSelectedMessages addObjectsFromArray:arrForAnnouncements];
        [arrOfSelectedMessages addObjectsFromArray:arrForNews];
    }
    else if(isAnnouncementSelected == YES && isMessageSelected == YES)
    {
        selectedIndexForFiltrationInbox = 4;
        [arrOfSelectedMessages addObjectsFromArray:arrForInboxMessagesOnly];
        [arrOfSelectedMessages addObjectsFromArray:arrForAnnouncements];
    }
    else if(isAnnouncementSelected == YES)
    {
        selectedIndexForFiltrationInbox = 2;
        // Sorting Implementation
        [arrOfSelectedMessages addObjectsFromArray:arrForAnnouncements];
    }
    else if(isNewsSelected == YES)
    {
        selectedIndexForFiltrationInbox = 3;
        // Sorting Implementation
        [arrOfSelectedMessages addObjectsFromArray:arrForNews];
    }
    else if(isMessageSelected == YES)
    {
        selectedIndexForFiltrationInbox = 1;
        // Sorting Implementation
        [arrOfSelectedMessages addObjectsFromArray:arrForInboxMessagesOnly];
    }
    
    arr1=arrOfSelectedMessages;

    // Sorting Implementation
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrOfSelectedMessages removeAllObjects];

    for(id obj in arr1)
    {
        [arrOfSelectedMessages addObject:obj];
    }
    
}


-(void)inboxMessgesDidGot:(NSNotification*)note
{
#if DEBUG
    NSLog(@"inboxMessgesDidGot %@",note.userInfo);
#endif
    
    [[AppDelegate getAppdelegate] hideIndicator];
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_getInboxMsz object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            for (NSDictionary *dictForAnn  in [note.userInfo objectForKey:@"messages"])
            {
                [[Service sharedInstance]parseInboxMessagesWithDictionary:dictForAnn];
            }
            NSArray *arr=[[Service sharedInstance]getStoredMessage:[NSNumber numberWithInteger:0]];
            for(Inbox *msz  in arr){
                if(![_arrForInbox containsObject:msz]){
                    [_arrForInbox addObject:msz];
                }
            }
            NSArray *arr1=_arrForInbox;
            // Sorting Implementation
            //NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:NO];
            NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
            
            NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
            arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
            
            [_arrForInbox removeAllObjects];
            for(id obj in arr1){
                [_arrForInbox addObject:obj];
            }
            [_tableView reloadData];
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}

#pragma mark Viewlife

-(void)viewDidAppear:(BOOL)animated{
    
}

-(void)syncMails
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        [[Service sharedInstance] synchronizeMails];
    }
    [self getInboxMessage];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isRowSelected = NO;
    isAnnouncementSelected = NO;
    isNewsSelected = NO;
    isMessageSelected = NO;
    // Sorting Implementation
    isArrowUP = NO;
    /************** Code Added For Push Notification  By Aman **********************/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [defaults objectForKey:@"UnVisited"];
    NSInteger t = [[dict objectForKey:@"T"] integerValue] ;
    NSInteger o = [[dict objectForKey:@"O"] integerValue] ;
    NSInteger i = [[dict objectForKey:@"I"] integerValue] ;
    
    NSInteger uvc = [[dict objectForKey:@"UnVisitedCount"] integerValue]- i;
    
    NSMutableDictionary *dicttemp = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:t],@"T",[NSNumber numberWithInteger:o],@"O",[NSNumber numberWithInteger:0],@"I",[NSNumber numberWithInteger:uvc],@"UnVisitedCount", nil];
    
    
    [AppHelper saveToUserDefaults:dicttemp withKey:@"UnVisited"];
    
    
    /************** Code Added For Push Notification  By Aman **********************/
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getInboxMessage) name:@"Mail synch complete" object:nil];
    _btnSortBydate.selected = YES ;
    _arrForInbox=[[NSMutableArray alloc]init];
    _arrForPicker=[[NSMutableArray alloc]init];
    sct=-1;
    prevousSect=-1;
    privousForBgImgV=-1;
    formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    [tblListView setHidden:YES] ;
    isListDisplayed = NO ;
    originalListTableViewHeight = tblListView.frame ;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(oneTap)];
    [tapGestureRecognizer setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapGestureRecognizer];
    tapGestureRecognizer=nil;
    
    if([profileData.type isEqualToString:@"Student"])
    {
        [self getTeacherFromDb];
        viewForTeacherLoginInbox.hidden = YES ;
        currentSelectedButton = 5 ;
        arrForList = arrTeacherList ;
    }
    else
    {
        currentSelectedButton = 1 ;
        viewForTeacherLoginInbox.hidden = NO ;
        [self getStudentFromDb];
        [self getParentFromDb];
        [self getClassFromDb];
        arrAllList = [[NSMutableArray alloc]init];
        [self getStudentAndParent];
    }
    
    // Do any additional setup after loading the view from its nib.
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"inboxpannel.png"]];
    _tableView.backgroundView = imageView;
    [imageView release];
    
    myTimer = [NSTimer scheduledTimerWithTimeInterval:120.0 target: self
                                             selector: @selector(syncMails) userInfo: nil repeats: YES];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        
        [self syncMails];
        [self getInboxMessage];
        //Stop your activity indicator or anything else with the GUI
        //Code here is run on the main thread
        
    });
    
    arrForNews = [[NSMutableArray alloc]init];
    arrForAnnouncements = [[NSMutableArray alloc]init];
    arrForInboxMessagesOnly = [[NSMutableArray alloc]init];
    arrOfSelectedMessages = [[NSMutableArray alloc]init];
    
    selectedIndexForFiltrationInbox = 0;
    
    if (_arrForInbox.count>0) {
        [self onViewDidLoadSelected:[_arrForInbox objectAtIndex:0]];
    }
    
    btnSelectSpecificStudents.hidden = YES ;
    btnDiscardChanges.hidden = YES ;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidDisappear:(BOOL)animated{
    if(myTimer!=nil){
        [myTimer invalidate];
        myTimer=nil;
    }
    
}
- (void)dealloc

{
    
    
    if(myTimer!=nil){
        [myTimer invalidate];
        myTimer=nil;
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Mail synch complete" object:nil];
    [_arrformsz release];
    [_popoverControler release];
    [formatter release];
    [_arrForInbox release];
    [_tableView release];
    [_createNewMsgView release];
    [_btnForNewMail release];
    [_viewForPicker release];
    [_picker release];
    [_textfieldForReciver release];
    [_textFieldSubject release];
    [_textViewForDescription release];
    [_tablViewForMsz release];
    [_viewForLinKContent release];
    [_webViewForLinkContent release];
    [viewForTeacherLoginInbox release];
    [tblListView release];
    [btnSelectAll release];
    [lblSelectAll release];
    [btnSelectSpecificStudents release];
    [btnAll release];
    [btnSend release];
    [btnDiscardChanges release];
    [_btnSortBydate release];
    [ParentButton release];
    [ParentLabel release];
    [ClassButton release];
    [ClassLabel release];
    [AllLabel release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [self set_tableView:nil];
    [_createNewMsgView release];
    _createNewMsgView = nil;
    [_btnForNewMail release];
    _btnForNewMail = nil;
    [_viewForPicker release];
    _viewForPicker = nil;
    [_picker release];
    _picker = nil;
    [_textfieldForReciver release];
    _textfieldForReciver = nil;
    [_textFieldSubject release];
    _textFieldSubject = nil;
    [_textViewForDescription release];
    _textViewForDescription = nil;
    [_tablViewForMsz release];
    _tablViewForMsz = nil;
    [_viewForLinKContent release];
    _viewForLinKContent = nil;
    [_webViewForLinkContent release];
    _webViewForLinkContent = nil;
    [viewForTeacherLoginInbox release];
    viewForTeacherLoginInbox = nil;
    
    [tblListView release];
    tblListView = nil;
    
    [btnSelectAll release];
    btnSelectAll = nil;
    [self setLblSelectAll:nil];
    [btnSelectSpecificStudents release];
    btnSelectSpecificStudents = nil;
    
    [btnDiscardChanges release];
    btnDiscardChanges = nil ;
    [super viewDidUnload];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
#if DEBUG
    NSLog(@"******  %d selectedIndexForFiltrationInbox %d",arrForAnnouncements.count,selectedIndexForFiltrationInbox    );
#endif
    
    if (tableView == tblListView)
    {
        return 1 ;
    }
    else if(tableView==_tableView)
    {
        // Sorting Implementation
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                return _arrForInbox.count;
                break;
            case 1:
                return arrOfSelectedMessages.count;
                break;
            case 2:
                return arrOfSelectedMessages.count;
                break;
            case 3:
                return arrOfSelectedMessages.count;
                break;
            case 4:
                return arrOfSelectedMessages.count;
                break;
            default:
                return _arrForInbox.count;
                break;
        }
    }
    else
    {
        return 1;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return height for rows.
    if (tableView == tblListView) {
        return HeightForListCell ;
    }
    else if(_tableView==tableView)
    {
        return 30;
    }
    else{
        if(privousForBgImgV>-1)
        {
            id model = [_arrForInbox objectAtIndex:privousForBgImgV];
            
            if([model isKindOfClass:[Inbox class]])
            {
                Inbox *msz=(Inbox*)model;
                float rowHeight=0;
                rowHeight=[self dynamicCellHeights:msz.discrptin];
#if DEBUG
                NSLog(@"inbox message = %@",msz.discrptin);
                NSLog(@"inbox message rowHeight = %f",rowHeight);
#endif

                if (rowHeight>570) {
                    return 570;
                }
                else{
                    return MAX(rowHeight+350, 300);
                }
                
            }
            else  if([model isKindOfClass:[Annoncements class]]){
                Annoncements *msz=(Annoncements*)model;
                float rowHeight=0;
                rowHeight=[self dynamicCellHeights:msz.details];
                if (rowHeight>570) {
                    return 570;
                }
                else{
                    //07/08/2013
                    return MAX(rowHeight+200, 300);
                }
                
            }
            else  if([model isKindOfClass:[News class]]){
                News *msz=(News*)model;
                float rowHeight=0;
                rowHeight=[self dynamicCellHeights:msz.details];
                if (rowHeight>570) {
                    return 570;
                }
                else{
                    return MAX(rowHeight+125, 376);
                }
            }
        }
        else{
            return 300;
        }
        
        
        return 0;
    }
}


-(float)dynamicCellHeights:(NSString*)commentText
{
    
    float rowHeight=0;
    CGSize constraint = CGSizeMake(1090.0f, 1000.0f);
    CGSize size = [commentText sizeWithFont:[UIFont fontWithName:@"Arial" size:25.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    rowHeight =  size.height;
    return rowHeight;
}

// calculate the row height for a text type content item

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == tblListView) {
        if (isSearchOn) {
            return [arrSearchedList count];
        }else{
            return [arrForList count];
        }
    }
    else if(_tableView==tableView)
    {
        if(sct==section)
        {
            if(self._arrformsz.count>0)
                return self._arrformsz.count+1;
            else
            {
                return self._arrformsz.count;
            }
        }
        else{
            return 0;
        }
    }
    else{
        if(_arrForInbox.count>0)
            return self._arrformsz.count+1;
        else
        {
            return self._arrformsz.count;
        }
        
        
        
    }
    
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(_tableView==tableView)
    {
        return 78;
    }
    else{
        return 1;
    }
    
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(_tableView==tableView)
    {
        
        UIView *headerView=[[[UIView alloc] init] autorelease];
        headerView.frame=CGRectMake(0, 0,216, 78);
        headerView.backgroundColor=[UIColor clearColor];
        headerView.tag=300+section;
        UIImageView *imgViewCellBg=[[UIImageView alloc] init];
        imgViewCellBg.backgroundColor=[UIColor clearColor];
        imgViewCellBg.frame=CGRectMake(0, 0,216, 78);
        imgViewCellBg.tag=section+200000000;
        [headerView addSubview:imgViewCellBg];
        
        UIImageView *imgViewForIcon=[[UIImageView alloc] init];
        imgViewForIcon.backgroundColor=[UIColor clearColor];
        imgViewForIcon.frame=CGRectMake(12, 7,15, 11);
        [headerView addSubview:imgViewForIcon];
        
        
        UILabel* lblForName=[[UILabel alloc] init];
        lblForName.frame=CGRectMake(36, 2,112, 21);
        lblForName.font=[UIFont boldSystemFontOfSize:11.0f];
        lblForName.backgroundColor=[UIColor clearColor];
        lblForName.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
        [headerView addSubview:lblForName];
        
        UILabel* lblForTime=[[UILabel alloc] init];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            lblForTime.frame=CGRectMake(147, 4,70, 17);
        }
        else
        {
            lblForTime.frame=CGRectMake(150, 4,70, 17);
        }
        lblForTime.font=[UIFont systemFontOfSize:12.0f];
        lblForTime.textColor=[UIColor colorWithRed:97.0/255.0 green:142.0/255.0 blue:195.0/255.0 alpha:1.0];
        // Inbox Time Label
        lblForTime.backgroundColor=[UIColor clearColor];
        [headerView addSubview:lblForTime];
        
        UILabel* lblForDesc=[[UILabel alloc] init];
        lblForDesc.frame= CGRectMake(12, 25,190, 50);
        lblForDesc.numberOfLines=3;
        lblForDesc.font=[UIFont systemFontOfSize:9.0f];
        lblForDesc.backgroundColor=[UIColor clearColor];
        lblForDesc.textColor=[UIColor darkGrayColor];
        [headerView addSubview:lblForDesc];
        
        UIButton *btnForBackGroundTap=[UIButton buttonWithType:UIButtonTypeCustom];
        btnForBackGroundTap.tag=section;
        [btnForBackGroundTap addTarget:self action:@selector(backGroundTapButttonAction:) forControlEvents:UIControlEventTouchUpInside];
        btnForBackGroundTap.frame=CGRectMake(0, 0,216, 78);
        btnForBackGroundTap.backgroundColor=[UIColor clearColor];
        [headerView addSubview:btnForBackGroundTap];
        
        id model = nil;
        
        // Sorting Implementation
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                model =   [_arrForInbox objectAtIndex:section];
                break;
            case 1:
                model =  [arrOfSelectedMessages objectAtIndex:section];
                //model = [arrForInboxMessagesOnly objectAtIndex:section];
                break;
            case 2:
                model =  [arrOfSelectedMessages objectAtIndex:section];
                //model = [ arrForAnnouncements objectAtIndex:section];
                break;
            case 3:
                model =  [arrOfSelectedMessages objectAtIndex:section];
                //model =  [arrForNews objectAtIndex:section];
                break;
            case 4:
                model =  [arrOfSelectedMessages objectAtIndex:section];
                break;
            default:
                model = [ _arrForInbox objectAtIndex:section];
                break;
        }
        
        
        if([model isKindOfClass:[Inbox class]])
        {
            Inbox *msz=(Inbox*)model;
            UIButton *btnForList=[UIButton buttonWithType:UIButtonTypeCustom];
            [btnForList setImage:[UIImage imageNamed:@"inboxlistno.png"] forState:UIControlStateNormal];
            btnForList.tag=section;
            [btnForList addTarget:self action:@selector(tapButtton:) forControlEvents:UIControlEventTouchUpInside];
            btnForList.frame=CGRectMake(170, 35,26, 18);
            [headerView addSubview:btnForList];
            imgViewForIcon.image=[UIImage imageNamed:@"inboxchaticon.png"];
            UILabel* lblForCount=[[UILabel alloc] init];
            lblForCount.frame=CGRectMake(175, 35,18, 18);
            lblForCount.font=[UIFont systemFontOfSize:9.0f];
            lblForCount.backgroundColor=[UIColor clearColor];
            lblForCount.textColor=[UIColor whiteColor];
            lblForCount.tag=section+100000000;
            [headerView addSubview:lblForCount];
            
            
            lblForName.text=msz.subject;
            lblForDesc.text=[self stripTags:msz.subject];
            NSArray *arr;
            
            if([msz.message_id integerValue]==0)
                arr=[[Service sharedInstance]getStoredMessage:msz.appId];
            else
                arr=[[Service sharedInstance]getStoredMessage:msz.message_id];

            if(arr.count>0)
            {
                lblForCount.hidden=NO;
                btnForList.hidden=NO;
                lblForCount.text=[NSString stringWithFormat:@"%lu",(unsigned long)arr.count+1];
            }
            else
            {
                lblForCount.hidden=YES;
                btnForList.hidden=YES;
            }
            [formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            
            [formatter setDateFormat:@"dd-MM-YYYY"];
            
            if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:msz.created_time]]){
                
                int noOfHours;
                int noOfmin;
                
                int timeDiff=(int)[[NSDate date] timeIntervalSinceDate:msz.created_time];

                noOfHours=timeDiff/(60*60);
                noOfmin=(timeDiff%(60*60))/60;
                
                if(noOfHours>0)
                {
                    lblForTime.text=[NSString stringWithFormat:@"%d hr ago",noOfHours];
                }
                else
                {
                    lblForTime.text=[NSString stringWithFormat:@"%d min ago",noOfmin];
                }
            }
            else{
                [formatter setDateFormat:kDATEFORMAT];
                lblForTime.text=[formatter stringFromDate:msz.created_time];
                
            }
            [lblForCount release];
        }
        else  if([model isKindOfClass:[Annoncements class]])
        {
            
            imgViewForIcon.image=[UIImage imageNamed:@"inboxmickicon.png"];
            Annoncements *msz=(Annoncements*)model;
            
            
            lblForName.text=@"Announcement";
            
            NSString *plaintext = [msz.details stringByConvertingHTMLToPlainText];
            lblForDesc.text=plaintext;
            
            [formatter setDateFormat:@"dd-MM-YYYY"];
            
            if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:msz.created_time]])
            {
                int noOfHours;
                int noOfmin;
                
                int timeDiff=(int)[[NSDate date] timeIntervalSinceDate:msz.created_time];
                noOfHours=timeDiff/(60*60);
                noOfmin=(timeDiff%(60*60))/60;
                
                if(noOfHours>0){
                    lblForTime.text=[NSString stringWithFormat:@"%d hr ago",noOfHours];
                }
                else{
                    lblForTime.text=[NSString stringWithFormat:@"%d min ago",noOfmin];
                }
            }
            else{
                [formatter setDateFormat:kDATEFORMAT];
                lblForTime.text=[formatter stringFromDate:msz.created_time];
                
            }
        }
        else  if([model isKindOfClass:[News class]]){
            
            imgViewForIcon.image=[UIImage imageNamed:@"inboxnewsicon"];
            News *msz=(News*)model;
            
            
            lblForName.text=@"News";
            NSString *plaintext = [msz.title stringByConvertingHTMLToPlainText];
            lblForDesc.text=plaintext;//[self stripTags:msz.title];
            
            [formatter setDateFormat:@"dd-MM-YYYY"];
            
            if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:msz.created_time]]){
                int noOfHours;
                int noOfmin;
                
                int timeDiff=(int)[[NSDate date] timeIntervalSinceDate:msz.created_time];
                noOfHours=timeDiff/(60*60);
                noOfmin=(timeDiff%(60*60))/60;
                
                if(noOfHours>0){
                    lblForTime.text=[NSString stringWithFormat:@"%d hr ago",noOfHours];
                }
                else{
                    lblForTime.text=[NSString stringWithFormat:@"%d min ago",noOfmin];
                }
            }
            else{
                [formatter setDateFormat:kDATEFORMAT];
                lblForTime.text=[formatter stringFromDate:msz.created_time];
                
            }
#if DEBUG
            NSLog(@"News lblForTime.text %@",lblForTime.text);
#endif
      
        }
#if DEBUG
        NSLog(@"%lu==%lu",privousForBgImgV,section);
#endif
        
        if(privousForBgImgV==section)
        {
            imgViewCellBg.image=[UIImage imageNamed:@"inboxmailselect.png"];
            
        }
        else{
            imgViewCellBg.image=[UIImage imageNamed:@"inboxmailbase.png"];
        }
        
        
        
        [imgViewCellBg release];
        [imgViewForIcon release];
        [lblForName release];
        [lblForDesc release];
        [lblForTime release];
        return headerView;
    }
    else{
        return nil;
    }
}

-(NSString *) getDaysOfOverdueUpcoming :(NSDate *) date
{
    NSString *strDifference = [[NSString alloc]init];
    NSInteger difference;
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    NSDate *startDate = [NSDate date];

    NSDate *endDate = date;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    difference = components.day;
    
    if (difference>0) //Upcoming
    {
        if (difference == 1)
            strDifference = [NSString stringWithFormat:@"%ld day later",(long)difference];
        else
            strDifference = [NSString stringWithFormat:@"%ld days later",(long)difference];
    }
    
    else if (difference <0) //Overdue
    {
        difference = labs(difference);
        
        if (difference == 1)
            strDifference = [NSString stringWithFormat:@"%ld day ago",(long)difference];
        else
            strDifference = [NSString stringWithFormat:@"%ld days ago",(long)difference];
    }
    else if (difference == 0)
    {
        strDifference = [NSString stringWithFormat:@"Today"];
    }
    
    return strDifference;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tblListView == tableView) {
        static NSString *CellIdentifier = @"ListIdentifier";
        InboxVcListTableCell *cell = (InboxVcListTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil) {
            NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"InboxVcListTableCell" owner:self options:nil];
            cell = [cellArray lastObject];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        cell.btnCheckListName.tag = indexPath.row ;
        [cell.btnCheckListName addTarget:self action:@selector(checkBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mailbase2.png"]];
        
        switch (currentSelectedButton) {
            case 1:
            {
                Student *student = nil ;
                if (isSearchOn) {
                    student = [arrSearchedList objectAtIndex:indexPath.row];
                }else{
                    student = [arrForList objectAtIndex:indexPath.row];
                }
                
                cell.lblFirst.text = student.firstname;
                cell.lblSecond.hidden = YES ;
                
                if (btnSelectAll.selected) {
                    cell.btnCheckListName.selected = YES ;
                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                }else{
                    
                    if (arrSelectedParticipernts.count != 0) {
                        for (Student *s in arrSelectedParticipernts) {
                            if ([s.student_id isEqualToString:student.student_id]) {
                                cell.btnCheckListName.selected = YES ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                break ;
                            }else{
                                cell.btnCheckListName.selected = NO ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                
                break;
                
            }
            case 2:
            {
                MyParent *parent = nil ;
                
                if (isSearchOn) {
                    parent = [arrSearchedList objectAtIndex:indexPath.row];
                }else{
                    parent = [arrForList objectAtIndex:indexPath.row];
                }
                
                cell.lblFirst.text = parent.name;
                cell.lblSecond.hidden = NO ;
                cell.lblSecond.text = parent.studentName ;
                
                if (btnSelectAll.selected)
                {
                    cell.btnCheckListName.selected = YES ;
                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                }
                else
                {
                    if (arrSelectedParticipernts.count != 0)
                    {
                        for (MyParent *p in arrSelectedParticipernts)
                        {
#if DEBUG
                            NSLog(@"p.name =%@",parent.name);
#endif
                            // arrSelectedParticipernts contains same parent Id But we need unique values due to this reason we compared with parent Id & Student ID Asw well.
                            
                            // New Code Aman Migration Prime
                            
                            if ([p.parent_id_str isEqualToString:parent.parent_id_str] && [p.studentId isEqualToString:parent.studentId] )
                            {
                                cell.btnCheckListName.selected = YES ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                break ;
                            }
                            else
                            {
                                cell.btnCheckListName.selected = NO ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                break;
            }
            case 3:{
                if (isSpecificStudentTapped) {
                    Student *student = nil ;
                    
                    if (isSearchOn) {
                        student = [arrSearchedList objectAtIndex:indexPath.row];
                    }else{
                        student = [arrForList objectAtIndex:indexPath.row];
                    }
                    
                    cell.lblFirst.text = student.firstname;
                    cell.lblSecond.hidden = YES ;
                    
                    if (btnSelectAll.selected) {
                        cell.btnCheckListName.selected = YES ;
                        [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                    }else{
                        
                        if (arrSelectedParticipernts.count != 0) {
                            for (Student *s in arrSelectedParticipernts) {
                                if ([s.student_id isEqualToString:student.student_id]) {
                                    cell.btnCheckListName.selected = YES ;
                                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                    break ;
                                }else{
                                    cell.btnCheckListName.selected = NO ;
                                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                                }
                            }
                        }
                    }
                    
                    
                }else
                {
                    
                    MyClass *class = nil ;
#if DEBUG
                    NSLog(@"arrForList %@",arrForList);
#endif
                    NSString *className = NSStringFromClass([[arrForList firstObject] class]);
                    
                    if (isSearchOn) {
                        class = [arrSearchedList objectAtIndex:indexPath.row];
                    }else{
                        if([className isEqualToString:@"MyClass"])
                        {
                            class = [arrForList objectAtIndex:indexPath.row] ;

                        }
                        else if ([className isEqualToString: @"Student"] )
                        {
                        class = [[arrForList objectAtIndex:indexPath.row] myClass];
                        }
                    
                        
                       
                    }
#if DEBUG
                    NSLog(@"Cell for row class %@",class);
                    NSLog(@"Cell for row class.name %@",class.class_name);
#endif
                    
                    // Campus Based Start
                    
                    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                    NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
                    
                    if (totalCampusFromDB > 1 )
                    {
                        NSString *classNameWithCampusCode = [NSString stringWithFormat:@"%@(%@)",class.class_name,class.campusCode];
                        cell.lblFirst.text=classNameWithCampusCode;
                        
                    }
                    else
                    {
                        cell.lblFirst.text = class.class_name;
                    }
                    // Campus Based End
                    
                    
                    
                    cell.lblSecond.hidden = YES ;
                    if (arrSelectedParticipernts.count != 0) {
                        for (MyClass *c in arrSelectedParticipernts) {
                            if ([c.class_id integerValue] == [class.class_id integerValue]) {
                                cell.btnCheckListName.selected = YES ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                break ;
                            }else{
                                cell.btnCheckListName.selected = NO ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                
                break ;
            }
            case 4:{
                
#if DEBUG
                NSLog(@"case 4");
#endif

                Student *student = nil ;
                MyParent *parent = nil ;
                NSObject *object = nil ;
                if (isSearchOn) {
                    object = [arrSearchedList objectAtIndex:indexPath.row];
                }else{
                    object = [arrForList objectAtIndex:indexPath.row];
                }
                
                if ([object isKindOfClass:[Student class]]) {
                    student = (Student *)object ;
                    cell.lblFirst.text = student.firstname;
                    cell.lblSecond.hidden = YES ;
                }else{
                    parent = (MyParent *) object ;
                    cell.lblFirst.text = parent.name;
                    cell.lblSecond.hidden = NO ;
                    cell.lblSecond.text = parent.studentName ;
                }
                
                
                if (btnSelectAll.selected) {
                    cell.btnCheckListName.selected = YES ;
                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                }else{
                    
                    if (arrSelectedParticipernts.count != 0) {
                        for (NSObject *obj in arrSelectedParticipernts) {
                            if ([obj isKindOfClass:[Student class]]) {
                                Student *s = (Student *)obj;
                                if ([s.student_id isEqualToString:student.student_id]) {
                                    cell.btnCheckListName.selected = YES ;
                                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                    break ;
                                }else{
                                    cell.btnCheckListName.selected = NO ;
                                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                                }
                            }else{
                                MyParent *p = (MyParent *)obj ;
                                // New Code Aman Migration Prime
                                
                                if ([p.parent_id_str isEqualToString:parent.parent_id_str]) {
                                    cell.btnCheckListName.selected = YES ;
                                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                    break ;
                                }else{
                                    cell.btnCheckListName.selected = NO ;
                                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                                }
                            }
                            
                        }
                    }
                }
                break ;
            }
            case 5:{
                MyTeacher *teacher = nil ;
                if (isSearchOn)
                {
                    teacher = [arrSearchedList objectAtIndex:indexPath.row];
                }
                else
                {
                    teacher = [arrForList objectAtIndex:indexPath.row];
                }
                cell.lblFirst.text = teacher.name;
                cell.lblSecond.hidden = NO ;
                cell.lblSecond.text = @"" ;
                
                if (btnSelectAll.selected) {
                    cell.btnCheckListName.selected = YES ;
                    [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                }else{
                    if (arrSelectedParticipernts.count != 0) {
                        for (MyTeacher *t in arrSelectedParticipernts) {
                            
                            if ([t.teacher_id isEqualToString:teacher.teacher_id])
                            {
                                cell.btnCheckListName.selected = YES ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
                                break ;
                            }
                            else
                            {
                                cell.btnCheckListName.selected = NO ;
                                [cell.btnCheckListName setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                
                break;
                
            }
            default:
                break;
        }
        return cell ;
    }
    
    else if(_tableView==tableView)
    {
        NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
        UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (myCell == nil)
        {
            myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
            myCell.backgroundColor=[UIColor clearColor ];
            myCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            myCell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            
        }
        
        myCell.textLabel.frame=CGRectMake(18, 0,240, 30);
        myCell.textLabel.font=[UIFont systemFontOfSize:15.0f];
        myCell.textLabel.backgroundColor=[UIColor clearColor];
        myCell.textLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
        
        if(indexPath.row<self._arrformsz.count)
        {
            Inbox *msz=[self._arrformsz objectAtIndex:indexPath.row];
            myCell.textLabel.text=msz.subject;
            
        }
        else
        {
            Inbox *msz=[_arrForInbox objectAtIndex:privousForBgImgV];
            myCell.textLabel.text=msz.subject;
        }
        
        return myCell;
    }
    else
    {
        static NSString *CellIdentifier = @"myIdenfier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSArray *nib;
        if(cell == nil)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"InboxVcCustomCellMsz" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            //cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            
        }
        cell.tag=indexPath.row;
        
        UIImageView *imgViewForIcon=(UIImageView*)[cell.contentView viewWithTag:2];
        UIView *contentView=(UIView*)[cell.contentView viewWithTag:500];
        UIView *contentView1=(UIView*)[cell.contentView viewWithTag:600];
        
        UILabel *sender=(UILabel*)[cell.contentView viewWithTag:101];
        UILabel *reciver=(UILabel*)[cell.contentView viewWithTag:102];
        UILabel *subject=(UILabel*)[cell.contentView viewWithTag:103];
        UILabel *time=(UILabel*)[cell.contentView viewWithTag:104];
        UILabel *info=(UILabel*)[cell.contentView viewWithTag:105];
        
        UIWebView *web=(UIWebView*)[cell.contentView viewWithTag:200];
        UIButton *replybtton=(UIButton*)[cell.contentView viewWithTag:107];
        UIButton *deleteBtton=(UIButton*)[cell.contentView viewWithTag:108];
        UILabel *lableForTag=(UILabel*)[cell.contentView viewWithTag:210];
        UIWebView *lableForAnnounce=(UIWebView*)[cell.contentView viewWithTag:211];
        UILabel *lableForCampuse=(UILabel*)[cell.contentView viewWithTag:212];
        UILabel *lableForName=(UILabel*)[cell.contentView viewWithTag:213];
        UILabel *lableForCamName=(UILabel*)[cell.contentView viewWithTag:214];
        UILabel *lableForAutherName=(UILabel*)[cell.contentView viewWithTag:215];
        UILabel *lableForAudianceCaption=(UILabel*)[cell.contentView viewWithTag:300];
        UILabel *lableForAudianceValue=(UILabel*)[cell.contentView viewWithTag:301];
        
        lableForAudianceCaption.hidden = YES ;
        lableForAudianceValue.hidden = YES ;
        
        replybtton.tag=indexPath.row;
        [replybtton addTarget:self action:@selector(replyButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        deleteBtton.tag=indexPath.row;
        [deleteBtton addTarget:self action:@selector(deleteButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if ((arrForInboxMessagesOnly.count < 1 && selectedIndexForFiltrationInbox == 1) || (_arrForInbox.count < 1))
        {
#if DEBUG
            NSLog(@"Message Count is 0");
#endif
      
        }
        else {
            if(privousForBgImgV>-1 )
            {
                
                id model = [_arrForInbox objectAtIndex:privousForBgImgV];
                
                @try {
                    
                    // Sorting Implementation
                    switch (selectedIndexForFiltrationInbox) {
                        case 0:
                            model = [_arrForInbox objectAtIndex:privousForBgImgV];
                            break;
                        case 1:
                            model = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                            break;
                        case 2:
                            model = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                            break;
                        case 3:
                            model = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                            break;
                        case 4:
                            model = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                            break;
                        default:
                            model = [_arrForInbox objectAtIndex:privousForBgImgV];
                            break;
                    }
                    
                    
                }
                @catch (NSException *exception)
                {
#if DEBUG
                    NSLog(@"Excepion occured in cellrow at indexpath line number 1669 %@",exception.description);
#endif
                }
                
                
                
                if([model isKindOfClass:[Inbox class]])
                {
                    time.textAlignment=NSTextAlignmentRight;
                    
                    contentView.hidden=YES;
                    contentView1.hidden=YES;
                    web.hidden=NO;
                    time.frame=CGRectMake(470, 15, time.frame.size.width+15, time.frame.size.height);
                    
                    lableForAnnounce.hidden=YES;
                    lableForCampuse.hidden=YES;
                    lableForName.hidden=YES;
                    lableForTag.hidden=YES;
                    lableForCamName.hidden=YES;
                    imgViewForIcon.hidden=YES;
                    deleteBtton.hidden=NO;
                    replybtton.hidden=NO;
                    
                    MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                    
                    
                    lableForAutherName.hidden=YES;
                    Inbox *msz=nil;
                    
                    // Sorting Implementation
                    switch (selectedIndexForFiltrationInbox) {
                        case 0:
                        {
                            if(indexPath.row<self._arrformsz.count)
                            {
                                msz=[self._arrformsz objectAtIndex:indexPath.row];
                                
                            }
                            else
                            {
                                if(privousForBgImgV>-1)
                                    msz=[_arrForInbox objectAtIndex:privousForBgImgV];
                            }
                        }
                            break;
                        case 1:
                            
                        {
                            if(indexPath.row<self._arrformsz.count)
                            {
                                msz=[self._arrformsz objectAtIndex:indexPath.row];
                                
                            }
                            else
                            {
                                if(privousForBgImgV>-1){
                                    msz=[arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                                    //msz=[arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                                }
                            }
                        }
                            
                            break;
                        case 2:
                        {
                            if(indexPath.row<self._arrformsz.count)
                            {
                                msz=[self._arrformsz objectAtIndex:indexPath.row];
                                
                            }
                            else
                            {
                                if(privousForBgImgV>-1){
                                    msz=[arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                                    //msz=[arrForAnnouncements objectAtIndex:privousForBgImgV];
                                }
                            }
                        }
                            
                            break;
                        case 3:
                        {
                            if(indexPath.row<self._arrformsz.count)
                            {
                                msz=[self._arrformsz objectAtIndex:indexPath.row];
                                
                            }
                            else
                            {
                                if(privousForBgImgV>-1){
                                    msz=[arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                                    //msz=[arrForNews objectAtIndex:privousForBgImgV];
                                }
                            }
                        }
                            break;
                            
                        case 4:
                            
                        {
                            if(indexPath.row<self._arrformsz.count)
                            {
                                msz=[self._arrformsz objectAtIndex:indexPath.row];
                                
                            }
                            else
                            {
                                if(privousForBgImgV>-1)
                                    msz=[arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                            }
                        }
                            
                            break;
                            
                        default:
                        {
                            if(indexPath.row<self._arrformsz.count)
                            {
                                msz=[self._arrformsz objectAtIndex:indexPath.row];
                                
                            }
                            else
                            {
                                if(privousForBgImgV>-1 && _arrForInbox.count > 0)
                                    msz=[_arrForInbox objectAtIndex:privousForBgImgV];
                            }
                        }
                            break;
                    }
                    
                    
                    
                    
                    //  - (MyProfile *)getProfileDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification
                    //                MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                    
                    
                    if([[AppHelper userDefaultsForKey:@"user_id"] isEqualToString:msz.sender_id]){
                        
                        replybtton.hidden = YES;
                        
                    }else{
                        if ([profileData.type isEqualToString:@"Teacher"]) {
                            replybtton.hidden = NO;
                            deleteBtton.hidden = NO ;
                        }else{
                            deleteBtton.hidden = YES ;
                        }
                        
                    }
                    
                    sender.text=[NSString stringWithFormat:@" From : %@",msz.senderName];
                    reciver.text=[NSString stringWithFormat:@" To : %@",msz.reciverName];
                    
                    
                    
                    
                    // reciver.text=@"BBB";
                    subject.text=msz.subject;
                    // discrption.text=msz.discrptin;
                    
                    [web loadHTMLString:msz.discrptin baseURL:nil];
                    web.delegate=self;
                    info.text=@"sent";
                    [formatter setDateFormat:kDATEFORMAT];
                    
                    //int timeDiff=(int)[[NSDate date] timeIntervalSinceDate:msz.created_time];
                    if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:msz.created_time]]){
                        [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                        time.text=[formatter stringFromDate:msz.created_time];
                    }
                    else{
                        NSString *strFrmt = [NSString stringWithFormat:@"%@ hh:mm a",kDATEFORMAT];
                        //[formatter setDateFormat:@"dd-MM-YYYY hh:mm a"];
                        [formatter setDateFormat:strFrmt];
                        time.text=[formatter stringFromDate:msz.created_time];
                        
                    }
                    
                }
                else  if([model isKindOfClass:[Annoncements class]])
                {
                    time.textAlignment=NSTextAlignmentRight;
                    
                    contentView.hidden=YES;
                    contentView1.hidden=YES;
                    lableForAnnounce.hidden=NO;
                    web.hidden=YES;
                    lableForCampuse.hidden=NO;
                    lableForName.hidden=NO;
                    lableForTag.hidden=YES;
                    lableForCamName.hidden=NO;
                    imgViewForIcon.hidden=NO;
                    deleteBtton.hidden=YES;
                    replybtton.hidden=YES;
                    lableForAutherName.hidden=NO;
                    imgViewForIcon.image=[UIImage imageNamed:@"inboxmickicon.png"];
                    
                    lableForAudianceCaption.hidden = NO ;
                    lableForAudianceValue.hidden = NO ;
                    //lableForTag.frame=CGRectMake(30, 192, lableForTag.frame.size.width, lableForTag.frame.size.height);
                    //lableForTag.text=@"Author:";
                    
                    lableForAutherName.text=@"Author:";
                    
                    //time.frame=CGRectMake(30, 215, time.frame.size.width, time.frame.size.height);
                    Annoncements *msz=(Annoncements*)model;
                    reciver.hidden=YES;
                    subject.hidden=YES;
                    sender.text=[NSString stringWithFormat:@"%@",@"Announcement"];
                    //discrption.text=msz.details;
                    //  lableForAnnounce.text=msz.details;
                    [lableForAnnounce loadHTMLString:msz.details baseURL:nil];
                    if(msz.campuse!=nil){
                        lableForCamName.text=msz.campuse;
                    }
                    else{
                        lableForCamName.text=nil;
                    }
                    
                    int p = [msz.all_parents integerValue];
                    int s = [msz.all_Student integerValue];
                    int t = [msz.all_Teacher integerValue];
                    NSString *str = @"";
                    if (p == 1) {
                        str = @"Parents";
                    }
                    
                    if (s == 1) {
                        if (str.length != 0) {
                            str = [str stringByAppendingString:@", Students"];
                        }else{
                            str = [str stringByAppendingString:@"Students"];
                        }
                    }
                    
                    if (t == 1) {
                        if (str.length != 0) {
                            str = [str stringByAppendingString:@", Teachers"];
                        }else{
                            str = [str stringByAppendingString:@"Teachers"];
                        }
                    }
                    
                    
                    lableForAudianceValue.text = str ;
                    
                    lableForName.text=msz.created_by;
                    info.hidden=YES;
                    [formatter setDateFormat:kDATEFORMAT];
                    
                    //int timeDiff=(int)[[NSDate date] timeIntervalSinceDate:msz.created_time];
                    if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:msz.created_time]]){
                        [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                        time.text=[formatter stringFromDate:msz.created_time];
                    }
                    else{
                        [formatter setDateFormat:kDATEFORMAT];
                        time.text=[formatter stringFromDate:msz.created_time];
                        
                    }
                }
                else  if([model isKindOfClass:[News class]])
                {
                    time.textAlignment=NSTextAlignmentRight;
                    
                    contentView.hidden=NO;
                    contentView1.hidden=NO;
                    web.hidden=NO;
                    //lableForTag.frame=CGRectMake(30, 259, lableForTag.frame.size.width, lableForTag.frame.size.height);
                    lableForTag.text=@"TAGS:";
                    time.frame=CGRectMake(486, 15, time.frame.size.width, time.frame.size.height);
                    
                    lableForAnnounce.hidden=YES;
                    lableForCampuse.hidden=YES;
                    lableForName.hidden=YES;
                    lableForTag.hidden=NO;
                    lableForCamName.hidden=YES;
                    deleteBtton.hidden=YES;
                    replybtton.hidden=YES;
                    lableForAutherName.hidden=YES;
                    imgViewForIcon.image=[UIImage imageNamed:@"inboxnewsicon"];
                    imgViewForIcon.hidden=NO;
                    News *msz=(News*)model;
                    reciver.hidden=YES;
                    subject.text=msz.title;
                    sender.text=[NSString stringWithFormat:@" %@",@"News"];
                    
                    
                    
                    
                    NSMutableArray *arrForAttach =[[NSMutableArray alloc] init];
                    
                    
                    for (News_Attachment *newAttObj in msz.attacments) {
                        [arrForAttach addObject:newAttObj];
                    }
                    
                    NSArray *arrForAttach1=[NSArray arrayWithArray:arrForAttach];
                    [arrForAttach release];
                    
                    //19AugChange
                    
                    if(arrForAttach1.count>0){
                        int x=85;
                        for(int i=1;i<=arrForAttach1.count;i++){
                            
                            UIButton *btnForBackGroundTap=[UIButton buttonWithType:UIButtonTypeCustom];
                            [btnForBackGroundTap addTarget:self action:@selector(attachmentTapButttonAction:) forControlEvents:UIControlEventTouchUpInside];

                            btnForBackGroundTap.frame=CGRectMake(x,0 , 26, 25);
                            [btnForBackGroundTap setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                            btnForBackGroundTap.backgroundColor=[UIColor clearColor];
                            News_Attachment *new=nil;
                            if (i==0) {
                                new= [arrForAttach1 objectAtIndex:0];
                            }
                            else{
                                new= [arrForAttach1 objectAtIndex:i-1];
                            }
                            
                            
                            [btnForBackGroundTap setTitle:new.atachment_content forState:UIControlStateNormal];
                            [btnForBackGroundTap setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                            [btnForBackGroundTap setTitleColor:[UIColor clearColor] forState:UIControlStateSelected];
                            
                            [btnForBackGroundTap setBackgroundImage:[UIImage imageNamed:@"inboxattachmenticon.png"] forState:UIControlStateNormal];
                            x=x+26+10;
                            [contentView1 addSubview:btnForBackGroundTap];
                        }
                    }
                    
                    
                    
                    //discrption.text=msz.details;
                    NSArray *arr=[msz.tags componentsSeparatedByString:@","];
                    NSMutableArray *filteredTagArray = [[NSMutableArray alloc]init];
                    
                    for (int i = 0; i < arr.count; i++) {
                        
                        if ([[arr objectAtIndex:i] isEqualToString:@""])  {
                        }
                        else
                        {
                            [filteredTagArray addObject:[arr objectAtIndex:i]];
                        }
                    }
                    
                    if(filteredTagArray.count>0){
                        int x=85;
                        for(int i=1;i<=filteredTagArray.count;i++){
                            UILabel *tag=[[UILabel alloc]initWithFrame:CGRectMake(x, lableForTag.frame.origin.y, 50, lableForTag.frame.size.height+5)];
                            tag.numberOfLines=2;
                            [tag setFont:[UIFont fontWithName:@"Arial" size:10]];
                            [tag.layer setMasksToBounds:YES];
                            [tag.layer setCornerRadius:5.0f];
                            [tag.layer setBorderWidth:1.0];
                            [tag.layer setBorderColor:[[UIColor clearColor] CGColor]];
                            tag.backgroundColor=[UIColor darkGrayColor];
                            tag.textColor=[UIColor whiteColor];
                            tag.textAlignment=NSTextAlignmentCenter;
                            if (i==0) {
                                tag.text=[filteredTagArray objectAtIndex:0];
                            }
                            else{
                                tag.text=[filteredTagArray objectAtIndex:i-1];
                            }
                            
                            x=x+50+10;
                            [contentView addSubview:tag];
                            [tag release];
                        }
                    }
                    
                    [web loadHTMLString:msz.details baseURL:nil];
                    web.delegate=self;
                    info.hidden=YES;
                    [formatter setDateFormat:@"dd-MM-YYYY"];
                    
                    if([[formatter stringFromDate:[NSDate date]] isEqualToString:[formatter stringFromDate:msz.created_time]])
                    {
                        [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
                        time.text=[formatter stringFromDate:msz.created_time];
                    }
                    else
                    {
                        [formatter setDateFormat:kDATEFORMAT];
                        time.text=[formatter stringFromDate:msz.created_time];
                    }
                    
                }
            }
            
        }
        
       [cell.contentView setBackgroundColor:[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1.0f] ];
        
        return cell;
        
        
    }
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_tableView==tableView)
    {
        
    }
}


#pragma mark
#pragma mark Button Actions

-(void)attachmentTapButttonAction:(id)sender
{
    UIButton *btn=(UIButton*)sender;

    NSString * str=[btn.titleLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:str];
    
    if (![[UIApplication sharedApplication] openURL:url])
    {
#if DEBUG
        NSLog(@"%@%@",@"Failed to open url:",[url description]);
#endif
      
    }
}

- (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}


-(void)showlinkContentMtehod
{
    [self.view addSubview:_viewForLinKContent];
    
}
- (IBAction)closeContentVButtonAction:(id)sender {
    [_viewForLinKContent removeFromSuperview];
}



- (IBAction)checkListButtonTapped:(UIButton *)sender {
    
}

-(void)deleteButtonAction:(id)sender
{
    
    UIButton *btn=(UIButton*)sender;
    Inbox *msz=nil;
    if(self._arrformsz.count>btn.tag)
    {
        msz=[self._arrformsz objectAtIndex:btn.tag];
    }
    else
    {
        // TODO: Need to test this after sorting...
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                msz=[_arrForInbox objectAtIndex:privousForBgImgV];
                break;
            case 1:
                msz=[arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                break;
            case 2:
                msz=[arrForAnnouncements objectAtIndex:privousForBgImgV];
                break;
            case 3:
                msz=[arrForNews objectAtIndex:privousForBgImgV];
                break;
            case 4:
                msz=[arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                break;
                
            default:
                msz=[_arrForInbox objectAtIndex:privousForBgImgV];
                break;
        }
        
    }
    
    if([msz.message_id integerValue]>0)
    {
        if(msz.ipAdd)
        {
            [[Service sharedInstance] messageDeletedWithAppId:msz.appId ipAddress:msz.ipAdd];
        }
        else
        {
            [[Service sharedInstance]messageDeletedWithAppId:msz.message_id];
        }
    }
    else
    {
        [[Service sharedInstance] deleteObjectOfInBoxMessgefromAppId:msz.appId ipAddress:msz.ipAdd];
    }
    
    [self inboxMessgesDidDeleted:msz.appId iPAdd:msz.ipAdd msgId:msz.message_id];
    [self syncMails];
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    NSMutableDictionary *dictFlurryParameter = [[NSMutableDictionary alloc]init];
    
    [dictFlurryParameter setValue:_textFieldSubject.text forKey:@"subject"];
    // Need to add colour here
    
    [dictFlurryParameter setValue:_textfieldForReciver.text forKey:@"receiver"];

    [dictFlurryParameter setValue:profobj.email forKey:@"sender"];
    [dictFlurryParameter setValue:profobj.profile_id forKey:@"userid"];
    [dictFlurryParameter setValue:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"schoolId"];
    [dictFlurryParameter setValue:profobj.type forKey:@"Role"];
    if ([profobj.type isEqualToString:@"Teacher"])
        [dictFlurryParameter setValue:@"0" forKey:@"grade"];
    else
        [dictFlurryParameter setValue:[NSString stringWithFormat:@"%@",profobj.year] forKey:@"grade"];
    
    [dictFlurryParameter setValue:@"iPad" forKey:@"platform"];
    
    [Flurry logEvent:FLURRY_EVENT_Message_Deleted withParameters:dictFlurryParameter];
    
    
}
-(void)replyButtonAction:(id)sender{
    UIButton *btn=(UIButton*)sender;
    
    btnSelectAll.hidden = YES ;
    lblSelectAll.hidden = YES ;
    _picker.tag=1;
    _textFieldSubject.tag=2;
    _textfieldForReciver.text=nil;

    _textFieldSubject.text=nil;
    _textViewForDescription.text=nil;
    _textViewForDescription.tag=btn.tag;
    Inbox *msz=nil;
    if(self._arrformsz.count>btn.tag){
        msz=[self._arrformsz objectAtIndex:btn.tag];
    }
    else{
        // TODO: Need to test this after sorting...
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                msz=[_arrForInbox objectAtIndex:privousForBgImgV];
                break;
            case 1:
                msz=[arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                break;
            case 2:
                msz=[arrForAnnouncements objectAtIndex:privousForBgImgV];
                break;
            case 3:
                msz=[arrForNews objectAtIndex:privousForBgImgV];
                break;
            case 4:
                msz=[arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                break;
            default:
                msz=[_arrForInbox objectAtIndex:privousForBgImgV];
                break;
        }
        
    }
    
    _textfieldForReciver.text=msz.senderName ;
    //strRecivers=msz.senderName;
    
    _textFieldSubject.text=msz.subject;
    _tablViewForMsz.hidden=YES;
    _btnForNewMail.selected=YES;
    CGRect rect=_createNewMsgView.frame;
    rect.origin.x=250;
    rect.origin.y=90;
    [_createNewMsgView setFrame:rect];
    _textfieldForReciver.userInteractionEnabled = FALSE ;
    btnSelectSpecificStudents.hidden = YES ;
    btnDiscardChanges.hidden = YES ;
    [self.view addSubview:_createNewMsgView];
    
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    NSMutableDictionary *dictFlurryParameter = [[NSMutableDictionary alloc]init];
    
    [dictFlurryParameter setValue:_textFieldSubject.text forKey:@"subject"];
    // Need to add colour here
    
    [dictFlurryParameter setValue:_textfieldForReciver.text forKey:@"receiver"];
    //[dictFlurryParameter setValue:strRecivers forKey:@"receiver"];
    [dictFlurryParameter setValue:profobj.email forKey:@"sender"];
    [dictFlurryParameter setValue:profobj.profile_id forKey:@"userid"];
    [dictFlurryParameter setValue:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"schoolId"];
    [dictFlurryParameter setValue:profobj.type forKey:@"Role"];
    if ([profobj.type isEqualToString:@"Teacher"])
        [dictFlurryParameter setValue:@"0" forKey:@"grade"];
    else
        [dictFlurryParameter setValue:[NSString stringWithFormat:@"%@",profobj.year] forKey:@"grade"];
    
    [dictFlurryParameter setValue:@"iPad" forKey:@"platform"];
    
    [Flurry logEvent:FLURRY_EVENT_Reply_to_Message withParameters:dictFlurryParameter];
    
}

-(void)tapButtton:(id)sender
{
    [_createNewMsgView removeFromSuperview];
    UIButton *btn=(UIButton*)sender;
    prevousSect=sct;
    sct=btn.tag;
    id model = [_arrForInbox objectAtIndex:btn.tag];
    privousForBgImgV=btn.tag;
    if([model isKindOfClass:[Inbox class]]){
        Inbox *msz=(Inbox*)model;
        self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.message_id];
        // Sorting Implementation
        //NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:YES];
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        self._arrformsz =[NSMutableArray arrayWithArray:[self._arrformsz sortedArrayUsingDescriptors:sortDescriptorArr]];
        _tablViewForMsz.hidden=NO;
        [_tablViewForMsz reloadData];
        
    }
    
    [_tableView reloadData];
    
    
}

#pragma  mark This Method Called When We Click Select Any thing Form Left TableView


-(void)backGroundTapButttonAction:(id)sender
{
    isRowSelected = YES;
    UIButton *btn=(UIButton*)sender;
    
    _btnForNewMail.selected=NO;
    selectedMsgRow = btn.tag ;
    [_createNewMsgView removeFromSuperview];
    privousForBgImgV=btn.tag;
    sct=-1;
    
    // TODO: Need to test this after sorting...
    id model = [_arrForInbox objectAtIndex:btn.tag];
    
    switch (selectedIndexForFiltrationInbox) {
        case 0:
            model = [_arrForInbox objectAtIndex:btn.tag];
            break;
        case 1:
            model = [arrForInboxMessagesOnly objectAtIndex:btn.tag];
            break;
        case 2:
            model = [arrForAnnouncements objectAtIndex:btn.tag];
            break;
        case 3:
            model = [arrForNews objectAtIndex:btn.tag];
            break;
        case 4:
            model = [arrOfSelectedMessages objectAtIndex:btn.tag];
            break;
            
        default:
            model = [_arrForInbox objectAtIndex:btn.tag];
            break;
    }
    
    
    
    if([model isKindOfClass:[Inbox class]])
    {
        Inbox *msz=(Inbox*)model;
        
        if([msz.message_id integerValue]==0)
            self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.appId];
        else
            self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.message_id];
        
        // Sorting Implementation
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        self._arrformsz =[NSMutableArray arrayWithArray:[self._arrformsz sortedArrayUsingDescriptors:sortDescriptorArr]];
        _tablViewForMsz.hidden=NO;
        _isShowOther=NO;
    }
    else if([model isKindOfClass:[Annoncements class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        Annoncements *msz=(Annoncements*)model;
        msz.isReadAnn=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
        MainViewController *mainVC= (MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
        [mainVC readNotificationOnInboox:model];

        self._arrformsz=nil;
    }
    else  if([model isKindOfClass:[News class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        News *msz=(News*)model;
        msz.isReadNews=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
        
        MainViewController *mainVC= (MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
        [mainVC readNotificationOnInboox:model];
        self._arrformsz=nil;
    }
    [_tablViewForMsz reloadData];
    [_tableView reloadData];
    
    UIView*cell2=(UIView*) btn.superview;
    [self._tableView scrollRectToVisible:cell2.frame animated:YES];
    
}

- (IBAction)sendButtonAction:(id)sender
{
    if(!(NSSTRING_HAS_DATA(_textfieldForReciver.text)))
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please select atleast one recipient." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    }
    
    else if(!NSSTRING_HAS_DATA(_textFieldSubject.text))
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter subject." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    }
    else if(_textFieldSubject.tag == 1){
        if(arrSelectedParticipernts.count == 0){
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please select atleast one recipient." message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
            [alert release];
        }else{
            [self sendMsg];
        }
    }else{
        [self sendMsg];
    }
}

- (void) sendMsg {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inboxMessgesDidSend:) name:@"Inbox Message Save" object:nil];
    //        BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    //        if (networkReachable)
    //        {
    _btnForNewMail.selected=NO;
    [_createNewMsgView removeFromSuperview];
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inboxMessgesDidSend:) name:NOTIFICATION_sendInboxMsz object:nil];
    NSString *reciverId=nil;
    
    //        MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if(_textFieldSubject.tag==1)
    {
        
        switch (currentSelectedButton) {
                
            case 1:// Student
            {
                insertedInboxCount = 0 ;
                
                NSMutableArray *arrForReceiverId=[[NSMutableArray alloc]init];
                
                for (Student *stu in arrSelectedParticipernts) {
                    
                    [arrForReceiverId addObject:stu.student_id];
                }
                
                NSString *strReceiverId=[arrForReceiverId componentsJoinedByString:@","];
                [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text des:_textViewForDescription.text reciver:strReceiverId parentId:nil localParentId:nil parentMailId:nil];
                insertedInboxCount++ ;
                break;
            }
            case 2:// Parents
            {
                insertedInboxCount = 0 ;
                NSMutableArray *arrForReceiverId = [[NSMutableArray alloc]init];
                NSMutableArray *arrForMailId = [[NSMutableArray alloc]init];
                
                for (MyParent *p in arrSelectedParticipernts) {
                    
                    // New Code Aman Migration Prime
                    
                    [arrForReceiverId addObject:p.parent_id_str];
                    [arrForMailId addObject:p.email];
                }
                
                NSString *strReceiverId=[arrForReceiverId componentsJoinedByString:@","];
                NSString *strMailId=[arrForReceiverId componentsJoinedByString:@","];
                
                [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text des:_textViewForDescription.text reciver:strReceiverId parentId:nil localParentId:nil parentMailId:strMailId];
                insertedInboxCount++ ;
                break;
            }
            case 3:{ // class wise students
                
                if (isSpecificStudentTapped) {
                    insertedInboxCount = 0 ;
                    NSMutableArray *arrForReceiverId=[[NSMutableArray alloc]init];
                    
                    for (Student *stu in arrSelectedParticipernts) {
                        
                        [arrForReceiverId addObject:stu.student_id];
                    }
                    
                    NSString *strReceiverId=[arrForReceiverId componentsJoinedByString:@","];
                    [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text
                                                            des:_textViewForDescription.text
                                                        reciver:strReceiverId
                                                       parentId:nil
                                                  localParentId:nil
                                                   parentMailId:nil];
                    insertedInboxCount++ ;
                    
                }else{
                    insertedInboxCount = 0;
                    for (MyClass *c in arrSelectedParticipernts) {
                        
                        NSMutableArray *arrForReceiverId=[[NSMutableArray alloc]init];
                        
                        for (Student *stu in c.arrRegisteredStudents) {
                            
                            [arrForReceiverId addObject:stu.student_id];
                        }
                        
                        NSString *strReceiverId=[arrForReceiverId componentsJoinedByString:@","];
                        [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text
                                                                des:_textViewForDescription.text
                                                            reciver:strReceiverId
                                                           parentId:nil
                                                      localParentId:nil
                                                       parentMailId:nil];
                        insertedInboxCount++ ;
                    }
                }
                break;
            }
            case 4: // Student and Parents
            {
                insertedInboxCount = 0 ;
                
                NSMutableArray *arrForReceiverId=[[NSMutableArray alloc]init];
                NSMutableArray *arrForMailId = [[NSMutableArray alloc]init];
                
                for (NSObject *obj in arrSelectedParticipernts) {
                    
                    if ([obj isKindOfClass:[Student class]]) {
                        Student *s = (Student *)obj;
                        [arrForReceiverId addObject:s.student_id];
                    }else{
                        MyParent *p = (MyParent *)obj ;
                        // New Code Aman Migration Prime
                        
                        [arrForReceiverId addObject:p.parent_id_str];
                        [arrForMailId addObject:p.email];
                    }
                }
                
                NSString *strReceiverId=[arrForReceiverId componentsJoinedByString:@","];
                NSString *strMailId=[arrForReceiverId componentsJoinedByString:@","];
                
                [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text
                                                        des:_textViewForDescription.text
                                                    reciver:strReceiverId
                                                   parentId:nil
                                              localParentId:nil
                                               parentMailId:strMailId];
                
                insertedInboxCount++ ;
                break;
            }
            case 5: // When Student logs in student can send mail to teacher
            {
                insertedInboxCount = 0 ;
                NSMutableArray *arrForReceiverId=[[NSMutableArray alloc]init];
                for (MyTeacher *t in arrSelectedParticipernts) {
                    [arrForReceiverId addObject:t.teacher_id];
                }
                NSString *strReceiverId=[arrForReceiverId componentsJoinedByString:@","];
                
                [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text
                                                        des:_textViewForDescription.text
                                                    reciver:strReceiverId
                                                   parentId:nil
                                              localParentId:nil
                                               parentMailId:nil];
                
                insertedInboxCount++ ;
                break;
            }
            default:
                break;
        }
        [self syncMails];
        //[[Service sharedInstance]storeInboxMessages:_textFieldSubject.text des:_textViewForDescription.text reciver:reciverId parentId:nil localParentId:nil];
    }
    else // Reply for Any Mail
    {
        // TODO: Need to test this after sorting...
        Inbox *msz=[_arrForInbox objectAtIndex:privousForBgImgV];
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                msz = [_arrForInbox objectAtIndex:privousForBgImgV];
                break;
            case 1:
                msz = [arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                break;
            case 2:
                msz = [arrForAnnouncements objectAtIndex:privousForBgImgV];
                break;
            case 3:
                msz = [arrForNews objectAtIndex:privousForBgImgV];
                break;
            case 4:
                msz = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                break;
                
                
            default:
                msz = [_arrForInbox objectAtIndex:privousForBgImgV];
                break;
        }
        
        
        
        //            if(_picker.tag==1)
        //            {
        Inbox *msz1=nil;
        if(self._arrformsz.count>_textViewForDescription.tag)
        {
            msz1=[self._arrformsz objectAtIndex:_textViewForDescription.tag];
        }
        else
        {
            // TODO: Need to test this after sorting...
            msz1=[_arrForInbox objectAtIndex:privousForBgImgV];
            
            switch (selectedIndexForFiltrationInbox) {
                case 0:
                    msz1 = [_arrForInbox objectAtIndex:privousForBgImgV];
                    break;
                case 1:
                    msz1 = [arrForInboxMessagesOnly objectAtIndex:privousForBgImgV];
                    break;
                case 2:
                    msz1 = [arrForAnnouncements objectAtIndex:privousForBgImgV];
                    break;
                case 3:
                    msz1 = [arrForNews objectAtIndex:privousForBgImgV];
                    break;
                case 4:
                    msz1 = [arrOfSelectedMessages objectAtIndex:privousForBgImgV];
                    break;
                    
                default:
                    msz1 = [_arrForInbox objectAtIndex:privousForBgImgV];
                    break;
            }
            
            
            
        }
        
        
        
        reciverId = msz1.sender_id ;
        
        NSString *strMailId = nil;
        
        // Added By Amol Gaikwad For Message Inbox Functionality Student Name empty is not impacting on functionaliy. Only parent Id is enough for this reply functionality. Thats why we are sending empty student ID.
        
        MyParent *parent = [[Service sharedInstance]getParenteDataInfoStored:msz1.sender_id studentID:@"" postNotification:NO];
        
        
        if(parent)
        {
            strMailId = parent.email ;
        }
        
        [[Service sharedInstance]storeInboxMessages:_textFieldSubject.text des:_textViewForDescription.text reciver:reciverId parentId:msz.message_id localParentId:msz.appId parentMailId:strMailId];
        
        [self syncMails];
        
        
        
    }
    
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    NSMutableDictionary *dictFlurryParameter = [[NSMutableDictionary alloc]init];
    
    [dictFlurryParameter setValue:_textFieldSubject.text forKey:@"subject"];
    // Need to add colour here
    
    [dictFlurryParameter setValue:[NSString stringWithFormat:@"%@",_textfieldForReciver.text] forKey:@"receiver"];

    [dictFlurryParameter setValue:profobj.email forKey:@"sender"];
    [dictFlurryParameter setValue:profobj.profile_id forKey:@"userid"];
    [dictFlurryParameter setValue:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"schoolId"];
    [dictFlurryParameter setValue:profobj.type forKey:@"Role"];
    
    if ([profobj.type isEqualToString:@"Teacher"])
    {
        [dictFlurryParameter setValue:@"0" forKey:@"grade"];
    }
    else
    {
        [dictFlurryParameter setValue:[NSString stringWithFormat:@"%@",profobj.year] forKey:@"grade"];
    }
    
    [dictFlurryParameter setValue:@"iPad" forKey:@"platform"];
    
    [Flurry logEvent:FLURRY_EVENT_Message_Created withParameters:dictFlurryParameter];
}

- (IBAction)writeNewMailButtonAction:(id)sender
{
    /**** Addded By Amol Gaikwad For Teacher can not send message to parant   ***/
    
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if([profobj.type isEqualToString:@"Student"] || ([profobj.type isEqualToString:@"Teacher"] && [DELEGATE isClassuserDataRecevivedFromServer] ))
    {
        if ([schoolDetailObj.allow_parent isEqualToNumber:[NSNumber numberWithInteger:1]] && [profobj.type isEqualToString:@"Teacher"] )
        {
            
            ParentLabel.hidden = NO;
            ParentButton.hidden = NO;
            
            ClassButton.frame = CGRectMake(349, -1, 27, 27);
            ClassLabel.frame = CGRectMake(390, 0, 47, 27);
            btnAll.frame = CGRectMake(457, -1, 27, 27);
            AllLabel.frame = CGRectMake(498, -1, 27, 27);
            
            
        }
        else
        {
            ParentLabel.hidden = YES;
            ParentButton.hidden = YES;
            
            ClassButton.frame = CGRectMake(238, -1, 27, 27);
            ClassLabel.frame = CGRectMake(273, 0, 47, 27);
            btnAll.frame = CGRectMake(349, -1, 27, 27);
            AllLabel.frame = CGRectMake(390, -1, 27, 27);
        }
        /***  Allow Parent check end ***/
        
        
        
        selectedMsgRow = 0 ;
        //    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        if (_btnForNewMail.selected==NO)
        {
            btnSelectAll.hidden = NO ;
            lblSelectAll.hidden = NO ;
            _textfieldForReciver.text=nil;
            //strRecivers=@"";
            _textFieldSubject.text=nil;
            _textViewForDescription.text=nil;
            prevousSect=-1;
            sct=-1;
            _tablViewForMsz.hidden=YES;
            //change
            // privousForBgImgV=-1;
            _textFieldSubject.tag=1;
            _btnForNewMail.selected=YES;
            
            
            CGRect rect=_createNewMsgView.frame;
            rect.origin.x=250;
            rect.origin.y=90;
            [_createNewMsgView setFrame:rect];
            
            MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
            arrSelectedParticipernts=nil;
            arrSelectedParticipernts = [[NSMutableArray alloc]init];
            arrSearchedList = nil;
            arrSearchedList = [[NSMutableArray alloc]init];
            dictForParticipent=nil;
            dictForParticipent = [[NSMutableDictionary alloc]init];
            btnSelectSpecificStudents.hidden = YES ;
            btnDiscardChanges.hidden = YES ;
            isSpecificStudentTapped = NO ;
            isSearchOn = NO ;
            _textfieldForReciver.userInteractionEnabled = TRUE ;
            if([profobj.type isEqualToString:@"Teacher"]){
                
                CGRect rect=viewForTeacherLoginInbox.frame;
                rect.origin.y=22;
                [viewForTeacherLoginInbox setFrame:rect];
                [_createNewMsgView addSubview:viewForTeacherLoginInbox];
                
                
            }
            else{
                
            }
            
            [self.view addSubview:_createNewMsgView];
        }
        else
        {
            //
        }
    }
    else
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:ERROR_FETCHING_DATA_FROM_SERVER_INBOX] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [myAlertView show];
        [myAlertView release];
        return;
        
    }
    
    
    
}

- (IBAction)savePickerButtonClick:(id)sender
{
    _picker.tag=2;
    if([self._popoverControler isPopoverVisible])
    {
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler=nil;
    }
    
    MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if([profileData.type isEqualToString:@"Student"])
    {
        MyTeacher *class=[_arrForPicker objectAtIndex:[_picker selectedRowInComponent:0]];
        if(class.salutation!=nil){
            _textfieldForReciver.text=[NSString stringWithFormat:@"%@ %@",class.salutation,class.name];
            //strRecivers = [NSString stringWithFormat:@"%@ %@",class.salutation,class.name];
            //  return ;
        }
        else{
            _textfieldForReciver.text=class.name;
            //strRecivers=class.name;
        }
        
    }
    else{
        Student *class=[_arrForPicker objectAtIndex:[_picker selectedRowInComponent:0]];
        _textfieldForReciver.text=class.firstname;
        //strRecivers=class.firstname;
    }
    
    
    
    _textfieldForReciver.tag=[_picker selectedRowInComponent:0];
}

- (IBAction)cancelPickerButtonClick:(id)sender {
    if([self._popoverControler isPopoverVisible]){
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler=nil;
    }
}

#pragma mark for Teacher Mail
-(void)sendNewMailToTeacher:(MyTeacher *)teacher
{
    arrSelectedParticipernts = [[NSMutableArray alloc]init];
    [arrSelectedParticipernts addObject:teacher];
    btnDiscardChanges.hidden = YES ;
    btnSelectAll.hidden = YES ;
    lblSelectAll.hidden = YES ;
    btnSelectSpecificStudents.hidden = YES ;
    _textfieldForReciver.text=teacher.name;
    //strRecivers=teacher.name;
    _textFieldSubject.text=nil;
    _textViewForDescription.text=nil;
    prevousSect=-1;
    sct=-1;
    _tablViewForMsz.hidden=YES;
    //change
    // privousForBgImgV=-1;
    _textFieldSubject.tag=1;
    _btnForNewMail.selected=YES;
    CGRect rect=_createNewMsgView.frame;
    rect.origin.x=250;
    rect.origin.y=90;
    [_createNewMsgView setFrame:rect];
    
    [self.view addSubview:_createNewMsgView];
}


#pragma mark serch method for news and Announcement
-(void)searchdNewsAnaAnnouncment:(id)model
{
    
    _btnForNewMail.selected=NO;
    
    [_createNewMsgView removeFromSuperview];
    NSInteger Aindex=0;
    sct=-1;
    
    
    
    if([model isKindOfClass:[Annoncements class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        Annoncements *msz=(Annoncements*)model;
        self._arrformsz=nil;
        Aindex = [_arrForInbox indexOfObject:msz];
        
        
        msz.isReadAnn=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
    }
    else  if([model isKindOfClass:[News class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        News *msz=(News*)model;
        self._arrformsz=nil;
        Aindex = [_arrForInbox indexOfObject:msz];
        
        msz.isReadNews=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
   }
    privousForBgImgV=Aindex;
    [_tablViewForMsz reloadData];
    [_tableView reloadData];
    
    [_tableView setContentOffset:CGPointMake(0, Aindex*78)];
}



-(void)onViewDidLoadSelected:(id)model
{
    
#if DEBUG
    NSLog(@"_arrForInbox...%lu",_arrForInbox.count);
#endif
    
    
    _btnForNewMail.selected=NO;
    
    [_createNewMsgView removeFromSuperview];
    NSInteger Aindex=0;
    sct=-1;
    
    
    
    if([model isKindOfClass:[Annoncements class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        Annoncements *msz=(Annoncements*)model;
        self._arrformsz=nil;
        Aindex = [_arrForInbox indexOfObject:msz];
        
        
        msz.isReadAnn=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
    }
    else  if([model isKindOfClass:[News class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        News *msz=(News*)model;
        self._arrformsz=nil;
        Aindex = [_arrForInbox indexOfObject:msz];
        
        msz.isReadNews=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
    }
    else if([model isKindOfClass:[Inbox class]])
    {
        Inbox *msz=(Inbox*)model;
        
        if([msz.message_id integerValue]==0)
            self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.appId];
        
        else
            self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.message_id];
        
        // Sorting Implementation
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        self._arrformsz =[NSMutableArray arrayWithArray:[self._arrformsz sortedArrayUsingDescriptors:sortDescriptorArr]];
        _tablViewForMsz.hidden=NO;
        _isShowOther=NO;
        
        Aindex = [_arrForInbox indexOfObject:msz];
    }
    
    
    privousForBgImgV=Aindex;
    [_tablViewForMsz reloadData];
    [_tableView reloadData];
    
    
}





#pragma mark pickerView delgates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_arrForPicker count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if([profileData.type isEqualToString:@"Student"])
    {
        MyTeacher *class=[_arrForPicker objectAtIndex:row];
        if(class.salutation!=nil){
            return [NSString stringWithFormat:@"%@ %@",class.salutation,class.name];
        }
        else{
            return class.name;
        }
    }
    else{
    }
    return @"";
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}

#pragma mark
#pragma mark WebView Delegate
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    //CAPTURE USER LINK-CLICK.
    
    NSURL *url = [request URL];
    NSString *str =   [url absoluteString];
    
    NSURL *url1 = [NSURL URLWithString:str];
    
    if ([str hasPrefix:@"http"]) {
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url1];
        //Load the request in the UIWebView.
        [_webViewForLinkContent loadRequest:requestObj];
        [self showlinkContentMtehod];
        return NO;
    }
    else{
        return YES;
    }
    
}

#pragma mark
#pragma mark Inbox New Functionality

- (IBAction)discardChangesTapped:(UIButton *)sender {
    
    [tblListView setScrollsToTop:YES];
    [arrSelectedParticipernts removeAllObjects];
    [dictForParticipent removeAllObjects];
    isSpecificStudentTapped = NO ;
    btnSelectAll.userInteractionEnabled = FALSE ;
    btnSelectAll.enabled = FALSE ;
    lblSelectAll.textColor = [UIColor grayColor];
    isSearchOn = NO ;
    listSearchBar.text = @"";
    [listSearchBar resignFirstResponder];
    if (btnSelectAll.selected) {
        btnSelectAll.selected = NO ;
        [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
    }
    
    _textfieldForReciver.text = @"";
    //strRecivers=@"";
    currentSelectedButton = sender.tag ;
    arrForList = arrClassList ;
    [self setDynamicHeightToTable];
    if (isListDisplayed) {
        [tblListView setHidden:NO] ;
        
        [tblListView reloadData];
    }
    
    sender.selected = YES ;
    for(UIView *view in [viewForTeacherLoginInbox subviews]){
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)view ;
            if(btn.tag ==3)
            {
                [btn setSelected:NO];
                currentSelectedButton = 100 ;
            }
        }
    }
    
    btnSelectSpecificStudents.hidden = YES;
    btnDiscardChanges.hidden = YES ;
    
    //ClassButton.selected = YES;
    [self inboxClassListTapped:ClassButton];
    
}


- (IBAction)selectSpecificStudentsTapped:(UIButton *)sender {
    
#if DEBUG
    NSLog(@"sender.title = %@",sender.titleLabel.text);
#endif
    
    NSString *strTitleLabelofButton = sender.titleLabel.text ;
    isSearchOn = NO ;
    listSearchBar.text = @"";
    isSpecificStudentTapped = YES ;
    isListDisplayed = YES ;
    
    arrForList = [AppDelegate getAppdelegate]._curentClass.arrRegisteredStudents ;
    if ([strTitleLabelofButton hasPrefix:@"Cha"]) {
        
    }else{
        [arrSelectedParticipernts removeAllObjects];
        [dictForParticipent removeAllObjects];
        _textfieldForReciver.text = @"";
        //strRecivers=@"";
    }
    
    [self setDynamicHeightToTable];
    tblListView.hidden = NO ;
    [tblListView reloadData];
    
}



// New Inbox Functionality

- (IBAction)selectallButtonTapped:(UIButton *)sender {
    [dictForParticipent removeAllObjects];
    [arrSelectedParticipernts removeAllObjects];
    
    if (sender.selected) {
        sender.selected = NO ;
        [sender setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
        
        _textfieldForReciver.text = @"" ;
        //strRecivers=@"";
    }else{
        sender.selected = YES ;
        [sender setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
        switch (currentSelectedButton) {
            case 1:
                for (Student *s in arrStudentList) {
                    [dictForParticipent setObject:s.firstname forKey:s.student_id];
                }
                
                [arrSelectedParticipernts addObjectsFromArray:arrStudentList];
                _textfieldForReciver.text = @"All students selected" ;
                //strRecivers=@"All students selected";
                break;
            case 2:{
                for (MyParent *p in arrParentsList) {
                    // New Code Aman Migration Prime
                    
                    [dictForParticipent setObject:p.name forKey:p.parent_id_str];
                }
                [arrSelectedParticipernts addObjectsFromArray:arrParentsList];
                _textfieldForReciver.text = @"All parents selected" ;
                //strRecivers = @"All parents selected";
                break ;
            }
            case 4:{
                for (NSObject *object in arrAllList) {
                    if ([object isKindOfClass:[Student class]]) {
                        Student *s = (Student *)object ;
                        [dictForParticipent setObject:s.firstname forKey:s.student_id];
                    }else{
                        MyParent *p = (MyParent *)object;
                        // New Code Aman Migration Prime
                        
                        [dictForParticipent setObject:p.name forKey:p.parent_id_str];                    }
                }
                [arrSelectedParticipernts addObjectsFromArray:arrAllList];
                _textfieldForReciver.text = @"All parents and students selected" ;
                //strRecivers = @"All parents and students selected" ;
                break ;
            }
            case 5:{
                for (MyTeacher *t in arrParentsList) {
                    [dictForParticipent setObject:t.name forKey:t.teacher_id];
                }
                [arrSelectedParticipernts addObjectsFromArray:arrTeacherList];
                _textfieldForReciver.text = @"All teachers selected" ;
                //strRecivers = @"All teachers selected" ;
                break ;
            }
            default:
                break;
        }
    }
    [tblListView reloadData];
}

- (void) checkBoxButtonClicked:(UIButton *) sender
{
#if DEBUG
    NSLog(@"sender.selected = %d",sender.selected);
    NSLog(@"Selected Index ID %d",sender.tag);
#endif
    
    
    if (btnSelectAll.selected) {
        btnSelectAll.selected = NO ;
        [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
    }
    if (sender.selected) {
        sender.selected = NO ;
        [sender setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
        switch (currentSelectedButton) {
            case 1:
            {
                Student *student = [arrForList objectAtIndex:sender.tag];
                if ([arrSelectedParticipernts containsObject:student]) {
                    [arrSelectedParticipernts removeObject:student];
                }
                
                [dictForParticipent removeObjectForKey:student.student_id];
                
                if ([_textfieldForReciver.text length] > 0) {
                    NSString *strName = @"";
                    int i = 0;
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        strName = [strName stringByAppendingString:name];
                        if (i == ([dictForParticipent allKeys].count -1)) {
                            
                        }else{
                            strName = [strName stringByAppendingString:@", "];
                        }
                        
                        i++ ;
                    }
                    if ([dictForParticipent allKeys].count == 1) {
                        strName = [strName stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
#if DEBUG
                    NSLog(@"strName %@",strName);
#endif
                    _textfieldForReciver.text = strName ;
                }
                else
                {
                    _textfieldForReciver.text = @"";
                }
                
                break;
            }
            case 2:
            {
                MyParent *parent = [arrForList objectAtIndex:sender.tag];
                if ([arrSelectedParticipernts containsObject:parent]) {
                    [arrSelectedParticipernts removeObject:parent];
                }
                
                // Added By Amol Gaikwad
                /**  Added  For dictForParticipent unique Key. Parent Ids  are  Same thats why we appended buttontag to parentID ***/
                
                // New Code Aman Migration Prime
                
                NSString *parentKey = [NSString stringWithFormat:@"%@%ld",parent.parent_id_str,(long)sender.tag];
                
                [dictForParticipent removeObjectForKey:parentKey];
                
                if ([_textfieldForReciver.text length] > 0) {
                    NSString *strName = @"";
                    int i = 0;
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        strName = [strName stringByAppendingString:name];
                        if (i == ([dictForParticipent allKeys].count -1)) {
                            
                        }else{
                            strName = [strName stringByAppendingString:@", "];
                        }
                        
                        i++ ;
                    }
                    if ([dictForParticipent allKeys].count == 1) {
                        strName = [strName stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
                    _textfieldForReciver.text = strName ;
                    //strRecivers=strName;
                }else{
                    _textfieldForReciver.text = @"";
                    //strRecivers=@"";
                }
                
                break;
            }
            case 3:
            {
                if (isSpecificStudentTapped) {
                    Student *student = [arrForList objectAtIndex:sender.tag];
                    if ([arrSelectedParticipernts containsObject:student]) {
                        [arrSelectedParticipernts removeObject:student];
                    }
                    
                    [dictForParticipent removeObjectForKey:student.student_id];
                    
                    
                    if ([_textfieldForReciver.text length] > 0) {
                        NSString *strName = @"";
                        int i = 0;
                        for (NSString *str in [dictForParticipent allKeys]) {
                            NSString *name = [dictForParticipent objectForKey:str];
                            strName = [strName stringByAppendingString:name];
                            if (i == ([dictForParticipent allKeys].count - 1 )) {
                                
                            }else{
                                strName = [strName stringByAppendingString:@", "];
                            }
                            
                            i++ ;
                        }
                        if ([dictForParticipent allKeys].count == 1) {
                            strName = [strName stringByReplacingOccurrencesOfString:@"," withString:@""];
                        }
                        
                        _textfieldForReciver.text = strName ;
                    }
                    else
                    {
                        _textfieldForReciver.text = @"";
                    }
                    
                    //=====
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    if ([_textfieldForReciver.text length] > 0)
                    {
                        for (NSString *str in [dictForParticipent allKeys])
                        {
                            NSString *name = [dictForParticipent objectForKey:str];
                            [arr addObject:name];
                        }
                        _textfieldForReciver.text = [[AppDelegate getAppdelegate]._curentClass.class_name stringByAppendingFormat:@" (%lu Students)",(unsigned long)arr.count] ;
                    }
                    else
                    {
                        _textfieldForReciver.text = [[AppDelegate getAppdelegate]._curentClass.class_name stringByAppendingFormat:@" (1 Student)"];
                    }
                    //=====
                    
                }else{
                    MyClass *class = [arrForList objectAtIndex:sender.tag];
                    
                    _textfieldForReciver.text = @"" ;
                    [dictForParticipent removeObjectForKey:class.class_id];
                    [arrSelectedParticipernts removeAllObjects];
                    [tblListView reloadData];
                }
                
                break;
                
            }
            case 4:{
                
                Student *student = nil ;
                MyParent *parent = nil ;
                NSObject *object = [arrForList objectAtIndex:sender.tag];
                if ([object isKindOfClass:[Student class]]) {
                    student = (Student *)object ;
                    [dictForParticipent removeObjectForKey:student.student_id];
                }else{
                    parent = (MyParent *)object ;
                    // New Code Aman Migration Prime
                    
                    [dictForParticipent removeObjectForKey:parent.parent_id_str];
                }
                if ([arrSelectedParticipernts containsObject:object]) {
                    [arrSelectedParticipernts removeObject:object];
                }
                
                if ([_textfieldForReciver.text length] > 0) {
                    NSString *strName = @"";
                    int i = 0;
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        strName = [strName stringByAppendingString:name];
                        if (i == ([dictForParticipent allKeys].count -1)) {
                            
                        }else{
                            strName = [strName stringByAppendingString:@", "];
                        }
                        
                        i++ ;
                    }
                    if ([dictForParticipent allKeys].count == 1) {
                        strName = [strName stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
                    _textfieldForReciver.text = strName ;
                }else{
                    _textfieldForReciver.text = @"";
                }
                
                break ;
            }
            case 5:
            {
                MyTeacher *teacher = [arrForList objectAtIndex:sender.tag];
                if ([arrSelectedParticipernts containsObject:teacher]) {
                    [arrSelectedParticipernts removeObject:teacher];
                }
                
                [dictForParticipent removeObjectForKey:teacher.teacher_id];
                
                if ([_textfieldForReciver.text length] > 0) {
                    NSString *strName = @"";
                    int i = 0;
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        strName = [strName stringByAppendingString:name];
                        if (i == ([dictForParticipent allKeys].count - 1 )) {
                            
                        }else{
                            strName = [strName stringByAppendingString:@", "];
                        }
                        
                        i++ ;
                    }
                    if ([dictForParticipent allKeys].count == 1) {
                        strName = [strName stringByReplacingOccurrencesOfString:@"," withString:@""];
                    }
                    _textfieldForReciver.text = strName ;
                }else{
                    _textfieldForReciver.text = @"";
                }
                
                break;
            }
                
            default:
                break;
        }
        
    }else{
        sender.selected = YES ;
        [sender setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
        switch (currentSelectedButton) {
            case 1:
            {
                Student *student = nil ;
                if (isSearchOn) {
                    student = [arrSearchedList objectAtIndex:sender.tag];
                }else{
                    student = [arrForList objectAtIndex:sender.tag];
                }
                
                [arrSelectedParticipernts addObject:student];
                [dictForParticipent setObject:student.firstname forKey:student.student_id];
                
                NSMutableArray *arr = [[NSMutableArray alloc]init];
                if ([_textfieldForReciver.text length] > 0) {
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        [arr addObject:name];
                    }
                    
                    NSString *strNames=[arr componentsJoinedByString:@","];
                    _textfieldForReciver.text = strNames ;
                }else{
                    _textfieldForReciver.text = student.firstname;
                }
                break;
            }
            case 2:
            {
                MyParent *parent = nil ;
                if (isSearchOn) {
                    parent = [arrSearchedList objectAtIndex:sender.tag];
                }else{
                    parent = [arrForList objectAtIndex:sender.tag];
                }
                
                [arrSelectedParticipernts addObject:parent];
                
                /**  Added  For dictForParticipent unique Key. Parent Ids  are  Same thats why we appended buttontag to parentID ***/
                // New Code Aman Migration Prime
                
                NSString *parentKey = [NSString stringWithFormat:@"%@%ld",parent.parent_id_str,(long)sender.tag];
                [dictForParticipent setObject:parent.name forKey:parentKey];
                
                NSMutableArray *arr = [[NSMutableArray alloc]init];
                if ([_textfieldForReciver.text length] > 0)
                {
                    for (NSString *str in [dictForParticipent allKeys])
                    {
                        NSString *name = [dictForParticipent objectForKey:str];
                        [arr addObject:name];
                    }
                    NSString *strNames=[arr componentsJoinedByString:@","];
                    _textfieldForReciver.text = strNames ;
                }
                else
                {
                    _textfieldForReciver.text = parent.name;
                }
                
                break;
            }
                // Specific Student
            case 3:
            {
                if (isSpecificStudentTapped) {
                    
                    [btnSelectSpecificStudents setTitle:@"Change selection " forState:UIControlStateNormal];
                    btnDiscardChanges.hidden = NO ;
                    
                    Student *student = nil ;
                    if (isSearchOn) {
                        student = [arrSearchedList objectAtIndex:sender.tag];
                    }else{
                        student = [arrForList objectAtIndex:sender.tag];
                    }
                    
                    [arrSelectedParticipernts addObject:student];
                    [dictForParticipent setObject:student.firstname forKey:student.student_id];
                    
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    if ([_textfieldForReciver.text length] > 0)
                    {
                        for (NSString *str in [dictForParticipent allKeys])
                        {
                            NSString *name = [dictForParticipent objectForKey:str];
                            [arr addObject:name];
                        }
                        
                        _textfieldForReciver.text = [[AppDelegate getAppdelegate]._curentClass.class_name stringByAppendingFormat:@" (%lu Students)",(unsigned long)arr.count] ;
                    }
                    else
                    {
                        _textfieldForReciver.text = [[AppDelegate getAppdelegate]._curentClass.class_name stringByAppendingFormat:@" (1 Student)"];

                    }
                    
                }
                else
                {
                    [btnSelectSpecificStudents setTitle:@"Select specific students " forState:UIControlStateNormal];
                    btnDiscardChanges.hidden = YES ;
                    [dictForParticipent removeAllObjects];
                    MyClass *class = nil ;
                    if (isSearchOn) {
                        class = [arrSearchedList objectAtIndex:sender.tag];
                    }else{
                        class = [arrForList objectAtIndex:sender.tag];
                    }
                    
                    [AppDelegate getAppdelegate]._curentClass = class ;
                    [arrSelectedParticipernts removeAllObjects];
                    [arrSelectedParticipernts addObject:class];
                    [dictForParticipent setObject:class.class_name forKey:class.class_id];
                    
                    [self getClassUserMethod];
                    [tblListView reloadData];
                    
                }
                
                break;
            }
            case 4:
            {
                Student *student = nil ;
                MyParent *parent = nil ;
                NSObject *object = nil ;
                if (isSearchOn) {
                    object = [arrSearchedList objectAtIndex:sender.tag];
                }else{
                    object = [arrForList objectAtIndex:sender.tag];
                }
                
                [arrSelectedParticipernts addObject:object];
                if ([object isKindOfClass:[Student class]]) {
                    student = (Student *)object ;
                    [dictForParticipent setObject:student.firstname forKey:student.student_id];
                }else{
                    parent = (MyParent *)object ;
                    // New Code Aman Migration Prime
                    
                    [dictForParticipent setObject:parent.name forKey:parent.parent_id_str];
                }
                NSMutableArray *arr = [[NSMutableArray alloc]init];
                if ([_textfieldForReciver.text length] > 0) {
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        [arr addObject:name];
                    }
                    NSString *strNames=[arr componentsJoinedByString:@","];
                    _textfieldForReciver.text = strNames ;
                }else{
                    if (student.firstname)
                    {
                        _textfieldForReciver.text = student.firstname;
                    }
                    else
                    {
                        _textfieldForReciver.text = parent.name;
                    }
                }
                break;
            }
                // specific student
            case 5:
            {
                MyTeacher *teacher = nil ;
                if (isSearchOn) {
                    teacher = [arrSearchedList objectAtIndex:sender.tag];
                }else{
                    teacher = [arrForList objectAtIndex:sender.tag];
                }
                
                [arrSelectedParticipernts addObject:teacher];
                [dictForParticipent setObject:teacher.name forKey:teacher.teacher_id];
                NSMutableArray *arr = [[NSMutableArray alloc]init];
                if ([_textfieldForReciver.text length] > 0) {
                    for (NSString *str in [dictForParticipent allKeys]) {
                        NSString *name = [dictForParticipent objectForKey:str];
                        [arr addObject:name];
                    }
                    NSString *strNames=[arr componentsJoinedByString:@","];
                    _textfieldForReciver.text = strNames ;
                    
                }
                else
                {
                    _textfieldForReciver.text = teacher.name;
                }
                break;
            }
                
            default:
                break;
        }
    }
    [listSearchBar resignFirstResponder];
}

- (IBAction)inboxAllListTapped:(UIButton *)sender {
    
    
    [self getStudentAndParent];
    isStudentRadioButtonSelectedOrNot = NO;
    [tblListView setScrollsToTop:YES];
    [arrSelectedParticipernts removeAllObjects];
    [dictForParticipent removeAllObjects];
    btnSelectSpecificStudents.hidden = YES ;
    btnDiscardChanges.hidden = YES ;
    
    isSpecificStudentTapped = NO ;
    isSearchOn = NO ;
    listSearchBar.text = @"";
    [listSearchBar resignFirstResponder];
    btnSelectAll.userInteractionEnabled = TRUE ;
    btnSelectAll.enabled = TRUE ;
    lblSelectAll.textColor = [UIColor blackColor];
    
    if (btnSelectAll.selected) {
        btnSelectAll.selected = NO ;
        [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
    }
    
    _textfieldForReciver.text = @"";
    //strRecivers=@"";
    currentSelectedButton = sender.tag ;
    arrForList = arrAllList ;
    [self setDynamicHeightToTable];
    if (isListDisplayed) {
        [tblListView setHidden:NO] ;
        [tblListView reloadData];
    }
    
    if (sender.selected) {
        currentSelectedButton = 100 ;
        sender.selected = NO ;
        arrForList = arrSelectedParticipernts ;
        isListDisplayed = NO ;
        [tblListView setHidden:YES] ;
        [tblListView reloadData];
    }else{
        sender.selected = YES ;
        for(UIView *view in [viewForTeacherLoginInbox subviews]){
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view ;
                if(btn.tag !=4)
                {
                    [btn setSelected:NO];
                }
            }
        }
    }
    if (arrForList.count == 0) {
        btnSelectAll.userInteractionEnabled = FALSE ;
        btnSelectAll.enabled = FALSE ;
        if (btnSelectAll.selected) {
            btnSelectAll.selected = NO ;
            [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
        }
    }else{
        btnSelectAll.userInteractionEnabled = TRUE ;
        btnSelectAll.enabled = TRUE ;
    }
    
}

- (IBAction)inboxParentListTapped:(UIButton *)sender {
    
    isStudentRadioButtonSelectedOrNot = NO;
    [tblListView setScrollsToTop:YES];
    [arrSelectedParticipernts removeAllObjects];
    [dictForParticipent removeAllObjects];
    btnSelectSpecificStudents.hidden = YES ;
    btnDiscardChanges.hidden = YES ;
    isSpecificStudentTapped = NO ;
    isSearchOn = NO ;
    listSearchBar.text = @"";
    [listSearchBar resignFirstResponder];
    btnSelectAll.userInteractionEnabled = TRUE ;
    btnSelectAll.enabled = TRUE ;
    lblSelectAll.textColor = [UIColor blackColor];
    
    if (btnSelectAll.selected) {
        btnSelectAll.selected = NO ;
        [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
    }
    
    _textfieldForReciver.text = @"";
    //strRecivers=@"";
    currentSelectedButton = sender.tag ;
    arrForList = arrParentsList ;
    [self setDynamicHeightToTable];
    if (isListDisplayed) {
        [tblListView setHidden:NO] ;
        [tblListView reloadData];
    }
    
    if (sender.selected) {
        currentSelectedButton = 100 ;
        sender.selected = NO ;
        arrForList = arrSelectedParticipernts ;
        isListDisplayed = NO ;
        [tblListView setHidden:YES] ;
        [tblListView reloadData];
    }else{
        sender.selected = YES ;
        for(UIView *view in [viewForTeacherLoginInbox subviews]){
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view ;
                if(btn.tag !=2)
                {
                    [btn setSelected:NO];
                }
            }
        }
        
    }
    if (arrForList.count == 0) {
        btnSelectAll.userInteractionEnabled = FALSE ;
        btnSelectAll.enabled = FALSE ;
        if (btnSelectAll.selected) {
            btnSelectAll.selected = NO ;
            [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
        }
    }else{
        btnSelectAll.userInteractionEnabled = TRUE ;
        btnSelectAll.enabled = TRUE ;
    }
}

- (IBAction)inboxClassListTapped:(UIButton *)sender {
    
    [self getClassFromDb];
    isStudentRadioButtonSelectedOrNot = NO;
    [tblListView setScrollsToTop:YES];
    [arrSelectedParticipernts removeAllObjects];
    [dictForParticipent removeAllObjects];
    isSpecificStudentTapped = NO ;
    btnSelectAll.userInteractionEnabled = FALSE ;
    btnSelectAll.enabled = FALSE ;
    lblSelectAll.textColor = [UIColor grayColor];
    isSearchOn = NO ;
    listSearchBar.text = @"";
    [listSearchBar resignFirstResponder];
    if (btnSelectAll.selected) {
        btnSelectAll.selected = NO ;
        [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
    }
    
    _textfieldForReciver.text = @"";
    //strRecivers=@"";
    currentSelectedButton = sender.tag ;
    arrForList = arrClassList ;
    [self setDynamicHeightToTable];
    if (isListDisplayed) {
        [tblListView setHidden:NO] ;
        
        [tblListView reloadData];
    }
    
    if (sender.selected) {
        currentSelectedButton = 100 ;
        sender.selected = NO ;
        arrForList = arrSelectedParticipernts ;
        isListDisplayed = NO ;
        [tblListView setHidden:YES] ;
        [tblListView reloadData];
    }else{
        sender.selected = YES ;
        for(UIView *view in [viewForTeacherLoginInbox subviews]){
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view ;
                if(btn.tag !=3)
                {
                    [btn setSelected:NO];
                }
            }
        }
    }
}

- (IBAction)inboxStudentListTapped:(UIButton *)sender {
    
    [self getStudentFromDb];
    isStudentRadioButtonSelectedOrNot = YES;
    [tblListView scrollRectToVisible:CGRectZero animated:NO];
    [arrSelectedParticipernts removeAllObjects];
    [dictForParticipent removeAllObjects];
    btnSelectSpecificStudents.hidden = YES ;
    btnDiscardChanges.hidden = YES ;
    isSpecificStudentTapped = NO ;
    isSearchOn = NO ;
    listSearchBar.text = @"";
    [listSearchBar resignFirstResponder];
    btnSelectAll.userInteractionEnabled = TRUE ;
    btnSelectAll.enabled = TRUE ;
    lblSelectAll.textColor = [UIColor blackColor];
    
    if (btnSelectAll.selected) {
        btnSelectAll.selected = NO ;
        [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
    }
    
    _textfieldForReciver.text = @"";
    //strRecivers=@"";
    currentSelectedButton = sender.tag ;
    arrForList = arrStudentList ;
    [self setDynamicHeightToTable];
    if (isListDisplayed) {
        [tblListView setHidden:NO] ;
        [tblListView reloadData];
    }
    
    if (sender.selected) {
        currentSelectedButton = 100 ;
        sender.selected = NO ;
        arrForList = arrSelectedParticipernts ;
        isListDisplayed = NO ;
        [tblListView setHidden:YES] ;
        [tblListView reloadData];
    }else{
        sender.selected = YES ;
        for(UIView *view in [viewForTeacherLoginInbox subviews]){
            if ([view isKindOfClass:[UIButton class]]) {
                UIButton *btn = (UIButton *)view ;
                if(btn.tag !=1)
                {
                    [btn setSelected:NO];
                }
            }
        }
    }
    
    if (arrForList.count == 0) {
        btnSelectAll.userInteractionEnabled = FALSE ;
        btnSelectAll.enabled = FALSE ;
        if (btnSelectAll.selected) {
            btnSelectAll.selected = NO ;
            [btnSelectAll setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
        }
    }else{
        btnSelectAll.userInteractionEnabled = TRUE ;
        btnSelectAll.enabled = TRUE ;
    }
    
}

#pragma mark
#pragma mark DB Methods

- (void) getStudentAndParent {
    
    arrAllList = nil;
    arrAllList = [[NSMutableArray alloc] init];
    for (Student *s in arrStudentList) {
        [arrAllList addObject:s];
        NSArray *arr = [[Service sharedInstance] getParenteDataForStudent:s.student_id];
        if (arr.count != 0) {
            for (MyParent *p in arr) {
                
                
                /**** Addded By Amol Gaikwad For Teacher can not send message to parant   ***/
                
                School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                
                if ([schoolDetailObj.allow_parent isEqualToNumber:[NSNumber numberWithInteger:1]] && [profobj.type isEqualToString:@"Teacher"]) {
                    [arrAllList addObject:p];
                }
                
                /***  Parent check end ***/
            }
        }
    }
}

- (void) getStudentFromDb{
    
    [arrStudentList removeAllObjects];
    arrStudentList = nil;
    arrStudentList = [[NSMutableArray alloc] init];
    
    NSArray *arr=[[Service sharedInstance] getStoredAllStudentDataWithTeacherId];
    for(Student *stu in arr)
    {
        [arrStudentList addObject:stu];
    }
    
    //21AugChange
    NSArray *arr1=arrStudentList;
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"firstname" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrStudentList removeAllObjects];
    NSMutableArray *arrName=[[NSMutableArray alloc]init];
    for(Student *stu in arr1){
        if(![arrName containsObject:stu.firstname])
        {
            if([stu.firstname length]>0)
            {
                [arrName addObject:stu.firstname];
                [arrStudentList addObject:stu];
            }
        }
    }
    [arrName release];
}

- (void) getTeacherFromDb{
    
    arrTeacherList = [[NSMutableArray alloc] init];
    MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    for(MyTeacher *tech in profileData.teacher){
        if([tech.status isEqualToString:@"1"])
            [arrTeacherList addObject:tech];
    }
    
    //21AugChange
    NSArray *arr1 = arrTeacherList;
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrTeacherList removeAllObjects];
    for(MyTeacher *tech in arr1){
        if(![arrTeacherList containsObject:tech])
            [arrTeacherList addObject:tech];
    }
    
}


- (void) getParentFromDb{
    
    arrParentsList = [[NSMutableArray alloc] init];
    
    
    for (Student *s in arrStudentList) {
        NSArray *arr = [[Service sharedInstance] getParenteDataForStudent:s.student_id];
        if (arr.count != 0) {
            for (MyParent *p in arr) {
                [arrParentsList addObject:p];
            }
        }
    }
    
    //21AugChange
    NSArray *arr1=arrParentsList;
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrParentsList removeAllObjects];
    NSMutableArray *arrName=[[NSMutableArray alloc]init];
    for(MyParent *parent in arr1){
        
        
        //  parentNameAndStudenId helps to create unique composite id. To avoid duplication in database.
        // Its used for display purpose
        
        NSMutableString *parentNameAndStudenId = [NSMutableString stringWithFormat:@"%@%@",parent.name,parent.studentId];
        
        if(![arrName containsObject:parentNameAndStudenId])
        {
            [arrName addObject:parentNameAndStudenId];
            [arrParentsList addObject:parent];
        }
    }
    [arrName release];
}

- (void) getClassFromDb
{
    arrClassList = [[NSMutableArray alloc] init];
    
    // Campus based TimeTable Start
    
    NSArray *arrAy=[[Service sharedInstance]getStoredAllTimeTableData];
    NSMutableArray *arrofTimeTabelID = [[[NSMutableArray alloc] init] autorelease];
    /////
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDATEFORMAT_Slash];
    
    NSDate *dDate = [NSDate date];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    NSString *strinDate = [format stringFromDate:dDate];
    NSDate *date = nil;
    
    date = [format dateFromString:strinDate];
    
    /////
    
    for(TimeTable *timetable in arrAy)
    {
        if([DELEGATE isDate:date inRangeFirstDate:timetable.startDate  lastDate:timetable. end_date])
            
        {
            [arrofTimeTabelID addObject:timetable];
            
        }
        
    }
    
    // Code added to support if current date doesn't lie between start and end date of any semester
    //----------------------------------------------------------------------
    if([arrofTimeTabelID count] == 0)
    {
        for(TimeTable *time in arrAy)
        {
#if DEBUG
            NSLog(@"time = %@",time);
            NSLog(@"NSDate = %@",[NSDate date]);
#endif
            
            if([[NSDate date] compare:time.startDate] == NSOrderedAscending)
            {
                [arrofTimeTabelID addObject:time];
                break;
            }
        }
        if([arrofTimeTabelID count] == 0)
        {
            [arrofTimeTabelID addObject:[arrAy lastObject]];
        }
    }
    //----------------------------------------------------------------------
    
    for(TimeTable *timetable in arrofTimeTabelID)
    {
        NSArray *arr=[[Service sharedInstance]getStoredAllClassDataInApp:timetable.timeTable_id];
        for(MyClass *classes in arr)
        {
            [arrClassList addObject:classes];
        }
    }
    // Campus based TimeTable End
    
    //21AugChange
    NSArray *arr1=arrClassList;
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"class_name" ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    [arrClassList removeAllObjects];
    NSMutableArray *arrName=[[NSMutableArray alloc]init];
    
    for(MyClass *class in arr1)
    {
        if(![arrName containsObject:class.class_id])
        {
            [arrName addObject:class.class_id];
            [arrClassList addObject:class];
        }
    }
    [arrName release];
    
}
#pragma mark
#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if (textField == _textfieldForReciver) {
        if (isListDisplayed) {
            isListDisplayed = NO ;
            [tblListView setHidden:YES] ;
        }else{
            isListDisplayed = YES ;
            switch (currentSelectedButton) {
                case 1:
                    [self getStudentFromDb];
                    arrForList = arrStudentList ;
                    
                    [tblListView setHidden:NO] ;
                    break;
                case 2:
                    [self getStudentFromDb];
                    
                    arrForList = arrParentsList ;
                    [tblListView setHidden:NO] ;
                    break ;
                case 3:
                    isSpecificStudentTapped = NO ;
                    arrForList = arrClassList ;
                    [tblListView setHidden:NO] ;
                    break ;
                case 4:
                    [self getStudentFromDb];
                    [self getStudentAndParent];
                    arrForList = arrAllList ;
                    [tblListView setHidden:NO] ;
                    break ;
                case 5:
                    arrForList = arrTeacherList ;
                    [tblListView setHidden:NO] ;
                    break ;
                case 100:
                    [tblListView setHidden:YES] ;
                    break ;
                default:
                    break;
            }
            if (arrForList.count == 0) {
                [tblListView setHidden:YES] ;
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No records available." message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alert show];
                [alert release];
                
            }
            [self setDynamicHeightToTable];
            [tblListView reloadData];
        }
        
        return NO ;
        
    }
    else{
        [tblListView setHidden:YES] ;
        return YES ;
        
    }
}




- (void) setDynamicHeightToTable {
    //    if (isSearchOn) {
    //        if (arrSearchedList.count < 5) {
    //            CGRect rect = tblListView.frame ;
    //            float h = arrSearchedList.count * HeightForListCell ;
    //            rect.size.height = h ;
    //            [tblListView setFrame:rect];
    //        }else{
    //            [tblListView setFrame:originalListTableViewHeight];
    //        }
    //    }else{
    /*if (arrForList.count < 5) {
     CGRect rect = tblListView.frame ;
     float h = arrForList.count * HeightForListCell ;
     rect.size.height = h ;
     [tblListView setFrame:rect];
     }else{
     [tblListView setFrame:originalListTableViewHeight];
     }*/
    //}
    
    
    
}

#pragma mark
#pragma mark Searching Methods

- (void) getSearchedData:(NSString *)searchText {
    
    searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@"%"];
    searchText = [searchText uppercaseString];
    switch (currentSelectedButton) {
        case 1:
            
        {
            for (Student *s in arrForList) {
                if ([s.firstname rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                {
                    
                }
                else
                {
                    [arrSearchedList addObject:s];
                }
            }
            break;
        }
        case 2:
        {
            for (MyParent *p in arrForList)
            {
                NSString * str = p.name ;
                str = [str stringByAppendingString:p.studentName];
                if ([str rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                {
                }
                else
                {
                    [arrSearchedList addObject:p];
                }
            }
            break ;
        }
        case 3:{
            if (isSpecificStudentTapped)
            {
                for (Student *s in arrForList)
                {
                    if ([s.firstname rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                    {
                        
                    }
                    else
                    {
                        [arrSearchedList addObject:s];
                    }
                }
            }
            else
            {
                for (MyClass *c in arrForList)
                {
                    if ([c.class_name rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                    {
                    }
                    else
                    {
                        [arrSearchedList addObject:c];
                    }
                }
            }
            break ;
        }
            
        case 4:{
            for (NSObject *object in arrForList)
            {
                if ([object isKindOfClass: [Student class]])
                {
                    Student *s = (Student *)object ;
                    if ([s.firstname rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                    {
                        
                    }
                    else
                    {
                        [arrSearchedList addObject:object];
                    }
                }
                else
                {
                    MyParent *p = (MyParent *)object;
                    NSString * str = p.name ;
                    str = [str stringByAppendingString:p.studentName];
                    if ([str rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                    {
                    }
                    else
                    {
                        [arrSearchedList addObject:object];
                    }
                }
            }
            break ;
        }
        case 5:
            
        {
            for (MyTeacher *t in arrForList)
            {
                if ([t.name rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound)
                {
                    
                }
                else
                {
                    [arrSearchedList addObject:t];
                }
            }
            break;
        }
        default:
            break;
    }
    [tblListView reloadData];
}


#pragma mark
#pragma mark UISearchBar Methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    // Set the return key and keyboard appearance of the search bar
    for (UIView *searchBarSubview in [listSearchBar subviews]) {
        if ([searchBarSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
            
            @try{
                [(UITextField *)searchBarSubview setReturnKeyType:UIReturnKeyDone];
            }
            @catch (NSException * e) {
                // ignore exception
            }
        }
    }
    
    if(isSearchOn)
        return;
    
    [tblListView reloadData];
    
    isSearchOn = NO ;
    
    
    if (theSearchBar.text.length != 0) {
        isSearchOn = YES ;
        
        [self getSearchedData:theSearchBar.text];

        [tblListView setHidden:NO];
    }
    else
    {
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    if([searchBar.text length] == 0) {
        isSearchOn = NO ;
        [searchBar resignFirstResponder];
    }else{
        isSearchOn = YES ;
    }
    [searchBar resignFirstResponder];
    [tblListView reloadData];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    //Delete the folder of online searched images here
    //    [FileUtils deleteFileFromDocumentsDirectory:@"searchOnline"];
    //    _currentStart = @"0";
    
    if([theSearchBar.text length] == 0) {
        [theSearchBar resignFirstResponder];
        isSearchOn = NO ;
        [tblListView reloadData];
        [listSearchBar resignFirstResponder];
        //[self cancelButtonClicked:nil];
        
    }else{
        isSearchOn = YES ;
        [arrSearchedList removeAllObjects];
        [tblListView reloadData];
        [self getSearchedData:searchText];
        // [self setDynamicHeightToTable];
        [tblListView setHidden:NO];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    isSearchOn = NO ;
    searchBar.text = nil;
    [searchBar resignFirstResponder];
}

- (void) oneTap {
    
    [tblListView setHidden:YES];
    isListDisplayed = NO ;
    isSearchOn = NO ;
    listSearchBar.text = @"";
}

#pragma mark -

-(void)getClassUserMethod
{
    //service for get class user
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        if([[Service sharedInstance] netManager]){ //Stop Previous Calls
            [[[Service sharedInstance] netManager] stopRequest];
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getClassUserDidGot:) name:NOTIFICATION_Get_Class_User object:nil];
        
        
        
        [[Service sharedInstance]getClassUserDetail];
        
    }
    else
    {
    }
    
}

-(void)getClassUserDidGot:(NSNotification*)note
{
    //07/08/2013
#if DEBUG
    NSLog(@"%@",note.userInfo);
#endif
    
    [[AppDelegate getAppdelegate] hideIndicator];
    
    if (note.userInfo) {
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            if(isStudentRadioButtonSelectedOrNot == FALSE)
            {
                [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Get_Class_User object:nil];
                
                NSArray *arrForClassUser=[[note userInfo] valueForKey:@"Class_user"];
                TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
                tabl.studentUpdateTime=[NSNumber numberWithDouble:[[[note userInfo] valueForKey:@"LastSynchTime"]doubleValue]];
                for(NSDictionary *dict in [[note userInfo] valueForKey:@"deleted"])
                {
                    Student *delClassobj=[[Service sharedInstance]getStudentDataInfoStoredClass:[dict objectForKey:@"UserId"] classe1:[dict objectForKey:@"ClassId"]];
                    
                    
                    NSError *error;
                    
                    if(delClassobj){
                        [[AppDelegate getAppdelegate].managedObjectContext
                         deleteObject:delClassobj];
                        
                        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
                        {
#if DEBUG
                            NSLog(@"Error deleting  - error:%@",error);
#endif
                        }
                    }
                }
                // M here
                for (NSDictionary *dict  in arrForClassUser)
                {
                    [[Service sharedInstance]parseStudentDataWithDictionary:dict];
                }
                
                MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                if([profileData.type isEqualToString:@"Teacher"])
                {
                    [arrStudentList removeAllObjects];
                    [self getStudentFromDb];
                    [arrAllList removeAllObjects];
                    [self getStudentAndParent];
                    [arrParentsList removeAllObjects];
                    [self getParentFromDb];
                }
                
                NSArray *assignedClassUser = [[Service sharedInstance] getStoredAllStudentDataWithClassId:[NSString stringWithFormat:@"%d",[[AppDelegate getAppdelegate]._curentClass.class_id integerValue]]];
                
                [AppDelegate getAppdelegate]._curentClass.arrRegisteredStudents = assignedClassUser ;
                
                _textfieldForReciver.text = [[AppDelegate getAppdelegate]._curentClass.class_name stringByAppendingFormat:@" (%lu Students)",(unsigned long)[AppDelegate getAppdelegate]._curentClass.arrRegisteredStudents.count];
                //strRecivers=[[AppDelegate getAppdelegate]._curentClass.class_name stringByAppendingFormat:@" (%d Students)",[AppDelegate getAppdelegate]._curentClass.arrRegisteredStudents.count];
                
                if ([AppDelegate getAppdelegate]._curentClass.arrRegisteredStudents.count == 0) {
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Selected class does not have student enrolled.\n You can not send message to this class." message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                    [alert release];
                    btnSend.hidden = YES ;
                    btnSelectSpecificStudents.hidden = YES ;
                    btnDiscardChanges.hidden = YES ;
                    return ;
                }else{
                    btnSend.hidden = NO;
                    btnSelectSpecificStudents.hidden = NO ;
                    //btnDiscardChanges.hidden = NO ;
                }
                
            }
            
            else
            {
                NSArray *arrForClassUser=[[note userInfo] valueForKey:@"Class_user"];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Get_Class_User object:nil];
                
                if (arrForClassUser.count != 0) {
                    [[Service sharedInstance] deleteStudent];
                }
                
                for (NSDictionary *dict  in arrForClassUser)
                {
                    [[Service sharedInstance]parseStudentDataWithDictionary:dict];
                }
                
                
                MyProfile *profileData=[[Service sharedInstance] getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
                if([profileData.type isEqualToString:@"Teacher"])
                {
                    [arrStudentList removeAllObjects];
                    [self getStudentFromDb];
                    [arrAllList removeAllObjects];
                    [self getStudentAndParent];
                    [arrParentsList removeAllObjects];
                    [self getParentFromDb];
                }
                
            }
            
            
        }
    }else
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
}

#pragma mark teacher
-(void)getTeacherProfileDetail
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(teacherProfileDidUpdate:) name:NOTIFICATION_Teacher_Detail object:nil];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        
        [[Service sharedInstance]getTeacherDetail:dict];
        [dict release];
        
        
    }
    else{
        //[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    
}

-(void)teacherProfileDidUpdate:(NSNotification*)note
{
#if DEBUG
    NSLog(@"%@",note.userInfo);
#endif
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Teacher_Detail object:nil];
        if([[[note userInfo] valueForKey:@"ErrorCode"]integerValue]==1)
        {
            
            NSArray *arr=[[note userInfo] valueForKey:@"teacher_details"] ;
            
            //for parse data in core data
            for(NSDictionary *dict in arr)
            {
                [[Service sharedInstance]parseMyTeacherDataWithDictionary:dict];
            }
            [self getTeacherFromDb];
        }
        else
        {
        }
    }
    
}




#pragma mark
#pragma mark UIAlertView Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    _textfieldForReciver.text = @"" ;
    //strRecivers=@"";
    _textFieldSubject.text = @"" ;
    _textViewForDescription.text = @"" ;
    [arrSelectedParticipernts removeAllObjects];
    [tblListView reloadData];
    
}


- (void) refreshTableView:(id)model {
    
    [_createNewMsgView removeFromSuperview];
    
    if([model isKindOfClass:[Inbox class]])
    {
        if(_textFieldSubject.tag == 1){
            
        }else if (_textFieldSubject.tag == 2){
            
            Inbox *msz=(Inbox*)model;
            
            if([msz.message_id integerValue]==0)
                self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.appId];
            
            else
                self._arrformsz=[[Service sharedInstance]getStoredMessage:msz.message_id];
            
            // Sorting Implementation
            NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
            NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
            self._arrformsz =[NSMutableArray arrayWithArray:[self._arrformsz sortedArrayUsingDescriptors:sortDescriptorArr]];
            _tablViewForMsz.hidden=NO;
            
            
            _isShowOther=NO;
        }
    }
    else  if([model isKindOfClass:[Annoncements class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        Annoncements *msz=(Annoncements*)model;
        msz.isReadAnn=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
        MainViewController *mainVC= (MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
        [mainVC readNotificationOnInboox:model];
        
        self._arrformsz=nil;
    }
    else  if([model isKindOfClass:[News class]])
    {
        _isShowOther=YES;
        _tablViewForMsz.hidden=NO;
        News *msz=(News*)model;
        msz.isReadNews=[NSNumber numberWithBool:YES];
        NSError *error=nil;
        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
        
        MainViewController *mainVC= (MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
        [mainVC readNotificationOnInboox:model];

        self._arrformsz=nil;
    }
    _tablViewForMsz.hidden=NO;
    [_tablViewForMsz reloadData];
    [_tableView reloadData];
    self._tableView.scrollsToTop = YES ;
}

- (IBAction)btnSortByDateTapped:(UIButton *)sender {
    
    
    [_createNewMsgView removeFromSuperview];
    if(sender.selected){
        sender.selected = NO ;
        NSArray *arr1 = nil;
        imgArrowImage.image = [UIImage imageNamed:@"inboxshortbydatearrowSelected.png"];
        
        // Sorting Implementation
        isArrowUP = YES;
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                arr1=_arrForInbox;
                break;
            case 1:
            {
                arr1=arrOfSelectedMessages;
                break;
            }
                
            case 2:
            {
                arr1=arrOfSelectedMessages;
                break;
            }
            case 3:
            {
                arr1=arrOfSelectedMessages;
                break;
            }
            case 4:
                arr1=arrOfSelectedMessages;
                break;
                
            default:
                arr1=_arrForInbox;
                break;
        }
        
        // Sorting Implementation
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                [_arrForInbox removeAllObjects];
                break;
            case 1:
            {
                [arrOfSelectedMessages removeAllObjects];
                break;
                
            }
            case 2:
            {
                [arrOfSelectedMessages removeAllObjects];
                break;
                
            }
            case 3:
            {
                [arrOfSelectedMessages removeAllObjects];
                break;
            }
            case 4:
                [arrOfSelectedMessages removeAllObjects];
                break;
                
            default:
                [_arrForInbox removeAllObjects];
                break;
        }
        
        
        for(id obj in arr1)
        {
            
            switch (selectedIndexForFiltrationInbox) {
                case 0:
                    [_arrForInbox addObject:obj];
                    break;
                case 1:
                {
                    [arrOfSelectedMessages addObject:obj];
                    break;
                }
                case 2:
                {
                    [arrOfSelectedMessages addObject:obj];
                    break;
                }
                case 3:
                {
                    [arrOfSelectedMessages addObject:obj];
                    break;
                }
                    
                case 4:
                    [arrOfSelectedMessages addObject:obj];
                    break;
                    
                default:
                    [_arrForInbox addObject:obj];
                    break;
            }
        }
    }
    else{
        
        imgArrowImage.image = [UIImage imageNamed:@"inboxshortbydatearrow.png"];
        sender.selected = YES ;
        NSArray *arr1 = nil;
        
        // Sorting Implementation
        isArrowUP = NO;
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                arr1=_arrForInbox;
                break;
            case 1:
            {
                arr1=arrOfSelectedMessages;
                break;
                
            }
            case 2:
            {
                arr1=arrOfSelectedMessages;
                break;
                
            }
            case 3:
            {
                arr1=arrOfSelectedMessages;
                break;
                
            }
            case 4:
                arr1=arrOfSelectedMessages;
                break;
                
            default:
                arr1=_arrForInbox;
                break;
        }
        
        // Sorting Implementation
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"created_time"ascending:isArrowUP];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        
        arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
        
        switch (selectedIndexForFiltrationInbox) {
            case 0:
                [_arrForInbox removeAllObjects];
                break;
            case 1:
            {
                [arrOfSelectedMessages removeAllObjects];
                break;
                
            }
            case 2:
            {
                [arrOfSelectedMessages removeAllObjects];
                break;
            }
            case 3:
            {
                [arrOfSelectedMessages removeAllObjects];
                break;
            }
                
            case 4:
                [arrOfSelectedMessages removeAllObjects];
                break;
                
            default:
                [_arrForInbox removeAllObjects];
                break;
        }
        
        for(id obj in arr1)
        {
            switch (selectedIndexForFiltrationInbox) {
                case 0:
                    [_arrForInbox addObject:obj];
                    break;
                case 1:
                {
                    [arrOfSelectedMessages addObject:obj];
                    break;
                }
                case 2:
                {
                    [arrOfSelectedMessages addObject:obj];
                    break;
                }
                case 3:
                {
                    [arrOfSelectedMessages addObject:obj];
                    break;
                }
                case 4:
                    [arrOfSelectedMessages addObject:obj];
                    break;
                    
                default:
                    [_arrForInbox addObject:obj];
                    break;
            }
            
        }
    }
    
    if (isRowSelected == YES) {
        _tablViewForMsz.hidden=NO;
        [_tablViewForMsz reloadData];
        
    }
    [_tableView reloadData];
    self._tableView.scrollsToTop = YES ;
}




- (IBAction)filterInboxImages:(UISegmentedControl *)sender {
    
    selectedIndexForFiltrationInbox = sender.selectedSegmentIndex;
#if DEBUG
    NSLog(@"Selected Index %d",selectedIndexForFiltrationInbox);
#endif
    
    
    if (sender.selectedSegmentIndex == 0)
    {
#if DEBUG
        NSLog(@"Segment %d deselected", sender.selectedSegmentIndex);
#endif
    }
    else
    {
#if DEBUG
        NSLog(@"Segment %lu selected", sender.selectedSegmentIndex);
#endif
      
    }
    
    [_tablViewForMsz reloadData];
    [_tableView reloadData];
    self._tableView.scrollsToTop = YES ;
    
    
}


@end
