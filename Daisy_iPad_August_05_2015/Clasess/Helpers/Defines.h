

#import "AppDelegate.h"
#define DELEGATE (AppDelegate *)[[UIApplication sharedApplication]delegate]
#define APP_NAME                         @""
#define APP_Key                          @"ipad"
#define LEAGELNUM @"0123456789"
//Test username and password
// username = קראט@קראט.קר
//password = קראט
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/*******************************************************************************************************************/
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#pragma mark HTTP Request header values
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define TIMEOUT_INTERVAL							30

//Facebook Constants
#define KToken                                  @"token"
#define KExpiryDate                             @"expiry_date"
#define KuserId									@"User_Id"
#define KFBEmailId							    @"userFBid"
#define KDaysAgo                                @"days ago"
#define KWeeksAgo                               @"weeks ago"
#define KHourAgo                                @"hr ago"
#define KMinAgo                                 @"mins ago"
#define KSecAgo                                 @"sec ago"
#define KFew                                    @"few"

#define kEmailKey                               @"kEmailKey"
#define kPasswordKey                            @"kPasswordKey"
#define kDomain                                 @"kDomain"
// getting Application Name
#define kAppName [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] isEqualToString:(@"Daisy")]

#define isAppDaisy ([[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] isEqualToString:(@"Daisy")] ? YES:NO)

enum passStatus
{
    passNotStarted, // == 0 (by default)
    passStarted, // == 1 (incremented by 1 from previous)
    passElapsed, // == 2
    passUnExpected
};

/*******************************************************************************************************************/
#pragma mark - Strings

#define NSSTRING_HAS_DATA(_x) (((_x) != nil) && ( [(_x) length] > 0 ))

#define kAttachmentSize 15728640 // For 15 MB // - For 10 MB - 10485760
#define kAttachmentCount 3
#define kWeightLimit 100



//NetManager
#define ERROR_INTERNET							@"Connection Error. Please check your internet connection and try again."
#define ERROR_TIMEOUT							@"Error occurred. Please check your internet connection and try again."
#define ERROR_INLOGIN                           @"Username OR Password is wrong."
#define ERROR_FETCHING_DATA_FROM_SERVER         @"Please wait data is getting downloaded." // ALERT ADDED FOR CALENDER AND TIMETABLE SCREEN FOR HANDLING CRASH

#define ERROR_FETCHING_DATA_FROM_SERVER_CALENDAR    @"Calendar loading...please wait"
#define ERROR_FETCHING_DATA_FROM_SERVER_TIMETABLE   @"Timetable loading...please wait"
#define ERROR_FETCHING_DATA_FROM_SERVER_INBOX       @"Contacts loading...please wait"
#define ERROR_FETCHING_DATA_FROM_SERVER_TaskTypesAndClasses      @"Task types/Classes are loading...please wait"


#define Notification_Facebook_Did_Login         @"facebook login "
#define ERROR_FEEDBACK							@"Error ! Please try later."
#define SUCCESS_FEEDBACK						@"Feedback sent successfully."

//Near Me Tab


#pragma mark Alert title and messages
#define Alert_Cancel_Button                     @"Cancel"
#define Alert_Ok_Button                         @"Ok"
#define ERROR_GPS                               @"Error getting Current Location"
#define Login_Credential_Error                  @"Please enter your credential for login."

/*****************************************************************************************************************/


#define Method_FeedBack                              @"feedback"

#define kKeyAppType                                  @"AppType"

#define Method_Login                                 @"login"
#define Method_Update_Profile                        @"userdetails"
#define Method_Parent_deatil                         @"getparent"
#define Method_Teacher_detail                        @"teachers"
#define Method_Parent_Invitaion                      @"sendinvitation"
#define Method_updateprofile                         @"updateprofile"
#define Method_GetCountry                            @"getstates"
#define Method_setdairyitems                         @"setdairyitems"
#define Method_getdairyitems                         @"dairyitems"
#define Method_GetSubject                            @"getsubjects"
#define Method_AddEventItem                          @"setevents"
#define Method_EditItem                              @"edititem"
#define Method_GetClass                              @"getclass"
#define Method_GetClass_User                         @"getclassuser"
#define Method_GetSchool_User_Class                  @"schooluserclass"
#define Method_ForgetPassword                        @"resetpassword"
#define Method_TimeTable                             @"timetabledetails"
#define Method_SchoolInfo                            @"getcontents"
#define Method_SchoolImages                          @"getimages"
#define Method_EditEventItem                         @"editevent"
#define Method_AddPeriod                             @"addclasssubject"
#define Method_editPeriod                            @"editclasssubject"
#define Method_deletPeroid                           @"deleteclasssubject"
#define Method_deletDairyItem                        @"deletedairyitem"
#define Method_getInboxMsz                           @"inboxmessages"
#define Method_sendInboxMsz                          @"sendmessage"
#define Method_replyInboxMsz                         @"replymessage"
#define Method_readInboxMsz                          @"readmessage"
#define Method_deleteInboxMsz                        @"deletemessage"
#define Method_sendReminder                          @"sendreminder"
#define Method_syncronizeDiaryItem                   @"insertupdatesync"
#define Method_syncronizeClass                       @"subjectclasssync"
//For note Entry
#define Method_insertupdatenotentry                  @"insertupdatenotentry"
#define Method_sendmessagesync                       @"sendmessagesync"
#define Method_GetGrade                              @"getgrades"
#define Method_For_PassApproval                      @"authenticatepasscode"


#define Method_SearchClasses                         @"searchclasses"
#define Method_AssociateClass                        @"associateclass"
#define Method_addtaskbasedmerit                     @"addtaskbasedmerit"
#define Method_DeleteAttachments                     @"deleteattachments"


//// *****************************   Report Start    ****************************** /////////
#define Method_GetReport                             @"getreports"
#define NOTIFICATION_Report                          @"NOTIFICATION_Report"

// Campus Based Start
#define Method_getcalcycledays                        @"getcalcycledays"
#define NOTIFICATION_CalendarCycleDays                @"NOTIFICATION_CalendarCycleDays"
#define NOTIFICATION_DeleteAttachments               @"NOTIFICATION_DeleteAttachments"

// Campus Based End

//Merit - Start
#define k_MERIT_CHECK @"Merit"


//Merit - End


/***********************************************************************************************/

#pragma mark- Notifications

#define NOTIFICATION_registration                  @"NOTIFICATION_registration"
#define NOTIFICATION_Login                         @"NOTIFICATION_Login"
#define NOTIFICATION_UpdateProfile                 @"NOTIFICATION_UpdateProfile"
#define NOTIFICATION_Parent_Detail                 @"NOTIFICATION_Parent_Detail"
#define NOTIFICATION_Teacher_Detail                @"NOTIFICATION_Teacher_Detail"
#define NOTIFICATION_Send_Invitaio_To_Parent       @"NOTIFICATION_Send_Invitaio_To_Parent"
#define NOTIFICATION_updateprofile                 @"NOTIFICATION_updateprofile"
#define NOTIFICATION_GetCountry                    @"NOTIFICATION_GetCountry"
#define NOTIFICATION_add_Dairy_Items               @"NOTIFICATION_add_Dairy_Items"
#define NOTIFICATION_get_Dairy_Items               @"NOTIFICATION_get_Dairy_Items"
#define NOTIFICATION_get_Subject                   @"NOTIFICATION_get_Subject"
#define NOTIFICATION_Add_Event                     @"NOTIFICATION_Add_Event"
#define NOTIFICATION_EditItem                      @"NOTIFICATION_EditItem "
#define NOTIFICATION_Get_Class                     @"NOTIFICATION_Get_Class"
#define NOTIFICATION_Get_Class_User                @"NOTIFICATION_Get_Class_User"
#define NOTIFICATION_Get_SchoolUser_Class          @"NOTIFICATION_Get_SchoolUser_Class"
#define NOTIFICATION_ForgetPassword                @"NOTIFICATION_ForgetPassword"
#define NOTIFICATION_TimeTable                     @"NOTIFICATION_TimeTable"
#define NOTIFICATION_SchoolInfo                    @"NOTIFICATION_SchoolInfo"
#define NOTIFICATION_SchoolImage                   @"NOTIFICATION_SchoolImage"
#define NOTIFICATION_EditEventItem                 @"NOTIFICATION_EditEventItem"
#define NOTIFICATION_AddPeriod                     @"NOTIFICATION_AddPeriod"
#define NOTIFICATION_EditPeriodData                @"NOTIFICATION_EditPeriodData"
#define NOTIFICATION_DeletePeriodData              @"NOTIFICATION_DeletePeriodData"
#define NOTIFICATION_DeleteDairyItem               @"NOTIFICATION_DeleteDairyItem"
#define NOTIFICATION_getInboxMsz                   @"NOTIFICATION_getInboxMsz"
#define NOTIFICATION_sendInboxMsz                  @"NOTIFICATION_sendInboxMsz"
#define NOTIFICATION_readnboxMsz                   @"NOTIFICATION_readnboxMsze"
#define NOTIFICATION_replyInboxMsz                 @"NOTIFICATION_replyInboxMsz"
#define NOTIFICATION_deleteInboxMsz                @"NOTIFICATION_deleteInboxMsz"
#define NOTIFICATION_sendReminderToStudent         @"NOTIFICATION_sendReminderToStudent"
#define NOTIFICATION_syncronizeDiaryItem           @"NOTIFICATION_syncronizeDiaryItem"
#define NOTIFICATION_syncronizeClass               @"NOTIFICATION_syncronizeClass"
#define NOTIFICATION_SendPass_For_Approval          @"NOTIFICATION_SendPass_For_Approval"

//For Note entry
#define NOTIFICATION_insertNoteEntry               @"NOTIFICATION_insertNoteEntry"
#define NOTIFICATION_getGrade_info                 @"NOTIFICATION_getGrade_info"

#define NOTIFICATION_Search_Classes                @"NOTIFICATION_Search_Classes"
#define NOTIFICATION_Associate_Class               @"NOTIFICATION_Associate_Class"

#define NOTIFICATION_AddTaskBaseMerit                @"NOTIFICATION_AddTaskBaseMerit"



#define kKeyDefRegion                       @"Region"
#define kKeyRegionAUS                       @"AUS" // Daisy And Honey Comb
#define kKeyRegionUS                        @"US" // Prime

// Data Base Migration Keys
#define vserionUpdate                       @"vserionUpdate11" // 8 - for Campus based Table Migration
#define vserionUpdateTimeTable              @"vserionUpdateTimeTable5" // 3 - for Campus based Table Migration
#define vserionCalenderCycleUpdate          @"vserionCalenderCycleUpdate7" // 5 - for Campus based Table Migration
#define vserionUpdateCampusBasedSchool      @"vserionUpdateCampusBasedSchool" // New added - for Campus based Table Migration
#define vserionUpdateSchoolContents         @"vserionUpdateSchoolContents"  // new Fields introduce to migrate School Content

//// *****************************   Flurry Events Start ****************************** /////////

#define  FLURRY_EVENT_Diary_Item_Created @"Diary Item Created"
#define  FLURRY_EVENT_Diary_Item_Updated @"Diary Item Updated"
#define  FLURRY_EVENT_Diary_Item_Deleted @"Diary Item Deleted"

#define  FLURRY_EVENT_Class_Created @"Class Created"
#define  FLURRY_EVENT_Class_Updated @"Class Updated"
#define  FLURRY_EVENT_Class_Deleted @"Class Deleted"

#define  FLURRY_EVENT_Message_Created @"Message Created"
#define  FLURRY_EVENT_Message_Deleted @"Message Deleted"
#define  FLURRY_EVENT_Reply_to_Message @"Reply to Message"
#define  FLURRY_EVENT_Parent_Invitation @"Parent Invitation"

#define  FLURRY_EVENT_Profile_View_Cilcked  @"Profile View Clicked"
#define  FLURRY_EVENT_Calender_View_Cilcked @"Day View Clicked"
#define  FLURRY_EVENT_Home_View_Cilcked @"Home View Clicked"
#define  FLURRY_EVENT_Inbox_View_Cilcked @"Inbox View Clicked"
#define  FLURRY_EVENT_SchoolInformation_View_Cilcked @"Schoolinformation View Clicked"
#define  FLURRY_EVENT_EducationalContent_View_Cilcked @"Educationalcontent View Clicked"
#define  FLURRY_EVENT_About_View_Cilcked @"About View Clicked"
#define  FLURRY_EVENT_Timetable_View_Cilcked @"Subject View Clicked"
#define  FLURRY_EVENT_MyClasses_View_Cilcked @"Myclasses View Clicked"
#define  FLURRY_EVENT_Parent_MyProfile_View_Cilcked @"Myprofile View Clicked"
#define  FLURRY_EVENT_Report_View_Cilcked @"Report View Cilcked"

#define  FLURRY_EVENT_Day_View_Cilcked @"Day View Clicked"
#define  FLURRY_EVENT_Week_View_Cilcked @"Week View Clicked"
#define  FLURRY_EVENT_Month_View_Cilcked @"Month View Clicked"
#define  FLURRY_EVENT_Glance_View_Cilcked @"Glance View Clicked"

#define  FLURRY_EVENT_Due_Callout_Clicked @"Due Callout Clicked"
#define  FLURRY_EVENT_Notifications_Callout_Clicked @"Notifications Callout Clicked"
#define  FLURRY_EVENT_Assigned_Callout_Clicked @"Assigned Callout Clicked"

#define  FLURRY_EVENT_Sync_Called @"Sync Called"

#define  FLURRY_EVENT_Error_in_fetching_Diary_Item @"EVENT Error in fetching Diary Item"
#define  FLURRY_EVENT_Error_in_updating_Diary_Item @"EVENT Error in updating Diary Item"
#define  FLURRY_EVENT_Error_in_creating_Diary_Item @"Error in creating Diary Item"
#define  FLURRY_EVENT_Error_in_deleting_Diary_Item @"Error in deleting Diary Item"

#define  FLURRY_EVENT_Error_in_fetching_Class @"Error in fetching Class"
#define  FLURRY_EVENT_Error_in_updating_Class @"Error in updating Class"
#define  FLURRY_EVENT_Error_in_creating_Class @"Error in creating Class"
#define  FLURRY_EVENT_Error_in_deleting_Class @"Error in deleting Class"

#define  FLURRY_EVENT_Error_in_fetching_Inbox_Item @"Error in fetching Inbox Item"
#define  FLURRY_EVENT_Error_in_replying_Inbox_Item @"Error in replying Inbox Item"
#define  FLURRY_EVENT_Error_in_creating_Inbox_Item @"Error in creating Inbox Item"
#define  FLURRY_EVENT_Error_in_deleting_Inbox_Item @"Error in deleting Inbox Item"

#define  FLURRY_EVENT_Error_in_sending_parent_invitation @"Error in sending parent invitation"

#define  FLURRY_EVENT_Feedback_Sent @"Feedback Sent"

//// *****************************   Flurry Events End    ****************************** /////////



// Daisy Flurry Key - Development
//#define FLURRY_API_KEY @"PXRZTZW3DXQRDQBRTBJ7"

// Daisy Flurry Key - Production
#define FLURRY_API_KEY @"MZRYDYW59GQFCDC2V5MH"

// Honeycomb iPad Flurry Key - Production
//#define FLURRY_API_KEY @"JQTM2N5P2MSWYX9CJRW7"


// Prime Flurry Key - Development
//#define FLURRY_API_KEY @"KB8JJQSTC5M83N545C9T"

//Prime Flurry Key - Production
//#define FLURRY_API_KEY @"KNKW9K2GVPYXVG374GXV"


//// ***************************** Webservice URL For All  Start *************************** ////


#pragma mark - Web Services Base Url and Methods


//------------------------------------------------------------------------------
// Daisy URL Starts
//------------------------------------------------------------------------------
// Daisy Production URLS

//#define BaseUrl                                 @"https://e-planner.com.au/webservicev5/"
//#define pic_BaseUrl                             @"https://e-planner.com.au/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"https://e-planner.com.au/"
//#define pic_BaseUrlForSchoolLogo                @"https://e-planner.com.au/"
//#define AttachmentURL @"https://e-planner.com.au/webservicev5/attachmentsync?"
////
//////Merit - Start
//#define merits_BaseUrl @"https://e-planner.com.au/images/merits/"
////Merit - End


//#define BaseUrl                                 @"https://e-planner.com.au/webservicev6/"
//#define pic_BaseUrl                             @"https://e-planner.com.au/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"https://e-planner.com.au/"
//#define pic_BaseUrlForSchoolLogo                @"https://e-planner.com.au/"
//#define AttachmentURL @"https://e-planner.com.au/webservicev6/attachmentsync?"
//////
////////Merit - Start
//#define merits_BaseUrl @"https://e-planner.com.au/images/merits/"
//////Merit - End



//#define BaseUrl                                 @"https://e-planner.com.au/webservicev7/"
//#define pic_BaseUrl                             @"https://e-planner.com.au/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"https://e-planner.com.au/"
//#define pic_BaseUrlForSchoolLogo                @"https://e-planner.com.au/"
//#define AttachmentURL @"https://e-planner.com.au/webservicev7/attachmentsync?"
//////
////////Merit - Start
//#define merits_BaseUrl @"https://e-planner.com.au/images/merits/"
////Merit - End


#define BaseUrl                                 @"https://e-planner.com.au/webservicev9/"
#define pic_BaseUrl                             @"https://e-planner.com.au/UserProfileImages/"
#define pic_BaseUrlForSchool                    @"https://e-planner.com.au/"
#define pic_BaseUrlForSchoolLogo                @"https://e-planner.com.au/"
#define AttachmentURL @"https://e-planner.com.au/webservicev9/attachmentsync?"
////
//////Merit - Start
#define merits_BaseUrl @"https://e-planner.com.au/images/merits/"
////Merit - End


//------------------------------------------------------------------------------
// Daisy Testing URLS

////#define BaseUrl                                 @"http://54.253.121.67/zend/html/index.php/webservicev6test/"
////For Merit - webservicev6 is to be used.
//#define BaseUrl                                 @"http://54.253.121.67/zend/html/index.php/webservicev6/"
//#define pic_BaseUrl                             @"http://54.253.121.67/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.253.121.67/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.253.121.67/zend/html/"
//#define AttachmentURL @"http://54.253.121.67/zend/html/index.php/webservicev6/attachmentsync?"
////Merit - Start
//#define merits_BaseUrl @"http://54.253.121.67/zend/html/images/merits/" // need to append school Id
////Merit - End

//
//#define BaseUrl                                 @"http://54.253.121.67/zend/html/index.php/webservicev7/"
//#define pic_BaseUrl                             @"http://54.253.121.67/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.253.121.67/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.253.121.67/zend/html/"
//#define AttachmentURL @"http://54.253.121.67/zend/html/index.php/webservicev7/attachmentsync?"
////Merit - Start
//#define merits_BaseUrl @"http://54.253.121.67/zend/html/images/merits/" // need to append school Id


//#define BaseUrl                                 @"http://54.253.121.67/zend/html/index.php/webservicev8/"
//#define pic_BaseUrl                             @"http://54.253.121.67/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.253.121.67/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.253.121.67/zend/html/"
//#define AttachmentURL @"http://54.253.121.67/zend/html/index.php/webservicev8/attachmentsync?"
////Merit - Start
//#define merits_BaseUrl @"http://54.253.121.67/zend/html/images/merits/" // need to append school Id

//#define BaseUrl                                 @"http://54.253.121.67/zend/html/index.php/webservicev9/"
//#define pic_BaseUrl                             @"http://54.253.121.67/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.253.121.67/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.253.121.67/zend/html/"
//#define AttachmentURL @"http://54.253.121.67/zend/html/index.php/webservicev9/attachmentsync?"
////Merit - Start
//#define merits_BaseUrl @"http://54.253.121.67/zend/html/images/merits/" // need to append school Id

//#define BaseUrl                                 @"http://54.253.121.67/zendnew/html/index.php/webservicev6/"
//#define pic_BaseUrl                             @"http://54.253.121.67/zendnew/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.253.121.67/zendnew/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.253.121.67/zendnew/html/"
//#define AttachmentURL @"http://54.253.121.67/zendnew/html/index.php/webservicev6/attachmentsync?"
////Merit - Start
//#define merits_BaseUrl @"http://54.253.121.67/zendnew/html/images/merits/" // need to append school Id


// Daisy URL Ends
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Honeycomb URL Starts
//------------------------------------------------------------------------------
// Production URLS

//#define BaseUrl                                 @"https://digiplanner.co.uk/webservicev4/"
//#define pic_BaseUrl                             @"https://digiplanner.co.uk/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"https://digiplanner.co.uk/"
//#define pic_BaseUrlForSchoolLogo                @"https://digiplanner.co.uk/"
//#define AttachmentURL @"https://digiplanner.co.uk/webservicev4/attachmentsync?"


//#define BaseUrl                                 @"https://digiplanner.co.uk/webservicev7/"
//#define pic_BaseUrl                             @"https://digiplanner.co.uk/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"https://digiplanner.co.uk/"
//#define pic_BaseUrlForSchoolLogo                @"https://digiplanner.co.uk/"
//#define AttachmentURL @"https://digiplanner.co.uk/webservicev7/attachmentsync?"
//////////Merit - Start
//#define merits_BaseUrl @"https://digiplanner.co.uk/images/merits/"
//////Merit - End

/// Honeycomb testing URL
//------------------------------------------------------------------------------

//#define BaseUrl                                 @"http://54.229.195.92/zend/html/index.php/webservicev4/"
//#define pic_BaseUrl                             @"http://54.229.195.92/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.229.195.92/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.229.195.92/zend/html/"
//#define AttachmentURL @"http://54.229.195.92/zend/html/index.php/webservicev4/attachmentsync?"

//#define BaseUrl                                 @"http://54.229.195.92/zend/html/index.php/webservicev7/"
//#define pic_BaseUrl                             @"http://54.229.195.92/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.229.195.92/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.229.195.92/zend/html/"
//#define AttachmentURL @"http://54.229.195.92/zend/html/index.php/webservicev7/attachmentsync?"
////Merit - Start
//#define merits_BaseUrl @"http://54.229.195.92/zend/html/images/merits/" // need to append school Id



// Honeycomb URL Ends
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Prime URL Starts
//------------------------------------------------------------------------------
// Prime Production URLS

//#define BaseUrl                                 @"https://prime-planner.com/index.php/webservicev4/"
//#define pic_BaseUrl                             @"https://prime-planner.com/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"https://prime-planner.com/"
//#define pic_BaseUrlForSchoolLogo                @"https://prime-planner.com/"
//#define AttachmentURL @"https://prime-planner.com/index.php/webservicev4/attachmentsync?"


// Prime Testing URLS
//------------------------------------------------------------------------------

//#define BaseUrl                                 @"http://54.225.84.136/zend/html/index.php/webservicev3/"
//#define pic_BaseUrl                             @"http://54.225.84.136/zend/html/UserProfileImages/"
//#define pic_BaseUrlForSchool                    @"http://54.225.84.136/zend/"
//#define pic_BaseUrlForSchoolLogo                @"http://54.225.84.136/zend/html/"
//#define AttachmentURL @"http://54.225.84.136/zend/html/index.php/webservicev3/attachmentsync?"


// Prime URL Ends
//------------------------------------------------------------------------------


//// *****************************   Webservice URL For All  End  ****************************** ////


// -----------------------------------------------------------------------------
// DateFormat to support 24 Hr and 12 Hr Format in all applications

#define kDateFormatDateWithTimeToSaveFetchFromDB @"yyyy-MM-dd HH:mm:ss"
#define kDateFormatOnlyDateToSaveFetchInFromDB @"yyyy-MM-dd"
#define kDateFormatOnlyTimeToSaveFetchInFromDB @"HH:mm:ss"
#define kDateFormatToShowOnUI_hhmma @"hh:mm a"

#define kDateFormatToShowOnUI_HHmmss @"HH:mm:ss"

#define kDateFormatForiOS7 @"hh:mm a"
#define kDateFormatForiOS8 @"h:mm a"

// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Document Folder Name

#define kApplicationDocumentDirectoryName   @"Daisy_iPad"
//
// Date Format
// For Daisy - AUS
#define kDATEFORMAT @"dd-MM-yyyy"
#define kDATEFORMAT_dd_MMM @"dd-MMM"
#define kDATEFORMAT_Slash @"dd/MM/yyyy"
#define kDATEFORMAT_with_day @"EEEE dd MMMM"
#define kDATEFORMAT_global_day @"EEEE, dd MMMM"
#define kDATEFORMAT_coma @"dd MMM yyyy"
#define kDATEFORMAT_ddMMM @"dd MMM"
#define kDATEFORMAT_ddMMMMyy_hhmma @"dd MMMM yy    hh:mm a"
#define kDATEFORMAT_ddMMMyyyy @"dd MMM yyyy"
#define kDATEFORMAT_ddMMMyy  @"dd MMM yy"
#define KDATEFORMAT_Day_ddMMMyyyy @"EEEE, dd MMM, yyyy"

// For Daisy
//----------------------------------------------------------------------------------------------------------
#define kAboutTeacherPDFURL        @"https://e-planner.com.au/AboutDaisy/Daisy-Teacher-Manual-2015.pdf"
#define kAboutStudentPDFURL        @"https://e-planner.com.au/AboutDaisy/Daisy-Student-Manual-2015.pdf"

#define kAboutTeacherYouTubeURL    @"https://www.youtube.com/watch?v=Sg2QQk_gF6I" // Mofdified on 17Dec 2014
#define kAboutStudentYouTubeURL    @"https://www.youtube.com/watch?v=qdwdI497E3w" // Modified on 17Dec 2014

#define kStrAboutImage @"about Daisy"
#define kAppNameForInviteParent @"Daisy"
#define kPlaceholderNameForDomainName @"<schoolname>.e-planner.com.au (optional)"

//TODO: Uncomment the code when creating the build for Honeycomb

// For Honeycomb
//----------------------------------------------------------------------------------------------------------
//#define kAboutYouTubeURL    @"http://youtu.be/afJX3dV0AwY"
//#define kAboutPDFURL        @"http://digiplanner.co.uk/AboutHoneycomb/e-book-honeycomb-5-a.pdf"

//#define kAboutTeacherPDFURL        @"http://digiplanner.co.uk/AboutHoneycomb/e-book-honeycomb-5-a.pdf"
//#define kAboutStudentPDFURL        @"http://digiplanner.co.uk/AboutHoneycomb/e-book-honeycomb-5-a.pdf"
//
//#define kAboutTeacherYouTubeURL    @"http://youtu.be/afJX3dV0AwY"
//#define kAboutStudentYouTubeURL    @"http://youtu.be/afJX3dV0AwY"
//
//#define kStrAboutImage @"about Honeycomb"
//#define kAppNameForInviteParent @"Honeycomb"
//#define kPlaceholderNameForDomainName @"<schoolname>.digiplanner.co.uk (optional)"


//TODO: Uncomment the code when creating the build for Prime

//#define kApplicationDocumentDirectoryName   @"Prime_iPad"
//
//// For Prime - US
//#define kDATEFORMAT @"MM-dd-yyyy"
//#define kDATEFORMAT_dd_MMM @"MMM-dd"
//#define kDATEFORMAT_Slash @"MM/dd/yyyy"
//#define kDATEFORMAT_with_day @"EEEE MMMM dd"
//#define kDATEFORMAT_global_day @"MMMM dd, yyyy"
//#define kDATEFORMAT_coma @"MMMM dd, yyyy"
//#define kDATEFORMAT_ddMMM @"MMM dd"
//#define kDATEFORMAT_ddMMMMyy_hhmma @"MMM dd, yyyy"
//#define kDATEFORMAT_ddMMMyyyy @"MMM dd yyyy"
//#define kDATEFORMAT_ddMMMyy  @"MMM dd yy"
//#define KDATEFORMAT_Day_ddMMMyyyy @"EEEE, MMM dd, yyyy"
//
//#define kAboutTeacherYouTubeURL    @"https://www.youtube.com/watch?v=496R-38cVSw"
//#define kAboutStudentYouTubeURL    @"https://www.youtube.com/watch?v=496R-38cVSw"
//
//#define kAboutPDFURL               @"http://prime-planner.com/AboutPrime/e-book-Prime.pdf"
//#define kAboutTeacherPDFURL        @"https://prime-planner.com/AboutPrime/TeacherGuide.pdf"
//#define kAboutStudentPDFURL        @"https://prime-planner.com/AboutPrime/StudentGuide.pdf"
//
//#define kStrAboutImage @"About_Prime"
//#define kAppNameForInviteParent @"Prime"
//#define kPlaceholderNameForDomainName @""

