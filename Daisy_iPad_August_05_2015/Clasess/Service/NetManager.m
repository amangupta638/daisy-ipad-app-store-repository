//
//  NetManager.m
//  PlacesYellHybrid
//
//  
//

#import "NetManager.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "Defines.h"
@implementation NetManager

@synthesize acceptTarget;
@synthesize finishLoadingSelector;
@synthesize actualConnection,aDic;

@synthesize error;

@synthesize storedObject;

@synthesize parameters;
@synthesize url, request;

- (id)initWithURL:(NSURL *)thisUrl 
				 target:(id)thisTarget 
  finishLoadingSelector:(NSString *)thisSelector 
		 postParameters:(NSDictionary *)thisParameters
	   showErrorMessage:(BOOL)thisShowErrorMessage 
		   storedObject:(id)thisObject {
    
    
	self.storedObject = thisObject;
	return [self initWithURL:thisUrl 
							target:thisTarget 
			 finishLoadingSelector:thisSelector
					postParameters:thisParameters
				  showErrorMessage:thisShowErrorMessage];
}

- (id)initWithURL:(NSURL *)thisUrl 
				 target:(id)thisTarget 
  finishLoadingSelector:(NSString *)thisSelector 
		 postParameters:(NSDictionary *)thisParameters
	   showErrorMessage:(BOOL)thisShowErrorMessage {
	
	
	self = [super init];
	if( nil == self ) {
		return nil;
	}
	shouldShowErrorMessage = thisShowErrorMessage;
	self.acceptTarget = thisTarget;
	self.finishLoadingSelector = thisSelector;
	self.url = thisUrl;
	self.parameters = thisParameters;
	
	return self;
}

- (id)init {
	shouldShowErrorMessage = YES;
	[self enableNotifications];
	self = [super init];
	return self;
}
-(void)stopRequest{
    [self.request clearDelegatesAndCancel];
    UIApplication* app = [UIApplication sharedApplication]; 
	app.networkActivityIndicatorVisible = NO;
}
- (void) startRequest {
	UIApplication* app = [UIApplication sharedApplication]; 
	app.networkActivityIndicatorVisible = YES;
	
	self.request = nil;
	if (parameters) {
		self.request = [ASIFormDataRequest requestWithURL:url];
	} else {
		self.request = [ASIHTTPRequest requestWithURL:url];
	}
	
	[self.request setDelegate:self];
	NSString *iphoneOs = [[UIDevice currentDevice] systemVersion];
	iphoneOs = [iphoneOs stringByReplacingOccurrencesOfString:@"." withString:@"_"];
	NSString *generalFormat = @"Mozilla/5.0 (iPhone; U; CPU iPhone OS %@ like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7D11 Safari/528.16";
	NSString *userAgent = [NSString stringWithFormat:generalFormat, iphoneOs];
	[request addRequestHeader:@"User-Agent" value:userAgent];
    
	bool isImageExist = NO;
    if (parameters)
    {
        if([[parameters objectForKey:@"isImage"] isEqualToString:@"1"])
        {
            NSMutableDictionary *aDicTemp = [[NSMutableDictionary alloc]init];
            self.aDic = aDicTemp;
            [aDicTemp release];
            
            isImageExist = YES;
            for (NSString *key in [parameters allKeys])
            {
                
                if(![key isEqualToString:@"isImage"])
                {
                    if([key isEqualToString:@"image"])
                    {
                        if(![[parameters objectForKey:key] isEqualToString:@"NO1234"])
                            [aDic setObject:[parameters objectForKey:key] forKey:key];
                    }
                    else
                        [aDic setObject:[parameters objectForKey:key] forKey:key];
                }
            }
        }
    }
    
    if (parameters && isImageExist==YES)
    {
        [self.request setUploadProgressDelegate:[aDic objectForKey:@"progress"] ];
        for (NSString *key in [aDic allKeys])
        {
            if([key isEqualToString:@"file"])
            {
                [self.request setFile:[aDic objectForKey:key] forKey:key];
            }
            else
            {
                [self.request setPostValue:[aDic objectForKey:key] forKey:key];
            }
        }
    }
    else{
        if (parameters)
        {
            [request addRequestHeader:@"Content-Type" value:@"application/json; charset=utf-8"];
            [request addRequestHeader:@"Data-Type" value:@"json"];
            [request appendPostString:[parameters JSONRepresentation]];
        }
    }

	[self.request setTimeOutSeconds:TIMEOUT_INTERVAL];
	[self.request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request1 {
    
    NSData *responseString = [request1 responseData];
    NSError* error1;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseString
                          
                          options:kNilOptions
                          error:&error1];
    
    NSData *responseData = [request1 responseData];
	UIApplication* app = [UIApplication sharedApplication]; 
	app.networkActivityIndicatorVisible = NO;
    
    
	if(acceptTarget && [acceptTarget isKindOfClass:[NSObject class]])
	{
		[acceptTarget performSelector:NSSelectorFromString(finishLoadingSelector) withObject:self withObject:responseData];
	}
}

- (void)requestFailed:(ASIHTTPRequest *)request1 {
	UIApplication* app = [UIApplication sharedApplication]; 
	app.networkActivityIndicatorVisible = NO;
	
	[error release];
	error = nil;
	error = [[self makeErrorLocalizable:request1.error] retain];
	if (shouldShowErrorMessage) {
		[self showLoadErrorMessage:[error localizedDescription]];
	}
	if (acceptTarget) {
		[acceptTarget performSelector:NSSelectorFromString(finishLoadingSelector) withObject:self withObject:nil];
	}
}

/**
 * Setup for reachability checks of the remote host
 *
 */
- (void) enableNotifications {
	if (!reachability) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
	
		reachability = [[Reachability reachabilityWithHostName: @"www.apple.com"] retain];
		[reachability startNotifier];
	}
}

- (void)updateReachabilityStates {
	NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
		if(remoteHostStatus == NotReachable)
        {
#if DEBUG
        NSLog(@"not reachable");
#endif
		
		UIApplication* app = [UIApplication sharedApplication]; 
		app.networkActivityIndicatorVisible = NO;
		if (shouldShowErrorMessage)
        {
            
			//[self showLoadErrorMessage:ERROR_INTERNET];
		}
	}
        else if (remoteHostStatus == ReachableViaWiFi)
    {
#if DEBUG
        NSLog(@"wifi");
#endif
		
	}
        else if (remoteHostStatus == ReachableViaWWAN)
        {
#if DEBUG
        NSLog(@"cell");
#endif
		
	}
}

- (BOOL)networkReachable {
	NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
	return (remoteHostStatus != NotReachable);
}

- (void)reachabilityChanged:(NSNotification *)note {	
    [self updateReachabilityStates];
}

- (void)showLoadErrorMessage:(NSString *)thisMessage {
//	UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"error" message:thisMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
//	[alert show];
}

- (NSError *)makeErrorLocalizable:(NSError *) netError {
	NSError *tmpError = nil;
	switch ([netError code]) {
		case ASIConnectionFailureErrorType:
		{
			NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithDictionary:[netError userInfo]];
			
            
			//[userInfo setObject:ERROR_INTERNET forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:@"There is some error occurred during transaction. Please try again." forKey:NSLocalizedDescriptionKey];
			tmpError = [NSError errorWithDomain:[netError domain] code:[netError code] userInfo:userInfo];
			break;
		}
		case ASIRequestTimedOutErrorType:
		{
			NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithDictionary:[netError userInfo]];
			
            //21AugChange
			[userInfo setObject:@"There is some error occurred during transaction. Please try again." forKey:NSLocalizedDescriptionKey];
            //[userInfo setObject:ERROR_TIMEOUT forKey:NSLocalizedDescriptionKey];
			tmpError = [NSError errorWithDomain:[netError domain] code:[netError code] userInfo:userInfo];
			break;
		}
		default:
		{
			NSMutableDictionary* userInfo = [NSMutableDictionary dictionaryWithDictionary:[netError userInfo]];
			
			//21AugChange
			//[userInfo setObject:ERROR_INTERNET forKey:NSLocalizedDescriptionKey];
            [userInfo setObject:@"There is some error occurred during transaction. Please try again." forKey:NSLocalizedDescriptionKey];
            
			tmpError = [NSError errorWithDomain:[netError domain] code:[netError code] userInfo:userInfo];
			break;
		}
	}
	return tmpError;
}

- (void)dealloc {
    [aDic release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[finishLoadingSelector release];
	[actualConnection release];
	[storedObject release];
	[error release];
	[parameters release];
    [self.request release];
	[url release];
	[super dealloc];
}
@end
