//
//  Service.h
//  Modizo
//
//  Created by AppStudioz on 13/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetManager.h"
#import "MyParent.h"
#import "MyTeacher.h"
#import "Country.h"
#import "Subjects.h"
#import "Item.h"
#import "School_detail.h"
#import "MyClass.h"
#import "Student.h"
#import "TimeTable.h"
#import "TimeTableCycle.h"
#import "Room.h"
#import "Period.h"
#import "School_Information.h"
#import "EventItem.h"
#import "Inbox.h"
#import "News.h"
#import "DayOfNotes.h"
#import "DairyItemUsers.h"
#import "TableUpdateTime.h"
#import "NoteEntry.h"
#import"TableForCycle.h"
#import "Grade.h"
#import "Events_Users.h"
#import "Report.h"
#import "ListOfValues.h"

// Campus Based Start
#import "CalendarCycleDay.h"
// Campus Based End


@class Celebrity;
@interface Service : NSObject
{
    NSManagedObjectContext *_managedObjectContext; 
    NetManager* netManager;
    NSInteger objectId;
    
//    BOOL tempPriority; //Aman added to be removed
    
}

@property (nonatomic, retain) NetManager *netManager;
+ (Service*) sharedInstance;
- (NSFetchRequest *)getBasicRequestForEntityName: (NSString *)entityName;
- (NSEntityDescription *) makeEntityDescription: (NSString *)name;
-(NSArray*)getStoredAllItemData:(NSDate*)date;
-(void)parseItemDataWithDictionary:(NSDictionary*)dictionary;
-(void)registrationWithUser:(NSMutableDictionary*)dict;
-(void)parseProfileDataWithDictionary:(NSDictionary*)dictionary;
-(MyParent*)parseMyParentDataWithDictionary:(NSDictionary*)dictionary forStudent:(NSString *)studentId andStudentName :(NSString *) strStudentName;
-(MyTeacher*)parseMyTeacherDataWithDictionary:(NSDictionary*)dictionary;
-(void)updateUserProfile:(NSMutableDictionary*)dict;
-(void)getParentDetail:(NSMutableDictionary*)dict;
-(void)getTeacherDetail:(NSMutableDictionary*)dict;
-(void)sendParentInvitation:(NSMutableDictionary*)dict;
-(MyProfile *)getProfileDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification;
- (NSArray *)getReportDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification;
- (NSArray *)getListOfValues;
- (Report *)getSingleReportDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification;

-(void)insertUserprfile:(NSMutableDictionary*)dict;
-(void)parseCountryDataWithDictionary:(NSDictionary*)dictionary;
-(NSArray*)getStoredAllStateData;
- (Country *)getCountrymData:(NSString*)state;
-(void)getCountryDetail:(NSString*)time;
- (States *)getStateData:(NSString*)state;
-(void)addDiaryTtems:(NSMutableDictionary*)dict;
-(void)getDiaryItems;
-(void)parseAnnonecementDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseEventItemDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseSubjectDataWithDictionary:(NSDictionary*)dictionary;
-(void)addEventItems:(NSMutableDictionary*)dict;
-(NSArray*)getStoredAllSubjectData;
- (Subjects *)getSubjectDataWithName:(NSString*)subject;
- (Item *)getItemDataInfoStored:(NSNumber*)itemId postNotification:(BOOL)postNotification;
- (Subjects *)getSubjectsDataInfoStored:(NSString*)subjectName postNotification:(BOOL)postNotification;
-(void)parseSchoolDetailDataWithDictionary:(NSDictionary*)dictionary;
-(void)editDiaryTtems:(NSMutableDictionary*)dict;
-(NSArray*)getStoredAllItemDataInApp;
-(NSArray*)getStoredAllItemDataInAppForDueOnly;
-(NSArray*)getStoredAllIAnnouncement:(NSString*)date;
#pragma mark myclass
//19 may
-(School_detail *)getSchoolDetailDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification;
-(void)getClassUserDetail;
-(void)parseMyClassDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseStudentDataWithDictionary:(NSDictionary*)dictionary;
-(NSArray*)getStoredAllClassData;
-(void)getSchoolUserClassDetail;
- (MyClass *)getClassDataInfoStored:(NSString*)classId  cycle:(NSString*)cycle  postNotification:(BOOL)postNotification;
-(void)sendPasswordOnMail:(NSString*)email;
-(NSArray*)getStoredAllItemDataTime:(NSDate*)fromdate toDate:(NSDate*)todate assign:(NSDate*)assignDate;
-(void)getTimeTableDetailsofSchool;
-(void)getReportData;

//timeTable
-(void)parsePeriodDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseRoomDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseTimeTableCycleDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseTimeTableDataWithDictionary:(NSDictionary*)dictionary;
-(void)parseReportDataWithDictionary:(NSDictionary*)dictionary;


-(NSArray*)getStoredAllTimeTableData;
-(TimeTableCycle*)getCurentTimeTableCycle:(NSNumber*)table_id  cycle:(NSString*)cycleno;
-(NSArray*)getStoredAllPeriodTableData:(NSNumber*)table_id cycle:(NSString*)cycleDay;
-(Period*)getStoredFilterPeriodTableData:(NSNumber*)table_id cycle:(NSString*)cycleDay startTime:(NSDate*)time;
-(MyClass*)getStoredFilterClass:(NSNumber*)peroid_id cycle:(NSString*)cycleDay timeTable:(NSNumber*)timeTableID
;
- (Room *)getRoomDataInfoStored:(NSString*)room postNotification:(BOOL)postNotification;

///School Info
-(void)getSchoolInformation:(NSString*)contentType;
-(NSArray*)getStoredDaisyAboutData:(NSString*)type ;
-(void)ParseSchoolInformatio:(NSDictionary*)dictionary;
-(void)ParseSchoolImageData:(NSDictionary*)dictionary;
-(NSArray*)getNotDownloadedImage:(NSString*)type ;
-(void)getSchoolImage;
-(NSArray*)getStoredAllEventItemDataInApp;

//For Event dairy item
-(void)editEventDairyItems:(NSMutableDictionary*)dict;
-(NSArray*)getStoredAllEventItemData:(NSDate*)date;
- (EventItem *)getEventItemDataInfoStored:(NSNumber*)itemId ipadd:(NSString*)ipadd;
- (EventItem *)getEventItemDataInfoStoredForDelete:(NSNumber*)itemId postNotification:(BOOL)postNotification;
-(void)addPeriodOnTimeTable:(NSDictionary*)contentType;
-(void)editPeriodOnTimeTable:(NSDictionary*)contentType;
-(void)deltePeriodOnTimeTable:(NSDictionary*)contentType;
- (void) deleteObjectOfClasUser:(NSString*)Class_id cycle:(NSString*)cycle ;
- (MyClass *)getClassDataInfoFromSubjectId:(NSString*)subjId ;
- (void) deleteObjectOfSubject:(NSString*)subName ;
- (Student*)getStudentDataInfoStored:(NSString*)studentId postNotification:(BOOL)postNotification;
//For Delete dairy items
-(void)deleteDairyItems:(NSMutableDictionary*)dict;
- (void) deleteObjectOfDairyItem:(NSNumber*)Item_id ;
- (void) deleteObjectOfEventDairyItem:(NSNumber*)Item_id;
-(NSArray*)getStoredAllEventDairyItem:(NSDate*)fromdate toDate:(NSDate*)todate satrtdate:(NSDate*)startDate;
-(NSArray*)getTodaysEvents:(NSDate*)startDate;
-(void)getInboxMessages;
-(void)sendInboxMessages:(NSString*)sub des:(NSString*)des reciver:(NSString*)reciver;
-(void)deleteInboxMessages:(NSNumber*)msz_id;
-(void)replyInboxMessages:(NSString*)sub des:(NSString*)des reciver:(NSString*)reciver parent:(NSNumber*)parent_id;
-(void)readInboxMessages:(NSString*)msz_id;
-(Inbox*)parseInboxMessagesWithDictionary:(NSDictionary*)dictionary;
-(NSArray*)getStoredMessage:(NSNumber*)parent_id;
- (void) deleteObjectOfInBoxMessge:(NSNumber*)messgeid;
- (Inbox *)getInboxDataInfoStored:(NSNumber*)message_id postNotification:(BOOL)postNotification;
- (NSArray *)getInboxDataStored:(NSNumber*)message_id postNotification:(BOOL)postNotification ;

-(NSArray*)getStoredAllItemDataWithSubjectId:(NSString*)sub_name;
-(NSArray*)getStoredAllStudentDataWithTeacherId;
-(NSArray*)getStoredAllStudentData;


-(NSArray*)getStoredAllIAnnouncementForInbox;
-(void)parseNewsDataWithDictionary:(NSDictionary*)dictionary;
-(NSArray*)getStoredAllINewsForInbox;
- (MyTeacher *)getTeacherDataInfoStored:(NSString*)teacherid postNotification:(BOOL)postNotification;
//-(ClassesInfo *)getClassesInfoDataInfoStored:(NSString*)class_id postNotification:(BOOL)postNotification;
-(NSNumber *)nextAvailble:(NSString *)idKey forEntityName:(NSString *)entityName;
-(void*)parseDayOfNotesWithDictionary:(NSDictionary*)dictionary;
- (NSArray *)getClassDeleteObject:(NSNumber*)del;
//new


//new
-(BOOL)dayOfNotesHaveWeekend:(NSDate*)date;
-(DayOfNotes*)dayOfNotesHaveHollyDay:(NSDate*)date semDate:(NSDate*)dateOfsem;
//for assigned student
-(NSArray*)getStoredAllAssignedItemDataWithItemId:(NSNumber*)itemID;
-(void)parseAssignedStudentDataWithDictionary:(NSDictionary*)dictionary;
-(void)sendRemiderToAssinStudent:(NSMutableDictionary*)dict;
-(DayOfNotes*)dayOfNotesIncludeCycleDay:(NSDate*)date note:(NSNumber*)day_id;

//
-(NSArray*)dayOfNotesCycleLength:(NSDate*)date semDate:(NSDate*)dateOfsem;
-(NSDate*)dateFromMilliSecond:(NSNumber*)milliSeconds;
-(NSString*)miliSecondFromDateMethod:(NSDate*)date;
// synchoniz
-(void)synchronizeDiaryItem:(NSMutableDictionary*)dict;
- (NSArray *)getItemDataInfoStoredwithNoSync:(NSNumber*)itemId ;
-(void)parseLastUpdateTableDictionary:(NSDictionary*)dictionary;
- (TableUpdateTime *)getlastUpdateTableDataInfoStored;
- (Item *)getItemDataInfoStoredForParseDate:(NSNumber*)itemId appid:(NSString*)appId;
;
//
- (NSArray *)getAllNoteEntryDataI:(NSNumber*)itemId ;
-(void)parseNoteEntryDataWithDictionary:(NSDictionary*)dictionary;
- (NSArray *)getEvnetsDataInfoStoredwithNoSync:(NSNumber*)isSyncVal;
- (NSArray *)getNoteEntryDataInfoStoredwithNoSync:(NSNumber*)isSyncVal;
- (void) deleteObjectOfNoteEntryItem:(NSNumber*)Item_id;

//nwe

-(void)parseCycleDaywithDictionary:(NSDictionary*)dictionary;
- (TableForCycle *)getlastcycleandDayTableDataInfoStored:(NSDate*)date;
- (NSArray *)getAllNoteEntryDataIwithUniqueId:(NSNumber*)itemId;
- (NSArray *)getItemDataInfoStoredwithdeletedItem;


//New Rohit
- (MyClass*)getClassDataInfoStoredForAppid:(NSString*)appId;
-(NSArray*)getStoredAllClassDataInApp:(NSNumber*)table_id;
- (NSArray *)getClassDataInfoStoredwithNoSync:(NSNumber*)sync;
-(void)synchronizeClass:(NSMutableDictionary*)dict;
- (MyClass *)getClassDataFromClassId:(NSString*)classId;
- (void) deleteClassObject:(MyClass*)Object;
-(void)updateClassTime:(TableUpdateTime*)table;
-(void)SyncMyClassDataWithServer:(NSDictionary*)dictionary;
- (void)deleteSubjectForClass:(MyClass*)class  ;
-(NSArray*)getStoredAllDueItemData:(NSDate*)date;
-(NSArray*)getStoredAllDueItemDataTime:(NSDate*)fromdate toDate:(NSDate*)todate assign:(NSDate*)dueDtae;
#pragma mark
#pragma mark My Add-Ons-Ishan
-(void)storeInboxMessages:(NSString*)sub des:(NSString*)des reciver:(NSString*)reciver parentId:(NSNumber *)parentId localParentId:(NSNumber *)localParentId parentMailId:(NSString *) parentMailId;
-(void)synchronizeMails;

- (Inbox *)getInboxDataInfoStoredForAppId:(NSNumber *)app_id :(NSString *)ipAddaress postNotification:(BOOL)postNotification;
- (Inbox *)getInboxDataInfoStoredForAppId:(NSNumber *)app_id postNotification:(BOOL)postNotification
;
- (void)messageDeletedWithAppId:(NSNumber *)appId ipAddress:(NSString *)ipAdd;
- (void)messageDeletedWithAppId:(NSNumber *)msgId;
- (void)deleteObjectOfInBoxMessgefromAppId:(NSNumber *)appId ipAddress:(NSString *)ipAdd;
-(NSArray*)getStoredMessageForLocal:(NSNumber*)mszId;
-(NSArray*)getStoredAllSchoolData;
-(NSArray*)getStoredAllINewsForWithDate:(NSString*)date; // Chnaged fo GMT 
-(void)getGradeDetail;
-(void)parseGradeDataWithDictionary:(NSDictionary*)dictionary;
-(NSArray*)getStoredAllGradeForProfile;
- (Grade *)getGradeData:(NSString*)gradeName;

-(NSArray*)getStoredAllClassDataWithSubjecName:(NSString*)subjectname;
-(MyClass*)getStoredClassDatawithClassId:(NSString*)class_id;
-(void)getSearchClasses:(NSMutableDictionary*)dict;
-(void)getAssociatesClasses:(NSMutableDictionary*)dict;
-(NSArray*)getStoredDaisyPaneltData:(NSDate*)Date;
//for parse student data //07/08/2013
- (Student*)getStudentDataInfoStoredClass:(NSString*)studentId classe1:(NSString*)classe1;
- (DairyItemUsers*)getAssigneStudentDataInfoStoredForClass:(NSString*)assStudentId;

- (School_Information *)getSchoolInfoDataInfoStored:(NSString*)schol postNotification:(BOOL)postNotification;
-(void)insertUpadteNoteEntry:(NSMutableDictionary*)dict;
- (void) deleteObjectOfAnnouncment:(NSNumber*)Item_id;
- (void) deleteObjectOfNewz:(NSNumber*)Item_id ;
- (Period *)getPeriodDataInfoStored:(NSString*)period postNotification:(BOOL)postNotification;
//
-(NSArray*)getStoredAllTimeTableCycleData;

// Aman added
-(void)parseAssignedStudentDataForEventUserWithDictionary:(NSDictionary*)dictionary ;
- (Events_Users *)getAssigneStudentForEventUsersDataInfoStored:(NSString*)assStudentId eventId:(NSString*)eventID postNotification:(BOOL)postNotification ;
- (void) deleteObjectOfEventUsersItem:(NSNumber*)Item_id ;

-(NSArray*)getStoredAllParentsData ;
-(NSArray*)getStoredAllStudentDataWithClassId :(NSString *)classId ;

-(void)parseDiaryItemTypesDataWithDictionary:(NSMutableDictionary*)dictionary;
-(NSArray *)getActiveItems:(NSArray *)results;
- (NSArray *)getParenteDataForStudent:(NSString *)studentId ;
- (NSArray *) getAttachmentsForItemId:(NSNumber *)itemId withType:(NSString *)DItype;
- (void) updateIsDeletedFlagForAttachment :(Attachment *)attachment ;
- (void) updateIsReadFlagForEventItem :(EventItem *)eventItem;
- (void) updateIsReadFlagForDiaryItem :(Item *)item;
- (NSArray *)getEventUserDataInfoStored:(NSNumber*)eventId postNotification:(BOOL)postNotification ;
- (void) deleteObjectOfAttachment:(NSString*)attachmentId;

- (MyParent *)getParenteDataInfoStored:(NSString*)parentid studentID:(NSString*)studentID postNotification:(BOOL)postNotification;
// Aman Added Code For Educationcontent

-(NSMutableArray *)arrayOfEducationContent:(NSMutableArray*)educationBundledata;


//----- Added By Mitul on -----//
- (NSArray *)parseManifestFile:(NSString *)contentType;
- (NSString *)getContentType;
//--------- By Mitul ---------//

-(NSArray*)getStoreDAllActiveDiaryItemTypes;

-(NSArray*)getStoredAllActiveDiaryItemTypesWithFormTemplate;
-(NSString*)getStoredFormTemplate:(NSString*)Name;
-(NSString*)getStoredNameForFormTemplate:(NSString*)formTemplate;
// Aman GP
-(NSString *)getDiaryItemTypeId:(NSString *)selectedtype;
-(NSString*)getNameForFormTemplateOnItemIDBasis:(NSNumber*)formTemplate;
// Aman GP
-(NSString *)getItemTypeID:(NSString *)selectedtype;

-(NSArray*)getStoredAllItemDataWithClassId:(NSString*)class_id ;
///

- (void) insertAtatchmentLocally :(NSDictionary *)dict ;
- (void) parseAttachmentData :(NSDictionary *)jsonDictD ;
- (void) deleteAttachmentForDiaryItemId :(NSNumber *)diaryItemId withType:(NSString*)DItype;
- (NSArray *) getNoSyncAttachmentsForItemId:(NSNumber *)itemId ;
- (void) deleteAttachmentForId :(NSString *)attachmentId ;

- (void) deleteStudentForClassID :(NSString *)classID ;
- (void) deleteStudent ;
-(void)feedBackFromUser:(NSMutableDictionary*)dict;
- (void)netManagerDidFinishFeedback:(NetManager *)thisNetManager
                           response:(NSMutableData *)responseData;

-(NSArray *)getActiveEvents:(NSArray *)results;
- (Student*)getStudentDataInfoStoredOnlyForNote:(NSString*)studentId postNotification:(BOOL)postNotification;
 /*************************New Code for deleting DeletedParents AND UpdatedParents 30 May 2014 *************/
-(void)parseToUpdateMyParentDataWithDictionary:(NSDictionary*)dictionary;
- (MyParent *)getUpdateParenteDataInfoStored:(NSString*)parentid;
- (void) deleteParentWithId :(NSString *)parentId;
- (NSArray *)getParentForParentID:(NSString *)parentId;


 /*************************New Code for deleting DeletedParents AND UpdatedParents 30 May 2014 *************/

- (void) deleteAllObjects: (NSString *) entityDescription ;

/*******************New Methods Added For Out Class********************************/

-(NSString*)getNameForPassOnPassTypeIDBasis:(NSNumber*)formPassID;
-(void)SendRequestForPassApproval:(NSDictionary*)dictionary;
-(void)updateDiaryItemIsApprovedStatus:(NSDictionary*)dictionary;
-(void)addAssignedUserAfterApproval:(NSDictionary*)dictionary;
-(NSString *)getApprovalNameFromLocalDB:(NSString*)itemID;
-(NSString *)getStudentName:(Item*)diaryitem;
-(void)updateLocalPass:(NSDictionary*)dict;
- (NSArray *)getOutOfClassDataInfoStoredwithNoSync:(NSNumber*)itemId;
-(NSArray*)getStoredAllActivePassTypesWithFormTemplate;


/*******************New Methods Added For Out Class********************************/
-(void)updateTableForCycle:(NSDictionary*)dictionary;

// Content Methods
- (NSString *)getContentTypeForEducationContent;

// Campus Based Start
-(MyClass*)getStoredAllClassesTableData:(NSNumber*)table_id cycle:(NSString*)cycleDay;
- (TimeTable *)getTimeTableDataInfoStored:(NSString*)timetable postNotification:(BOOL)postNotification;
-(NSString*)getTimeTableCodeOnthebasisofClass:(MyClass*)classselected;
- (MyClass *)getClassDataFromClassIdAndCycleDay:(NSString*)classId cycleDay:(NSString *)cycleDay;
-(NSArray*)getStoredAllClassData:(MyClass*)classe;
-(void)parseCalendarCycleDayWithDictionary:(NSDictionary*)dictionary;
-(void)getCalendarCycleDays;
- (void)netManagerDidFinishCalendarCycleDaysDetail:(NetManager *)thisNetManager
                                          response:(NSMutableData *)responseData;
- (void) deleteObjectCalendarCycleDay:(NSNumber*)CalendarCycleDay_id;

-(NSArray*)getStoredCalendarCycleDayForGivenDate:(NSDate *)date;
-(NSArray*)getUniqueClassOntimeTableId:(TimeTable *)timeTableObj classes:(MyClass*)classeObj;


-(NSArray*)getStoredCalendarCycleDay:(MyClass*)classeselected;
-(TimeTable*)getTimeTableonthebasisOfClassTimeTableId:(MyClass*)classeselected;

// Campus Based End

// Campus Based Migration Start
-(void)deleteAllTablesData;
-(void)updateTableupdateTimeForCampusBasedMigration;
// Campus Based Migration End

// more than 2 semester support for multi campus
- (TimeTable *)getTimeTableDataFromTimetableId:(NSString*)timetableId andCampusCode:(NSString *)campusCode;
- (NSArray *)getTimeTableDataBasedOnUniqueCampusCode;
- (NSArray *)getTimeTableDataFromCampusCode:(NSString *)campusCode;

//Merit - Start
- (void)addOrEditDIOnServerWithType:(NSString*)type
                             typeid:(NSNumber *)typeId
                           schoolId:(NSNumber *)schoolId
                            classId:(NSNumber *)classId
                              title:(NSString *)title
                        description:(NSString *)description
                 assignedAndDueDate:(NSString *)assignedAndDueDate
                   assignAndDueTime:(NSString *)assignAndDueTime
                        createdDate:(NSString *)createdDate
                          createdBy:(NSString *)createdBy
                        lastUpdated:(NSString *)lastUpdated
                       assignedtoMe:(NSNumber *)assignedtoMe
                       assignUserId:(NSString *)assignUserId
                             isSync:(NSNumber *)isSync
                           isNotify:(NSNumber *)isNotify
                          meritType:(NSString *)meritType
                           onTheDay:(NSNumber *)onTheDay
                         passTypeId:(NSNumber *)passTypeId
                         isApproved:(NSNumber *)isApproved
                        isCompelete:(NSNumber *)isCompelete
                           isDelete:(NSNumber *)isDelete
                             isRead:(NSNumber *)isRead
                             itemId:(NSNumber *)itemId
                        subjectName:(NSString *)subjectName
                             isEdit:(BOOL)isEdit;

- (void)editTaskBasedMeritOnServerWithDIType:(NSString*)type
                                      typeid:(NSNumber *)typeId
                                    schoolId:(NSNumber *)schoolId
                                     classId:(NSNumber *)classId
                                       title:(NSString *)title
                                 description:(NSString *)description
                                assignedDate:(NSString *)assignedDate
                                     dueDate:(NSString *)dueDate
                                  assignTime:(NSString *)assignTime
                                     dueTime:(NSString *)dueTime
                                 createdDate:(NSString *)createdDate
                                   createdBy:(NSString *)createdBy
                                 lastUpdated:(NSString *)lastUpdated
                                assignedtoMe:(NSNumber *)assignedtoMe
                                      isSync:(NSNumber *)isSync
                                    onTheDay:(NSNumber *)onTheDay
                                  isApproved:(NSNumber *)isApproved
                                 isCompelete:(NSNumber *)isCompelete
                                    isDelete:(NSNumber *)isDelete
                                      isRead:(NSNumber *)isRead
                                      itemId:(NSNumber *)itemId
                                 subjectName:(NSString *)subjectName
                                   completed:(NSNumber *)completed
                                    priority:(NSString *)priority
                                    progress:(NSString *)progress
                                      weight:(NSNumber *)weight
                                    isNotify:(NSNumber *)isNotify
                                meritComment:(NSString *)meritComment
                                   meritName:(NSString *)meritName
                                   meritType:(NSString *)meritType
                                  passTypeId:(NSNumber *)passTypeId
                                assignUserId:(NSString *)assignUserId;

- (void)downloadMeritsImages:(NSMutableArray *)arrLOV;
- (TimeTableCycle *)getTimeTableCycleDataInfoStored:(NSString*)timetablecycle postNotification:(BOOL)postNotification;

- (NSArray *)getAllActiveMeritsFromListofValues;
- (NSArray *)getAllMeritsAsMeritModels;
- (ListofValues *)getListOfValuesInfoStored:(NSNumber*)listofvaluesTypeid;
//Merit - End
//                assignedTime:(NSString*)assignedTime
//                     dueTime:(NSString*)dueTime


//                            Priority:(NSString*)Priority
//EstimatedHours:(NSString*)EstimatedHours
//EstimatedMins:(NSString*)EstimatedMins
//Completed:(NSString*)Completed
//Progress:(NSString*)Progress
//AssignedtoMe:(NSString*)AssignedtoMe
//AllStudents:(NSString*)AllStudents
//isOnTheDay:(NSString*)isOnTheDay
//weight:(NSString*)weight
//                             subjectId:(NSString*)subjectId
//                             weblink:(NSString*)weblink
//                          passTypeid:(NSNumber *)passtypeId;
-(void)updateNumberOfCampus:(NSString*)numberOfCampus;
- (void) deleteDiaryItemUserForDiaryItemId :(NSNumber *)diaryItemId;

- (BOOL)isMeritActive;
- (NSArray *)getActiveLOVWithDIU:(NSArray *)arrDIU;


/*************Start Methods added for calender crash ( when user click repetedly on calender tab jsut after logging before getting all data from server)***********************/
- (NSInteger )getCountforAllStoredCalenderCycleDays;
-(NSInteger)getCountForStoredAllTimeTableData;
-(NSInteger)getCountForStoredDiaryItemTypes;

/*************End Methods added for calender crash ( when user click repetedly on calender tab jsut after logging before getting all data from server)***********************/

- (BOOL)isNumberOfCampusUpdated;
-(NSString*)getFormTemplateOnItemIDBasis:(NSNumber*)formTemplateID;
-(void)updateDiaryItemForMeritDescription:(NSNumber*)itemId meritdesription:(NSString*)descriptionmerit;
- (ListofValues *)getactiveListOfValuesInfoStoredInMyclass:(NSNumber*)listofvaluesTypeid;

-(BOOL)isMeritStudentActiveOrNot:(NSString*)studentId;
- (DairyItemUsers*)getAssigneStudentDataInfoStored:(NSString*)assStudentId itemId:(NSString*)itemID postNotification:(BOOL)postNotification;
- (NSArray *)getTaskBasedMeritStoredwithNoSync:(NSNumber*)isSyncVal;
-(NSArray*)getStoredAllActiveDiaryItemTypesWithFormTemplateWithOnlyTeacherFlag;
- (MyClass *)getClassDataRoom:(NSNumber*)roomID period:(NSNumber*)periodID cycle:(NSNumber*)cycleday classID:(NSNumber*)classID;

// Attachment offline
-(void)updateAttachement:(NSNumber *)diaryItemId universalid:(NSNumber *)universalID withType:(NSString*)DItype;
-(NSArray*)getStoredDiaryItemIDForAttachementWithNosync;
-(NSArray*)getStoredDiaryItemIDForAttachementWithNosyncAndIsDeleteOne;

-(void)deleteAttachmentsOnServer:(NSArray*)arrayOfDict;

// Attachment offline


@end

