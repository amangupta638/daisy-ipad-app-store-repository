//
//  AttachmentService.h
//  StudyPlaner
//
//  Created by Nandini Tomke on 27/12/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AttachmentService : NSObject

/* Input param
 1. DiaryItemId 
 2. SchoolId
 3. UserId
 4. Dict 0f file data
 */


//- (void) postAttachmentsForDiaryItem:(NSString *)diaryItemId schoolId :(NSString *)sId userId :(NSString *)uId andAttachmentDictionary :(Attachment *) attachment ;
- (void) postAttachmentsForDiaryItem:(NSString *)diaryItemId schoolId :(NSString *)sId userId :(NSString *)uId andAttachmentDictionary :(Attachment *) attachment isCopyDI:(BOOL)isCopyDI;

+ (AttachmentService*) sharedInstance ;

@end
