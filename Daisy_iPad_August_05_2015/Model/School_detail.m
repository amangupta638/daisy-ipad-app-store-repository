//
//  School_detail.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 05/09/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "School_detail.h"


@implementation School_detail

@dynamic allow_parent;
@dynamic address_Id;
@dynamic dairy_Titled;
@dynamic display_Name;
@dynamic fax_Numbers;
@dynamic has_Upload;
@dynamic include_Impower;
@dynamic is_Deleted;
@dynamic logo_Image;
@dynamic logo_Image_Url;
@dynamic mission_Statement;
@dynamic motto;
@dynamic name;
@dynamic noOfCampus;
@dynamic phone_no;
@dynamic region;
@dynamic school_conf_Id;
@dynamic school_Id;
@dynamic school_Image;
@dynamic school_Image_Url;
@dynamic theme_color;
@dynamic updated;
@dynamic web_Site_Url;
@dynamic school_BgImge;
@dynamic school_BgImageUrl;
@dynamic empower_type;
@dynamic domainName;
@end
