//
//  Subjects.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Subjects : NSManagedObject

@property (nonatomic, retain) NSNumber * campus_id;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSString * subject_color;
@property (nonatomic, retain) NSNumber * subject_id;
@property (nonatomic, retain) NSString * subject_name;
@property (nonatomic, retain) NSString * user_id;

@end
