//
//  Period.h
//  StudyPlaner
//
//  Created by Aman Gupta on 12/25/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Period : NSManagedObject

@property (nonatomic, retain) NSString * cycle_day;
@property (nonatomic, retain) NSDate * end_time;
@property (nonatomic, retain) NSNumber * has_Break;
@property (nonatomic, retain) NSNumber * period_id;
@property (nonatomic, retain) NSNumber * school_id;
@property (nonatomic, retain) NSDate * start_time;
@property (nonatomic, retain) NSNumber * timeTable_id;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSNumber * forteacher;

@end
