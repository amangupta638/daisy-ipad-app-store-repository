//
//  Attachment.h
//  StudyPlaner
//
//  Created by Aman gupta on 10/07/15.
//  Copyright (c) 2015 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventItem, Item;

@interface Attachment : NSManagedObject

@property (nonatomic, retain) NSNumber * aID;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSDate * dateAttached;
@property (nonatomic, retain) NSNumber * diaryItemId;
@property (nonatomic, retain) NSString * displayFileName;
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic, retain) NSNumber * isDeletedOnLocal;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSString * itemtype;
@property (nonatomic, retain) NSDate * lastupdatedDate;
@property (nonatomic, retain) NSNumber * localAttachmentId;
@property (nonatomic, retain) NSString * remoteFilePath;
@property (nonatomic, retain) NSNumber * size;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) EventItem *eventItem;
@property (nonatomic, retain) Item *item;


@property (nonatomic, retain) NSString * driveName;
@property (nonatomic, retain) NSString * indexPath;
@property (nonatomic, retain) NSString * localFolderName;
@property (nonatomic, retain) NSString * displayName;
@property (nonatomic, retain) NSString * fileType;
@property (nonatomic, retain) NSString * localFilePath;
@property (nonatomic, retain) NSData *fileData ;

@end
