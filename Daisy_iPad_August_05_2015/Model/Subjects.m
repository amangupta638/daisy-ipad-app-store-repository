//
//  Subjects.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Subjects.h"


@implementation Subjects

@dynamic campus_id;
@dynamic isActive;
@dynamic school_Id;
@dynamic subject_color;
@dynamic subject_id;
@dynamic subject_name;
@dynamic user_id;

@end
