//
//  Inbox_Users.m
//  StudyPlaner
//
//  Created by Nandini Tomke on 13/12/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Inbox_Users.h"


@implementation Inbox_Users

@dynamic messageId;
@dynamic iD;
@dynamic userId;
@dynamic localMessageId ;
@dynamic isDel;
@end
