//
//  ClassesInfo.m
//  StudyPlaner
//
//  Created by Dhirendra on 23/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "ClassesInfo.h"
#import "MyProfile.h"
#import "Student.h"


@implementation ClassesInfo

@dynamic class_id;
@dynamic class_name;
@dynamic class_UserId;
@dynamic code;
@dynamic period_id;
@dynamic room_id;
@dynamic schoo_id;
@dynamic subject_id;
@dynamic subject_name;
@dynamic updateTime;
@dynamic profile;
@dynamic students;

@end
