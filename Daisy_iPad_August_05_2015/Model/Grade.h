//
//  Grade.h
//  StudyPlaner
//
//  Created by Dhirendra on 17/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Grade : NSManagedObject

@property (nonatomic, retain) NSString * grade_name;
@property (nonatomic, retain) NSNumber * grade_id;

@end
