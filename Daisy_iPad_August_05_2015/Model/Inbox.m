//
//  Inbox.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Inbox.h"


@implementation Inbox

@dynamic created_time;
@dynamic discrptin;
@dynamic inbox_user;
@dynamic message_id;
@dynamic parent_id;
@dynamic reciver_id;
@dynamic sender_id;
@dynamic subject;

@end
