//
//  States.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Country;

@interface States : NSManagedObject

@property (nonatomic, retain) NSNumber * state_id;
@property (nonatomic, retain) NSString * state_name;
@property (nonatomic, retain) Country *country;

@end
