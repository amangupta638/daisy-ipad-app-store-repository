//
//  CalendarCycleDay.m
//  StudyPlaner
//
//  Created by Apple on 09/08/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import "CalendarCycleDay.h"


@implementation CalendarCycleDay

@dynamic dateRef;
@dynamic cycleDayLabel;
@dynamic created;
@dynamic createdBy;
@dynamic updated;
@dynamic updatedBy;
@dynamic isDelete;
@dynamic calendarCycleDayId;
@dynamic cycleDay;
@dynamic campusId;
@dynamic formatedCycleDayLabel;

@end
