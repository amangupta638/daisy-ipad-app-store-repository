//
//  TimeTable.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 01/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TimeTable : NSManagedObject

@property (nonatomic, retain) NSString * cycle_Length;
@property (nonatomic, retain) NSDate * end_date;
@property (nonatomic, retain) NSNumber * school_id;
@property (nonatomic, retain) NSString * semester_name;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSNumber * timeTable_id;
@property (nonatomic, retain) NSString * cycleLable;
// Campus Based Start
@property (nonatomic, retain) NSString * campusCode;

// Campus Based End


@end
