//
//  NoteEntry.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 01/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface NoteEntry : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * ipadd;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSNumber * note_Id;
@property (nonatomic, retain) NSNumber * note_localId;
@property (nonatomic, retain) NSNumber * noteEntry_id;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSNumber * unique_id;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSNumber * isDelete;

@end
