//
//  MyClass.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 03/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Student;

@interface MyClass : NSManagedObject

@property (nonatomic, retain) NSNumber * app_id;
@property (nonatomic, retain) NSNumber * class_id;
@property (nonatomic, retain) NSString * class_name;
@property (nonatomic, retain) NSString * class_userID;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSDate * createdTime;
@property (nonatomic, retain) NSNumber * cycle_day;
@property (nonatomic, retain) NSString * ipaddress;
@property (nonatomic, retain) NSNumber * isDelete;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSNumber * period_Id;
@property (nonatomic, retain) NSNumber * room_Id;
@property (nonatomic, retain) NSString * room_name;
@property (nonatomic, retain) NSNumber * subject_d;
@property (nonatomic, retain) NSString * subject_name;
@property (nonatomic, retain) NSNumber * timeTable_id;
@property (nonatomic, retain) NSDate * updatedTime;
@property (nonatomic, retain) NSSet *student;
@property (nonatomic, retain) NSArray *arrRegisteredStudents ;
// Campus Based Start
@property (nonatomic, retain) NSNumber * campusId;
@property (nonatomic, retain) NSString * campusCode;

// Campus Based End


@end

@interface MyClass (CoreDataGeneratedAccessors)

- (void)addStudentObject:(Student *)value;
- (void)removeStudentObject:(Student *)value;
- (void)addStudent:(NSSet *)values;
- (void)removeStudent:(NSSet *)values;

@end
