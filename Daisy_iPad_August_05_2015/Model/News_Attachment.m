//
//  News_Attachment.m
//  StudyPlaner
//
//  Created by Dhirendra on 22/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "News_Attachment.h"
#import "News.h"


@implementation News_Attachment

@dynamic atachment_content;
@dynamic atachment_id;
@dynamic created;
@dynamic news_id;
@dynamic updated;
@dynamic news;

@end
