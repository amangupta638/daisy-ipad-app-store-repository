//
//  Report.m
//  StudyPlaner
//
//  Created by Amol Gaikwad on 20/06/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import "Report.h"


@implementation Report

@dynamic report_id;
@dynamic school_Id;
@dynamic isReportSchoolActive;
@dynamic isActive;
@dynamic isAdministrator;
@dynamic isParent;
@dynamic isTeacher;
@dynamic isStudent;
@dynamic report_url;
@dynamic report_description;
@dynamic report_name;

@end
