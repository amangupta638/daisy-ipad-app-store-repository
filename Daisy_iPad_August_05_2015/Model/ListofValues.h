//
//  ListofValues.h
//  StudyPlaner
//
//  Created by Apple on 21/06/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
//
@interface ListofValues : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSString * descriptions;
@property (nonatomic, retain) NSString * diaryItemType;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSNumber * listofvaluesTypeid;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSNumber * sortedOrder;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSString *iconsmall;

@end
