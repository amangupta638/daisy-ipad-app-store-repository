//
//  DayOfNotes.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 18/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DayOfNotes : NSManagedObject

@property (nonatomic, retain) NSDate * dateOfNote;
@property (nonatomic, retain) NSNumber * dayOfNotes_id;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSNumber * includeInCycle;
@property (nonatomic, retain) NSNumber * isGreyOut;
@property (nonatomic, retain) NSNumber * isWeekend;
@property (nonatomic, retain) NSNumber * overrideCycle;
@property (nonatomic, retain) NSNumber * overrideCycleDay;
@property (nonatomic, retain) NSNumber * schoolId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * uniqueId;
@property (nonatomic, retain) NSDate * updateDate;
@property (nonatomic, retain) NSString * user_id;

@end
