//
//  Student.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 03/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyClass;

@interface Student : NSManagedObject

@property (nonatomic, retain) NSNumber * class_id;
@property (nonatomic, retain) NSString * email_Id;
@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSString * phone_no;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSString * student_id;
@property (nonatomic, retain) NSString * student_name;
@property (nonatomic, retain) NSString * teacher_id;
@property (nonatomic, retain) NSDate * updatedTime;
@property (nonatomic, retain) MyClass *myClass;
//Aman added
@property (nonatomic, retain) NSString * status;



@end
