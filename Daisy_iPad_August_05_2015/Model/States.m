//
//  States.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "States.h"
#import "Country.h"


@implementation States

@dynamic state_id;
@dynamic state_name;
@dynamic country;

@end
