//
//  MyTask.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MyTask : NSManagedObject

@property (nonatomic, retain) NSDate * created_date;
@property (nonatomic, retain) NSNumber * task_id;
@property (nonatomic, retain) NSString * task_name;

@end
