//
//  MyParent.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "MyParent.h"
#import "MyProfile.h"


@implementation MyParent

@dynamic email;
@dynamic image;
@dynamic imageUrl;
@dynamic name;
@dynamic parent_id;
@dynamic phone;
@dynamic profile;
@dynamic studentId ;
@dynamic studentName ;
// New Code Aman Migration Prime

@dynamic parent_id_str;
@end
