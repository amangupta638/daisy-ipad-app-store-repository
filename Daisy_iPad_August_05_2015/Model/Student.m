//
//  Student.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 03/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Student.h"
#import "MyClass.h"


@implementation Student

@dynamic class_id;
@dynamic email_Id;
@dynamic firstname;
@dynamic image;
@dynamic imageUrl;
@dynamic lastname;
@dynamic phone_no;
@dynamic school_Id;
@dynamic student_id;
@dynamic student_name;
@dynamic teacher_id;
@dynamic updatedTime;
@dynamic myClass;
//Aman added
@dynamic status;


@end
