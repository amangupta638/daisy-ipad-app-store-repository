//
//  TableUpdateTime.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 26/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "TableUpdateTime.h"


@implementation TableUpdateTime

@dynamic about;
@dynamic calenderPanel;
@dynamic classUpdateTime;
@dynamic diaryItem;
@dynamic empowerContent;
@dynamic inbox;
@dynamic profile;
@dynamic schoolInfo;
@dynamic studentUpdateTime;
@dynamic table_id;
@dynamic timeTble;
@dynamic report;
// Campus Based Start
@dynamic calendarCycleUpdate;
// Campus Based End

@dynamic teacherUpdateTime;
@dynamic parentsUpdateTime;

@end
