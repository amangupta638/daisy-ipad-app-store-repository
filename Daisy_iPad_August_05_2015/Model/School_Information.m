//
//  School_Information.m
//  StudyPlaner
//
//  Created by Dhirendra on 31/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "School_Information.h"


@implementation School_Information

@dynamic content;
@dynamic content_type;
@dynamic item_id;
@dynamic order_by;
@dynamic order_no;
@dynamic parent_id;
@dynamic school_Id;
@dynamic status;
@dynamic title;
@dynamic update_date;
@dynamic startDate;
@dynamic endDate;

@end
