//
//  ReportViewController.m
//  StudyPlaner
//
//  Created by Amol Gaikwad on 19/06/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import "ReportViewController.h"
#import "Service.h"

@interface ReportViewController ()

@end

@implementation ReportViewController
@synthesize arrayReportList_Name,arrayReportList_URL,_popoverControler1,_popoverControler2,_popoverControler3;
@synthesize arrayClassNameList,arrayStudentList,_currentTimeTable,arrayClassIDList,arrayStudentIDList;
@synthesize _itemArr;
@synthesize strSelectedClassID,strSelectedStudentID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
      MyProfile *profobj =[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if ([profobj.type isEqualToString:@"Student"]) {
        
        btnStudentList.hidden = YES;
        lblStudentName.hidden = YES;
        imgStudentButton.hidden = YES;
    }
    else /// Teacher
        
    {
        btnStudentList.hidden = YES;
        lblStudentName.hidden = YES;
        imgStudentButton.hidden = YES;
    
    }
    
    slectedIndex = 0;
    selectedPickerView = 0;
    NSArray *arrReport=[[Service sharedInstance]getReportDataInfoStored:@"" postNotification:nil];
    
//     profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
  
     arrayReportList_Name = [[NSMutableArray alloc]init];
     arrayReportList_URL = [[NSMutableArray alloc]init];
     arrayClassNameList = [[NSMutableArray alloc]init];
     arrayStudentList = [[NSMutableArray alloc]init];
     _itemArr = [[NSMutableArray alloc]init];
    arrayStudentIDList = [[NSMutableArray alloc]init];
    arrayClassIDList = [[NSMutableArray alloc]init];
    if (arrReport.count != 0) {
        for (Report *p in arrReport) {
        
            if([self checkToDisplayReportButton:p])
            {
            [arrayReportList_Name addObject:p.report_name];
            [arrayReportList_URL addObject:p.report_url];
            }
        }

    }
    // Do any additional setup after loading the view from its nib.
  
    if([arrayReportList_Name count]>0)
    {
        lblReportName.text = [NSString stringWithFormat:@"%@",[arrayReportList_Name objectAtIndex:0]];

    }

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&TeacherID=%@&SchoolID=%@",[arrayReportList_URL objectAtIndex:0],profobj.normal_user_id,[AppHelper userDefaultsForKey:@"school_Id"]]];
    
    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    [webViewDisplayReports loadRequest:requestURL];
    
    
    [self getClassDetails];
    [self getStudenDetails];
}

-(void)updateReportsAfterSync
{
    [arrayReportList_Name removeAllObjects];
    [arrayReportList_URL removeAllObjects];
    NSArray *arrReport=[[Service sharedInstance]getReportDataInfoStored:@"" postNotification:nil];
    if (arrReport.count != 0) {
        for (Report *p in arrReport) {
            
            if([self checkToDisplayReportButton:p])
            {
                [arrayReportList_Name addObject:p.report_name];
                [arrayReportList_URL addObject:p.report_url];
            }
        }
        
    }
}
-(BOOL)checkToDisplayReportButton:(Report *)reportObj
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
        if (([reportObj.isActive integerValue] == 1) && ([reportObj.isReportSchoolActive integerValue] == 1))
        {
            if([profobj.type isEqualToString:@"Student"])
            {
                if([reportObj.isStudent integerValue]==1)
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }
            // Teacher Login
            else{
                
                if([reportObj.isTeacher integerValue]==1)
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }
        }
    
    return NO;
}


-(void)getStudenDetails
{
    NSArray *arr=[[Service sharedInstance] getStoredAllStudentDataWithTeacherId];
    for(Student *stu in arr){
        [arrayStudentList addObject:stu.firstname];
        [arrayStudentIDList addObject:stu.student_id];
    }
}

-(void)getClassDetails
{
    
    /***  GET TIME TABLE ID TO FETCH CLASS DETAILS ******/
    
    NSArray *arrForTimeTable=[[Service sharedInstance]getStoredAllTimeTableData];
    int index =1;
    for(TimeTable *time in arrForTimeTable){
        
        if([DELEGATE isDate:[NSDate date] inRangeFirstDate:time.startDate  lastDate:time.end_date])
        {
            self._currentTimeTable=time;
        }
        index++;
    }
    
    NSArray *arr=[[Service sharedInstance]getStoredAllClassDataInApp:self._currentTimeTable.timeTable_id];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"class_name"ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSMutableArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    for (MyClass *classObj in arr) {
        if(![arrayClassNameList containsObject:classObj.class_name]){
            [self.arrayClassNameList addObject:classObj.class_name];
            [self.arrayClassIDList addObject:classObj.class_id];
        }
    }
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
     return  _itemArr.count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, pickerView.frame.size.width, 50)];
    label.backgroundColor = [UIColor clearColor];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:0];
    label.textColor = [UIColor blackColor];
    label.font=[UIFont systemFontOfSize:14.0f];
    label.text = [NSString stringWithFormat:@"%@", [_itemArr objectAtIndex:row]];
    
    return label;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    slectedIndex = row;

    switch (selectedPickerView) {
        case 1:
        {
            lblReportName.text = [NSString stringWithFormat:@"%@",[_itemArr objectAtIndex:row]];
            [self getReportButtonAction:nil];
            
        }
            break;
        case 2:
           lblStudentName.text = [NSString stringWithFormat:@"%@",[_itemArr objectAtIndex:row]];
            break;
        case 3:
          lblClassName.text = [NSString stringWithFormat:@"%@",[_itemArr objectAtIndex:row]];
            break;
        default:
            lblReportName.text = [NSString stringWithFormat:@"%@",[_itemArr objectAtIndex:row]];
            break;
    }
    
}


- (IBAction)getReportButtonAction:(id)sender
{
    if(self._popoverControler1)
    {
        [self._popoverControler1 dismissPopoverAnimated:YES];
    }
    
     MyProfile *profobj =[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
   NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&TeacherID=%@&SchoolID=%@",[arrayReportList_URL objectAtIndex:slectedIndex],profobj.normal_user_id,[AppHelper userDefaultsForKey:@"school_Id"]]];

#if DEBUG
    NSLog(@"URL %@",url);
#endif

    NSURLRequest *requestURL = [NSURLRequest requestWithURL:url];
    [webViewDisplayReports loadRequest:requestURL];
}


- (IBAction)reportListButtonAction:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    selectedPickerView = btn.tag;
    _itemArr = nil;
    switch (selectedPickerView) {
        case 1:
        {
            [self updateReportsAfterSync];
            _itemArr = [arrayReportList_Name mutableCopy];
         
            self._popoverControler1=nil;
            UIViewController* popoverContent = [[UIViewController alloc]init];
            
            [_pickerView1 removeFromSuperview];
            popoverContent.view=nil;
            _pickerView1.frame=CGRectMake(_pickerView1.frame.origin.x, _pickerView1.frame.origin.y, 300, 176);
            popoverContent.view = _pickerView1;
            [_picker1 reloadAllComponents];
            _picker1.tag=btn.tag;
            _pickerView1.tag=btn.tag;
            UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler1=pop;
            [self._popoverControler1 setPopoverContentSize:_pickerView1.frame.size animated:YES];
            self._popoverControler1.delegate = self;
            [self._popoverControler1 presentPopoverFromRect:lblReportName.frame  inView:btn
                                   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
            [pop release];
            [popoverContent release];
            

        }break;
        case 2:
        {
            
        _itemArr = [arrayStudentList mutableCopy];
        
            self._popoverControler2=nil;
            UIViewController* popoverContent = [[UIViewController alloc]init];
            
            [_pickerView2 removeFromSuperview];
            popoverContent.view=nil;
            _pickerView1.frame=CGRectMake(_pickerView2.frame.origin.x, _pickerView2.frame.origin.y, 300, 176);
            popoverContent.view = _pickerView2;
            
            _picker2.tag=btn.tag;
            _pickerView2.tag=btn.tag;
            UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler2=pop;
            [self._popoverControler2 setPopoverContentSize:_pickerView2.frame.size animated:YES];
            self._popoverControler2.delegate = self;
            [self._popoverControler2 presentPopoverFromRect:lblReportName.frame  inView:btn
                                   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            [pop release];
            [popoverContent release];
            

        
        }    break;
        case 3:
        { _itemArr = [arrayClassNameList mutableCopy];
        
            self._popoverControler3=nil;
            UIViewController* popoverContent = [[UIViewController alloc]init];
            
            [_pickerView3 removeFromSuperview];
            popoverContent.view=nil;
            _pickerView3.frame=CGRectMake(_pickerView3.frame.origin.x, _pickerView3.frame.origin.y, 150, 176);
            popoverContent.view = _pickerView3;
            
            _picker3.tag=btn.tag;
            _pickerView3.tag=btn.tag;
            UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
            self._popoverControler3=pop;
            [self._popoverControler3 setPopoverContentSize:_pickerView3.frame.size animated:YES];
            self._popoverControler3.delegate = self;
            [self._popoverControler3 presentPopoverFromRect:lblReportName.frame  inView:btn
                                   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            [pop release];
            [popoverContent release];
            

        }  break;
        default:
              _itemArr = [arrayReportList_Name mutableCopy];
            break;
    }

    
    
   }

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator setHidden:YES];
    [activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [activityIndicator release];
    [super dealloc];
}
@end
