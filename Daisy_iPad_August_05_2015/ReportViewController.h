//
//  ReportViewController.h
//  StudyPlaner
//
//  Created by Amol Gaikwad on 19/06/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Report.h"
#import "MyProfile.h"
#import "TimeTable.h"

@interface ReportViewController : UIViewController  <UIPopoverControllerDelegate>
{
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIWebView *webViewDisplayReports;
    IBOutlet UILabel *lblReportName;
    IBOutlet UILabel *lblStudentName;
    IBOutlet UILabel *lblClassName;

    IBOutlet UIImageView *imgStudentButton;
    IBOutlet UIButton *btnStudentList;
     IBOutlet UIView *_pickerView1;
     IBOutlet UIView *_pickerView2;
     IBOutlet UIView *_pickerView3;
    
     IBOutlet UIPickerView *_picker1;
     IBOutlet UIPickerView *_picker2;
     IBOutlet UIPickerView *_picker3;
   
    NSInteger slectedIndex;
    NSInteger selectedPickerView;
    
}
@property(nonatomic,retain) TimeTable *_currentTimeTable;
@property(nonatomic,retain) UIPopoverController *_popoverControler1;
@property(nonatomic,retain) UIPopoverController *_popoverControler2;
@property(nonatomic,retain) UIPopoverController *_popoverControler3;

@property (nonatomic,strong)NSMutableArray *arrayReportList_Name;
@property (nonatomic,strong)NSMutableArray *arrayReportList_URL;
@property (nonatomic,strong)NSMutableArray *arrayStudentList;
@property (nonatomic,strong)NSMutableArray *arrayStudentIDList;
@property (nonatomic,strong)NSMutableArray *arrayClassNameList;
@property (nonatomic,strong)NSMutableArray *arrayClassIDList;
@property (nonatomic,strong)NSString *strSelectedClassID;
@property (nonatomic,strong)NSString *strSelectedStudentID;

@property(nonatomic,retain) NSMutableArray *_itemArr;
- (IBAction)reportListButtonAction:(id)sender;
- (IBAction)getReportButtonAction:(id)sender;

@end
